package model;

import java.sql.Date;

public class Anam {

	private String encaminhamento;

	private String diagnostico;

	private String medico;

	private String sexo;

	private String religiao;

	private String historia;

	private String historicoClin;

	private String queixa;

	private String motivosImpExp;

	private String formulacaoHipotese;

	private String planoTerap;

	private String dadosPsicoSociais;

	private String estrutFamiliar;

	private String infEAdos;

	private String aspecPsico;

	private String enfrentDoenca;
        
        private int idpsicologa;
        
        private int idPaciente;
        
        private String nsuspaciente;
        
        private Date data;
        
        private int id;
        

    public String getEncaminhamento() {
        return encaminhamento;
    }

    public void setEncaminhamento(String encaminhamento) {
        this.encaminhamento = encaminhamento;
    }

    public String getDiagnostico() {
        return diagnostico;
    }

    public void setDiagnostico(String diagnostico) {
        this.diagnostico = diagnostico;
    }

    public String getMedico() {
        return medico;
    }

    public void setMedico(String medico) {
        this.medico = medico;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getReligiao() {
        return religiao;
    }

    public void setReligiao(String religiao) {
        this.religiao = religiao;
    }

    public String getHistoria() {
        return historia;
    }

    public void setHistoria(String historia) {
        this.historia = historia;
    }

    public String getHistoricoClin() {
        return historicoClin;
    }

    public void setHistoricoClin(String historicoClin) {
        this.historicoClin = historicoClin;
    }

    public String getQueixa() {
        return queixa;
    }

    public void setQueixa(String queixa) {
        this.queixa = queixa;
    }

    public String getMotivosImpExp() {
        return motivosImpExp;
    }

    public void setMotivosImpExp(String motivosImpExp) {
        this.motivosImpExp = motivosImpExp;
    }

    public String getFormulacaoHipotese() {
        return formulacaoHipotese;
    }

    public void setFormulacaoHipotese(String formulacaoHipotese) {
        this.formulacaoHipotese = formulacaoHipotese;
    }

    public String getPlanoTerap() {
        return planoTerap;
    }

    public void setPlanoTerap(String planoTerap) {
        this.planoTerap = planoTerap;
    }

    public String getDadosPsicoSociais() {
        return dadosPsicoSociais;
    }

    public void setDadosPsicoSociais(String dadosPsicoSociais) {
        this.dadosPsicoSociais = dadosPsicoSociais;
    }

    public String getEstrutFamiliar() {
        return estrutFamiliar;
    }

    public void setEstrutFamiliar(String estrutFamiliar) {
        this.estrutFamiliar = estrutFamiliar;
    }

    public String getInfEAdos() {
        return infEAdos;
    }

    public void setInfEAdos(String infEAdos) {
        this.infEAdos = infEAdos;
    }

    public String getAspecPsico() {
        return aspecPsico;
    }

    public void setAspecPsico(String AspecPsico) {
        this.aspecPsico = AspecPsico;
    }

    public String getEnfrentDoenca() {
        return enfrentDoenca;
    }

    public void setEnfrentDoenca(String enfrentDoenca) {
        this.enfrentDoenca = enfrentDoenca;
    }

  

    /**
     * @return the idpsicologa
     */
    public int getIdpsicologa() {
        return idpsicologa;
    }

    /**
     * @param idpsicologa the idpsicologa to set
     */
    public void setIdpsicologa(int idpsicologa) {
        this.idpsicologa = idpsicologa;
    }

    /**
     * @return the idPaciente
     */
    public int getIdPaciente() {
        return idPaciente;
    }

    /**
     * @param idPaciente the idPaciente to set
     */
    public void setIdPaciente(int idPaciente) {
        this.idPaciente = idPaciente;
    }

    /**
     * @return the nsuspaciente
     */
    public String getNsuspaciente() {
        return nsuspaciente;
    }

    /**
     * @param nsuspaciente the nsuspaciente to set
     */
    public void setNsuspaciente(String nsuspaciente) {
        this.nsuspaciente = nsuspaciente;
    }

    /**
     * @return the data
     */
    public Date getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(Date data) {
        this.data = data;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

}
