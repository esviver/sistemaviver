package model;

public class Psicologa extends Usuario {

    private String crp;
    private int idP;
    private int idUsuario;

    public String getCrp() {
        return crp;
    }

    public void setCrp(String crp) {
        this.crp = crp;
    }

    public int getIdP() {
        return idP;
    }

    public void setIdP(int idP) {
        this.idP = idP;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idusuario) {
        this.idUsuario = idusuario;
    }

}
