package model;

import java.sql.Date;

public class Paciente {
            
        private int id;

	private String pnome;
        
        private String snome;

	private String patologia;

	private Date dataNasc;

	private String nsus;

	private String localNasc;

	private String nomeEscola;

	private int serie;

	private String moradia;

	private int comodos;

	private String material;

	private boolean esgoto;

	private boolean asfalto;

	private String banheiro;

	private float agua;

	private float energia;

	private float gas;

	private float aluguel;

	private float transporte;

	private float medicamentos;

	private float alimentacao;

	private boolean creche;

	private boolean centroCom;

	private boolean bolsaFam;

	private boolean auxilioGas;

	private boolean luzFraterna;

	private String religiao;

	private boolean religiaoCont;

	private String outroAux;

	private String apoioFam;

	private float rendaMensal;

	private String historicoFamiliar;

	private String histPatologia;
        
        private int id_end;
        
        private boolean obito;
        
        private boolean situacao;
        
        private boolean alta;
        
        private Date entrada;
        
        private Date saida;
        
        private int id_end_escola;

    public int getId_end_escola() {
        return id_end_escola;
    }

    public void setId_end_escola(int id_end_escola) {
        this.id_end_escola = id_end_escola;
    }

    public String getNsus() {
        return nsus;
    }

    public void setNsus(String nsus) {
        this.nsus = nsus;
    }

    public boolean isObito() {
        return obito;
    }

    public void setObito(boolean obito) {
        this.obito = obito;
    }

    public boolean isSituacao() {
        return situacao;
    }

    public void setSituacao(boolean situacao) {
        this.situacao = situacao;
    }

    public boolean isAlta() {
        return alta;
    }

    public void setAlta(boolean alta) {
        this.alta = alta;
    }

    public Date getEntrada() {
        return entrada;
    }

    public void setEntrada(Date entrada) {
        this.entrada = entrada;
    }

    public Date getSaida() {
        return saida;
    }

    public void setSaida(Date saida) {
        this.saida = saida;
    }
    
    public String getPnome() {
        return pnome;
    }

    public void setPnome(String pnome) {
        this.pnome = pnome;
    }

    public String getSnome() {
        return snome;
    }

    public void setSnome(String snome) {
        this.snome = snome;
    }

    public int getId_end() {
        return id_end;
    }

    public void setId_end(int id_end) {
        this.id_end = id_end;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPNome() {
        return pnome;
    }

    public void setPNome(String pnome) {
        this.pnome = pnome;
    }

    public String getPatologia() {
        return patologia;
    }

    public void setPatologia(String patologia) {
        this.patologia = patologia;
    }

    public Date getDataNasc() {
        return dataNasc;
    }

    public void setDataNasc(Date dataNasc) {
        this.dataNasc = dataNasc;
    }

    public String getLocalNasc() {
        return localNasc;
    }

    public void setLocalNasc(String localNasc) {
        this.localNasc = localNasc;
    }

    public String getNomeEscola() {
        return nomeEscola;
    }

    public void setNomeEscola(String nomeEscola) {
        this.nomeEscola = nomeEscola;
    }

    public int getSerie() {
        return serie;
    }

    public void setSerie(int serie) {
        this.serie = serie;
    }

    public String getMoradia() {
        return moradia;
    }

    public void setMoradia(String moradia) {
        this.moradia = moradia;
    }

    public int getComodos() {
        return comodos;
    }

    public void setComodos(int comodos) {
        this.comodos = comodos;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public boolean isEsgoto() {
        return esgoto;
    }

    public void setEsgoto(boolean esgoto) {
        this.esgoto = esgoto;
    }

    public boolean isAsfalto() {
        return asfalto;
    }

    public void setAsfalto(boolean asfalto) {
        this.asfalto = asfalto;
    }

    public String getBanheiro() {
        return banheiro;
    }

    public void setBanheiro(String banheiro) {
        this.banheiro = banheiro;
    }

    public float getAgua() {
        return agua;
    }

    public void setAgua(float agua) {
        this.agua = agua;
    }

    public float getEnergia() {
        return energia;
    }

    public void setEnergia(float energia) {
        this.energia = energia;
    }

    public float getGas() {
        return gas;
    }

    public void setGas(float gas) {
        this.gas = gas;
    }

    public float getAluguel() {
        return aluguel;
    }

    public void setAluguel(float aluguel) {
        this.aluguel = aluguel;
    }

    public float getTransporte() {
        return transporte;
    }

    public void setTransporte(float transporte) {
        this.transporte = transporte;
    }

    public float getMedicamentos() {
        return medicamentos;
    }

    public void setMedicamentos(float medicamentos) {
        this.medicamentos = medicamentos;
    }

    public float getAlimentacao() {
        return alimentacao;
    }

    public void setAlimentacao(float alimentacao) {
        this.alimentacao = alimentacao;
    }

    public boolean isCreche() {
        return creche;
    }

    public void setCreche(boolean creche) {
        this.creche = creche;
    }

    public boolean isCentroCom() {
        return centroCom;
    }

    public void setCentroCom(boolean centroCom) {
        this.centroCom = centroCom;
    }

    public boolean isBolsaFam() {
        return bolsaFam;
    }

    public void setBolsaFam(boolean bolsaFam) {
        this.bolsaFam = bolsaFam;
    }

    public boolean isAuxilioGas() {
        return auxilioGas;
    }

    public void setAuxilioGas(boolean auxilioGas) {
        this.auxilioGas = auxilioGas;
    }

    public boolean isLuzFraterna() {
        return luzFraterna;
    }

    public void setLuzFraterna(boolean luzFraterna) {
        this.luzFraterna = luzFraterna;
    }

    public String getReligiao() {
        return religiao;
    }

    public void setReligiao(String religiao) {
        this.religiao = religiao;
    }

    public boolean isReligiaoCont() {
        return religiaoCont;
    }

    public void setReligiaoCont(boolean religiaoCont) {
        this.religiaoCont = religiaoCont;
    }

    public String getOutroAux() {
        return outroAux;
    }

    public void setOutroAux(String outroAux) {
        this.outroAux = outroAux;
    }

    public String getApoioFam() {
        return apoioFam;
    }

    public void setApoioFam(String apoioFam) {
        this.apoioFam = apoioFam;
    }

    public float getRendaMensal() {
        return rendaMensal;
    }

    public void setRendaMensal(float rendaMensal) {
        this.rendaMensal = rendaMensal;
    }

    public String getHistoricoFamiliar() {
        return historicoFamiliar;
    }

    public void setHistoricoFamiliar(String historicoFamiliar) {
        this.historicoFamiliar = historicoFamiliar;
    }

    public String getHistPatologia() {
        return histPatologia;
    }

    public void setHistPatologia(String histPatologia) {
        this.histPatologia = histPatologia;
    }

}
