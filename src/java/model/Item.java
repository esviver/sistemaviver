/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Auxiliar
 */
public class Item {
    private String nome;
    private int qtde;
    private int id;

    public String getNome() {
        return nome;
    }

    public int getQtde() {
        return qtde;
    }

    public int getId() {
        return id;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setQtde(int qtde) {
        this.qtde = qtde;
    }

    public void setId(int id) {
        this.id = id;
    }
    
}
