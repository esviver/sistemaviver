package model;

import java.util.Date;

public class Doacao {
	private String doador;

	private String telDoador;

	private String celDoador;

	private boolean doadorAnon;

	private Date data;
        
        private int id;

	private float dinheiro;

	private boolean isDinheiro;

	private boolean isOutro;

	private String outro;

	private String descricao;
        
        private boolean status;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getDoador() {
        return doador;
    }

    public void setDoador(String doador) {
        this.doador = doador;
    }

    public String getTelDoador() {
        return telDoador;
    }

    public void setTelDoador(String telDoador) {
        this.telDoador = telDoador;
    }

    public String getCelDoador() {
        return celDoador;
    }

    public void setCelDoador(String celDoador) {
        this.celDoador = celDoador;
    }

    public boolean isDoadorAnon() {
        return doadorAnon;
    }

    public void setDoadorAnon(boolean doadorAnon) {
        this.doadorAnon = doadorAnon;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public float getDinheiro() {
        return dinheiro;
    }

    public void setDinheiro(float dinheiro) {
        this.dinheiro = dinheiro;
    }

    public boolean isIsDinheiro() {
        return isDinheiro;
    }

    public void setIsDinheiro(boolean isDinheiro) {
        this.isDinheiro = isDinheiro;
    }

    public boolean isIsOutro() {
        return isOutro;
    }

    public void setIsOutro(boolean isOutro) {
        this.isOutro = isOutro;
    }

    public String getOutro() {
        return outro;
    }

    public void setOutro(String outro) {
        this.outro = outro;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }


}
