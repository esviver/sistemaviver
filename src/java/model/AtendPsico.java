package model;

import java.sql.Date;
import java.sql.Time;

public class AtendPsico {

	private int id;
        
	private String patologia;

	private Date dataAtendimento;

	private String atendimento;
        
        private String queixa;
        
	private int idPaciente;
        
        private int idPsicologa;

	private Time hora;

    public String getPatologia() {
        return patologia;
    }

    public void setPatologia(String patologia) {
        this.patologia = patologia;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDataAtendimento() {
        return dataAtendimento;
    }

    public void setDataAtendimento(Date dataAtendimento) {
        this.dataAtendimento = dataAtendimento;
    }



    public int getIdPaciente() {
        return idPaciente;
    }

    public void setIdPaciente(int idPaciente) {
        this.idPaciente = idPaciente;
    }

    public int getIdPsicologa() {
        return idPsicologa;
    }

    public void setIdPsicologa(int idPsicologa) {
        this.idPsicologa = idPsicologa;
    }

    public Time getHora() {
        return hora;
    }

    public void setHora(Time hora) {
        this.hora = hora;
    }

    /**
     * @return the atendimento
     */
    public String getAtendimento() {
        return atendimento;
    }

    /**
     * @param atendimento the atendimento to set
     */
    public void setAtendimento(String atendimento) {
        this.atendimento = atendimento;
    }

    /**
     * @return the queixa
     */
    public String getQueixa() {
        return queixa;
    }

    /**
     * @param queixa the queixa to set
     */
    public void setQueixa(String queixa) {
        this.queixa = queixa;
    }
        
        
    

}
