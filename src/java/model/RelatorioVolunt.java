package model;

import java.sql.Time;
import java.sql.Date;

public class RelatorioVolunt {

        private int id;
    
        private boolean status;
        
	private int idVoluntario;

	private String setor; 

	private Date dia;
        
        private Time hora;

	private String observacao;

	private String ativDesenvolvida;
        
        private boolean aprovado;
        
        private String coordenador;
    
        private String atrbExtraNome;

        private String atrbExtraSobrenome;

    public String getAtrbExtraNome() {
        return atrbExtraNome;
    }

    public void setAtrbExtraNome(String atrbExtraNome) {
        this.atrbExtraNome = atrbExtraNome;
    }

    public String getAtrbExtraSobrenome() {
        return atrbExtraSobrenome;
    }

    public void setAtrbExtraSobrenome(String atrbExtraSobrenome) {
        this.atrbExtraSobrenome = atrbExtraSobrenome;
    }

    public String getCoordenador() {
        return coordenador;
    }

    public void setCoordenador(String coordenador) {
        this.coordenador = coordenador;
    }

    public boolean isAprovado() {
        return aprovado;
    }

    public void setAprovado(boolean aprovado) {
        this.aprovado = aprovado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
        
    public int getIdVoluntario() {
        return idVoluntario;
    }

    public void setIdVoluntario(int idVoluntario) {
        this.idVoluntario = idVoluntario;
    }

    public String getSetor() {
        return setor;
    }

    public void setSetor(String setor) {
        this.setor = setor;
    }

    public Date getDia() {
        return dia;
    }

    public void setDia(Date dia) {
        this.dia = dia;
    }

    public Time getHora() {
        return hora;
    }

    public void setHora(Time hora) {
        this.hora = hora;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public String getAtivDesenvolvida() {
        return ativDesenvolvida;
    }

    public void setAtivDesenvolvida(String ativDesenvolvida) {
        this.ativDesenvolvida = ativDesenvolvida;
    }

}
