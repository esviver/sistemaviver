package model;

import java.sql.Date;
import java.util.List;

public class Distribuicao {

	private int paciente;

	private Date data;

	private List<Item> item;
        
        private boolean cesta;
        
        private boolean leite;

	private String local;
        
        private int id;
        
        private String nome;
        
        private boolean status;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
        
        

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }
        

    public void setCesta(boolean cesta) {
        this.cesta = cesta;
    }

    public void setLeite(boolean leite) {
        this.leite = leite;
    }

    public boolean isCesta() {
        return cesta;
    }

    public boolean isLeite() {
        return leite;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public int getPaciente() {
        return paciente;
    }

    public void setPaciente(int paciente) {
        this.paciente = paciente;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public List<Item> getItem() {
        return item;
    }

    public void setItem(List<Item> item) {
        this.item = item;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local.toUpperCase();
    }

}
