/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author matheus
 */
public class Atendimento {
    private Paciente paciente;
    private AtendPsico atendPsico;
    private Anam anam;
    private AtendAss atendAss;

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    public AtendPsico getAtendPsico() {
        return atendPsico;
    }

    public void setAtendPsico(AtendPsico atendPsico) {
        this.atendPsico = atendPsico;
    }

    public Anam getAnam() {
        return anam;
    }

    public void setAnam(Anam anam) {
        this.anam = anam;
    }

    public AtendAss getAtendAss() {
        return atendAss;
    }

    public void setAtendAss(AtendAss atendAss) {
        this.atendAss = atendAss;
    }
    
}
