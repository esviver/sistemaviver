package model;

import java.sql.Date;

public class Responsavel {
    
    private int id;
    private boolean status;
    private Date datanasc;
    private String cpf;
    private String escolaridade;
    private String estadoCivil;
    private String pnomeResp;
    private String snomeResp;
    private String profissao;
    private String rgNum;
    private String rgOrg;
    private String telefone;
    private int idEndereco;
    private int id_pac;

    public String getPnomeResp() {
        return pnomeResp;
    }

    public void setPnomeResp(String pnomeResp) {
        this.pnomeResp = pnomeResp;
    }

    public String getSnomeResp() {
        return snomeResp;
    }

    public void setSnomeResp(String snomeResp) {
        this.snomeResp = snomeResp;
    }

    public int getId_pac() {
        return id_pac;
    }

    public void setId_pac(int id_pac) {
        this.id_pac = id_pac;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDatanasc() {
        return datanasc;
    }

    public void setDatanasc(Date datanasc) {
        this.datanasc = datanasc;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEscolaridade() {
        return escolaridade;
    }

    public void setEscolaridade(String escolaridade) {
        this.escolaridade = escolaridade;
    }

    public String getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }
  
    public String getProfissao() {
        return profissao;
    }

    public void setProfissao(String profissao) {
        this.profissao = profissao;
    }

    public String getRgNum() {
        return rgNum;
    }

    public void setRgNum(String rgNum) {
        this.rgNum = rgNum;
    }

    public String getRgOrg() {
        return rgOrg;
    }

    public void setRgOrg(String rgOrg) {
        this.rgOrg = rgOrg;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public int getIdEndereco() {
        return idEndereco;
    }

    public void setIdEndereco(int idEndereco) {
        this.idEndereco = idEndereco;
    }
    
}
