/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Auxiliar
 */
public class Acompanhante {
    private int idresponsavel;
    private int idhospedagem;

    public int getIdresponsavel() {
        return idresponsavel;
    }

    public int getIdhospedagem() {
        return idhospedagem;
    }

    public void setIdresponsavel(int idresponsavel) {
        this.idresponsavel = idresponsavel;
    }

    public void setIdhospedagem(int idhospedagem) {
        this.idhospedagem = idhospedagem;
    }
    
}
