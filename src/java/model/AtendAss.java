package model;

import java.sql.Date;
import java.sql.Time;

public class AtendAss {
        
        private int id;
        
	private String patologia;

	private Date dataAtendimento;

	private String Relatorio;

	private int idPaciente;
        
        private int idAss;

	private Time hora;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDataAtendimento() {
        return dataAtendimento;
    }

    public void setDataAtendimento(Date dataAtendimento) {
        this.dataAtendimento = dataAtendimento;
    }

    public String getRelatorio() {
        return Relatorio;
    }

    public void setRelatorio(String Relatorio) {
        this.Relatorio = Relatorio;
    }

    public int getIdPaciente() {
        return idPaciente;
    }

    public void setIdPaciente(int idPaciente) {
        this.idPaciente = idPaciente;
    }

    public int getIdAss() {
        return idAss;
    }

    public void setIdAss(int idAss) {
        this.idAss = idAss;
    }

    public Time getHora() {
        return hora;
    }

    public void setHora(Time hora) {
        this.hora = hora;
    }

    /**
     * @return the patologia
     */
    public String getPatologia() {
        return patologia;
    }

    /**
     * @param patologia the patologia to set
     */
    public void setPatologia(String patologia) {
        this.patologia = patologia;
    }

  
        
        
    
 
    

}
