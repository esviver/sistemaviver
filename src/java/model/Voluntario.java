package model;

import java.util.ArrayList;
import java.sql.Date;
import java.sql.Time;

public class Voluntario extends Usuario {

    private int idVoluntario;
        
    private int idUsuario;

    private String profissao;

    private String escolaridade;

    private String localTrab;

    private String estadoCivil;

    private String email;

    private boolean usaMed;
    
    private String remedioQueUsa;

    private String CDENome;

    private String CDETelefone;

    private String perg1;

    private String perg2;

    private String perg3;

    private String perg4;

    private ArrayList<String> aptdoes;

    private String possuiDisp;

    private ArrayList<String> diasEHorariosDisp;

    private String obsSobreHor;

    private String setor;

    private ArrayList<String> diasSetor;

    private Date dataInicio;
    
    private Time hora;
    
    private boolean aceitacaoDosTermos;

    public Time getHora() {
        return hora;
    }

    public void setHora(Time hora) {
        this.hora = hora;
    }

    public boolean isAceitacaoDosTermos() {
        return aceitacaoDosTermos;
    }

    public void setAceitacaoDosTermos(boolean aceitacaoDosTermos) {
        this.aceitacaoDosTermos = aceitacaoDosTermos;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getRemedioQueUsa() {
        return remedioQueUsa;
    }

    public void setRemedioQueUsa(String remedioQueUsa) {
        this.remedioQueUsa = remedioQueUsa;
    }

    public int getIdVoluntario() {
        return idVoluntario;
    }

    public void setIdVoluntario(int idVoluntario) {
        this.idVoluntario = idVoluntario;
    }
    
    public String getProfissao() {
        return profissao;
    }

    public void setProfissao(String profissao) {
        this.profissao = profissao;
    }

    public String getEscolaridade() {
        return escolaridade;
    }

    public void setEscolaridade(String escolaridade) {
        this.escolaridade = escolaridade;
    }

    public String getLocalTrab() {
        return localTrab;
    }

    public void setLocalTrab(String localTrab) {
        this.localTrab = localTrab;
    }

    public String getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isUsaMed() {
        return usaMed;
    }

    public void setUsaMed(boolean usaMed) {
        this.usaMed = usaMed;
    }

    public String getCDENome() {
        return CDENome;
    }

    public void setCDENome(String CDENome) {
        this.CDENome = CDENome;
    }

    public String getCDETelefone() {
        return CDETelefone;
    }

    public void setCDETelefone(String CDETelefone) {
        this.CDETelefone = CDETelefone;
    }

    public String getPerg1() {
        return perg1;
    }

    public void setPerg1(String perg1) {
        this.perg1 = perg1;
    }

    public String getPerg2() {
        return perg2;
    }

    public void setPerg2(String perg2) {
        this.perg2 = perg2;
    }

    public String getPerg3() {
        return perg3;
    }

    public void setPerg3(String perg3) {
        this.perg3 = perg3;
    }

    public String getPerg4() {
        return perg4;
    }

    public void setPerg4(String perg4) {
        this.perg4 = perg4;
    }

    public ArrayList<String> getAptdoes() {
        return aptdoes;
    }

    public void setAptdoes(ArrayList<String> aptdoes) {
        this.aptdoes = aptdoes;
    }

    public String getPossuiDisp() {
        return possuiDisp;
    }

    public void setPossuiDisp(String possuiDisp) {
        this.possuiDisp = possuiDisp;
    }

    public ArrayList<String> getDiasEHorariosDisp() {
        return diasEHorariosDisp;
    }

    public void setDiasEHorariosDisp(ArrayList<String> diasEHorariosDisp) {
        this.diasEHorariosDisp = diasEHorariosDisp;
    }

    public String getObsSobreHor() {
        return obsSobreHor;
    }

    public void setObsSobreHor(String obsSobreHor) {
        this.obsSobreHor = obsSobreHor;
    }

    public String getSetor() {
        return setor;
    }

    public void setSetor(String setor) {
        this.setor = setor;
    }

    public ArrayList<String> getDiasSetor() {
        return diasSetor;
    }

    public void setDiasSetor(ArrayList<String> diasSetor) {
        this.diasSetor = diasSetor;
    }

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

}
