package model;

public class Assistente extends Usuario {

    private String cress;
    private int idAss;
    private int idUsuario;

    public String getCress() {
        return cress;
    }

    public void setCress(String cress) {
        this.cress = cress;
    }

    public int getIdAss() {
        return idAss;
    }

    public void setIdAss(int idAss) {
        this.idAss = idAss;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }
    
    
}
