package global;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Constantes {
    
    public static java.sql.Date getInitialDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date datee = null;
        try {
            datee = sdf.parse("2016-20-02");
        } catch (ParseException ex) {
            Logger.getLogger(Constantes.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        java.sql.Date date = new java.sql.Date(datee.getTime());
        
        return date;
    }
    
    public static java.sql.Date getSQLDate(String data) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date datee = null;
        try {
            datee = sdf.parse(data);
        } catch (ParseException ex) {
            Logger.getLogger(Constantes.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        java.sql.Date date = new java.sql.Date(datee.getTime());
        
        return date;
    }
    
    public static java.sql.Time getSQLTime(String time) {
        java.sql.Time t = java.sql.Time.valueOf(time);
        return t;
    }
    
    public static java.sql.Date getTodaysDate() {
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        
        return date;
    }
}
