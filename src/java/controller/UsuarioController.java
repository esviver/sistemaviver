package controller;

import dao.AssistenteDAO;
import dao.DAOFactory;
import dao.EnderecoDAO;
import dao.PsicologaDAO;
import dao.UsuarioDAO2;
import dao.VoluntarioDAO;
import global.Constantes;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Assistente;
import model.Endereco;
import model.Psicologa;
import model.Usuario;
import model.Voluntario;

@WebServlet(name = "UsuarioController", 
        urlPatterns = {
            "/view/menu/account",
            "/view/menu/account/create",
            "/view/menu/account/read",
            "/view/menu/account/update",
            "/view/menu/account/delete",
            "/view/menu/account/readBySearch",
            "/view/menu/account/readAll",
            "/view/menu/account/recover"
        })
public class UsuarioController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        DAOFactory daoFactory;
        UsuarioDAO2 daoUsuario;
        PsicologaDAO daoPsicologa;
        AssistenteDAO daoAssistente;
        VoluntarioDAO daoVoluntario;
        RequestDispatcher requestDispatcher;
        EnderecoDAO daoEndereco;
        
        switch(request.getServletPath()){
            case "/view/menu/account": {
                requestDispatcher = request.getRequestDispatcher("/view/menu/account/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            } case "/view/menu/account/create": {
                requestDispatcher = request.getRequestDispatcher("/view/menu/account/create/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/view/menu/account/readAll": {
                List<Usuario> lista = null;

                try {
                    daoFactory = new DAOFactory();
                    daoUsuario = daoFactory.getUsuarioDAO();
                    
                    if(request.getParameter("trash").equals("true")){
                        lista = daoUsuario.all();
                    }else if(request.getParameter("trash").equals("false")){
                        lista = daoUsuario.allWithoutDeleted();
                    }
                    
                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                request.setAttribute("listaPResultados", lista);
                requestDispatcher = request.getRequestDispatcher("/view/menu/account/read/resultado.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/view/menu/account/readBySearch": {
                List<Usuario> lista = null;
                
                try {
                    daoFactory = new DAOFactory();
                    daoUsuario = daoFactory.getUsuarioDAO();
                    
                    String searchParam = request.getParameter("input");

                    if(request.getParameter("trash").equals("true")){
                        lista = daoUsuario.readByName(searchParam);
                    }else if(request.getParameter("trash").equals("false")){
                        lista = daoUsuario.readByNameWithoutDeleted(searchParam);
                    }
                    
                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                request.setAttribute("listaPResultados", lista);
                requestDispatcher = request.getRequestDispatcher("/view/menu/account/read/resultado.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            
            case "/view/menu/account/update": {
                Usuario user = null;
                int id = Integer.parseInt(request.getParameter("id"));
                
                try {
                    daoFactory = new DAOFactory();
                    daoUsuario = daoFactory.getUsuarioDAO();

                    user = daoUsuario.read(id);

                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                request.setAttribute("usuarioParaUpdate", user);
                //request.setAttribute("idEnd", user.getId_end());
                HttpSession session = request.getSession();
                session.setAttribute("idEnd", user.getId_end());
                session.setAttribute("idUser", user.getId());
                
                String tel = user.getTelefone();
                request.setAttribute("telDDD", tel.substring(0, 2));
                request.setAttribute("tel", tel.substring(2));
                String cel = user.getCelular();
                request.setAttribute("celDDD", cel.substring(0, 2));
                request.setAttribute("cel", cel.substring(2));
                String data[] = user.getDataNasc().toString().split("-");
                if(data.length >= 1){
                    request.setAttribute("ano", data[0]);
                    request.setAttribute("mes", data[1]);
                    request.setAttribute("dia", data[2]);
                }
                
                //--------------------------------------------------------------------------------
                Endereco endereco = null;
                try {
                    daoFactory = new DAOFactory();
                    daoEndereco = daoFactory.getEnderecoDAO();
                    
                    endereco = daoEndereco.read(user.getId_end());
                    
                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
                request.setAttribute("userEnderecoParaUpdate", endereco);
                //--------------------------------------------------------------------------------
                Psicologa psico = null;
                int idusuarioPsico = user.getId();
                
                try {
                    daoFactory = new DAOFactory();
                    daoPsicologa = daoFactory.getPsicologaDAO();

                    psico = daoPsicologa.read(idusuarioPsico);

                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                boolean psicoExiste;
                if(psico!=null){
                    psicoExiste = true;
                }else {
                    psicoExiste = false;
                }
                if(psicoExiste){
                    request.setAttribute("isPsico", true);
                    request.setAttribute("crpPsico", psico.getCrp());
                }
                //--------------------------------------------------------------------------------
                Assistente ass = null;
                int idusuarioAss = user.getId();
                
                try {
                    daoFactory = new DAOFactory();
                    daoAssistente = daoFactory.getAssistenteDAO();

                    ass = daoAssistente.read(idusuarioAss);

                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                boolean assExiste;
                if(ass!=null){
                    assExiste = true;
                }else {
                    assExiste = false;
                }
                if(assExiste){
                    request.setAttribute("isAss", true);
                    request.setAttribute("cressAss", ass.getCress());
                }
                //--------------------------------------------------------------------------------
                Voluntario volunt = null;
                int idusuarioVolunt = user.getId();
                
                try {
                    daoFactory = new DAOFactory();
                    daoVoluntario = daoFactory.getVoluntarioDAO();

                    volunt = daoVoluntario.read(idusuarioVolunt);

                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                boolean voluntExiste;
                if(volunt!=null){
                    voluntExiste = true;
                }else {
                    voluntExiste = false;
                }
                if(voluntExiste){
                    /*private final String updateQuery = "UPDATE ong2.voluntario SET profissao = ?, "
            + "escolaridade = ?, cdenome = ?, cdetelefone = ?, local_trab = ?, estado_civil = ?, "
            + "email = ?, usa_med = ?, remedio_que_usa = ?, possui_disp = ?, obs_sobre_hor = ?, "
            + "setor = ?, hora = ?, aceitacaoDosTermos = ? WHERE id = ?;";*/
                    request.setAttribute("isVolunt", true);
                    request.setAttribute("profissaoVolunt", volunt.getProfissao());
                    request.setAttribute("escolaridadeVolunt", volunt.getEscolaridade());
                    request.setAttribute("cdenomeVolunt", volunt.getCDENome());
                    request.setAttribute("cdetelefoneVolunt", volunt.getCDETelefone());
                    request.setAttribute("localtrabVolunt", volunt.getLocalTrab());
                    request.setAttribute("estadocivilVolunt", volunt.getEstadoCivil());
                    request.setAttribute("emailVolunt", volunt.getEmail());
                    request.setAttribute("usamedVolunt", volunt.isUsaMed());
                    request.setAttribute("remedioqueusaVolunt", volunt.getRemedioQueUsa());
                    request.setAttribute("possuidispVolunt", volunt.getPossuiDisp());
                    request.setAttribute("obssobrehorVolunt", volunt.getObsSobreHor());
                    request.setAttribute("setorVolunt", volunt.getSetor());
                    request.setAttribute("horaVolunt", volunt.getHora());
                    request.setAttribute("profissaoVolunt", volunt.getProfissao());
                }
                
                requestDispatcher = request.getRequestDispatcher("/view/menu/account/update/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/view/menu/account/recover": {
                int id = Integer.parseInt(request.getParameter("id"));
                
                try {
                    daoFactory = new DAOFactory();
                    daoUsuario = daoFactory.getUsuarioDAO();
                    
                    daoUsuario.recover(id);
                    
                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                requestDispatcher = request.getRequestDispatcher("/view/menu/account/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        DAOFactory daoFactory;
        UsuarioDAO2 daoUsuario;
        PsicologaDAO daoPsicologa;
        AssistenteDAO daoAssistente;
        VoluntarioDAO daoVoluntario;
        RequestDispatcher requestDispatcher;
        EnderecoDAO daoEndereco;
        
        switch(request.getServletPath()){
            case "/view/menu/account/create": {
                Endereco endereco = new Endereco();
                
                endereco.setBairro(request.getParameter("bairro")); //OK
                endereco.setCep(request.getParameter("cep")); //OK
                endereco.setCidade(request.getParameter("cidade")); //OK
                endereco.setEstado(request.getParameter("estado")); //OK
                endereco.setNumero(Integer.parseInt(request.getParameter("numero"))); //OK
                endereco.setLogradouro(request.getParameter("logradouro")); //OK
                endereco.setPontoreferencia(request.getParameter("pontoreferencia")); //OK
                
                try {
                    daoFactory = new DAOFactory();
                    daoEndereco = daoFactory.getEnderecoDAO();
                    
                    daoEndereco.create(endereco);
                    
                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                Usuario usuario = new Usuario();
                
                usuario.setId_end(endereco.getId()); //OK
                usuario.setLogin(request.getParameter("login")); //OK
                usuario.setSenha(request.getParameter("senha")); //OK
                usuario.setCpf(request.getParameter("cpf")); //OK
                usuario.setSexo(request.getParameter("sexo")); //OK
                usuario.setAnotacoes(request.getParameter("anotacoes")); //OK
                usuario.setTipoDeUser(request.getParameter("tipo")); //OK
                usuario.setPnome(request.getParameter("pNome")); //OK
                usuario.setSnome(request.getParameter("sNome")); //OK
                usuario.setRg_num(request.getParameter("rg_num")); //OK
                usuario.setRg_org(endereco.getEstado());
                usuario.setStatus(true);

                String diaDia, diaMes, diaAno, data;
                diaDia = request.getParameter("dataNascUsuarioDia"); //OK
                diaMes = request.getParameter("dataNascUsuarioMes"); //OK
                diaAno = request.getParameter("dataNascUsuarioAno"); //OK
                data = diaAno + "-" + diaMes + "-" + diaDia;
                usuario.setDataNasc(Constantes.getSQLDate(data));

                usuario.setCelular(request.getParameter("dddCel") + request.getParameter("celular")); //OK
                usuario.setTelefone(request.getParameter("dddTel") + request.getParameter("telefone")); //OK
                
                try {
                    daoFactory = new DAOFactory();
                    daoUsuario = daoFactory.getUsuarioDAO();
                    
                    daoUsuario.create(usuario);
                    
                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    request.setAttribute("error", ex.getMessage());
                    
                    requestDispatcher = request.getRequestDispatcher("/error.jsp");
                    requestDispatcher.forward(request, response);
                    break;
                }
                
                String tipoUser = usuario.getTipoDeUser();
                switch(tipoUser){
                    case "psicologo": {
                        Psicologa psicologa = new Psicologa();

                        psicologa.setCrp(request.getParameter("crp")); //OK
                        psicologa.setIdUsuario(usuario.getId());

                        try {
                            daoFactory = new DAOFactory();
                            daoPsicologa = daoFactory.getPsicologaDAO();

                            daoPsicologa.create(psicologa);

                            daoFactory.close();
                        } catch (ClassNotFoundException | SQLException ex) {
                            request.setAttribute("error", ex.getMessage());
                            requestDispatcher = request.getRequestDispatcher("/error.jsp");
                            requestDispatcher.forward(request, response);
                            break;
                        }
                        break;
                    }
                    case "assistente": {
                        Assistente assistente = new Assistente();

                        assistente.setCress(request.getParameter("cress")); //OK
                        assistente.setIdUsuario(usuario.getId());

                        try {
                            daoFactory = new DAOFactory();
                            daoAssistente = daoFactory.getAssistenteDAO();

                            daoAssistente.create(assistente);

                            daoFactory.close();
                        } catch (ClassNotFoundException | SQLException ex) {
                            request.setAttribute("error", ex.getMessage());
                            requestDispatcher = request.getRequestDispatcher("/error.jsp");
                            requestDispatcher.forward(request, response);
                            break;
                        }
                        break;
                    }
                    case "voluntario": {
                        Voluntario voluntario = new Voluntario();

                        voluntario.setIdUsuario(usuario.getId());
                        voluntario.setProfissao(request.getParameter("profissao")); //OK
                        voluntario.setEscolaridade(request.getParameter("escolaridade")); //OK
                        voluntario.setCDENome(request.getParameter("cdenome")); //OK
                        voluntario.setCDETelefone(request.getParameter("cdetelefoneDDD") + request.getParameter("cdetelefone")); //OK
                        voluntario.setLocalTrab(request.getParameter("localTrabalho")); //OK
                        voluntario.setEstadoCivil(request.getParameter("estadoCivil")); //OK
                        voluntario.setEmail(request.getParameter("email")); //OK
                        voluntario.setRemedioQueUsa(request.getParameter("nomeMedicamento")); //OK
                        voluntario.setUsaMed(Boolean.parseBoolean(request.getParameter("medicamento"))); //OK
                        voluntario.setPerg1(request.getParameter("perg1")); //OK
                        voluntario.setPerg2(request.getParameter("perg2")); //OK
                        voluntario.setPerg3(request.getParameter("perg3")); //OK
                        voluntario.setPerg4(request.getParameter("perg4")); //OK
                        
                        diaDia = request.getParameter("diaDia"); //OK
                        diaMes = request.getParameter("diaMes"); //OK
                        diaAno = request.getParameter("diaAno"); //OK
                        data = diaAno + "-" + diaMes + "-" + diaDia;
                        voluntario.setDataInicio(Constantes.getSQLDate(data));
                        //-----------------------------------------------------------------------------
                        List<String> listaAptdoes = new ArrayList<>();
                        
                        if(request.getParameter("adm") != null)
                            listaAptdoes.add(request.getParameter("adm"));
                        if(request.getParameter("art") != null)
                            listaAptdoes.add(request.getParameter("art"));
                        if(request.getParameter("bar") != null)
                            listaAptdoes.add(request.getParameter("bar"));
                        if(request.getParameter("cap") != null)
                            listaAptdoes.add(request.getParameter("cap"));
                        if(request.getParameter("eve") != null)
                            listaAptdoes.add(request.getParameter("eve"));
                        if(request.getParameter("lim") != null)
                            listaAptdoes.add(request.getParameter("lim"));
                        if(request.getParameter("mon") != null)
                            listaAptdoes.add(request.getParameter("mon"));
                        if(request.getParameter("rece") != null)
                            listaAptdoes.add(request.getParameter("rece"));
                        if(request.getParameter("recr") != null)
                            listaAptdoes.add(request.getParameter("recr"));
                        if(request.getParameter("recrH") != null)
                            listaAptdoes.add(request.getParameter("recrH"));
                        if(request.getParameter("out") != null)
                            listaAptdoes.add("Outro: " + request.getParameter("outroNome"));
                        
                        voluntario.setAptdoes((ArrayList<String>) listaAptdoes);
                        //-----------------------------------------------------------------------------
                        List<String> listaDispHor = new ArrayList<>();
                        
                        if(request.getParameter("manSeg") != null)
                            listaDispHor.add(request.getParameter("manSeg"));
                        if(request.getParameter("manTer") != null)
                            listaDispHor.add(request.getParameter("manTer"));
                        if(request.getParameter("manQua") != null)
                            listaDispHor.add(request.getParameter("manQua"));
                        if(request.getParameter("manQui") != null)
                            listaDispHor.add(request.getParameter("manQui"));
                        if(request.getParameter("manSex") != null)
                            listaDispHor.add(request.getParameter("manSex"));
                        if(request.getParameter("tarSeg") != null)
                            listaDispHor.add(request.getParameter("tarSeg"));
                        if(request.getParameter("tarTer") != null)
                            listaDispHor.add(request.getParameter("tarTer"));
                        if(request.getParameter("tarQua") != null)
                            listaDispHor.add(request.getParameter("tarQua"));
                        if(request.getParameter("tarQui") != null)
                            listaDispHor.add(request.getParameter("tarQui"));
                        if(request.getParameter("tarSex") != null)
                            listaDispHor.add(request.getParameter("tarSex"));
                        
                        voluntario.setDiasEHorariosDisp((ArrayList<String>) listaDispHor);
                        //-----------------------------------------------------------------------------
                        voluntario.setPossuiDisp(request.getParameter("disponibilidade_atividades_externas")); //OK
                        voluntario.setObsSobreHor(request.getParameter("obs_sobre_hor")); //OK
                        voluntario.setSetor(request.getParameter("setor")); //OK
                        //-----------------------------------------------------------------------------
                        List<String> listaDiasSetor = new ArrayList<>();
                        
                        if(request.getParameter("seg") != null)
                            listaDiasSetor.add(request.getParameter("seg"));
                        if(request.getParameter("ter") != null)
                            listaDiasSetor.add(request.getParameter("ter"));
                        if(request.getParameter("qua") != null)
                            listaDiasSetor.add(request.getParameter("qua"));
                        if(request.getParameter("qui") != null)
                            listaDiasSetor.add(request.getParameter("qui"));
                        if(request.getParameter("sex") != null)
                            listaDiasSetor.add(request.getParameter("sex"));
                        
                        voluntario.setDiasSetor((ArrayList<String>) listaDiasSetor);
                        //-----------------------------------------------------------------------------
                        String hora = request.getParameter("horas") + ":" + request.getParameter("minutos") + ":00"; //OK
                        voluntario.setHora(Constantes.getSQLTime(hora));
                        
                        if(request.getParameter("aceitacaoDosTermos") != null)
                            voluntario.setAceitacaoDosTermos(Boolean.parseBoolean(request.getParameter("aceitacaoDosTermos")));
                        else
                            voluntario.setAceitacaoDosTermos(false);
                        //-----------------------------------------------------------------------------
                        try {
                            daoFactory = new DAOFactory();
                            daoVoluntario = daoFactory.getVoluntarioDAO();

                            daoVoluntario.create(voluntario);

                            daoFactory.close();
                        } catch (ClassNotFoundException | SQLException ex) {
                            Logger.getLogger(VoluntarioController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        break;
                    }
                    default: {
                        break;
                    }
                }
                requestDispatcher = request.getRequestDispatcher("/view/menu/account/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/view/menu/account/read": {
                
                break;
            }
            case "/view/menu/account/update": {
                Endereco endereco = new Endereco();
                
                HttpSession session = request.getSession();
                int idDoEnd = (int) session.getAttribute("idEnd");
                
                endereco.setId(idDoEnd);
                endereco.setBairro(request.getParameter("bairro")); //OK
                endereco.setCep(request.getParameter("cep")); //OK
                endereco.setCidade(request.getParameter("cidade")); //OK
                endereco.setEstado(request.getParameter("estado")); //OK
                endereco.setNumero(Integer.parseInt(request.getParameter("numero"))); //OK
                endereco.setLogradouro(request.getParameter("logradouro")); //OK
                endereco.setPontoreferencia(request.getParameter("pontoreferencia")); //OK
                
                try {
                    daoFactory = new DAOFactory();
                    daoEndereco = daoFactory.getEnderecoDAO();
                    
                    daoEndereco.update(endereco);
                    
                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                Usuario usuario = new Usuario();
                
                session = request.getSession();
                int idDoUser = (int) session.getAttribute("idUser");
                
                usuario.setId(idDoUser);
                usuario.setId_end(endereco.getId()); //OK
                usuario.setLogin(request.getParameter("login")); //OK
                usuario.setSenha(request.getParameter("senha")); //OK
                usuario.setCpf(request.getParameter("cpf")); //OK
                usuario.setSexo(request.getParameter("sexo")); //OK
                usuario.setAnotacoes(request.getParameter("anotacoes")); //OK
                usuario.setTipoDeUser(request.getParameter("tipo")); //OK
                usuario.setPnome(request.getParameter("pNome")); //OK
                usuario.setSnome(request.getParameter("sNome")); //OK
                usuario.setRg_num(request.getParameter("rg_num")); //OK
                usuario.setRg_org(endereco.getEstado());
                usuario.setStatus(true);

                String diaDia, diaMes, diaAno, data;
                diaDia = request.getParameter("dataNascUsuarioDia"); //OK
                diaMes = request.getParameter("dataNascUsuarioMes"); //OK
                diaAno = request.getParameter("dataNascUsuarioAno"); //OK
                data = diaAno + "-" + diaMes + "-" + diaDia;
                usuario.setDataNasc(Constantes.getSQLDate(data));

                usuario.setCelular(request.getParameter("dddCel") + request.getParameter("celular")); //OK
                usuario.setTelefone(request.getParameter("dddTel") + request.getParameter("telefone")); //OK
                
                try {
                    daoFactory = new DAOFactory();
                    daoUsuario = daoFactory.getUsuarioDAO();
                    
                    daoUsuario.update(usuario);
                    
                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                String tipoUser = usuario.getTipoDeUser();
                switch(tipoUser){
                    case "psicologo": {
                        Psicologa psicologa = new Psicologa();

                        psicologa.setId(Integer.parseInt(request.getParameter("idPsico")));
                        psicologa.setCrp(request.getParameter("crp")); //OK
                        psicologa.setIdUsuario(usuario.getId());

                        try {
                            daoFactory = new DAOFactory();
                            daoPsicologa = daoFactory.getPsicologaDAO();

                            daoPsicologa.update(psicologa);

                            daoFactory.close();
                        } catch (ClassNotFoundException | SQLException ex) {
                            Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        break;
                    }
                    case "assistente": {
                        Assistente assistente = new Assistente();

                        assistente.setId(Integer.parseInt(request.getParameter("idAss")));
                        assistente.setCress(request.getParameter("cress")); //OK
                        assistente.setIdUsuario(usuario.getId());

                        try {
                            daoFactory = new DAOFactory();
                            daoAssistente = daoFactory.getAssistenteDAO();

                            daoAssistente.update(assistente);

                            daoFactory.close();
                        } catch (ClassNotFoundException | SQLException ex) {
                            Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        break;
                    }
                    case "voluntario": {
                        Voluntario voluntario = new Voluntario();

                        voluntario.setId(Integer.parseInt(request.getParameter("idVolunt")));
                        voluntario.setIdUsuario(usuario.getId());
                        voluntario.setProfissao(request.getParameter("profissao")); //OK
                        voluntario.setEscolaridade(request.getParameter("escolaridade")); //OK
                        voluntario.setCDENome(request.getParameter("cdenome")); //OK
                        voluntario.setCDETelefone(request.getParameter("cdetelefoneDDD") + request.getParameter("cdetelefone")); //OK
                        voluntario.setLocalTrab(request.getParameter("localTrabalho")); //OK
                        voluntario.setEstadoCivil(request.getParameter("estadoCivil")); //OK
                        voluntario.setEmail(request.getParameter("email")); //OK
                        voluntario.setRemedioQueUsa(request.getParameter("nomeMedicamento")); //OK
                        voluntario.setUsaMed(Boolean.parseBoolean(request.getParameter("medicamento"))); //OK
                        voluntario.setPerg1(request.getParameter("perg1")); //OK
                        voluntario.setPerg2(request.getParameter("perg2")); //OK
                        voluntario.setPerg3(request.getParameter("perg3")); //OK
                        voluntario.setPerg4(request.getParameter("perg4")); //OK
                        
                        diaDia = request.getParameter("diaDia"); //OK
                        diaMes = request.getParameter("diaMes"); //OK
                        diaAno = request.getParameter("diaAno"); //OK
                        data = diaAno + "-" + diaMes + "-" + diaDia;
                        voluntario.setDataInicio(Constantes.getSQLDate(data));
                        //-----------------------------------------------------------------------------
                        List<String> listaAptdoes = new ArrayList<>();
                        
                        if(request.getParameter("adm") != null)
                            listaAptdoes.add(request.getParameter("adm"));
                        if(request.getParameter("art") != null)
                            listaAptdoes.add(request.getParameter("art"));
                        if(request.getParameter("bar") != null)
                            listaAptdoes.add(request.getParameter("bar"));
                        if(request.getParameter("cap") != null)
                            listaAptdoes.add(request.getParameter("cap"));
                        if(request.getParameter("eve") != null)
                            listaAptdoes.add(request.getParameter("eve"));
                        if(request.getParameter("lim") != null)
                            listaAptdoes.add(request.getParameter("lim"));
                        if(request.getParameter("mon") != null)
                            listaAptdoes.add(request.getParameter("mon"));
                        if(request.getParameter("rece") != null)
                            listaAptdoes.add(request.getParameter("rece"));
                        if(request.getParameter("recr") != null)
                            listaAptdoes.add(request.getParameter("recr"));
                        if(request.getParameter("recrH") != null)
                            listaAptdoes.add(request.getParameter("recrH"));
                        if(request.getParameter("out") != null)
                            listaAptdoes.add("Outro: " + request.getParameter("outroNome"));
                        
                        voluntario.setAptdoes((ArrayList<String>) listaAptdoes);
                        //-----------------------------------------------------------------------------
                        List<String> listaDispHor = new ArrayList<>();
                        
                        if(request.getParameter("manSeg") != null)
                            listaDispHor.add(request.getParameter("manSeg"));
                        if(request.getParameter("manTer") != null)
                            listaDispHor.add(request.getParameter("manTer"));
                        if(request.getParameter("manQua") != null)
                            listaDispHor.add(request.getParameter("manQua"));
                        if(request.getParameter("manQui") != null)
                            listaDispHor.add(request.getParameter("manQui"));
                        if(request.getParameter("manSex") != null)
                            listaDispHor.add(request.getParameter("manSex"));
                        if(request.getParameter("tarSeg") != null)
                            listaDispHor.add(request.getParameter("tarSeg"));
                        if(request.getParameter("tarTer") != null)
                            listaDispHor.add(request.getParameter("tarTer"));
                        if(request.getParameter("tarQua") != null)
                            listaDispHor.add(request.getParameter("tarQua"));
                        if(request.getParameter("tarQui") != null)
                            listaDispHor.add(request.getParameter("tarQui"));
                        if(request.getParameter("tarSex") != null)
                            listaDispHor.add(request.getParameter("tarSex"));
                        
                        voluntario.setDiasEHorariosDisp((ArrayList<String>) listaDispHor);
                        //-----------------------------------------------------------------------------
                        voluntario.setPossuiDisp(request.getParameter("disponibilidade_atividades_externas")); //OK
                        voluntario.setObsSobreHor(request.getParameter("obs_sobre_hor")); //OK
                        voluntario.setSetor(request.getParameter("setor")); //OK
                        //-----------------------------------------------------------------------------
                        List<String> listaDiasSetor = new ArrayList<>();
                        
                        if(request.getParameter("seg") != null)
                            listaDiasSetor.add(request.getParameter("seg"));
                        if(request.getParameter("ter") != null)
                            listaDiasSetor.add(request.getParameter("ter"));
                        if(request.getParameter("qua") != null)
                            listaDiasSetor.add(request.getParameter("qua"));
                        if(request.getParameter("qui") != null)
                            listaDiasSetor.add(request.getParameter("qui"));
                        if(request.getParameter("sex") != null)
                            listaDiasSetor.add(request.getParameter("sex"));
                        
                        voluntario.setDiasSetor((ArrayList<String>) listaDiasSetor);
                        //-----------------------------------------------------------------------------
                        String hora = request.getParameter("horas") + ":" + request.getParameter("minutos") + ":00"; //OK
                        voluntario.setHora(Constantes.getSQLTime(hora));
                        
                        if(request.getParameter("aceitacaoDosTermos") != null)
                            voluntario.setAceitacaoDosTermos(Boolean.parseBoolean(request.getParameter("aceitacaoDosTermos")));
                        else
                            voluntario.setAceitacaoDosTermos(false);
                        //-----------------------------------------------------------------------------
                        try {
                            daoFactory = new DAOFactory();
                            daoVoluntario = daoFactory.getVoluntarioDAO();

                            daoVoluntario.update(voluntario);

                            daoFactory.close();
                        } catch (ClassNotFoundException | SQLException ex) {
                            Logger.getLogger(VoluntarioController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        break;
                    }
                    default: {
                        break;
                    }
                }
                requestDispatcher = request.getRequestDispatcher("/view/menu/account/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/view/menu/account/delete": {
                int id = Integer.parseInt(request.getParameter("id"));
                
                try {
                    daoFactory = new DAOFactory();
                    daoUsuario = daoFactory.getUsuarioDAO();
                    
                    daoUsuario.delete(id);
                    
                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(DoacaoController.class.getName()).log(Level.SEVERE, null, ex);
                }

                requestDispatcher = request.getRequestDispatcher("/view/menu/account/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
