package controller;

import dao.AtendimentoPsicoDAO;
import dao.DAOFactory;
import dao.PacienteDAO;
import dao.PsicologaDAO;
import global.Constantes;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.AtendPsico;
import model.Atendimento;
import model.Paciente;
import model.Psicologa;
import model.Usuario;

@WebServlet(name = "AtendPsicoController", 
        urlPatterns = {
            "/AtendPsicoController",
            "/view/menu/agendamentos/psychology",
            "/view/menu/agendamentos/psychology/create",
            "/view/menu/agendamentos/psychology/read",
            "/view/menu/agendamentos/psychology/readAll",
            "/view/menu/agendamentos/psychology/readBySearch"
        })
public class AtendPsicoController extends HttpServlet {
    DAOFactory daoFactory;
    AtendimentoPsicoDAO atendimentoPsicoDAO;
    PacienteDAO pacienteDAO;
    PsicologaDAO psicologaDAO;
    RequestDispatcher requestDispatcher;
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        switch(request.getServletPath()){
            case "/view/menu/agendamentos/psychology":{
                requestDispatcher = request.getRequestDispatcher("/view/menu/agendamentos/psychology/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            
            } 
            
            case "/view/menu/agendamentos/psychology/create": {
               List<Paciente> pacientes =  new ArrayList();
                try {

                    daoFactory = new DAOFactory();
                    pacienteDAO = daoFactory.getPacienteDAO();
                    pacientes = pacienteDAO.all();

                    daoFactory.close();
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(AtendPsicoController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(AtendPsicoController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                request.setAttribute("pacientes", pacientes);
                requestDispatcher = request.getRequestDispatcher("/view/menu/agendamentos/psychology/create/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/view/menu/agendamentos/psychology/read": {
                AtendPsico atendPsico = null;
                Paciente paciente = null;
                Atendimento atendimento = new Atendimento();
            try {
                daoFactory =  new DAOFactory();
                atendimentoPsicoDAO = daoFactory.getAtendimentoPsicoDAO();
                pacienteDAO = daoFactory.getPacienteDAO();
                int idPaciente = Integer.parseInt(request.getParameter("idPaciente"));
                int idAtendimento = Integer.parseInt(request.getParameter("idAtendimento"));
                
                paciente = pacienteDAO.read(idPaciente);
                atendPsico =  atendimentoPsicoDAO.read(idAtendimento);
                
                atendimento.setAtendPsico(atendPsico);
                atendimento.setPaciente(paciente);
                
                daoFactory.close();
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(AtendPsicoController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(AtendPsicoController.class.getName()).log(Level.SEVERE, null, ex);
            }
                request.setAttribute("atendimento", atendimento);
                requestDispatcher = request.getRequestDispatcher("/view/menu/agendamentos/psychology/read/visualizar.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/view/menu/agendamentos/psychology/readAll":{
                List<AtendPsico> atendimentosPsico = null;
                List<Atendimento> atendimentos =  new ArrayList<>();
                Paciente paciente  = null;
                Atendimento  atendimento = null;
            try {
                daoFactory = new DAOFactory();
                atendimentoPsicoDAO = daoFactory.getAtendimentoPsicoDAO();
                pacienteDAO = daoFactory.getPacienteDAO();
                atendimentosPsico = atendimentoPsicoDAO.all();
                
                for(AtendPsico atendPsico : atendimentosPsico){
                    atendimento = new Atendimento();
                    int idPaciente = atendPsico.getIdPaciente();
                    paciente = pacienteDAO.read(idPaciente);
                    
                    atendimento.setAtendPsico(atendPsico);
                    atendimento.setPaciente(paciente);
                    
                    atendimentos.add(atendimento);
                    
                }
                
                daoFactory.close();
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(AtendPsicoController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(AtendPsicoController.class.getName()).log(Level.SEVERE, null, ex);
            }
                request.setAttribute("atendimentos", atendimentos);
                //Cremonez ver a view para qual será direcionada
                requestDispatcher = request.getRequestDispatcher("/view/menu/agendamentos/psychology/read/resultado.jsp");
                requestDispatcher.forward(request, response);
                break;
                
            }
            case "/view/menu/agendamentos/psychology/readBySearch":{
                List<Atendimento> atendimentos = new ArrayList<>();
                List<AtendPsico> atendimentosPsico = null;
                List<Paciente> pacientes = null;
                Atendimento atendimento = null;
            try {
                daoFactory = new DAOFactory();
                pacienteDAO = daoFactory.getPacienteDAO();
                atendimentoPsicoDAO = daoFactory.getAtendimentoPsicoDAO();
                
                String searchParam = request.getParameter("input");
                
                pacientes = pacienteDAO.readByName(searchParam);
                
                for(Paciente paciente : pacientes){
                   atendimentosPsico = atendimentoPsicoDAO.readByidPaciente(paciente.getId());
                   for(AtendPsico atendPsico : atendimentosPsico){
                       atendimento = new Atendimento();
                       atendimento.setAtendPsico(atendPsico);
                       atendimento.setPaciente(paciente);
                       atendimentos.add(atendimento);
                   }
                }
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(AtendPsicoController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(AtendPsicoController.class.getName()).log(Level.SEVERE, null, ex);
            }   
                request.setAttribute("atendimentos", atendimentos);
                //Cremonez ver a view para qual será direcionada
                requestDispatcher = request.getRequestDispatcher("/view/menu/agendamentos/psychology/read/resultado.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "//update": {
                
                break;
            }
            case "//delete": {
                
                break;
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        switch(request.getServletPath()){
            case "/":{} 
            case "/view/menu/agendamentos/psychology/create": {
            try {
                daoFactory = new DAOFactory();
                atendimentoPsicoDAO = daoFactory.getAtendimentoPsicoDAO();
                psicologaDAO = daoFactory.getPsicologaDAO();
                AtendPsico atendPsico = new AtendPsico();
                HttpSession sessao = request.getSession(true);
                Usuario usuario = (Usuario) sessao.getAttribute("usuario");
                Psicologa psico = psicologaDAO.read(usuario.getId());
                atendPsico.setIdPsicologa(psico.getIdP());
                atendPsico.setIdPaciente(Integer.parseInt(request.getParameter("idPaciente")));
                atendPsico.setPatologia(request.getParameter("patologia"));
                atendPsico.setQueixa(request.getParameter("queixa"));
                atendPsico.setAtendimento(request.getParameter("atendimento"));
                atendPsico.setDataAtendimento(Constantes.getTodaysDate());
                //atendPsico.setHora();
                
                atendimentoPsicoDAO.create(atendPsico);
                
                
                
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(AtendPsicoController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(AtendPsicoController.class.getName()).log(Level.SEVERE, null, ex);
            }
                requestDispatcher = request.getRequestDispatcher("/view/menu/agendamentos/psychology/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "//read": {
                
                break;
            }
            case "//update": {
                
                break;
            }
            case "//delete": {
                
                break;
            }
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
