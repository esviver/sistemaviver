package controller;

import dao.DAOFactory;
import dao.DistribuicaoDAO;
import dao.PacienteDAO;
import global.Constantes;
import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Distribuicao;
import model.Paciente;
import model.Usuario;

@WebServlet(name = "DistribuicaoController", urlPatterns = {
    "/view/menu/basket",
    "/view/menu/basket/create",
    "/view/menu/basket/update",
    "/view/menu/basket/readBySearch",
    "/view/menu/basket/delete",
    "/view/menu/basket/readAll"
})
public class DistribuicaoController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DAOFactory daoFactory;
        DistribuicaoDAO distribuicaoDao;
        switch (request.getServletPath()) {
            case "/view/menu/basket/create": {
                List<Paciente> pacientes = new ArrayList();
                try {

                    daoFactory = new DAOFactory();
                    PacienteDAO pacienteDAO = daoFactory.getPacienteDAO();
                    pacientes = pacienteDAO.all();
                    daoFactory.close();
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(AtendPsicoController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(AtendPsicoController.class.getName()).log(Level.SEVERE, null, ex);
                }

                request.setAttribute("pacientes", pacientes);
                RequestDispatcher requestDispatcher = request.getRequestDispatcher("/view/menu/basket/create/index.jsp");
                requestDispatcher.forward(request, response);

                break;
            }
            case "/view/menu/basket/readBySearch": {
                List<Distribuicao> lista = null;
                System.out.println("ab");
                try {
                    daoFactory = new DAOFactory();
                    distribuicaoDao = daoFactory.getDistribuicaoDAO();

                    String searchParam = request.getParameter("input");

                    if (request.getParameter("trash").equals("true")) {
                        System.out.println("com apagado");
                        lista = distribuicaoDao.readByNameWithDeleted(searchParam);
                    } else if (request.getParameter("trash").equals("false")) {
                        System.out.println("sem apagado");
                        lista = distribuicaoDao.readByNameWithoutDeleted(searchParam);
                    }

                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(DoacaoController.class.getName()).log(Level.SEVERE, null, ex);
                }

                if (lista.isEmpty()) {
                    System.out.println("Vazio");
                }
                for (Distribuicao distribuicao : lista) {
                    System.out.println(distribuicao.getId() + "  " + distribuicao.getData());

                }

                request.setAttribute("listaPResultados", lista);

                RequestDispatcher requestDispatcher = request.getRequestDispatcher("/view/menu/basket/read/resultado.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/view/menu/basket/readAll": {
                List<Distribuicao> lista = new ArrayList<>();
                System.out.println("all");
                System.out.println("AQUI");
                System.out.println(request.getParameter("trash").toString());
                try {
                    daoFactory = new DAOFactory();
                    distribuicaoDao = daoFactory.getDistribuicaoDAO();

                    if (request.getParameter("trash").equals("true")) {
                        lista = distribuicaoDao.allArq();
                    } else {
                        lista = distribuicaoDao.all();
                    }

                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(DoacaoController.class.getName()).log(Level.SEVERE, null, ex);
                }
                if (lista.isEmpty()) {
                    System.out.println("Vazio");
                }
                for (Distribuicao distribuicao : lista) {
                    System.out.println(distribuicao.getId() + "  " + distribuicao.getData());

                }
                request.setAttribute("listaPResultados", lista);

                RequestDispatcher requestDispatcher = request.getRequestDispatcher("/view/menu/basket/read/resultado.jsp");
                requestDispatcher.forward(request, response);
                break;
            }

            case "/view/menu/basket/delete": {
                try {
                    daoFactory = new DAOFactory();
                    distribuicaoDao = daoFactory.getDistribuicaoDAO();
                    distribuicaoDao.delete(Integer.parseInt(request.getParameter("id")));
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(HospedagemController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(HospedagemController.class.getName()).log(Level.SEVERE, null, ex);
                }

                RequestDispatcher requestDispatcher = request.getRequestDispatcher("/view/menu/basket/read/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/view/menu/basket/update": {
                Distribuicao distribuicao = null;
                System.out.println("eeeeee");
                try {
                    daoFactory = new DAOFactory();
                    distribuicaoDao = daoFactory.getDistribuicaoDAO();
                    distribuicao = distribuicaoDao.read(Integer.parseInt(request.getParameter("id")));
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(HospedagemController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(HospedagemController.class.getName()).log(Level.SEVERE, null, ex);
                }
                System.out.println(distribuicao.getData());
                request.setAttribute("ano", distribuicao.getData().toString().subSequence(0, distribuicao.getData().toString().indexOf("-")));
                request.setAttribute("mes", distribuicao.getData().toString().subSequence(distribuicao.getData().toString().indexOf("-") + 1, distribuicao.getData().toString().lastIndexOf("-")));
                request.setAttribute("dia", distribuicao.getData().toString().subSequence(distribuicao.getData().toString().lastIndexOf("-") + 1, distribuicao.getData().toString().length()));
                request.setAttribute("distribuicao", distribuicao);
                RequestDispatcher requestDispatcher = request.getRequestDispatcher("/view/menu/basket/update/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DAOFactory daoFactory;
        DistribuicaoDAO distribuicaoDao;
        PacienteDAO pacienteDao;
        switch (request.getServletPath()) {
            case "/view/menu/basket/create": {
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-mm-dd");

                HttpSession sessao = request.getSession(true);
                Usuario usuario = (Usuario) sessao.getAttribute("usuario");
                System.out.println(usuario.getLogin());
                Distribuicao distribuicao = new Distribuicao();
                distribuicao.setPaciente(Integer.parseInt(request.getParameter("idPaciente")));
                distribuicao.setLocal(request.getParameter("local"));
                System.out.println("cesta " + request.getParameter("cesta"));
                if (request.getParameter("cesta") != null) {
                    if (request.getParameter("cesta").equals("cesta")) {
                        distribuicao.setCesta(true);
                    }
                } else {
                    distribuicao.setCesta(false);
                }
                if (request.getParameter("leite") != null) {
                    if (request.getParameter("leite").equals("leite")) {
                        distribuicao.setLeite(true);
                    }
                } else {
                    distribuicao.setLeite(false);
                }
                try {
                    distribuicao.setData(Constantes.getSQLDate(request.getParameter("ano") + "-" + request.getParameter("mes") + "-" + request.getParameter("dia")));
                    daoFactory = new DAOFactory();
                    distribuicaoDao = daoFactory.getDistribuicaoDAO();
                    distribuicaoDao.create(distribuicao);
                    pacienteDao = daoFactory.getPacienteDAO();
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(DistribuicaoController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(DistribuicaoController.class.getName()).log(Level.SEVERE, null, ex);
                }
                RequestDispatcher requestDispatcher = request.getRequestDispatcher("/view/menu/basket/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }

           case "/view/menu/basket/update": {
                HttpSession sessao = request.getSession(true);
                Usuario usuario = (Usuario) sessao.getAttribute("usuario");
                System.out.println(usuario.getLogin());
                Distribuicao distribuicao = new Distribuicao();
                distribuicao.setPaciente(Integer.parseInt(request.getParameter("idPaciente")));
                distribuicao.setId(Integer.parseInt(request.getParameter("idH")));
                System.out.println(distribuicao.getId());
                if(request.getParameter("local")==null)
                    distribuicao.setLocal(request.getParameter("localC"));
                else
                    distribuicao.setLocal(request.getParameter("local"));
                System.out.println("cesta " + request.getParameter("cesta"));
                if (request.getParameter("cesta") != null) {
                    if (request.getParameter("cesta").equals("cesta")) {
                        distribuicao.setCesta(true);
                    }
                } else {
                    distribuicao.setCesta(false);
                }
                if (request.getParameter("leite") != null) {
                    if (request.getParameter("leite").equals("leite")) {
                        distribuicao.setLeite(true);
                    }
                } else {
                    distribuicao.setLeite(false);
                }
                try {
                    distribuicao.setData(Constantes.getSQLDate(request.getParameter("ano") + "-" + request.getParameter("mes") + "-" + request.getParameter("dia")));
                    daoFactory = new DAOFactory();
                    distribuicaoDao = daoFactory.getDistribuicaoDAO();
                    distribuicaoDao.update(distribuicao);
                    pacienteDao = daoFactory.getPacienteDAO();
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(DistribuicaoController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(DistribuicaoController.class.getName()).log(Level.SEVERE, null, ex);
                }
                RequestDispatcher requestDispatcher = request.getRequestDispatcher("/view/menu/basket/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
