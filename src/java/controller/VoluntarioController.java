package controller;

import dao.DAOFactory;
import dao.VoluntarioDAO;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Voluntario;

@WebServlet(name = "VoluntarioController",
        urlPatterns = {
            "/view/menu/voluntario",
            "/view/menu/voluntario/create",
            "/view/menu/voluntario/read",
            "/view/menu/voluntario/update",
            "/view/menu/voluntario/delete",
            "/view/menu/voluntario/readBySearch",
            "/view/menu/voluntario/readAll",
            "/view/menu/voluntario/recover"
        })
public class VoluntarioController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DAOFactory daoFactory;
        VoluntarioDAO daoVoluntario;
        RequestDispatcher requestDispatcher;

        switch (request.getServletPath()) {
            case "/view/menu/voluntario": {
                requestDispatcher = request.getRequestDispatcher("/view/menu/voluntario/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/view/menu/voluntario/create": {
                requestDispatcher = request.getRequestDispatcher("/view/menu/voluntario/create/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/view/menu/voluntario/readAll": {
                List<Voluntario> lista = null;
                try {
                    daoFactory = new DAOFactory();
                    daoVoluntario = daoFactory.getVoluntarioDAO();

                    if (request.getParameter("trash").equals("true")) {
                        lista = daoVoluntario.all();
                    } else if (request.getParameter("trash").equals("false")) {
                        lista = daoVoluntario.allWithoutDeleted();
                    }

                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(VoluntarioController.class.getName()).log(Level.SEVERE, null, ex);
                }

                request.setAttribute("listaPResultados", lista);
                requestDispatcher = request.getRequestDispatcher("/view/menu/voluntario/read/resultado.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/view/menu/voluntario/readBySearch": {
                List<Voluntario> lista = null;

                try {
                    daoFactory = new DAOFactory();
                    daoVoluntario = daoFactory.getVoluntarioDAO();

                    String searchParam = request.getParameter("input");

                    if(request.getParameter("trash").equals("true")){
                        lista = daoVoluntario.readByName(searchParam);
                    } else if (request.getParameter("trash").equals("false")) {
                        lista = daoVoluntario.readByNameWithoutDeleted(searchParam);
                    }

                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(VoluntarioController.class.getName()).log(Level.SEVERE, null, ex);
                }

                request.setAttribute("listaPResultados", lista);
                requestDispatcher = request.getRequestDispatcher("/view/menu/voluntario/read/resultado.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/view/menu/voluntario/update": {
                Voluntario voluntario = null;
                int id = Integer.parseInt(request.getParameter("id"));

                try {
                    daoFactory = new DAOFactory();
                    daoVoluntario = daoFactory.getVoluntarioDAO();

                    voluntario = daoVoluntario.read(id);

                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(VoluntarioController.class.getName()).log(Level.SEVERE, null, ex);
                }

                request.setAttribute("voluntarioParaUpdate", voluntario);

                requestDispatcher = request.getRequestDispatcher("/view/menu/voluntario/update/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/view/menu/voluntario/recover": {
                int id = Integer.parseInt(request.getParameter("id"));

                try {
                    daoFactory = new DAOFactory();
                    daoVoluntario = daoFactory.getVoluntarioDAO();

                    daoVoluntario.recover(id);

                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(VoluntarioController.class.getName()).log(Level.SEVERE, null, ex);
                }

                requestDispatcher = request.getRequestDispatcher("/view/menu/voluntario/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/view/menu/voluntario/delete": {

                break;
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        DAOFactory daoFactory;
        VoluntarioDAO daoVoluntario;
        RequestDispatcher requestDispatcher;

        switch (request.getServletPath()) {
            case "/view/menu/voluntario/create": {
                Voluntario voluntario = new Voluntario();

                voluntario.setIdUsuario(Integer.parseInt(request.getParameter("idusuario")));
                voluntario.setProfissao(request.getParameter("profissao"));
                voluntario.setEscolaridade(request.getParameter("escolaridade"));
                voluntario.setCDENome(request.getParameter("cdenome"));
                voluntario.setCDETelefone(request.getParameter("cdetelefone"));
                voluntario.setLocalTrab(request.getParameter("local_trab"));
                voluntario.setEstadoCivil(request.getParameter("estado_civil"));
                voluntario.setEmail(request.getParameter("estado_civil"));
                voluntario.setRemedioQueUsa(request.getParameter("remedio_que_usa"));
                voluntario.setPossuiDisp(request.getParameter("possui_disp"));
                voluntario.setObsSobreHor(request.getParameter("obs_sobre_hor"));
                voluntario.setSetor(request.getParameter("setor"));

                if (request.getParameter("usa_med") == "1") {
                    voluntario.setUsaMed(true);
                } else {
                    voluntario.setUsaMed(false);
                }

                voluntario.setAptdoes((ArrayList<String>) Arrays.asList(request.getParameter("aptidoes").split(",")));
                voluntario.setDiasEHorariosDisp((ArrayList<String>) Arrays.asList(request.getParameter("dhdisp").split(",")));
                voluntario.setDiasSetor((ArrayList<String>) Arrays.asList(request.getParameter("diassetor").split(",")));

                try {
                    daoFactory = new DAOFactory();
                    daoVoluntario = daoFactory.getVoluntarioDAO();

                    daoVoluntario.create(voluntario);

                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(VoluntarioController.class.getName()).log(Level.SEVERE, null, ex);
                }

                requestDispatcher = request.getRequestDispatcher("/view/menu/voluntario/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }

            case "/view/menu/voluntario/read": {

                break;
            }

            case "/view/menu/voluntario/update": {
                Voluntario voluntario = new Voluntario();

                voluntario.setProfissao(request.getParameter("profissao"));
                voluntario.setEscolaridade(request.getParameter("escolaridade"));
                voluntario.setCDENome(request.getParameter("cdenome"));
                voluntario.setCDETelefone(request.getParameter("cdetelefone"));
                voluntario.setLocalTrab(request.getParameter("local_trab"));
                voluntario.setEstadoCivil(request.getParameter("estado_civil"));
                voluntario.setEmail(request.getParameter("estado_civil"));
                voluntario.setRemedioQueUsa(request.getParameter("remedio_que_usa"));
                voluntario.setPossuiDisp(request.getParameter("possui_disp"));
                voluntario.setObsSobreHor(request.getParameter("obs_sobre_hor"));
                voluntario.setSetor(request.getParameter("setor"));

                if (request.getParameter("usa_med") == "1") {
                    voluntario.setUsaMed(true);
                } else {
                    voluntario.setUsaMed(false);
                }

                voluntario.setAptdoes((ArrayList<String>) Arrays.asList(request.getParameter("aptidoes").split(",")));
                voluntario.setDiasEHorariosDisp((ArrayList<String>) Arrays.asList(request.getParameter("dhdisp").split(",")));
                voluntario.setDiasSetor((ArrayList<String>) Arrays.asList(request.getParameter("diassetor").split(",")));

                try {
                    daoFactory = new DAOFactory();
                    daoVoluntario = daoFactory.getVoluntarioDAO();

                    daoVoluntario.update(voluntario);

                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(VoluntarioController.class.getName()).log(Level.SEVERE, null, ex);
                }

                requestDispatcher = request.getRequestDispatcher("/view/menu/voluntario/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }

            case "/view/menu/voluntario/delete": {
                int id = Integer.parseInt(request.getParameter("idusuario"));

                try {
                    daoFactory = new DAOFactory();
                    daoVoluntario = daoFactory.getVoluntarioDAO();

                    daoVoluntario.delete(id);

                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(VoluntarioController.class.getName()).log(Level.SEVERE, null, ex);
                }

                requestDispatcher = request.getRequestDispatcher("/view/menu/voluntario/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
        }
    }
}

