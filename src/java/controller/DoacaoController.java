package controller;

import global.Constantes;
import dao.DAOFactory;
import dao.DoacaoDAO;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Doacao;

@WebServlet(name = "DoacaoController", 
        urlPatterns = {
            "/view/menu/donate",
            "/view/menu/donate/create",
            "/view/menu/donate/read",
            "/view/menu/donate/update",
            "/view/menu/donate/delete",
            "/view/menu/donate/readBySearch",
            "/view/menu/donate/readAll",
            "/view/menu/donate/recover"
        })
public class DoacaoController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        DAOFactory daoFactory;
        DoacaoDAO daoDoacao;
        RequestDispatcher requestDispatcher;
        
        switch(request.getServletPath()){
            case "/view/menu/donate": {
                requestDispatcher = request.getRequestDispatcher("/view/menu/donate/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/view/menu/donate/create": {
                requestDispatcher = request.getRequestDispatcher("/view/menu/donate/create/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/view/menu/donate/readAll": {
                List<Doacao> lista = null;

                try {
                    daoFactory = new DAOFactory();
                    daoDoacao = daoFactory.getDoacaoDAO();
                    
                    if(request.getParameter("trash").equals("true")){
                        lista = daoDoacao.all();
                    }else if(request.getParameter("trash").equals("false")){
                        lista = daoDoacao.allWithoutDeleted();
                    }
                    
                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(DoacaoController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                request.setAttribute("listaPResultados", lista);
                requestDispatcher = request.getRequestDispatcher("/view/menu/donate/read/resultado.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/view/menu/donate/readBySearch": {
                List<Doacao> lista = null;
                
                try {
                    daoFactory = new DAOFactory();
                    daoDoacao = daoFactory.getDoacaoDAO();
                    
                    String searchParam = request.getParameter("input");

                    if(request.getParameter("trash").equals("true")){
                        lista = daoDoacao.readByName(searchParam);
                    }else if(request.getParameter("trash").equals("false")){
                        lista = daoDoacao.readByNameWithoutDeleted(searchParam);
                    }
                    
                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(DoacaoController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                request.setAttribute("listaPResultados", lista);
                requestDispatcher = request.getRequestDispatcher("/view/menu/donate/read/resultado.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/view/menu/donate/update": {
                Doacao doacao = null;
                int id = Integer.parseInt(request.getParameter("id"));
                
                try {
                    daoFactory = new DAOFactory();
                    daoDoacao = daoFactory.getDoacaoDAO();

                    doacao = daoDoacao.read(id);

                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(DoacaoController.class.getName()).log(Level.SEVERE, null, ex);
                }
                String telDoador = doacao.getTelDoador();
                request.setAttribute("telDDD", telDoador.substring(0, 2));
                request.setAttribute("tel", telDoador.substring(2));
                String celDoador = doacao.getCelDoador();
                request.setAttribute("celDDD", celDoador.substring(0, 2));
                request.setAttribute("cel", celDoador.substring(2));
                String data[] = doacao.getData().toString().split("-");
                if(data.length >= 1){
                    request.setAttribute("ano", data[0]);
                    request.setAttribute("mes", data[1]);
                    request.setAttribute("dia", data[2]);
                }
                
                float n = doacao.getDinheiro();
                int intPart = (int) n;
                float x = n - intPart;
                float decPart = x*100;
                int decPart2 = (int) decPart;
                request.setAttribute("intPart", intPart);
                request.setAttribute("decPart", decPart2);
                
                request.setAttribute("doacaoParaUpdate", doacao);
                
                requestDispatcher = request.getRequestDispatcher("/view/menu/donate/update/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/view/menu/donate/recover": {
                int id = Integer.parseInt(request.getParameter("id"));
                
                try {
                    daoFactory = new DAOFactory();
                    daoDoacao = daoFactory.getDoacaoDAO();
                    
                    daoDoacao.recover(id);
                    
                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(DoacaoController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                requestDispatcher = request.getRequestDispatcher("/view/menu/donate/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/view/menu/donate/delete": {
                
                break;
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        DAOFactory daoFactory;
        DoacaoDAO daoDoacao;
        RequestDispatcher requestDispatcher;
        
        switch(request.getServletPath()){
            case "/view/menu/donate/create": {
                Doacao doacao = new Doacao();
                
                doacao.setCelDoador(request.getParameter("celulardoadorDDD") + request.getParameter("celulardoador"));
                doacao.setTelDoador(request.getParameter("telefonedoadorDDD") + request.getParameter("telefonedoador"));
                doacao.setDoador(request.getParameter("doadorNome"));                
                doacao.setDescricao(request.getParameter("descricao"));
                
                if(request.getParameter("metodo").equals("dinheiro")){
                    doacao.setIsDinheiro(true);
                    doacao.setIsOutro(false);
                    
                    String ptInt = request.getParameter("dinheiro");
                    String ptDec = request.getParameter("dinheiroPosVirgula");
                    String dinheiroo = ptInt + "." + ptDec;
                    doacao.setDinheiro(Float.parseFloat(dinheiroo));
                    doacao.setOutro("");
                }else if(request.getParameter("metodo").equals("outro")){
                    doacao.setIsOutro(true);
                    doacao.setIsDinheiro(false);
                    
                    doacao.setOutro(request.getParameter("outro"));
                    doacao.setDinheiro(0);
                }
                
                boolean anonimo;
                String str = request.getParameter("anonimo");
                if(str==null){
                    anonimo = false;
                }else{
                    if(str.equals("on")){
                        anonimo = true;
                    }else {
                        anonimo = false;
                    }
                }
                doacao.setDoadorAnon(anonimo);

                String diaDia = request.getParameter("diaDia");
                String diaMes = request.getParameter("diaMes");
                String diaAno = request.getParameter("diaAno");
                String data = diaAno + "-" + diaMes + "-" + diaDia;
                doacao.setData(Constantes.getSQLDate(data));
                
                ///*
                try {
                    daoFactory = new DAOFactory();
                    daoDoacao = daoFactory.getDoacaoDAO();
                    
                    daoDoacao.create(doacao);
                    
                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(DoacaoController.class.getName()).log(Level.SEVERE, null, ex);
                }
                //*/
                
                requestDispatcher = request.getRequestDispatcher("/view/menu/donate/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/view/menu/donate/read": {
                
                break;
            }
            case "/view/menu/donate/update": {
                Doacao doacao = new Doacao();
                
                doacao.setCelDoador(request.getParameter("celulardoadorDDD") + request.getParameter("celulardoador"));
                doacao.setTelDoador(request.getParameter("telefonedoadorDDD") + request.getParameter("telefonedoador"));
                doacao.setDoador(request.getParameter("doadorNome"));                
                doacao.setDescricao(request.getParameter("descricao"));
                doacao.setId(Integer.parseInt(request.getParameter("id")));
                
                if(request.getParameter("metodo").equals("dinheiro")){
                    doacao.setIsDinheiro(true);
                    doacao.setIsOutro(false);
                    
                    String ptInt = request.getParameter("dinheiro");
                    String ptDec = request.getParameter("dinheiroPosVirgula");
                    String dinheiroo = ptInt + "." + ptDec;
                    doacao.setDinheiro(Float.parseFloat(dinheiroo));
                    doacao.setOutro("");
                }else if(request.getParameter("metodo").equals("outro")){
                    doacao.setIsOutro(true);
                    doacao.setIsDinheiro(false);
                    
                    doacao.setOutro(request.getParameter("outro"));
                    doacao.setDinheiro(0);
                }
                
                boolean anonimo;
                String str = request.getParameter("anonimo");
                if(str==null){
                    anonimo = false;
                }else{
                    if(str.equals("on")){
                        anonimo = true;
                    }else {
                        anonimo = false;
                    }
                }
                doacao.setDoadorAnon(anonimo);

                String diaDia = request.getParameter("diaDia");
                String diaMes = request.getParameter("diaMes");
                String diaAno = request.getParameter("diaAno");
                String data = diaAno + "-" + diaMes + "-" + diaDia;
                doacao.setData(Constantes.getSQLDate(data));
                
                ///*
                try {
                    daoFactory = new DAOFactory();
                    daoDoacao = daoFactory.getDoacaoDAO();
                    
                    daoDoacao.update(doacao);
                    
                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(DoacaoController.class.getName()).log(Level.SEVERE, null, ex);
                }
                //*/
                
                requestDispatcher = request.getRequestDispatcher("/view/menu/donate/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/view/menu/donate/delete": {
                int id = Integer.parseInt(request.getParameter("id"));
                
                try {
                    daoFactory = new DAOFactory();
                    daoDoacao = daoFactory.getDoacaoDAO();
                    
                    daoDoacao.delete(id);
                    
                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(DoacaoController.class.getName()).log(Level.SEVERE, null, ex);
                }

                requestDispatcher = request.getRequestDispatcher("/view/menu/donate/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
