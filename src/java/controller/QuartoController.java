package controller;

import dao.DAOFactory;
import dao.QuartoDAO;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Quarto;

@WebServlet(name = "QuartoController",
        urlPatterns = {
            "/view/menu/sleep/room",
            "/view/menu/sleep/room/create",
            "/view/menu/sleep/room/read",
            "/view/menu/sleep/room/update",
            "/view/menu/sleep/room/delete",
            "/view/menu/sleep/room/readBySearch",
            "/view/menu/sleep/room/readAll",
            "/view/menu/sleep/room/recover"
        })
public class QuartoController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        DAOFactory daoFactory;
        QuartoDAO daoQuarto;
        RequestDispatcher requestDispatcher;

        switch (request.getServletPath()) {
            case "/view/menu/sleep/room": {
                requestDispatcher = request.getRequestDispatcher("/view/menu/sleep/room/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/view/menu/sleep/room/create": {
                requestDispatcher = request.getRequestDispatcher("/view/menu/sleep/room/create.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/view/menu/sleep/room/readAll": {
                List<Quarto> lista = null;

                try {
                    daoFactory = new DAOFactory();
                    daoQuarto = daoFactory.getQuartoDAO();
                    
                    if(request.getParameter("trash").equals("true")){
                        lista = daoQuarto.all();
                    }else if(request.getParameter("trash").equals("false")){
                        lista = daoQuarto.allWithoutDeleted();
                    }
                    
                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(QuartoController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                request.setAttribute("listaPResultados", lista);
                requestDispatcher = request.getRequestDispatcher("/view/menu/sleep/room/read/resultado.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/view/menu/sleep/room/readBySearch": {
                Quarto quarto = null;
                
                try {
                    daoFactory = new DAOFactory();
                    daoQuarto = daoFactory.getQuartoDAO();
                    
                    int searchParam = Integer.parseInt(request.getParameter("input"));

                    if(request.getParameter("trash").equals("true")){
                        quarto = daoQuarto.read(searchParam);
                    }else if(request.getParameter("trash").equals("false")){
                        quarto = daoQuarto.readIfNotDeleted(searchParam);
                    }
                    
                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(QuartoController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                List<Quarto> lista = new ArrayList<>();
                lista.add(quarto);
                
                request.setAttribute("listaPResultados", lista);
                requestDispatcher = request.getRequestDispatcher("/view/menu/sleep/room/read/resultado.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/view/menu/sleep/room/update": {
                Quarto quarto = null;
                int id = Integer.parseInt(request.getParameter("id"));
                
                try {
                    daoFactory = new DAOFactory();
                    daoQuarto = daoFactory.getQuartoDAO();

                    quarto = daoQuarto.read(id);

                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(QuartoController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                request.setAttribute("quartoParaUpdate", quarto);
                
                requestDispatcher = request.getRequestDispatcher("/view/menu/sleep/room/update/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/view/menu/sleep/room/recover": {
                int id = Integer.parseInt(request.getParameter("id"));
                
                try {
                    daoFactory = new DAOFactory();
                    daoQuarto = daoFactory.getQuartoDAO();
                    
                    daoQuarto.recover(id);
                    
                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(QuartoController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                requestDispatcher = request.getRequestDispatcher("/view/menu/sleep/room/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/view/menu/sleep/room/delete": {

                break;
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        DAOFactory daoFactory;
        QuartoDAO daoQuarto;
        RequestDispatcher requestDispatcher;

        switch (request.getServletPath()) {
            case "/view/menu/sleep/room/create": {
                Quarto room = new Quarto();
                room.setId(Integer.parseInt(request.getParameter("id")));
                room.setCapacidade(Integer.parseInt(request.getParameter("capacidade")));

                try {
                    daoFactory = new DAOFactory();
                    daoQuarto = daoFactory.getQuartoDAO();

                    daoQuarto.create(room);

                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    request.setAttribute("error", ex.getMessage());
                    requestDispatcher = request.getRequestDispatcher("/error.jsp");
                    requestDispatcher.forward(request, response);
                    break;
                } 

                requestDispatcher = request.getRequestDispatcher("/view/menu/sleep/room/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/view/menu/sleep/room/read": {

                break;
            }
            case "/view/menu/sleep/room/update": {
                Quarto room = new Quarto();

                room.setId(Integer.parseInt(request.getParameter("id")));
                room.setCapacidade(Integer.parseInt(request.getParameter("capacidade")));
                
                boolean ocupado;
                String str = request.getParameter("ocupado");
                if(str==null){
                    ocupado = false;
                }else{
                    if(str.equals("on")){
                        ocupado = true;
                    }else {
                        ocupado = false;
                    }
                }
                room.setOcupado(ocupado);

                try {
                    daoFactory = new DAOFactory();
                    daoQuarto = daoFactory.getQuartoDAO();

                    daoQuarto.update(room);

                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(QuartoController.class.getName()).log(Level.SEVERE, null, ex);
                }

                requestDispatcher = request.getRequestDispatcher("/view/menu/sleep/room/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/view/menu/sleep/room/delete": {
                int id = Integer.parseInt(request.getParameter("id"));

                try {
                    daoFactory = new DAOFactory();
                    daoQuarto = daoFactory.getQuartoDAO();

                    daoQuarto.delete(id);

                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(QuartoController.class.getName()).log(Level.SEVERE, null, ex);
                }

                requestDispatcher = request.getRequestDispatcher("/view/menu/sleep/room/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
