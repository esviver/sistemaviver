package controller;

import dao.DAOFactory;
import dao.HospedagemDAO;
import dao.PacienteDAO;
import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Hospedagem;
import model.Paciente;

@WebServlet(name = "HospedagemController", urlPatterns = 
        {   "/view/menu/sleep",
            "/view/menu/sleep/sleeps/create",
            "/view/menu/sleep/sleeps/read",
            "/view/menu/sleep/sleeps/update",
            "/view/menu/sleep/sleeps/delete",
            "/view/menu/sleep/sleeps/readBySearch",
            "/view/menu/sleep/sleeps/readAll",
            "/view/menu/sleep/sleeps/recover"})
public class HospedagemController extends HttpServlet {
        HospedagemDAO hospedagemDao;
        DAOFactory daoFactory;
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        switch(request.getServletPath()){
            case "/view/menu/sleep/sleeps/create":
            {
            List<Paciente> pacientes =  new ArrayList();
                try {

                    daoFactory = new DAOFactory();
                    PacienteDAO pacienteDAO = daoFactory.getPacienteDAO();
                    pacientes = pacienteDAO.all();
                    daoFactory.close();
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(AtendPsicoController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(AtendPsicoController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                request.setAttribute("pacientes", pacientes);
                RequestDispatcher requestDispatcher = request.getRequestDispatcher("/view/menu/sleep/sleeps/create/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            } 
            case "/view/menu/sleep/sleeps/readBySearch": {
                List<Hospedagem> lista = null;
                System.out.println("ab");
                try {
                    daoFactory = new DAOFactory();
                    hospedagemDao = daoFactory.getHospedagemDAO();
                    
                    String searchParam = request.getParameter("input");

                    if(request.getParameter("trash").equals("true")){
                        System.out.println("com apagado");
                        lista = hospedagemDao.readByNameWithDeleted(searchParam);
                    }else if(request.getParameter("trash").equals("false")){
                        System.out.println("sem apagado");
                        lista = hospedagemDao.readByNameWithoutDeleted(searchParam);
                    }
                    
                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(DoacaoController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                if(lista.isEmpty())
                    System.out.println("Vazio");
                for (Hospedagem hospedagem : lista) {
                    System.out.println(hospedagem.getNome()+"  "+hospedagem.getData());
                    
                }
                
                request.setAttribute("listaPResultados", lista);
                
                RequestDispatcher requestDispatcher = request.getRequestDispatcher("/view/menu/sleep/sleeps/read/resultado.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/view/menu/sleep/sleeps/readAll": {
                List<Hospedagem> lista = new ArrayList<>();
                System.out.println("all");
                System.out.println(request.getParameter("trash").toString());
                try {
                    daoFactory = new DAOFactory();
                    hospedagemDao = daoFactory.getHospedagemDAO();
                    
                    
                    if(request.getParameter("trash").equals("true"))
                        lista = hospedagemDao.allArq();
                    else
                        lista = hospedagemDao.all();
                    
                daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(DoacaoController.class.getName()).log(Level.SEVERE, null, ex);
                }
                if(lista.isEmpty())
                    System.out.println("Vazio");
                for (Hospedagem hospedagem : lista) {
                    System.out.println(hospedagem.getNome()+"  "+hospedagem.getData());
                    
                }
                request.setAttribute("listaPResultados", lista);
                
                RequestDispatcher requestDispatcher = request.getRequestDispatcher("/view/menu/sleep/sleeps/read/resultado.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
                case "/view/menu/sleep/sleeps/readAllArq": {
                List<Hospedagem> lista = new ArrayList<>();
                System.out.println("all");
                try {
                    daoFactory = new DAOFactory();
                    hospedagemDao = daoFactory.getHospedagemDAO();
                    
                    String searchParam = request.getParameter("input");
                    if(request.getParameter("trash").equals("true"))
                    {
                        System.out.println("true");
                        lista = hospedagemDao.all();
                    }
                    else
                        lista = hospedagemDao.allArq();
                    
                daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(DoacaoController.class.getName()).log(Level.SEVERE, null, ex);
                }
                if(lista.isEmpty())
                    System.out.println("Vazio");
                for (Hospedagem hospedagem : lista) {
                    System.out.println(hospedagem.getNome()+"  "+hospedagem.getData());
                    
                }
                request.setAttribute("listaPResultados", lista);
                
                RequestDispatcher requestDispatcher = request.getRequestDispatcher("/view/menu/sleep/sleeps/read/resultado.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/view/menu/sleep/sleeps/delete": {
            try {
                daoFactory = new DAOFactory();
                hospedagemDao = daoFactory.getHospedagemDAO();
                hospedagemDao.arquivar(Integer.parseInt(request.getParameter("id")));
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(HospedagemController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(HospedagemController.class.getName()).log(Level.SEVERE, null, ex);
            }
                
            RequestDispatcher requestDispatcher = request.getRequestDispatcher("/view/menu/sleep/sleeps/read/index.jsp");
            requestDispatcher.forward(request, response);
            break;
            }
            case "/view/menu/sleep/sleeps/update": {
                Hospedagem hospedagem = null;
            try {
                daoFactory = new DAOFactory();
                hospedagemDao = daoFactory.getHospedagemDAO();
                hospedagem = hospedagemDao.read(Integer.parseInt(request.getParameter("id")));
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(HospedagemController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(HospedagemController.class.getName()).log(Level.SEVERE, null, ex);
            }
            request.setAttribute("ano", hospedagem.getData().toString().subSequence(0, hospedagem.getData().toString().indexOf("-")));
            request.setAttribute("mes", hospedagem.getData().toString().subSequence(hospedagem.getData().toString().indexOf("-")+1, hospedagem.getData().toString().lastIndexOf("-")));
            request.setAttribute("dia", hospedagem.getData().toString().subSequence(hospedagem.getData().toString().lastIndexOf("-")+1, hospedagem.getData().toString().length() ));
            request.setAttribute("hospedagem", hospedagem);
            RequestDispatcher requestDispatcher = request.getRequestDispatcher("/view/menu/basket/update/index.jsp");
            requestDispatcher.forward(request, response);                
                break;
            }
            case "//delete": {
                
                break;
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PacienteDAO pacienteDao;
        HospedagemDAO hospedagemDao;
        DAOFactory daoFactory;
        switch(request.getServletPath()){
            case "/view/menu/sleep/sleeps/create":
            {
                Hospedagem hospedagem = new Hospedagem();
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-mm-dd");
                
            try {
                daoFactory = new DAOFactory();
                
                hospedagem.setIdpaciente(Integer.parseInt(request.getParameter("idPaciente")));
                hospedagem.setAnotacoes(request.getParameter("anotacoes"));
                hospedagem.setData((Date) formatter.parse(request.getParameter("ano")+"-"+request.getParameter("mes")+"-"+request.getParameter("dia")));
                hospedagem.setQuarto(Integer.parseInt(request.getParameter("quarto")));
                hospedagemDao = daoFactory.getHospedagemDAO();
                hospedagemDao.create(hospedagem);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(HospedagemController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(HospedagemController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ParseException ex) {
                Logger.getLogger(HospedagemController.class.getName()).log(Level.SEVERE, null, ex);
            }
                RequestDispatcher requestDispatcher = request.getRequestDispatcher("/view/menu/sleep/sleeps/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            } 
            case "/view/menu/sleep/sleeps/update": {
                Hospedagem hospedagem = new Hospedagem();                
            try {
                daoFactory = new DAOFactory();
                
                hospedagem.setIdpaciente(Integer.parseInt(request.getParameter("idPaciente")));
                hospedagem.setId(Integer.parseInt(request.getParameter("idH")));
                hospedagem.setAnotacoes(request.getParameter("anotacoes"));
                hospedagem.setData(new java.sql.Date(Integer.parseInt(request.getParameter("ano")),Integer.parseInt(request.getParameter("mes")),Integer.parseInt(request.getParameter("dia"))));
                hospedagem.setQuarto(Integer.parseInt(request.getParameter("quarto")));
                
                hospedagemDao = daoFactory.getHospedagemDAO();
                hospedagemDao.update(hospedagem);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(HospedagemController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(HospedagemController.class.getName()).log(Level.SEVERE, null, ex);
            }
                RequestDispatcher requestDispatcher = request.getRequestDispatcher("/view/menu/sleep/sleeps/index.jsp");
                requestDispatcher.forward(request, response);                
                
                break;
            }
            case "//read": {
                
                break;
            }
            case "//update": {
                
                break;
            }
            case "//delete": {
                
                break;
            }
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
