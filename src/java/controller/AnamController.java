package controller;

import dao.AnamDAO;
import dao.DAOFactory;
import dao.PacienteDAO;
import dao.PsicologaDAO;
import global.Constantes;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Anam;
import model.Atendimento;
import model.Paciente;
import model.Psicologa;
import model.Usuario;

@WebServlet(name = "AnamController", 
        urlPatterns = {
            "/AnamController",
            "/view/menu/agendamentos/anamnese",
            "/view/menu/agendamentos/anamnese/create",
            "/view/menu/agendamentos/anamnese/read",
            "/view/menu/agendamentos/anamnese/readAll",
            "/view/menu/agendamentos/anamnese/readBySearch"
        
        })
public class AnamController extends HttpServlet {
     DAOFactory daoFactory;
     AnamDAO anamDAO;
     PacienteDAO pacienteDAO;
     RequestDispatcher requestDispatcher;
      PsicologaDAO psicologaDAO;
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        switch(request.getServletPath()){
            case "/view/menu/agendamentos/anamnese":{
                requestDispatcher = request.getRequestDispatcher("/view/menu/agendamentos/anamnese/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            } 
            
            case "/view/menu/agendamentos/anamnese/create": {
                List<Paciente> pacientes =  new ArrayList();
                try {

                    daoFactory = new DAOFactory();
                    PacienteDAO pacienteDAO = daoFactory.getPacienteDAO();
                    pacientes = pacienteDAO.all();

                    daoFactory.close();
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(AtendPsicoController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(AtendPsicoController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                request.setAttribute("pacientes", pacientes);
                
                requestDispatcher = request.getRequestDispatcher("/view/menu/agendamentos/anamnese/create/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/view/menu/agendamentos/anamnese/read": {
                 Anam anam = null;
                 Paciente paciente = null;
                 Atendimento atendimento = new Atendimento();
             try {
               
                daoFactory = new DAOFactory();
                anamDAO = daoFactory.getAnamDAO();
                pacienteDAO = daoFactory.getPacienteDAO();
                 int idPaciente = Integer.parseInt(request.getParameter("idPaciente"));
                 int idAnam = Integer.parseInt(request.getParameter("idAnam"));
                 
                anam =  anamDAO.read(idAnam);
                paciente = pacienteDAO.read(idPaciente);
                
                atendimento.setAnam(anam);
                atendimento.setPaciente(paciente);
                
                daoFactory.close();
                
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(AnamController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(AnamController.class.getName()).log(Level.SEVERE, null, ex);
            }
                request.setAttribute("atendimento",atendimento);
                requestDispatcher = request.getRequestDispatcher("/view/menu/agendamentos/anamnese/read/visualizar.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case"/view/menu/agendamentos/anamnese/readAll":{
                List<Anam> anamneses = null;
                Paciente paciente = null;
                Atendimento atendimento = null;
                List<Atendimento> atendimentos = new ArrayList<>();
            try {
                daoFactory = new DAOFactory();
                anamDAO = daoFactory.getAnamDAO();
                pacienteDAO = daoFactory.getPacienteDAO();
                
                anamneses = anamDAO.all();
                for(int i = 0; i < anamneses.size(); i++){
                    atendimento = new Atendimento();
                    int idPaciente = anamneses.get(i).getIdPaciente();
                    paciente = pacienteDAO.read(idPaciente);
                    atendimento.setPaciente(paciente);
                    atendimento.setAnam(anamneses.get(i));
                    
                    atendimentos.add(atendimento);
                }
                
                daoFactory.close();
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(AnamController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(AnamController.class.getName()).log(Level.SEVERE, null, ex);
            }   
                request.setAttribute("atendimentos", atendimentos);
                requestDispatcher = request.getRequestDispatcher("/view/menu/agendamentos/anamnese/read/resultado.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/view/menu/agendamentos/anamnese/readBySearch":{
                System.out.println("dsfskdfhkj");
                List<Atendimento> atendimentos = new ArrayList<>();
                List<Anam> anamneses = null;
                List<Paciente> pacientes = null;
                Atendimento atendimento = null;
            try {
                daoFactory = new DAOFactory();
                pacienteDAO = daoFactory.getPacienteDAO();
                anamDAO = daoFactory.getAnamDAO();
                
                String searchParam = request.getParameter("input");
                
                pacientes = pacienteDAO.readByName(searchParam);
                
                for(Paciente paciente: pacientes){
                    anamneses = anamDAO.readByidPaciente(paciente.getId());
                    for(Anam anam : anamneses){
                        atendimento = new Atendimento();
                        atendimento.setPaciente(paciente);
                        atendimento.setAnam(anam);
                        atendimentos.add(atendimento);
                    }
                }
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(AnamController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(AnamController.class.getName()).log(Level.SEVERE, null, ex);
            }
                request.setAttribute("atendimentos", atendimentos);
                requestDispatcher = request.getRequestDispatcher("/view/menu/agendamentos/anamnese/read/resultado.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "//update": {
                
                break;
            }
            case "//delete": {
                
                break;
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
         RequestDispatcher requestDispatcher;
        
        switch(request.getServletPath()){
            case "/":{} 
            
            case "/view/menu/agendamentos/anamnese/create": {
            try {
                daoFactory =  new DAOFactory();
                anamDAO = daoFactory.getAnamDAO();
                psicologaDAO = daoFactory.getPsicologaDAO();
                HttpSession sessao = request.getSession(true);
                Usuario usuario = (Usuario) sessao.getAttribute("usuario");
                Psicologa psico = psicologaDAO.read(usuario.getId());
                Anam anam = new Anam();
                anam.setIdpsicologa(psico.getIdP());
                anam.setIdPaciente(Integer.parseInt(request.getParameter("idPaciente")));
                anam.setEncaminhamento(request.getParameter("encaminhamento"));
                anam.setDiagnostico(request.getParameter("diagnostico"));
                anam.setMedico(request.getParameter("medico"));
                anam.setHistoria(request.getParameter("historia"));
                anam.setHistoricoClin(request.getParameter("historico"));
                anam.setQueixa(request.getParameter("queixa"));
                anam.setMotivosImpExp(request.getParameter("motivos"));
                anam.setFormulacaoHipotese(request.getParameter("hipoteses"));
                anam.setPlanoTerap(request.getParameter("planoterap"));
                anam.setData(Constantes.getTodaysDate());
                anam.setInfEAdos(request.getParameter("infaciaadolescencia"));
                anam.setAspecPsico(request.getParameter("aspectospsicologicos"));
                anam.setEstrutFamiliar(request.getParameter("estrutFamiliar"));
                anam.setDadosPsicoSociais(request.getParameter("dadosPsicoSociais"));
                anam.setReligiao(request.getParameter("religiao"));
                
                anamDAO.create(anam);  
                
                daoFactory.close();
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(AnamController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(AnamController.class.getName()).log(Level.SEVERE, null, ex);
            }
                requestDispatcher = request.getRequestDispatcher("/view/menu/agendamentos/anamnese/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "//read": {
                
                break;
            }
            case "//update": {
                
                break;
            }
            case "//delete": {
                
                break;
            }
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
