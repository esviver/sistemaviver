package controller;

import dao.DAOFactory;
import dao.EnderecoDAO;
import dao.PacienteDAO;
import dao.ResponsavelDAO;
import global.Constantes;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Endereco;
import model.Paciente;
import model.Responsavel;

@WebServlet(name = "PacienteController", 
        urlPatterns = {
            "/view/menu/patient",
            "/view/menu/patient/create",
            "/view/menu/patient/read",
            "/view/menu/patient/update",
            "/view/menu/patient/delete",
            "/view/menu/patient/readBySearch",
            "/view/menu/patient/readAll",
            "/view/menu/patient/recover"
        })
public class PacienteController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        DAOFactory daoFactory;
        PacienteDAO daoPaciente;
        EnderecoDAO daoEndereco;
        ResponsavelDAO daoResponsavel;
        RequestDispatcher requestDispatcher;
        
        switch(request.getServletPath()){
            case "/view/menu/patient":{
                requestDispatcher = request.getRequestDispatcher("/view/menu/patient/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/view/menu/patient/create": {
                requestDispatcher = request.getRequestDispatcher("/view/menu/patient/create/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/view/menu/patient/readAll": {
                List<Paciente> lista = null;

                try {
                    daoFactory = new DAOFactory();
                    daoPaciente = daoFactory.getPacienteDAO();
                    
                    if(request.getParameter("trash").equals("true")){
                        lista = daoPaciente.all();
                    }else if(request.getParameter("trash").equals("false")){
                        lista = daoPaciente.allWithoutDeleted();
                    }
                    
                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(PacienteController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                request.setAttribute("listaPResultados", lista);
                requestDispatcher = request.getRequestDispatcher("/view/menu/patient/read/resultado.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/view/menu/patient/readBySearch": {
                List<Paciente> lista = null;
                
                try {
                    daoFactory = new DAOFactory();
                    daoPaciente = daoFactory.getPacienteDAO();
                    
                    String searchParam = request.getParameter("input");

                    if(request.getParameter("trash").equals("true")){
                        lista = daoPaciente.readByName(searchParam);
                    }else if(request.getParameter("trash").equals("false")){
                        lista = daoPaciente.readByNameWithoutDeleted(searchParam);
                    }
                    
                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(PacienteController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                request.setAttribute("listaPResultados", lista);
                requestDispatcher = request.getRequestDispatcher("/view/menu/patient/read/resultado.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/view/menu/patient/update": {
                Paciente pac = null;
                int id = Integer.parseInt(request.getParameter("id"));
                
                try {
                    daoFactory = new DAOFactory();
                    daoPaciente = daoFactory.getPacienteDAO();

                    pac = daoPaciente.read(id);

                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(PacienteController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                request.setAttribute("pacienteParaUpdate", pac);
                
                String data[] = pac.getDataNasc().toString().split("-");
                if(data.length >= 1){
                    request.setAttribute("pacDtNascAno", data[0]);
                    request.setAttribute("pacDtNascMes", data[1]);
                    request.setAttribute("pacDtNascDia", data[2]);
                }
                float n, x, decPart;
                int intPart, decPart2;
                n = pac.getAgua();
                intPart = (int) n;
                x = n - intPart;
                decPart = x*100;
                decPart2 = (int) decPart;
                request.setAttribute("aguaInt", intPart);
                request.setAttribute("aguaDec", decPart2);
                n = pac.getAlimentacao();
                intPart = (int) n;
                x = n - intPart;
                decPart = x*100;
                decPart2 = (int) decPart;
                request.setAttribute("alimentacaoInt", intPart);
                request.setAttribute("alimentacaoDec", decPart2);
                n = pac.getAluguel();
                intPart = (int) n;
                x = n - intPart;
                decPart = x*100;
                decPart2 = (int) decPart;
                request.setAttribute("aluguelInt", intPart);
                request.setAttribute("aluguelDec", decPart2);
                n = pac.getEnergia();
                intPart = (int) n;
                x = n - intPart;
                decPart = x*100;
                decPart2 = (int) decPart;
                request.setAttribute("energiaInt", intPart);
                request.setAttribute("energiaDec", decPart2);
                n = pac.getGas();
                intPart = (int) n;
                x = n - intPart;
                decPart = x*100;
                decPart2 = (int) decPart;
                request.setAttribute("gasInt", intPart);
                request.setAttribute("gasDec", decPart2);
                n = pac.getMedicamentos();
                intPart = (int) n;
                x = n - intPart;
                decPart = x*100;
                decPart2 = (int) decPart;
                request.setAttribute("medicamentosInt", intPart);
                request.setAttribute("medicamentosDec", decPart2);
                n = pac.getRendaMensal();
                intPart = (int) n;
                x = n - intPart;
                decPart = x*100;
                decPart2 = (int) decPart;
                request.setAttribute("rendaMensalInt", intPart);
                request.setAttribute("rendaMensalDec", decPart2);
                n = pac.getTransporte();
                intPart = (int) n;
                x = n - intPart;
                decPart = x*100;
                decPart2 = (int) decPart;
                request.setAttribute("transporteInt", intPart);
                request.setAttribute("transporteDec", decPart2);
                
                // por valor
                //moradia
                //material
                //banheiro

                // boolean, ou true ou false
                //esgoto
                //asfalto
                //creche
                //centro comunitario
                //bolsa familia
                //auxilio gas
                //luz fraterna
                //religiao contribui com a familia
                //--------------------------------------------------------------------------------
                Responsavel resp = null;
                try {
                    daoFactory = new DAOFactory();
                    daoResponsavel = daoFactory.getResponsavelDAO();
                    
                    resp = daoResponsavel.readByIdPac(pac.getId());
                    
                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(PacienteController.class.getName()).log(Level.SEVERE, null, ex);
                }
                request.setAttribute("pacResponsavelParaUpdate", resp);
                
                data = resp.getDatanasc().toString().split("-");
                if(data.length >= 1){
                    request.setAttribute("respAno", data[0]);
                    request.setAttribute("respMes", data[1]);
                    request.setAttribute("respDia", data[2]);
                }
                //--------------------------------------------------------------------------------
                Endereco endereco = null;
                Endereco enderecoEscola = null;
                try {
                    daoFactory = new DAOFactory();
                    daoEndereco = daoFactory.getEnderecoDAO();
                    
                    endereco = daoEndereco.read(pac.getId_end());
                    enderecoEscola = daoEndereco.read(pac.getId_end_escola());
                    
                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(PacienteController.class.getName()).log(Level.SEVERE, null, ex);
                }
                request.setAttribute("pacEnderecoParaUpdate", endereco);
                request.setAttribute("pacEnderecoEscolaParaUpdate", enderecoEscola);
                //--------------------------------------------------------------------------------
                requestDispatcher = request.getRequestDispatcher("/view/menu/patient/update/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/view/menu/patient/recover": {
                int id = Integer.parseInt(request.getParameter("id"));
                
                try {
                    daoFactory = new DAOFactory();
                    daoPaciente = daoFactory.getPacienteDAO();
                    
                    daoPaciente.recover(id);
                    
                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(PacienteController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                requestDispatcher = request.getRequestDispatcher("/view/menu/patient/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        DAOFactory daoFactory;
        PacienteDAO daoPaciente;
        EnderecoDAO daoEndereco;
        ResponsavelDAO daoResponsavel;
        RequestDispatcher requestDispatcher;
        
        switch(request.getServletPath()){
            case "/view/menu/patient/create": {
                Endereco endereco = new Endereco();
                
                endereco.setBairro(request.getParameter("bairro")); //OK
                endereco.setCep(request.getParameter("cep")); //OK
                endereco.setCidade(request.getParameter("cidade")); //OK
                endereco.setEstado(request.getParameter("estado")); //OK
                endereco.setNumero(Integer.parseInt(request.getParameter("numero"))); //OK
                endereco.setLogradouro(request.getParameter("logradouro")); //OK
                endereco.setPontoreferencia(request.getParameter("pontoreferencia")); //OK
                
                try {
                    daoFactory = new DAOFactory();
                    daoEndereco = daoFactory.getEnderecoDAO();
                    
                    daoEndereco.create(endereco);
                    
                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(PacienteController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                Endereco enderecoEscola = new Endereco();
                
                enderecoEscola.setBairro(request.getParameter("bairroEscola"));
                enderecoEscola.setEstado(request.getParameter("estado"));
                enderecoEscola.setNumero(Integer.parseInt(request.getParameter("numEscola")));
                enderecoEscola.setLogradouro(request.getParameter("endEscola"));
                
                try {
                    daoFactory = new DAOFactory();
                    daoEndereco = daoFactory.getEnderecoDAO();
                    
                    daoEndereco.create(enderecoEscola);
                    
                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(PacienteController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                Paciente pac = new Paciente();
                
                String ptInt, ptDec, dinheiroo;
                
                ptInt = request.getParameter("aguaInt"); //OK
                ptDec = request.getParameter("aguaDec"); //OK
                dinheiroo = ptInt + "." + ptDec;
                pac.setAgua(Float.parseFloat(dinheiroo));
                ptInt = request.getParameter("alimentacaoInt"); //OK
                ptDec = request.getParameter("alimentacaoDec"); //OK
                dinheiroo = ptInt + "." + ptDec;
                pac.setAlimentacao(Float.parseFloat(dinheiroo));
                ptInt = request.getParameter("aluguelInt"); //OK
                ptDec = request.getParameter("aluguelDec"); //OK
                dinheiroo = ptInt + "." + ptDec;
                pac.setAluguel(Float.parseFloat(dinheiroo));
                ptInt = request.getParameter("energiaInt"); //OK
                ptDec = request.getParameter("energiaDec"); //OK
                dinheiroo = ptInt + "." + ptDec;
                pac.setEnergia(Float.parseFloat(dinheiroo));
                ptInt = request.getParameter("gasInt"); //OK
                ptDec = request.getParameter("gasDec"); //OK
                dinheiroo = ptInt + "." + ptDec;
                pac.setGas(Float.parseFloat(dinheiroo));
                ptInt = request.getParameter("transporteInt"); //OK
                ptDec = request.getParameter("transporteDec"); //OK
                dinheiroo = ptInt + "." + ptDec;
                pac.setTransporte(Float.parseFloat(dinheiroo));
                ptInt = request.getParameter("medicamentosInt"); //OK
                ptDec = request.getParameter("medicamentosDec"); //OK
                dinheiroo = ptInt + "." + ptDec;
                pac.setMedicamentos(Float.parseFloat(dinheiroo));
                ptInt = request.getParameter("rendaMensalInt"); //OK
                ptDec = request.getParameter("rendaMensalDec"); //OK
                dinheiroo = ptInt + "." + ptDec;
                pac.setRendaMensal(Float.parseFloat(dinheiroo)); //OK
                
                pac.setBanheiro(request.getParameter("banheiro")); //OK
                pac.setId_end_escola(enderecoEscola.getId()); //OK
                pac.setId_end(endereco.getId()); //OK
                pac.setReligiaoCont(Boolean.parseBoolean(request.getParameter("contribui_familia"))); //OK
                pac.setComodos(Integer.parseInt(request.getParameter("comodos"))); //OK
                
                pac.setAuxilioGas(Boolean.parseBoolean(request.getParameter("auxilio_gas"))); //OK
                pac.setBolsaFam(Boolean.parseBoolean(request.getParameter("bolsa_familia"))); //OK
                pac.setCentroCom(Boolean.parseBoolean(request.getParameter("centro_comunitario"))); //OK
                pac.setCreche(Boolean.parseBoolean(request.getParameter("creche"))); //OK
                pac.setLuzFraterna(Boolean.parseBoolean(request.getParameter("luz_fraterna"))); //OK
                
                pac.setHistPatologia(request.getParameter("histPatologia")); //OK
                pac.setHistoricoFamiliar(request.getParameter("historicoFamiliar")); //OK
                pac.setLocalNasc(request.getParameter("localNasc")); //OK
                pac.setNsus(request.getParameter("nsus")); //OK
                pac.setPatologia(request.getParameter("patologia")); //OK
                pac.setMaterial(request.getParameter("material")); //OK
                pac.setMoradia(request.getParameter("moradia")); //OK
                pac.setPNome(request.getParameter("nomePaciente")); //OK
                pac.setSnome(request.getParameter("sobrenomePaciente")); //OK
                pac.setNomeEscola(request.getParameter("nomeEscola")); //OK
                pac.setSerie(Integer.parseInt(request.getParameter("serie"))); //OK
                pac.setOutroAux(request.getParameter("outroAux")); //OK
                pac.setApoioFam(request.getParameter("apoioFam")); //OK
                pac.setReligiao(request.getParameter("religiao")); //OK
                
                pac.setAsfalto(Boolean.parseBoolean(request.getParameter("asfalto"))); //OK
                pac.setEsgoto(Boolean.parseBoolean(request.getParameter("esgoto"))); //OK
                pac.setObito(false);
                pac.setSituacao(true);
                pac.setAlta(false);
                String diaDia2, diaMes2, diaAno2, data2;
                diaDia2 = request.getParameter("dataNascPacDia"); //OK
                diaMes2 = request.getParameter("dataNascPacMes"); //OK
                diaAno2 = request.getParameter("dataNascPacAno"); //OK
                data2 = diaAno2 + "-" + diaMes2 + "-" + diaDia2;
                pac.setDataNasc(Constantes.getSQLDate(data2));
                pac.setEntrada(Constantes.getTodaysDate()); //OK
                
                try {
                    daoFactory = new DAOFactory();
                    daoPaciente = daoFactory.getPacienteDAO();
                    
                    daoPaciente.create(pac);
                    
                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    request.setAttribute("error", ex.getMessage());
                    requestDispatcher = request.getRequestDispatcher("/error.jsp");
                    requestDispatcher.forward(request, response);
                    break;
                }
                
                
                Responsavel r = new Responsavel();
                
                r.setCpf(request.getParameter("cpf")); //OK
                r.setEscolaridade(request.getParameter("escolaridade")); //OK
                r.setEstadoCivil(request.getParameter("estadocivil")); //OK
                r.setPnomeResp(request.getParameter("pnomeResp")); //OK
                r.setSnomeResp(request.getParameter("snomeResp")); //OK
                r.setProfissao(request.getParameter("profissao")); //OK
                r.setRgNum(request.getParameter("rgnum")); //OK
                r.setTelefone(request.getParameter("telefone")); //OK
                r.setIdEndereco(endereco.getId()); //OK
                r.setId_pac(pac.getId());
                
                String diaDia = request.getParameter("datanascDia"); //OK
                String diaMes = request.getParameter("datanascMes"); //OK
                String diaAno = request.getParameter("datanascAno"); //OK
                String data = diaAno + "-" + diaMes + "-" + diaDia;
                r.setDatanasc(Constantes.getSQLDate(data));
                
                try {
                    daoFactory = new DAOFactory();
                    daoResponsavel = daoFactory.getResponsavelDAO();
                    
                    daoResponsavel.create(r);
                    
                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(PacienteController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                
                requestDispatcher = request.getRequestDispatcher("/view/menu/patient/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/view/menu/patient/read": {
                
                break;
            }
            case "/view/menu/patient/update": {
                Endereco endereco = new Endereco();
                
                endereco.setBairro(request.getParameter("bairro")); //OK
                endereco.setCep(request.getParameter("cep")); //OK
                endereco.setCidade(request.getParameter("cidade")); //OK
                endereco.setEstado(request.getParameter("estado")); //OK
                endereco.setNumero(Integer.parseInt(request.getParameter("numero"))); //OK
                endereco.setLogradouro(request.getParameter("logradouro")); //OK
                endereco.setPontoreferencia(request.getParameter("pontoreferencia")); //OK
                
                try {
                    daoFactory = new DAOFactory();
                    daoEndereco = daoFactory.getEnderecoDAO();
                    
                    daoEndereco.update(endereco);
                    
                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(PacienteController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                Endereco enderecoEscola = new Endereco();
                
                enderecoEscola.setBairro(request.getParameter("bairroEscola"));
                enderecoEscola.setEstado(request.getParameter("estado"));
                enderecoEscola.setNumero(Integer.parseInt(request.getParameter("numEscola")));
                enderecoEscola.setLogradouro(request.getParameter("endEscola"));
                
                try {
                    daoFactory = new DAOFactory();
                    daoEndereco = daoFactory.getEnderecoDAO();
                    
                    daoEndereco.update(enderecoEscola);
                    
                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(PacienteController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                Paciente pac = new Paciente();
                pac.setId(Integer.parseInt(request.getParameter("id")));
                
                String ptInt, ptDec, dinheiroo;
                
                ptInt = request.getParameter("aguaInt"); //OK
                ptDec = request.getParameter("aguaDec"); //OK
                dinheiroo = ptInt + "." + ptDec;
                pac.setAgua(Float.parseFloat(dinheiroo));
                ptInt = request.getParameter("alimentacaoInt"); //OK
                ptDec = request.getParameter("alimentacaoDec"); //OK
                dinheiroo = ptInt + "." + ptDec;
                pac.setAlimentacao(Float.parseFloat(dinheiroo));
                ptInt = request.getParameter("aluguelInt"); //OK
                ptDec = request.getParameter("aluguelDec"); //OK
                dinheiroo = ptInt + "." + ptDec;
                pac.setAluguel(Float.parseFloat(dinheiroo));
                ptInt = request.getParameter("energiaInt"); //OK
                ptDec = request.getParameter("energiaDec"); //OK
                dinheiroo = ptInt + "." + ptDec;
                pac.setEnergia(Float.parseFloat(dinheiroo));
                ptInt = request.getParameter("gasInt"); //OK
                ptDec = request.getParameter("gasDec"); //OK
                dinheiroo = ptInt + "." + ptDec;
                pac.setGas(Float.parseFloat(dinheiroo));
                ptInt = request.getParameter("transporteInt"); //OK
                ptDec = request.getParameter("transporteDec"); //OK
                dinheiroo = ptInt + "." + ptDec;
                pac.setTransporte(Float.parseFloat(dinheiroo));
                ptInt = request.getParameter("medicamentosInt"); //OK
                ptDec = request.getParameter("medicamentosDec"); //OK
                dinheiroo = ptInt + "." + ptDec;
                pac.setMedicamentos(Float.parseFloat(dinheiroo));
                ptInt = request.getParameter("rendaMensalInt"); //OK
                ptDec = request.getParameter("rendaMensalDec"); //OK
                dinheiroo = ptInt + "." + ptDec;
                pac.setRendaMensal(Float.parseFloat(dinheiroo)); //OK
                
                pac.setBanheiro(request.getParameter("banheiro")); //OK
                pac.setId_end_escola(enderecoEscola.getId()); //OK
                pac.setId_end(endereco.getId()); //OK
                pac.setReligiaoCont(Boolean.parseBoolean(request.getParameter("contribui_familia"))); //OK
                pac.setComodos(Integer.parseInt(request.getParameter("comodos"))); //OK
                
                pac.setAuxilioGas(Boolean.parseBoolean(request.getParameter("auxilio_gas"))); //OK
                pac.setBolsaFam(Boolean.parseBoolean(request.getParameter("bolsa_familia"))); //OK
                pac.setCentroCom(Boolean.parseBoolean(request.getParameter("centro_comunitario"))); //OK
                pac.setCreche(Boolean.parseBoolean(request.getParameter("creche"))); //OK
                pac.setLuzFraterna(Boolean.parseBoolean(request.getParameter("luz_fraterna"))); //OK
                
                pac.setHistPatologia(request.getParameter("histPatologia")); //OK
                pac.setHistoricoFamiliar(request.getParameter("historicoFamiliar")); //OK
                pac.setLocalNasc(request.getParameter("localNasc")); //OK
                pac.setNsus(request.getParameter("nsus")); //OK
                pac.setPatologia(request.getParameter("patologia")); //OK
                pac.setMaterial(request.getParameter("material")); //OK
                pac.setMoradia(request.getParameter("moradia")); //OK
                pac.setPNome(request.getParameter("nomePaciente")); //OK
                pac.setSnome(request.getParameter("sobrenomePaciente")); //OK
                pac.setNomeEscola(request.getParameter("nomeEscola")); //OK
                pac.setSerie(Integer.parseInt(request.getParameter("serie"))); //OK
                pac.setOutroAux(request.getParameter("outroAux")); //OK
                pac.setApoioFam(request.getParameter("apoioFam")); //OK
                pac.setReligiao(request.getParameter("religiao")); //OK
                
                pac.setAsfalto(Boolean.parseBoolean(request.getParameter("asfalto"))); //OK
                pac.setEsgoto(Boolean.parseBoolean(request.getParameter("esgoto"))); //OK
                pac.setObito(false);
                pac.setSituacao(true);
                pac.setAlta(false);
                String diaDia2, diaMes2, diaAno2, data2;
                diaDia2 = request.getParameter("dataNascPacDia"); //OK
                diaMes2 = request.getParameter("dataNascPacMes"); //OK
                diaAno2 = request.getParameter("dataNascPacAno"); //OK
                data2 = diaAno2 + "-" + diaMes2 + "-" + diaDia2;
                pac.setDataNasc(Constantes.getSQLDate(data2));
                pac.setEntrada(Constantes.getTodaysDate()); //OK
                
                try {
                    daoFactory = new DAOFactory();
                    daoPaciente = daoFactory.getPacienteDAO();
                    
                    daoPaciente.update(pac);
                    
                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(PacienteController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                
                Responsavel r = new Responsavel();
                
                r.setCpf(request.getParameter("cpf")); //OK
                r.setEscolaridade(request.getParameter("escolaridade")); //OK
                r.setEstadoCivil(request.getParameter("estadocivil")); //OK
                r.setPnomeResp(request.getParameter("pnomeResp")); //OK
                r.setSnomeResp(request.getParameter("snomeResp")); //OK
                r.setProfissao(request.getParameter("profissao")); //OK
                r.setRgNum(request.getParameter("rgnum")); //OK
                r.setTelefone(request.getParameter("telefone")); //OK
                r.setIdEndereco(endereco.getId()); //OK
                r.setId_pac(pac.getId());
                
                String diaDia = request.getParameter("datanascDia"); //OK
                String diaMes = request.getParameter("datanascMes"); //OK
                String diaAno = request.getParameter("datanascAno"); //OK
                String data = diaAno + "-" + diaMes + "-" + diaDia;
                r.setDatanasc(Constantes.getSQLDate(data));
                
                try {
                    daoFactory = new DAOFactory();
                    daoResponsavel = daoFactory.getResponsavelDAO();
                    
                    daoResponsavel.update(r);
                    
                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(PacienteController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                
                requestDispatcher = request.getRequestDispatcher("/view/menu/patient/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/view/menu/patient/delete": {
                int id = Integer.parseInt(request.getParameter("id"));
                
                try {
                    daoFactory = new DAOFactory();
                    daoPaciente = daoFactory.getPacienteDAO();
                    
                    daoPaciente.delete(id);
                    
                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(PacienteController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                requestDispatcher = request.getRequestDispatcher("/view/menu/patient/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
