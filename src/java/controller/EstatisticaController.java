/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.AnamDAO;
import dao.AtendimentoAssDAO;
import dao.AtendimentoPsicoDAO;
import dao.DAOFactory;
import dao.DistribuicaoDAO;
import dao.DoacaoDAO;
import dao.HospedagemDAO;
import dao.PacienteDAO;
import dao.VoluntarioDAO;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
@WebServlet(name = "EstatisticaController", 
        urlPatterns = {
            "/view/menu/report",
            "/view/menu/report/getReportAll"
        })
/**
 *
 * @author Auxiliar
 */
public class EstatisticaController extends HttpServlet{

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        DAOFactory daoFactory;

        
        
        switch(request.getServletPath())
        {
            case "/view/menu/report":
            {
            try {
                daoFactory = new DAOFactory();
                request.setAttribute("pacientes", daoFactory.getPacienteDAO().contar());
                request.setAttribute("voluntarios", daoFactory.getVoluntarioDAO().contar());
                request.setAttribute("hospedagens", daoFactory.getHospedagemDAO().contar());
                request.setAttribute("atendimentoPS", daoFactory.getAtendimentoPsicoDAO().contar());
                request.setAttribute("atendimentoASS", daoFactory.getAtendimentoAssDAO().contar());
                request.setAttribute("anamnese", daoFactory.getAnamDAO().contar());
                request.setAttribute("doacoes", daoFactory.getDoacaoDAO().contar());
                request.setAttribute("distribuicoes", daoFactory.getDistribuicaoDAO().contar());
                RequestDispatcher requestDispatcher = request.getRequestDispatcher("/view/menu/report/index.jsp");
                requestDispatcher.forward(request, response);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(EstatisticaController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(EstatisticaController.class.getName()).log(Level.SEVERE, null, ex);
            }
                
                break;
            }
            case "/view/menu/report/getReportAll":
            {
            try {
                daoFactory = new DAOFactory();
                request.setAttribute("distribuicoes", daoFactory.getDistribuicaoDAO().contar());
                RequestDispatcher requestDispatcher = request.getRequestDispatcher("/view/menu/report/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(EstatisticaController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(EstatisticaController.class.getName()).log(Level.SEVERE, null, ex);
            }

            }
        }    
    }
     @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        switch(request.getServletPath())
        {
            
        }
    }
}
