package controller;

import dao.DAOFactory;
import dao.RelatorioVoluntDAO;
import dao.VoluntarioDAO;
import global.Constantes;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.RelatorioVolunt;
import model.Usuario;

@WebServlet(name = "RelatorioVoluntController", 
        urlPatterns = {
            "/view/menu/volunteer",
            "/view/menu/volunteer/create",
            "/view/menu/volunteer/read",
            "/view/menu/volunteer/update",
            "/view/menu/volunteer/delete",
            "/view/menu/volunteer/readBySearch",
            "/view/menu/volunteer/readAll",
            "/view/menu/volunteer/recover"
        })
public class RelatorioVoluntController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                
        DAOFactory daoFactory;
        RelatorioVoluntDAO daoRelatorio;
        VoluntarioDAO daoVoluntario;
        RequestDispatcher requestDispatcher;

        switch (request.getServletPath()) {
            case "/view/menu/volunteer": {
                requestDispatcher = request.getRequestDispatcher("/view/menu/volunteer/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/view/menu/volunteer/create": {
                requestDispatcher = request.getRequestDispatcher("/view/menu/volunteer/create/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/view/menu/volunteer/readAll": {
                List<RelatorioVolunt> lista = null;

                try {
                    daoFactory = new DAOFactory();
                    daoRelatorio = daoFactory.getRelatorioVoluntDAO();
                    
                    boolean optTrash = false;
                    if(request.getParameter("trash").equals("true")){
                        optTrash = true;
                    }else if(request.getParameter("trash").equals("false")){
                        optTrash = false;
                    }
                    
                    switch (request.getParameter("opcao")) {
                        case "nao_analisados": {
                            lista = daoRelatorio.notAnalysedReportsWithVolunteerName(optTrash);
                            break;
                        }
                        case "aprovados": {
                            lista = daoRelatorio.aprovadoReportsWithVolunteerName(optTrash);
                            break;
                        }
                        case "reprovados": {
                            lista = daoRelatorio.reprovadoReportsWithVolunteerName(optTrash);
                            break;
                        }
                        case "todos": {
                            lista = daoRelatorio.allReportsWithVolunteerName(optTrash);
                            break;
                        }
                    }
                    
                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(RelatorioVoluntController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                request.setAttribute("listaPResultados", lista);
                requestDispatcher = request.getRequestDispatcher("/view/menu/volunteer/read/resultado.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/view/menu/volunteer/readBySearch": {
                List<RelatorioVolunt> lista = null;
                
                try {
                    daoFactory = new DAOFactory();
                    daoRelatorio = daoFactory.getRelatorioVoluntDAO();
                    
                    String searchParam = request.getParameter("input");

                    if(request.getParameter("trash").equals("true")){
                        lista = daoRelatorio.readByNameVolunt(searchParam);
                    }else if(request.getParameter("trash").equals("false")){
                        lista = daoRelatorio.readByNameVoluntWithoutDeleted(searchParam);
                    }
                    
                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(RelatorioVoluntController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                request.setAttribute("listaPResultados", lista);
                requestDispatcher = request.getRequestDispatcher("/view/menu/volunteer/read/resultado.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/view/menu/volunteer/update": {
                RelatorioVolunt relatorioV = null;
                int id = Integer.parseInt(request.getParameter("id"));
                
                try {
                    daoFactory = new DAOFactory();
                    daoRelatorio = daoFactory.getRelatorioVoluntDAO();

                    relatorioV = daoRelatorio.read(id);

                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(RelatorioVoluntController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                request.setAttribute("v", relatorioV);
                
                String data[] = relatorioV.getDia().toString().split("-");
                if(data.length >= 1){
                    request.setAttribute("ano", data[0]);
                    request.setAttribute("mes", data[1]);
                    request.setAttribute("dia", data[2]);
                }
                
                String data2[] = relatorioV.getHora().toString().split(":");
                if(data2.length >= 1){
                    request.setAttribute("horas", data2[0]);
                    request.setAttribute("minutos", data2[1]);
                }
                
                requestDispatcher = request.getRequestDispatcher("/view/menu/volunteer/update/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/view/menu/volunteer/recover": {
                int id = Integer.parseInt(request.getParameter("id"));
                
                try {
                    daoFactory = new DAOFactory();
                    daoRelatorio = daoFactory.getRelatorioVoluntDAO();
                    
                    daoRelatorio.recover(id);
                    
                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(RelatorioVoluntController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                requestDispatcher = request.getRequestDispatcher("/view/menu/volunteer/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/view/menu/volunteer/delete": {

                break;
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        DAOFactory daoFactory;
        RelatorioVoluntDAO daoRelatorio;
        VoluntarioDAO daoVoluntario;
        RequestDispatcher requestDispatcher;

        switch (request.getServletPath()) {
            case "/view/menu/volunteer/create": {
                RelatorioVolunt relatorio = new RelatorioVolunt();

                HttpSession sessao = request.getSession(true);
                Usuario user = (Usuario) sessao.getAttribute("usuario");
                int userId = user.getId();
                int voluntId = 0;
                try {
                    daoFactory = new DAOFactory();
                    
                    daoVoluntario = daoFactory.getVoluntarioDAO();

                    voluntId = daoVoluntario.idVPorIdusuario(userId);

                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(RelatorioVoluntController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                relatorio.setIdVoluntario(voluntId);
                relatorio.setSetor(request.getParameter("setor")); //OK
                relatorio.setObservacao(request.getParameter("observacao")); //OK
                relatorio.setAtivDesenvolvida(request.getParameter("ativ_desenvolvida")); //OK
                relatorio.setCoordenador(request.getParameter("coordenador")); //OK
                
                String hora = request.getParameter("horas") + ":" + request.getParameter("minutos") + ":00"; //OK
                relatorio.setHora(Constantes.getSQLTime(hora));
                
                String diaDia, diaMes, diaAno, data;
                diaDia = request.getParameter("diaDia"); //OK
                diaMes = request.getParameter("diaMes"); //OK
                diaAno = request.getParameter("diaAno"); //OK
                data = diaAno + "-" + diaMes + "-" + diaDia;
                relatorio.setDia(Constantes.getSQLDate(data));
                
                try {
                    daoFactory = new DAOFactory();
                    
                    daoRelatorio = daoFactory.getRelatorioVoluntDAO();

                    daoRelatorio.create(relatorio);

                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(RelatorioVoluntController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                requestDispatcher = request.getRequestDispatcher("/view/menu/volunteer/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/view/menu/volunteer/read": {

                break;
            }
            case "/view/menu/volunteer/update": {
                RelatorioVolunt relatorio = new RelatorioVolunt();
                relatorio.setId(Integer.parseInt(request.getParameter("id")));

                HttpSession sessao = request.getSession(true);
                Usuario user = (Usuario) sessao.getAttribute("usuario");
                int userId = user.getId();
                int voluntId = 0;
                try {
                    daoFactory = new DAOFactory();
                    
                    daoVoluntario = daoFactory.getVoluntarioDAO();

                    voluntId = daoVoluntario.idVPorIdusuario(userId);

                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(RelatorioVoluntController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                relatorio.setIdVoluntario(voluntId);
                relatorio.setSetor(request.getParameter("setor")); //OK
                relatorio.setObservacao(request.getParameter("observacao")); //OK
                relatorio.setAtivDesenvolvida(request.getParameter("ativ_desenvolvida")); //OK
                relatorio.setCoordenador(request.getParameter("coordenador")); //OK
                relatorio.setAprovado(Boolean.parseBoolean(request.getParameter("aprovar"))); //OK
                
                String hora = request.getParameter("horas") + ":" + request.getParameter("minutos") + ":00"; //OK
                relatorio.setHora(Constantes.getSQLTime(hora));
                
                String diaDia, diaMes, diaAno, data;
                diaDia = request.getParameter("diaDia"); //OK
                diaMes = request.getParameter("diaMes"); //OK
                diaAno = request.getParameter("diaAno"); //OK
                data = diaAno + "-" + diaMes + "-" + diaDia;
                relatorio.setDia(Constantes.getSQLDate(data));
                
                try {
                    daoFactory = new DAOFactory();
                    
                    daoRelatorio = daoFactory.getRelatorioVoluntDAO();

                    daoRelatorio.update(relatorio);

                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(RelatorioVoluntController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                requestDispatcher = request.getRequestDispatcher("/view/menu/volunteer/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/view/menu/volunteer/delete": {
                int id = Integer.parseInt(request.getParameter("id"));
                
                try {
                    daoFactory = new DAOFactory();
                    daoRelatorio = daoFactory.getRelatorioVoluntDAO();
                    
                    daoRelatorio.delete(id);
                    
                    daoFactory.close();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(RelatorioVoluntController.class.getName()).log(Level.SEVERE, null, ex);
                }

                requestDispatcher = request.getRequestDispatcher("/view/menu/volunteer/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
