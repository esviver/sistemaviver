/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.DAOFactory;
import dao.UsuarioDAO2;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Usuario;

@WebServlet(name = "LoginController",
        urlPatterns = {
            "/LoginController",
            "/view/menu/login",
            "/view/menu/login/create"
        })
/**
 *
 * @author Auxiliar
 */
public class LoginController extends HttpServlet {

    DAOFactory daoFactory;
    UsuarioDAO2 usuarioDAO;
    RequestDispatcher requestDispatcher;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        switch (request.getServletPath()) {
            case "/view/menu/logout": {
                HttpSession session = request.getSession(false);
                session.invalidate();

                response.sendRedirect(request.getContextPath() + "/view/menu/account/index.jsp");
                break;
            }
            case "/view/menu/login": {

                requestDispatcher = request.getRequestDispatcher("/view/menu/account/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        switch (request.getServletPath()) {
            case "/view/menu/login": {
                HttpSession sessao = request.getSession(true);
                try {

                    daoFactory = new DAOFactory();
                    usuarioDAO = daoFactory.getUsuarioDAO();
                    Usuario usuario = usuarioDAO.authenticate(request.getParameter("login"), request.getParameter("senha"));
                    boolean isPsicologo = false;
                    boolean isFuncionario = false;
                    boolean isAssistente = false;
                    boolean isVoluntario = false;
                    boolean isAdm = false;

                    if (usuario != null) {
                        if(usuario.getTipoDeUser().equals("adm") && usuario.getLogin().equals("adm"))
                            isAdm = true;
                        switch (usuario.getTipoDeUser()) {
                            case "psicologo": {
                                isPsicologo = true;
                                break;
                            }
                            case "funcionario": {
                                isFuncionario = true;
                                break;
                            }
                            case "assistente": {
                                isAssistente = true;
                                break;
                            }
                            case "voluntario": {
                                isVoluntario = true;
                                break;
                            }
                        }
                        sessao.setAttribute("isPsicologo", isPsicologo);
                        sessao.setAttribute("isFuncionario", isFuncionario);
                        sessao.setAttribute("isAssistente", isAssistente);
                        sessao.setAttribute("isVoluntario", isVoluntario);
                        sessao.setAttribute("isAdm", isAdm);

                        sessao.setAttribute("usuario", usuario);
                        requestDispatcher = request.getRequestDispatcher("/view/menu/index.jsp");
                        requestDispatcher.forward(request, response);
                    } else {
                        sessao.setAttribute("isPsicologo", false);
                        sessao.setAttribute("isFuncionario", false);
                        sessao.setAttribute("isAssistente", false);
                        sessao.setAttribute("isVoluntario", false);
                        sessao.setAttribute("isAdm", false);
                        sessao.setAttribute("usuario", null);
                        requestDispatcher = request.getRequestDispatcher("/view/index.jsp");
                        requestDispatcher.forward(request, response);

                    }
                } catch (ClassNotFoundException |SecurityException| SQLException ex) {
                    request.setAttribute("error", ex.getMessage());
                    requestDispatcher = request.getRequestDispatcher("/error.jsp");
                    requestDispatcher.forward(request, response);
                    break;
                }

                break;
            }
        }
    }
}
