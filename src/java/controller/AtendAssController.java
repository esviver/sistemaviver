package controller;

import dao.AssistenteDAO;
import dao.AtendimentoAssDAO;
import dao.DAOFactory;
import dao.PacienteDAO;
import global.Constantes;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Assistente;
import model.AtendAss;
import model.Atendimento;
import model.Paciente;
import model.Usuario;

@WebServlet(name = "AtendAssController", 
        urlPatterns = {
            "/AtendAssController",
            "/view/menu/agendamentos/social_care",
            "/view/menu/agendamentos/social_care/create",
            "/view/menu/agendamentos/social_care/read",
            "/view/menu/agendamentos/social_care/readAll",
            "/view/menu/agendamentos/social_care/readBySearch"
        
        })
public class AtendAssController extends HttpServlet {
    RequestDispatcher requestDispatcher;
    DAOFactory daoFactory;
    AtendimentoAssDAO atendimentoAssDAO;
    PacienteDAO pacienteDAO;
    AssistenteDAO assistenteDAO;
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        switch(request.getServletPath()){
            case "/view/menu/agendamentos/social_care":{
                requestDispatcher = request.getRequestDispatcher("/view/menu/agendamentos/social_care/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            
            case "/view/menu/agendamentos/social_care/create": {
                List<Paciente> pacientes = null;
                try {

                    daoFactory = new DAOFactory();
                    PacienteDAO pacienteDAO = daoFactory.getPacienteDAO();
                    pacientes = pacienteDAO.all();
                    
                    

                    daoFactory.close();
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(AtendPsicoController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(AtendPsicoController.class.getName()).log(Level.SEVERE, null, ex);
                }
                request.setAttribute("pacientes", pacientes);
                requestDispatcher = request.getRequestDispatcher("/view/menu/agendamentos/social_care/create/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/view/menu/agendamentos/social_care/read": {
                AtendAss atendAss = null;
                Paciente paciente = null;
                Atendimento atendimento = new Atendimento();
            try {
                daoFactory =  new DAOFactory();
                atendimentoAssDAO = daoFactory.getAtendimentoAssDAO();
                pacienteDAO = daoFactory.getPacienteDAO();
                
                int idPaciente = Integer.parseInt(request.getParameter("idPaciente"));
                int idAtendimento = Integer.parseInt(request.getParameter("idAtendimento"));
                
                atendAss = atendimentoAssDAO.read(idAtendimento);
                paciente = pacienteDAO.read(idPaciente);
                
                atendimento.setPaciente(paciente);
                atendimento.setAtendAss(atendAss);
                
                daoFactory.close();
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(AtendAssController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(AtendAssController.class.getName()).log(Level.SEVERE, null, ex);
            }
                request.setAttribute("atendimento", atendimento);
                
                //Cremonez ver a view para qual será direcionada
                requestDispatcher = request.getRequestDispatcher("/view/menu/agendamentos/social_care/read/visualizar.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/view/menu/agendamentos/social_care/readAll":{
                List<AtendAss> atendimentosAss = null;
                List<Atendimento> atendimentos = new ArrayList<>();
                Paciente paciente = null;
                Atendimento atendimento = null;
            try {
                daoFactory = new DAOFactory();
                atendimentoAssDAO = daoFactory.getAtendimentoAssDAO();
                pacienteDAO = daoFactory.getPacienteDAO();
                atendimentosAss = atendimentoAssDAO.all();
                
                for(AtendAss atendAss : atendimentosAss){
                    atendimento =  new Atendimento();
                    
                    int idPaciente = atendAss.getIdPaciente();
                    paciente =  pacienteDAO.read(idPaciente);
                    
                    atendimento.setAtendAss(atendAss);
                    atendimento.setPaciente(paciente);
                    
                    atendimentos.add(atendimento);
                    
                }
                
                daoFactory.close();
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(AtendAssController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(AtendAssController.class.getName()).log(Level.SEVERE, null, ex);
            }
                request.setAttribute("atendimentos", atendimentos);
                //Cremonez ver a view para qual será direcionada
                requestDispatcher = request.getRequestDispatcher("/view/menu/agendamentos/social_care/read/resultado.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/view/menu/agendamentos/social_care/readBySearch":{
                List<Atendimento> atendimentos = new ArrayList<>();
                List<AtendAss> atendimentosAss = null;
                List<Paciente> pacientes = null;
                Atendimento atendimento = null;
            try {
                daoFactory = new DAOFactory();
                pacienteDAO = daoFactory.getPacienteDAO();
                atendimentoAssDAO = daoFactory.getAtendimentoAssDAO();
                
                String searchParam = request.getParameter("input");
                
                pacientes = pacienteDAO.readByName(searchParam);
                
                for(Paciente paciente : pacientes){
                    atendimentosAss = atendimentoAssDAO.readByidPaciente(paciente.getId());
                    for(AtendAss atendAss: atendimentosAss){
                        atendimento = new Atendimento();
                        atendimento.setAtendAss(atendAss);
                        atendimento.setPaciente(paciente);
                        atendimentos.add(atendimento);
                    }
                }
                
                
                
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(AtendAssController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(AtendAssController.class.getName()).log(Level.SEVERE, null, ex);
            }
                request.setAttribute("atendimentos", atendimentos);
                //Cremonez ver a view para qual será direcionada
                requestDispatcher = request.getRequestDispatcher("/view/menu/agendamentos/social_care/read/resultado.jsp");
                requestDispatcher.forward(request, response);
                
                break;
            }
            case "//update": {
                
                break;
            }
            case "//delete": {
                
                break;
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        switch(request.getServletPath()){
            case "/":{}
            case "/view/menu/agendamentos/social_care/create": {
            try {
                daoFactory = new DAOFactory();
                assistenteDAO = daoFactory.getAssistenteDAO();
                atendimentoAssDAO = daoFactory.getAtendimentoAssDAO();
                AtendAss atendAss = new AtendAss();
                HttpSession sessao = request.getSession(true);
                Usuario usuario = (Usuario) sessao.getAttribute("usuario");
                Assistente assistente = assistenteDAO.read(usuario.getId());
                atendAss.setIdAss(assistente.getIdAss());
                atendAss.setIdPaciente(Integer.parseInt(request.getParameter("idPaciente")));
                atendAss.setRelatorio(request.getParameter("atendimento"));
                atendAss.setDataAtendimento(Constantes.getTodaysDate());
                //atendAss.setHora(null);
                atendimentoAssDAO.create(atendAss);
                daoFactory.close();
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(AtendAssController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(AtendAssController.class.getName()).log(Level.SEVERE, null, ex);
            }
                requestDispatcher = request.getRequestDispatcher("/view/menu/agendamentos/social_care/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "//read": {
                
                break;
            }
            case "//update": {
                
                break;
            }
            case "//delete": {
                
                break;
            }
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
