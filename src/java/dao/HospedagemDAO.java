/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Hospedagem;
import model.Paciente;

/**
 *
 * @author Auxiliar
 */
public class HospedagemDAO extends DAO<Hospedagem>{
    private final String createQuery = "INSERT INTO ong2.HOSPEDAGEM(idpaciente, dia, idquarto, anotacoes)"
            + "VALUES(?, ?, ?, ?)RETURNING id;";
    private final String updateQuery = "UPDATE ong2.HOSPEDAGEM SET idpaciente = ?, dia = ?, idquarto = ?, anotacoes = ? WHERE id = ?";
    private final String readQuery = "SELECT * FROM ong2.HOSPEDAGEM WHERE id = ?;";
    private final String allQuery = "SELECT h.idpaciente, h.idquarto, h.anotacoes,h.id,h.status, h.status, h.dia, p.pnome, p.snome "
            + "FROM ong2.HOSPEDAGEM h,ong2.Paciente p where h.idpaciente = p.id and h.status=true;";
    private final String allQueryArq = "SELECT h.idpaciente, h.idquarto, h.anotacoes,h.id,h.status, h.status, h.dia, p.pnome, p.snome "
            + "FROM ong2.HOSPEDAGEM h,ong2.Paciente p where h.idpaciente = p.id;";
    private final String countQuery = "SELECT COUNT(*) FROM ong2.HOSPEDAGEM;";
    private final String readQueryByNameWithoutDeleted = "SELECT * FROM ong2.HOSPEDAGEM WHERE idpaciente = ? AND status = true;";
    private final String readQueryByNameWithoutDeletedWithLike = "SELECT * FROM ong2.HOSPEDAGEM WHERE idpaciente = ? status = true;";
    private final String readQueryByNameWithDeleted = "SELECT * FROM ong2.HOSPEDAGEM WHERE idpaciente = ?;";
    private final String arquivarQuery = "UPDATE ong2.hospedagem set status = false where id = ?;";
    public HospedagemDAO(Connection connection) {
        super(connection);
    }
  public int contar()
    {
        try {
            PreparedStatement statement = connection.prepareStatement(countQuery);
            ResultSet result = statement.executeQuery();
            if(result.next())
                return result.getInt("count");
        } catch (SQLException ex) {
            Logger.getLogger(PacienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
    @Override
    public void create(Hospedagem obj) {           
          PreparedStatement statement;
        DAOFactory daoFactory = null;
       AcompanhanteDAO acompanhanteDao;
       try {
            daoFactory = new DAOFactory();
            acompanhanteDao = daoFactory.getAcompanhanteDAO();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(HospedagemDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(HospedagemDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(HospedagemDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        acompanhanteDao = daoFactory.getAcompanhanteDAO();
        try {
            statement = connection.prepareStatement(createQuery);
            statement.setInt(1, obj.getIdpaciente());
            statement.setDate(2, obj.getData());
            statement.setInt(3, obj.getQuarto());
            statement.setString(4, obj.getAnotacoes());
            ResultSet result = statement.executeQuery();
            //for(int x =0; x<obj.getAcompanhantes().size();x++)
            //acompanhanteDao.create(obj.getAcompanhantes().get(0));

        } catch (SQLException ex) {
            Logger.getLogger(HospedagemDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public Hospedagem read(int id) {
       PreparedStatement statement;
       DAOFactory daoFactory = null;
       AcompanhanteDAO acompanhanteDao;
       try {
            daoFactory = new DAOFactory();
            acompanhanteDao = daoFactory.getAcompanhanteDAO();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(HospedagemDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(HospedagemDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(HospedagemDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        acompanhanteDao = daoFactory.getAcompanhanteDAO();
       Hospedagem hospedagem;
        try { 
            statement = connection.prepareStatement(readQuery);
            statement.setInt(1, id);
            ResultSet result = statement.executeQuery();
            if(result.next())
            {
                hospedagem = new Hospedagem();
                hospedagem.setAnotacoes(result.getString("anotacoes"));
                hospedagem.setData(result.getDate("dia"));
                hospedagem.setIdpaciente(result.getInt("idpaciente"));
                hospedagem.setQuarto(result.getInt("idquarto"));
                hospedagem.setId(result.getInt("id"));
                hospedagem.setStatus(result.getBoolean("status"));
                hospedagem.setAcompanhantes(acompanhanteDao.allAc(hospedagem.getId()));
                return hospedagem;
            }
        } catch (SQLException ex) {
            Logger.getLogger(HospedagemDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     *
     * @param id
     * @return
     */

    public void arquivar(int id) {
        PreparedStatement statement;
        try {
            statement = connection.prepareStatement(arquivarQuery);
            statement.setInt(1, id);
            statement.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(HospedagemDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    }

    @Override
    public void delete(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Hospedagem> all() {
        
        try{
        DAOFactory daoFactory = null;
       AcompanhanteDAO acompanhanteDao;
       try {
            daoFactory = new DAOFactory();
            acompanhanteDao = daoFactory.getAcompanhanteDAO();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(HospedagemDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(HospedagemDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(HospedagemDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        acompanhanteDao = daoFactory.getAcompanhanteDAO();               
        Hospedagem hospedagem;
        List<Hospedagem> hospedagens = new ArrayList<Hospedagem>();
        PreparedStatement statement = connection.prepareStatement(allQuery);
        ResultSet result = statement.executeQuery();
        while(result.next())
        {
            hospedagem = new Hospedagem();
            hospedagem.setAnotacoes(result.getString("anotacoes"));
            hospedagem.setData(result.getDate("dia"));
            hospedagem.setIdpaciente(result.getInt("idpaciente"));
            hospedagem.setQuarto(result.getInt("idquarto"));
            hospedagem.setId(result.getInt("id"));
            hospedagem.setAcompanhantes(acompanhanteDao.allAc(hospedagem.getId()));
            hospedagem.setNome(result.getString("pnome")+" "+result.getString("snome"));
            hospedagem.setStatus(result.getBoolean("status"));
            
            hospedagens.add(hospedagem);
        }
        return hospedagens;
    } catch (SQLException ex) {
        Logger.getLogger(HospedagemDAO.class.getName()).log(Level.SEVERE, null, ex);
    }
    return null;  
    }
public List<Hospedagem> allArq() {
        
        try{
    DAOFactory daoFactory = null;
       AcompanhanteDAO acompanhanteDao;
       try {
            daoFactory = new DAOFactory();
            acompanhanteDao = daoFactory.getAcompanhanteDAO();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(HospedagemDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(HospedagemDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(HospedagemDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        acompanhanteDao = daoFactory.getAcompanhanteDAO();               
        Hospedagem hospedagem;
        List<Hospedagem> hospedagens = new ArrayList<Hospedagem>();
        PreparedStatement statement = connection.prepareStatement(allQueryArq);
        ResultSet result = statement.executeQuery();
        while(result.next())
        {
            hospedagem = new Hospedagem();
            hospedagem.setAnotacoes(result.getString("anotacoes"));
            hospedagem.setData(result.getDate("dia"));
            hospedagem.setIdpaciente(result.getInt("idpaciente"));
            hospedagem.setQuarto(result.getInt("idquarto"));
            hospedagem.setId(result.getInt("id"));
            hospedagem.setAcompanhantes(acompanhanteDao.allAc(hospedagem.getId()));
            hospedagem.setNome(result.getString("pnome")+" "+result.getString("snome"));
            hospedagem.setStatus(result.getBoolean("status"));
            
            hospedagens.add(hospedagem);
        }
        return hospedagens;
    } catch (SQLException ex) {
        Logger.getLogger(HospedagemDAO.class.getName()).log(Level.SEVERE, null, ex);
    }
    return null;  
    }

    
    public List<Hospedagem> readByName(String searchParam) {
        List<Hospedagem> hospedagens = new ArrayList<>();
         PreparedStatement statement;
         Hospedagem hospedagem;
        try {
            statement = connection.prepareStatement(readQueryByNameWithoutDeleted);
            //statement = connection.prepareStatement(readQueryByNameWithLike);
            statement.setString(1, searchParam);
            statement.setString(2, searchParam);
            System.out.println(statement.toString());
            ResultSet result = statement.executeQuery();
            while(result.next())
            {
                hospedagem = new Hospedagem();
                hospedagem.setAnotacoes(result.getString("anotacoes"));
               
                hospedagens.add(hospedagem);
            }
            return hospedagens;
        } catch (SQLException ex) {
            Logger.getLogger(DoacaoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    public List<Hospedagem> readByNameWithDeleted(String searchParam) {
        List<Hospedagem> hospedagens = new ArrayList<>();
        List<Paciente> pacientes = new ArrayList<>();
        PreparedStatement statement;
        Hospedagem hospedagem;
        PacienteDAO pacienteDao;
        try {
            DAOFactory daoFactory= new DAOFactory();
            pacienteDao = daoFactory.getPacienteDAO();
            pacientes = pacienteDao.readByName(searchParam);
            
        } catch (ClassNotFoundException | IOException | SQLException ex) {
            Logger.getLogger(HospedagemDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        try {
            statement = connection.prepareStatement(readQueryByNameWithDeleted);
            //statement = connection.prepareStatement(readQueryByNameWithLike);
            for (Paciente paciente : pacientes) {
                statement.setInt(1, paciente.getId());
                System.out.println(statement.toString());
                ResultSet result = statement.executeQuery();
                while(result.next())
                {
                    hospedagem = new Hospedagem();
                    hospedagem.setAnotacoes(result.getString("anotacoes"));
                    hospedagem.setData(result.getDate("dia"));
                    hospedagem.setIdpaciente(result.getInt("idpaciente"));
                    hospedagem.setQuarto(result.getInt("idquarto"));
                    hospedagem.setNome(paciente.getPNome()+" "+paciente.getSnome());
                    hospedagem.setStatus(result.getBoolean("status"));
                    hospedagens.add(hospedagem);
                }
            }
            return hospedagens;
        } catch (SQLException ex) {
            Logger.getLogger(DoacaoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        return null;
    }
    public List<Hospedagem> readByNameWithoutDeleted(String searchParam) {
        List<Hospedagem> hospedagens = new ArrayList<>();
        List<Paciente> pacientes = new ArrayList<>();
        PreparedStatement statement;
        Hospedagem hospedagem;
        PacienteDAO pacienteDao;
        try {
            DAOFactory daoFactory= new DAOFactory();
            pacienteDao = daoFactory.getPacienteDAO();
            pacientes = pacienteDao.readByName(searchParam);
            
        } catch (ClassNotFoundException | IOException | SQLException ex) {
            Logger.getLogger(HospedagemDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        try {
            statement = connection.prepareStatement(readQueryByNameWithoutDeleted);
            //statement = connection.prepareStatement(readQueryByNameWithLike);
            for (Paciente paciente : pacientes) {
                statement.setInt(1, paciente.getId());
                System.out.println(statement.toString());
                ResultSet result = statement.executeQuery();
                while(result.next())
                {
                    hospedagem = new Hospedagem();
                    hospedagem.setAnotacoes(result.getString("anotacoes"));
                    hospedagem.setData(result.getDate("dia"));
                    hospedagem.setIdpaciente(result.getInt("idpaciente"));
                    hospedagem.setQuarto(result.getInt("idquarto"));
                    hospedagem.setNome(paciente.getPNome()+" "+paciente.getSnome());
                    hospedagem.setStatus(result.getBoolean("status"));
                    hospedagens.add(hospedagem);
                }
            }
            return hospedagens;
        } catch (SQLException ex) {
            Logger.getLogger(DoacaoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        return null;
    }

    @Override
    public void update(Hospedagem obj) {
        PreparedStatement statement;
        DAOFactory daoFactory = null;
       AcompanhanteDAO acompanhanteDao;
       try {
            daoFactory = new DAOFactory();
            acompanhanteDao = daoFactory.getAcompanhanteDAO();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(HospedagemDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(HospedagemDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(HospedagemDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        acompanhanteDao = daoFactory.getAcompanhanteDAO();
        try {
            statement = connection.prepareStatement(updateQuery);
            statement.setInt(1, obj.getIdpaciente());
            statement.setDate(2, obj.getData());
            statement.setInt(3, obj.getQuarto());
            statement.setString(4, obj.getAnotacoes());
            statement.setInt(5, obj.getId());
            statement.executeUpdate();
            //for(int x =0; x<obj.getAcompanhantes().size();x++)
            //acompanhanteDao.create(obj.getAcompanhantes().get(0));

        } catch (SQLException ex) {
            Logger.getLogger(HospedagemDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    
    
}

