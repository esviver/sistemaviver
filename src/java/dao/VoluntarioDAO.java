/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Voluntario;

/**
 *
 * @author lshigaki
 */
public class VoluntarioDAO extends DAO<Voluntario> {

    private final String createQuery = "INSERT INTO ong2.voluntario (idusuario, profissao, escolaridade, "
            + "cdenome, cdetelefone, local_trab, estado_civil, email, usa_med, remedio_que_usa, perg1, perg2, "
            + "perg3, perg4, possui_disp, obs_sobre_hor, setor, data_inicio, hora, aceitacaoDosTermos) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, "
            + "?, ?, ?, ?, ?, ?, ?, ?) RETURNING id;";

    private final String aptidoesCreateQuery = "INSERT INTO ong2.aptidoes (id, aptidao) VALUES (?, ?);";

    private final String datahoraCreateQuery = "INSERT INTO ong2.dataehora (id, dataehora) VALUES (?, ?);";

    private final String diasCreateQuery = "INSERT INTO ong2.diassetor (id, dia) VALUES (?, ?);";

    private final String updateQuery = "UPDATE ong2.voluntario SET profissao = ?, "
            + "escolaridade = ?, cdenome = ?, cdetelefone = ?, local_trab = ?, estado_civil = ?, "
            + "email = ?, usa_med = ?, remedio_que_usa = ?, possui_disp = ?, obs_sobre_hor = ?, "
            + "setor = ?, hora = ?, aceitacaoDosTermos = ? WHERE id = ?;";

    private final String aptidoesDeleteQuery = "DELETE FROM ong2.aptidoes WHERE id = ?;";

    private final String datahoraDeleteQuery = "DELETE FROM ong2.dataehora WHERE id = ?;";

    private final String diasDeleteQuery = "DELETE FROM ong2.diassetor WHERE id = ?;";

    private final String readQuery = "SELECT * FROM ong2.voluntario WHERE idusuario = ?;";

    private final String readByCDENomeQuery = "SELECT * FROM ong2.voluntario WHERE cdenome = ?;";

    private final String readByCDENomeQuery2 = "SELECT v.* FROM ong2.voluntario v, ong2.usuario u"
            + " WHERE cdenome = ? AND v.idusuario = u.id AND u.status = true;";

    private final String aptidoesReadQuery = "SELECT aptidao FROM ong2.aptidoes WHERE id = ?;";

    private final String datahoraReadQuery = "SELECT dataehora FROM ong2.dataehora WHERE id = ?;";

    private final String diasReadQuery = "SELECT dias FROM ong2.diassetor WHERE id = ?;";

    private final String deleteQuery = "UPDATE ong2.usuario SET STATUS = FALSE WHERE id = ?;";

    private final String allQuery = "SELECT * FROM ong2.voluntario;";
    
    private final String listarTodosVoluntariosPEscolher = "SELECT v.id as id_table_voluntario, u.pnome as nome, u.snome as sobrenome FROM ong2.usuario u, ong2.voluntario v WHERE v.idusuario = u.id ORDER BY u.pnome;";

    private final String allQuery2 = "SELECT v.* FROM ong2.voluntario v, ong2.usuario u WHERE "
            + "v.idusuario = u.id AND u.status = true;";

    private final String recoverQuery = "UPDATE ong2.usuario u, ong2.voluntario v SET u.status = true "
            + "WHERE id = ? AND u.id = v.idusuario;";
private final String countQuery = "SELECT COUNT(*) FROM ong2.voluntario;";

    private final String idVPorIdusuario = "SELECT id FROM ong2.voluntario WHERE idusuario = ?;";
            
    public VoluntarioDAO(Connection connection) {
        super(connection);
    }
    public int contar()
    {
        try {
            PreparedStatement statement = connection.prepareStatement(countQuery);
            ResultSet result = statement.executeQuery();
            if(result.next())
                return result.getInt("count");
        } catch (SQLException ex) {
            Logger.getLogger(PacienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
    
    public int idVPorIdusuario(int idusuario)
    {
        try {
            PreparedStatement statement = connection.prepareStatement(idVPorIdusuario);
            
            statement.setInt(1, idusuario);
            
            ResultSet result = statement.executeQuery();
            if(result.next())
                return result.getInt("id");
        } catch (SQLException ex) {
            Logger.getLogger(PacienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
    
    @Override
    public void create(Voluntario v) {
        try {
            PreparedStatement statement = connection.prepareStatement(createQuery);

            statement.setInt(1, v.getIdUsuario());
            statement.setString(2, v.getProfissao());
            statement.setString(3, v.getEscolaridade());
            statement.setString(4, v.getCDENome());
            statement.setString(5, v.getCDETelefone());
            statement.setString(6, v.getLocalTrab());
            statement.setString(7, v.getEstadoCivil());
            statement.setString(8, v.getEmail());
            statement.setBoolean(9, v.isUsaMed());
            statement.setString(10, v.getRemedioQueUsa());
            statement.setString(11, v.getPerg1());
            statement.setString(12, v.getPerg2());
            statement.setString(13, v.getPerg3());
            statement.setString(14, v.getPerg4());
            statement.setString(15, v.getPossuiDisp());
            statement.setString(16, v.getObsSobreHor());
            statement.setString(17, v.getSetor());
            statement.setDate(18, v.getDataInicio());
            statement.setTime(19, v.getHora());
            statement.setBoolean(20, v.isAceitacaoDosTermos());

            ResultSet result = statement.executeQuery();

            if (result.next()) {
                v.setIdVoluntario(result.getInt("id"));
            }

            PreparedStatement statement1;
            List<String> aptList = v.getAptdoes();
            for (String el : aptList) {
                statement1 = connection.prepareStatement(aptidoesCreateQuery);
                statement1.setInt(1, v.getIdVoluntario());
                statement1.setString(2, el);
                statement1.executeUpdate();
            }

            PreparedStatement statement2;
            List<String> dhList = v.getDiasEHorariosDisp();
            for (String el : dhList) {
                statement2 = connection.prepareStatement(datahoraCreateQuery);
                statement2.setInt(1, v.getIdVoluntario());
                statement2.setString(2, el);
                statement2.executeUpdate();
            }

            PreparedStatement statement3;
            List<String> diasList = v.getDiasSetor();
            for (String el : diasList) {
                statement3 = connection.prepareStatement(diasCreateQuery);
                statement3.setInt(1, v.getIdVoluntario());
                statement3.setString(2, el);
                statement3.executeUpdate();
            }
        } catch (SQLException ex) {
            Logger.getLogger(VoluntarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public Voluntario read(int idUsuario) {
        try {
            PreparedStatement statement = connection.prepareStatement(readQuery);

            statement.setInt(1, idUsuario);

            ResultSet result = statement.executeQuery();

            if (result.next()) {
                Voluntario v = new Voluntario();

                v.setIdVoluntario(result.getInt("id"));
                v.setIdUsuario(result.getInt("idusuario"));
                v.setProfissao(result.getString("profissao"));
                v.setEscolaridade(result.getString("escolaridade"));
                v.setLocalTrab(result.getString("local_trab"));
                v.setEstadoCivil(result.getString("estado_civil"));
                v.setEmail(result.getString("email"));
                v.setUsaMed(result.getBoolean("usa_med"));
                v.setRemedioQueUsa(result.getString("remedio_que_usa"));
                v.setCDENome(result.getString("cdenome"));
                v.setCDETelefone(result.getString("cdetelefone"));
                v.setPerg1(result.getString("perg1"));
                v.setPerg2(result.getString("perg2"));
                v.setPerg3(result.getString("perg3"));
                v.setPerg4(result.getString("perg4"));
                v.setPossuiDisp(result.getString("possui_disp"));
                v.setObsSobreHor(result.getString("obs_sobre_hor"));
                v.setSetor(result.getString("setor"));
                v.setDataInicio(result.getDate("data_inicio"));
                v.setAceitacaoDosTermos(result.getBoolean("aceitacaoDosTermos"));
                v.setHora(result.getTime("hora"));

                PreparedStatement statement1 = connection.prepareStatement(aptidoesReadQuery);

                statement1.setInt(1, v.getIdVoluntario());

                ResultSet result1 = statement1.executeQuery();

                ArrayList<String> aptList = new ArrayList<>();

                while (result1.next()) {
                    aptList.add(result1.getString("aptidao"));
                }

                v.setAptdoes(aptList);

                PreparedStatement statement2 = connection.prepareStatement(datahoraReadQuery);

                statement1.setInt(1, v.getIdVoluntario());

                ResultSet result2 = statement2.executeQuery();

                ArrayList<String> dhList = new ArrayList<>();

                while (result2.next()) {
                    dhList.add(result1.getString("dataehora"));
                }

                v.setDiasEHorariosDisp(dhList);

                PreparedStatement statement3 = connection.prepareStatement(diasReadQuery);

                statement1.setInt(1, v.getIdVoluntario());

                ResultSet result3 = statement3.executeQuery();

                ArrayList<String> diasList = new ArrayList<>();

                while (result3.next()) {
                    diasList.add(result1.getString("dia"));
                }

                v.setDiasSetor(diasList);

                return v;
            }
        } catch (SQLException ex) {
            Logger.getLogger(VoluntarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

    @Override
    public void update(Voluntario v) {

        try {
            PreparedStatement statement = connection.prepareStatement(updateQuery);

            statement.setString(1, v.getProfissao());
            statement.setString(2, v.getEscolaridade());
            statement.setString(3, v.getCDENome());
            statement.setString(4, v.getCDETelefone());
            statement.setString(5, v.getLocalTrab());
            statement.setString(6, v.getEstadoCivil());
            statement.setString(7, v.getEmail());
            statement.setBoolean(8, v.isUsaMed());
            statement.setString(9, v.getRemedioQueUsa());
            statement.setString(10, v.getPossuiDisp());
            statement.setString(11, v.getObsSobreHor());
            statement.setString(12, v.getSetor());
            statement.setInt(13, v.getIdVoluntario());
            statement.setTime(14, v.getHora());
            statement.setBoolean(15, v.isAceitacaoDosTermos());

            PreparedStatement statement1a = connection.prepareStatement(aptidoesDeleteQuery);

            statement1a.setInt(1, v.getIdVoluntario());

            PreparedStatement statement1b = connection.prepareStatement(aptidoesCreateQuery);

            statement1b.setInt(1, v.getIdVoluntario());

            ArrayList<String> aptList = v.getAptdoes();
            for (int i = 0; i < aptList.size() - 1; i++) {
                statement1b.setString(2, aptList.get(i));
                statement1b.executeQuery();
            }

            PreparedStatement statement2a = connection.prepareStatement(datahoraDeleteQuery);

            statement2a.setInt(1, v.getIdVoluntario());

            PreparedStatement statement2b = connection.prepareStatement(datahoraCreateQuery);

            statement2b.setInt(1, v.getIdVoluntario());

            ArrayList<String> dhList = v.getDiasEHorariosDisp();
            for (int i = 0; i < dhList.size() - 1; i++) {
                statement2b.setString(2, dhList.get(i));
                statement2b.executeQuery();
            }

            PreparedStatement statement3a = connection.prepareStatement(diasDeleteQuery);

            statement3a.setInt(1, v.getIdVoluntario());

            PreparedStatement statement3b = connection.prepareStatement(diasCreateQuery);

            statement3b.setInt(1, v.getIdVoluntario());

            ArrayList<String> diasList = v.getDiasSetor();
            for (int i = 0; i < diasList.size() - 1; i++) {
                statement3b.setString(2, diasList.get(i));
                statement3b.executeQuery();
            }
        } catch (SQLException ex) {
            Logger.getLogger(VoluntarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public void delete(int idUsuario) { //passar idUsuario

        try {
            PreparedStatement statement = connection.prepareStatement(deleteQuery);

            statement.setInt(1, idUsuario);

            statement.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(VoluntarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public List<Voluntario> all() {
        try {
            PreparedStatement statement = connection.prepareStatement(allQuery);

            ResultSet result = statement.executeQuery();

            ArrayList<Voluntario> vList = new ArrayList<>();

            Voluntario v;

            while (result.next()) {
                v = new Voluntario();

                v.setIdVoluntario(result.getInt("id"));
                v.setIdUsuario(result.getInt("idusuario"));
                v.setProfissao(result.getString("profissao"));
                v.setEscolaridade(result.getString("escolaridade"));
                v.setLocalTrab(result.getString("local_trab"));
                v.setEstadoCivil(result.getString("estado_civil"));
                v.setEmail(result.getString("email"));
                v.setUsaMed(result.getBoolean("usa_med"));
                v.setRemedioQueUsa(result.getString("remedio_que_usa"));
                v.setCDENome(result.getString("cdenome"));
                v.setCDETelefone(result.getString("cdetelefone"));
                v.setPerg1(result.getString("perg1"));
                v.setPerg2(result.getString("perg2"));
                v.setPerg3(result.getString("perg3"));
                v.setPerg4(result.getString("perg4"));
                v.setPossuiDisp(result.getString("possui_disp"));
                v.setObsSobreHor(result.getString("obs_sobre_hor"));
                v.setSetor(result.getString("setor"));
                v.setDataInicio(result.getDate("data_inicio"));
                v.setAceitacaoDosTermos(result.getBoolean("aceitacaoDosTermos"));
                v.setHora(result.getTime("hora"));

                PreparedStatement statement1 = connection.prepareStatement(aptidoesReadQuery);

                statement1.setInt(1, v.getIdVoluntario());

                ResultSet result1 = statement1.executeQuery();

                ArrayList<String> aptList = new ArrayList<>();

                while (result1.next()) {
                    aptList.add(result1.getString("aptidao"));
                }

                v.setAptdoes(aptList);

                PreparedStatement statement2 = connection.prepareStatement(datahoraReadQuery);

                statement1.setInt(1, v.getIdVoluntario());

                ResultSet result2 = statement2.executeQuery();

                ArrayList<String> dhList = new ArrayList<>();

                while (result2.next()) {
                    dhList.add(result1.getString("dataehora"));
                }

                v.setDiasEHorariosDisp(dhList);

                PreparedStatement statement3 = connection.prepareStatement(diasReadQuery);

                statement1.setInt(1, v.getIdVoluntario());

                ResultSet result3 = statement3.executeQuery();

                ArrayList<String> diasList = new ArrayList<>();

                while (result3.next()) {
                    diasList.add(result1.getString("dia"));
                }

                v.setDiasSetor(diasList);

                vList.add(v);
            }
            return vList;
        } catch (SQLException ex) {
            Logger.getLogger(VoluntarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

    public List<Voluntario> allWithoutDeleted() {
        try {
            PreparedStatement statement = connection.prepareStatement(allQuery2);

            ResultSet result = statement.executeQuery();

            ArrayList<Voluntario> vList = new ArrayList<>();

            Voluntario v;

            while (result.next()) {
                v = new Voluntario();

                v.setIdVoluntario(result.getInt("id"));
                v.setIdUsuario(result.getInt("idusuario"));
                v.setProfissao(result.getString("profissao"));
                v.setEscolaridade(result.getString("escolaridade"));
                v.setLocalTrab(result.getString("local_trab"));
                v.setEstadoCivil(result.getString("estado_civil"));
                v.setEmail(result.getString("email"));
                v.setUsaMed(result.getBoolean("usa_med"));
                v.setRemedioQueUsa(result.getString("remedio_que_usa"));
                v.setCDENome(result.getString("cdenome"));
                v.setCDETelefone(result.getString("cdetelefone"));
                v.setPerg1(result.getString("perg1"));
                v.setPerg2(result.getString("perg2"));
                v.setPerg3(result.getString("perg3"));
                v.setPerg4(result.getString("perg4"));
                v.setPossuiDisp(result.getString("possui_disp"));
                v.setObsSobreHor(result.getString("obs_sobre_hor"));
                v.setSetor(result.getString("setor"));
                v.setDataInicio(result.getDate("data_inicio"));
                v.setAceitacaoDosTermos(result.getBoolean("aceitacaoDosTermos"));
                v.setHora(result.getTime("hora"));

                PreparedStatement statement1 = connection.prepareStatement(aptidoesReadQuery);

                statement1.setInt(1, v.getIdVoluntario());

                ResultSet result1 = statement1.executeQuery();

                ArrayList<String> aptList = new ArrayList<>();

                while (result1.next()) {
                    aptList.add(result1.getString("aptidao"));
                }

                v.setAptdoes(aptList);

                PreparedStatement statement2 = connection.prepareStatement(datahoraReadQuery);

                statement1.setInt(1, v.getIdVoluntario());

                ResultSet result2 = statement2.executeQuery();

                ArrayList<String> dhList = new ArrayList<>();

                while (result2.next()) {
                    dhList.add(result1.getString("dataehora"));
                }

                v.setDiasEHorariosDisp(dhList);

                PreparedStatement statement3 = connection.prepareStatement(diasReadQuery);

                statement1.setInt(1, v.getIdVoluntario());

                ResultSet result3 = statement3.executeQuery();

                ArrayList<String> diasList = new ArrayList<>();

                while (result3.next()) {
                    diasList.add(result1.getString("dia"));
                }

                v.setDiasSetor(diasList);

                vList.add(v);
            }
            return vList;
        } catch (SQLException ex) {
            Logger.getLogger(VoluntarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

    public List<Voluntario> readByName(String name) {

        try {
            PreparedStatement statement = connection.prepareStatement(readByCDENomeQuery);

            statement.setString(1, name);

            ResultSet result = statement.executeQuery();

            List<Voluntario> vList = new ArrayList<>();

            Voluntario v;

            while (result.next()) {
                v = new Voluntario();

                v.setIdVoluntario(result.getInt("id"));
                v.setIdUsuario(result.getInt("idusuario"));
                v.setProfissao(result.getString("profissao"));
                v.setEscolaridade(result.getString("escolaridade"));
                v.setLocalTrab(result.getString("local_trab"));
                v.setEstadoCivil(result.getString("estado_civil"));
                v.setEmail(result.getString("email"));
                v.setUsaMed(result.getBoolean("usa_med"));
                v.setRemedioQueUsa(result.getString("remedio_que_usa"));
                v.setCDENome(result.getString("cdenome"));
                v.setCDETelefone(result.getString("cdetelefone"));
                v.setPerg1(result.getString("perg1"));
                v.setPerg2(result.getString("perg2"));
                v.setPerg3(result.getString("perg3"));
                v.setPerg4(result.getString("perg4"));
                v.setPossuiDisp(result.getString("possui_disp"));
                v.setObsSobreHor(result.getString("obs_sobre_hor"));
                v.setSetor(result.getString("setor"));
                v.setDataInicio(result.getDate("data_inicio"));
                v.setAceitacaoDosTermos(result.getBoolean("aceitacaoDosTermos"));
                v.setHora(result.getTime("hora"));

                PreparedStatement statement1 = connection.prepareStatement(aptidoesReadQuery);

                statement1.setInt(1, v.getIdVoluntario());

                ResultSet result1 = statement1.executeQuery();

                ArrayList<String> aptList = new ArrayList<>();

                while (result1.next()) {
                    aptList.add(result1.getString("aptidao"));
                }

                v.setAptdoes(aptList);

                PreparedStatement statement2 = connection.prepareStatement(datahoraReadQuery);

                statement1.setInt(1, v.getIdVoluntario());

                ResultSet result2 = statement2.executeQuery();

                ArrayList<String> dhList = new ArrayList<>();

                while (result2.next()) {
                    dhList.add(result1.getString("dataehora"));
                }

                v.setDiasEHorariosDisp(dhList);

                PreparedStatement statement3 = connection.prepareStatement(diasReadQuery);

                statement1.setInt(1, v.getIdVoluntario());

                ResultSet result3 = statement3.executeQuery();

                ArrayList<String> diasList = new ArrayList<>();

                while (result3.next()) {
                    diasList.add(result1.getString("dia"));
                }

                v.setDiasSetor(diasList);

                vList.add(v);
            }
            return vList;

        } catch (SQLException ex) {
            Logger.getLogger(VoluntarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

    public List<Voluntario> readByNameWithoutDeleted(String name) {
        try {

            PreparedStatement statement = connection.prepareStatement(readByCDENomeQuery2);

            statement.setString(1, name);

            ResultSet result = statement.executeQuery();

            List<Voluntario> vList = new ArrayList<>();

            Voluntario v;

            while (result.next()) {
                v = new Voluntario();

                v.setIdVoluntario(result.getInt("id"));
                v.setIdUsuario(result.getInt("idusuario"));
                v.setProfissao(result.getString("profissao"));
                v.setEscolaridade(result.getString("escolaridade"));
                v.setLocalTrab(result.getString("local_trab"));
                v.setEstadoCivil(result.getString("estado_civil"));
                v.setEmail(result.getString("email"));
                v.setUsaMed(result.getBoolean("usa_med"));
                v.setRemedioQueUsa(result.getString("remedio_que_usa"));
                v.setCDENome(result.getString("cdenome"));
                v.setCDETelefone(result.getString("cdetelefone"));
                v.setPerg1(result.getString("perg1"));
                v.setPerg2(result.getString("perg2"));
                v.setPerg3(result.getString("perg3"));
                v.setPerg4(result.getString("perg4"));
                v.setPossuiDisp(result.getString("possui_disp"));
                v.setObsSobreHor(result.getString("obs_sobre_hor"));
                v.setSetor(result.getString("setor"));
                v.setDataInicio(result.getDate("data_inicio"));
                v.setAceitacaoDosTermos(result.getBoolean("aceitacaoDosTermos"));
                v.setHora(result.getTime("hora"));

                PreparedStatement statement1 = connection.prepareStatement(aptidoesReadQuery);

                statement1.setInt(1, v.getIdVoluntario());

                ResultSet result1 = statement1.executeQuery();

                ArrayList<String> aptList = new ArrayList<>();

                while (result1.next()) {
                    aptList.add(result1.getString("aptidao"));
                }

                v.setAptdoes(aptList);

                PreparedStatement statement2 = connection.prepareStatement(datahoraReadQuery);

                statement1.setInt(1, v.getIdVoluntario());

                ResultSet result2 = statement2.executeQuery();

                ArrayList<String> dhList = new ArrayList<>();

                while (result2.next()) {
                    dhList.add(result1.getString("dataehora"));
                }

                v.setDiasEHorariosDisp(dhList);

                PreparedStatement statement3 = connection.prepareStatement(diasReadQuery);

                statement1.setInt(1, v.getIdVoluntario());

                ResultSet result3 = statement3.executeQuery();

                ArrayList<String> diasList = new ArrayList<>();

                while (result3.next()) {
                    diasList.add(result1.getString("dia"));
                }

                v.setDiasSetor(diasList);

                vList.add(v);
            }
            return vList;

        } catch (SQLException ex) {
            Logger.getLogger(VoluntarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

    public void recover(int id) {

        try {
            PreparedStatement statement = connection.prepareStatement(recoverQuery);      
            statement.setInt(1, id); 
            statement.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(VoluntarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
