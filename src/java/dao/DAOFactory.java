package dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Savepoint;
import jdbc.ConnectionFactory;

public class DAOFactory implements AutoCloseable {

    private Connection connection = null;

    public DAOFactory() throws ClassNotFoundException, IOException, SQLException {
        connection = ConnectionFactory.getInstance().getConnection();
    }

    public void beginTransaction() throws SQLException {
        try {
            connection.setAutoCommit(false);
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());

            throw new SQLException("Erro ao abrir transação.");
        }
    }

    public Savepoint createSavepoint(String name) throws SQLException {
        try {
            return connection.setSavepoint(name);
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());

            throw new SQLException("Erro ao executar transação.");
        }
    }

    public void commitTransaction() throws SQLException {
        try {
            connection.commit();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());

            throw new SQLException("Erro ao finalizar transação.");
        }
    }

    public void rollbackTransaction() throws SQLException {
        try {
            connection.rollback();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());

            throw new SQLException("Erro ao executar transação.");
        }
    }

    public void rollbackTransactionTo(Savepoint savepoint) throws SQLException {
        try {
            connection.rollback(savepoint);
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());

            throw new SQLException("Erro ao executar transação.");
        }
    }

    public void endTransaction() throws SQLException {
        try {
            connection.setAutoCommit(true);
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());

            throw new SQLException("Erro ao finalizar transação.");
        }
    }

    public void closeConnection() throws SQLException {
        try {
            connection.close();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());

            throw new SQLException("Erro ao fechar conexão ao banco de dados.");
        }
    }

    public AnamDAO getAnamDAO() {
        return new AnamDAO(connection);
    }
    
    public DistribuicaoDAO getDistribuicaoDAO() {
        return new DistribuicaoDAO(connection);
    }
    
    public PacienteDAO getPacienteDAO() {
        return new PacienteDAO(connection);
    }
    
    public VoluntarioDAO getVoluntarioDAO() {
        return new VoluntarioDAO(connection);
    }
    
    public RelatorioVoluntDAO getRelatorioVoluntDAO() {
        return new RelatorioVoluntDAO(connection);
    }
    
    public ResponsavelDAO getResponsavelDAO() {
        return new ResponsavelDAO(connection);
    }
    
    public QuartoDAO getQuartoDAO() {
        return new QuartoDAO(connection);
    }
    
    public PsicologaDAO getPsicologaDAO() {
        return new PsicologaDAO(connection);
    }
    
    public UsuarioDAO2 getUsuarioDAO() {
        return new UsuarioDAO2(connection);
    }
    
    public AtendimentoAssDAO getAtendimentoAssDAO(){
        return new AtendimentoAssDAO(connection);
    }
    
    public AtendimentoPsicoDAO getAtendimentoPsicoDAO(){
        return new AtendimentoPsicoDAO(connection);
    }
    public AcompanhanteDAO getAcompanhanteDAO(){
        return new AcompanhanteDAO(connection);
    }
    public DoacaoDAO getDoacaoDAO(){
        return new DoacaoDAO(connection);
    }
    public EnderecoDAO getEnderecoDAO(){
        return new EnderecoDAO(connection);
    }
    public EntregueDAO getEntregueDAO(){
        return new EntregueDAO(connection);
    }
    public HospedagemDAO getHospedagemDAO(){
        return new HospedagemDAO(connection);
    }
    public ItemDAO getItemDAO(){
        return new ItemDAO(connection);
    }
    public AssistenteDAO getAssistenteDAO(){
        return new AssistenteDAO(connection);
    }
    
    @Override
    public void close() throws SQLException {
        closeConnection();
    }
}