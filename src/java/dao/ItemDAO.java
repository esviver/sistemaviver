/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;


import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Item;

/**
 *
 * @author Auxiliar
 */
public class ItemDAO extends DAO<Item> {
    private final String createQuery = "INSERT INTO ong2.ITEM(nome, qtdade)VALUES(?, ?) RETURNING id;";
    private final String readQuery = "SELECT * INTO ong2.ITEM WHERE id = ?;";
    private final String deleteQuery = "DELETE FROM ong2.ITEM WHERE id = ?;";
    private final String updateQuery = "UPDATE ong2.ITEM SET  nome = ?, qtde = ? WHERE id = ?;";
    private final String all = "SELECT * FROM ong2.ITEM;";
    
    public ItemDAO(Connection connection) {
        super(connection);
    }

    @Override
    public void create(Item obj) {
        PreparedStatement statement;
        try {
            statement = connection.prepareStatement(createQuery);
            statement.setString(1, obj.getNome());
            statement.setInt(2, obj.getQtde());
            ResultSet result = statement.executeQuery();
            
        } catch (SQLException ex) {
            Logger.getLogger(ItemDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    @Override
    public Item read(int id) {
        Item item = new Item();
        PreparedStatement statement;
        try {
            statement = connection.prepareStatement(createQuery);
            statement.setInt(1, id);
            ResultSet result = statement.executeQuery();
            if(result.next())
            {
                item.setId(result.getInt("id"));
                item.setNome(result.getString("nome"));
                item.setQtde(result.getInt("qtde"));
                return item;
            }
        } catch (SQLException ex) {
            Logger.getLogger(ItemDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
     return null; 
    }

    @Override
    public void update(Item obj) {
      PreparedStatement statement;
        try {
            statement = connection.prepareStatement(createQuery);
            statement.setString(1, obj.getNome());
            statement.setInt(2, obj.getQtde());
            statement.setInt(3, obj.getId());
            ResultSet result = statement.executeQuery();
            
        } catch (SQLException ex) {
            Logger.getLogger(ItemDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void delete(int id) {
        Item item = new Item();
        PreparedStatement statement;
        try {
            statement = connection.prepareStatement(deleteQuery);
            statement.setInt(1, id);
            statement.executeUpdate();
            } catch (SQLException ex) {
            Logger.getLogger(ItemDAO.class.getName()).log(Level.SEVERE, null, ex);
        }    
        
    }

    @Override
    public List<Item> all() {
        Item item = new Item();
        PreparedStatement statement;
        List<Item> itens = new ArrayList<Item>();
        try {
            statement = connection.prepareStatement(all);
            ResultSet result = statement.executeQuery();
            if(result.next())
            {
                item.setId(result.getInt("id"));
                item.setNome(result.getString("nome"));
                item.setQtde(result.getInt("qtde"));
                itens.add(item);
            }
            return itens;
        } catch (SQLException ex) {
            Logger.getLogger(ItemDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
     return null; 
    }
    public boolean check(List<Item> obj)
    {
        DAOFactory daoFactory = null;
        try {
            daoFactory= new DAOFactory();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ItemDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ItemDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ItemDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        ItemDAO itemDao = daoFactory.getItemDAO();
        for (Item item : obj) {
            Item dobanco = itemDao.read(item.getId());
            if(dobanco.getQtde()>=item.getQtde())
                continue;
            else
                return false;
        }
        return true;
    }

    public void desconta(List<Item> item) {
        DAOFactory daoFactory = null;
        try {
            daoFactory= new DAOFactory();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ItemDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ItemDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ItemDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        ItemDAO itemDao = daoFactory.getItemDAO();
        Item dadoacao, dobanco;
        for(int x = 0; x<item.size(); x++)
        {
           dobanco = itemDao.read(item.get(x).getId());
           dobanco.setQtde(dobanco.getQtde()-item.get(x).getQtde());
           itemDao.update(dobanco);
           
        }
    }
}
    

