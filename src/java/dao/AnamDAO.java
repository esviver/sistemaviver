/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Anam;

/**
 *
 * @author matheus
 */
public class AnamDAO extends DAO <Anam>{
    private final String createQuery = "INSERT INTO ong2.anamnese(idpsicologa, idpaciente,encaminhamento, diagnostico, medico, historia, historico, queixa, motivos, hipoteses, planoterapeutico, dia, infaciaadolescencia, aspectospsicologicos,estruturafamiliar,psicosociais, religiao ) "
                                     + " VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) RETURNING id;";
    private final String readQuery = "SELECT * FROM ong2.anamnese WHERE id = ?;";
    private final String allQuery = "SELECT * FROM ong2.anamnese";
    private final String countQuery = "SELECT COUNT(*) FROM ong2.anamnese;";
    private final String readByidPaciente = "SELECT * FROM ong2.anamnese WHERE idpaciente = ?";
    public AnamDAO(Connection connection) {
        super(connection);
    }
    
  public int contar()
    {
        try {
            PreparedStatement statement = connection.prepareStatement(countQuery);
            ResultSet result = statement.executeQuery();
            if(result.next())
                return result.getInt("count");
        } catch (SQLException ex) {
            Logger.getLogger(PacienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
    @Override
    public void create(Anam anam) {
        try {
            PreparedStatement statement = connection.prepareStatement(createQuery);
            statement.setInt(1, anam.getIdpsicologa());
            statement.setInt(2, anam.getIdPaciente());
            statement.setString(3, anam.getEncaminhamento());
            statement.setString(4, anam.getDiagnostico());
            statement.setString(5, anam.getMedico());
            statement.setString(6, anam.getHistoria());
            statement.setString(7, anam.getHistoricoClin());
            statement.setString(8, anam.getQueixa());
            statement.setString(9, anam.getMotivosImpExp());
            statement.setString(10, anam.getFormulacaoHipotese());
            statement.setString(11, anam.getPlanoTerap());
            statement.setDate(12, anam.getData());
            statement.setString(13, anam.getInfEAdos());
            statement.setString(14, anam.getAspecPsico());
            statement.setString(15, anam.getEstrutFamiliar());
            statement.setString(16, anam.getDadosPsicoSociais());
            statement.setString(17, anam.getReligiao());
            
            ResultSet result = statement.executeQuery();
            
            if(result.next()){
               anam.setId(result.getInt("id"));
            }

            
        } catch (SQLException ex) {
            Logger.getLogger(AnamDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public Anam read(int id) {
        try {
            PreparedStatement statement = connection.prepareStatement(readQuery);
            statement.setInt(1, id);
            ResultSet result = statement.executeQuery();
            
            Anam anam = new Anam();
            
            if(result.next()){
                anam.setId(result.getInt("id"));
                anam.setIdpsicologa(result.getInt("idpsicologa"));
                anam.setIdPaciente(result.getInt("idpaciente"));
                anam.setEncaminhamento(result.getString("encaminhamento"));
                anam.setDiagnostico(result.getString("diagnostico"));
                anam.setMedico(result.getString("medico"));
                anam.setHistoria(result.getString("historia"));
                anam.setQueixa(result.getString("queixa"));
                anam.setHistoricoClin(result.getString("historico"));
                anam.setMotivosImpExp(result.getString("motivos"));
                anam.setFormulacaoHipotese(result.getString("hipoteses"));
                anam.setPlanoTerap(result.getString("planoterapeutico"));
                anam.setData(result.getDate("dia"));
                anam.setInfEAdos(result.getString("infaciaadolescencia"));
                anam.setAspecPsico(result.getString("aspectospsicologicos"));
                anam.setEstrutFamiliar(result.getString("estruturafamiliar"));
                anam.setDadosPsicoSociais(result.getString("psicosociais"));
                anam.setReligiao(result.getString("religiao"));
                
                return anam;
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(AnamDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public void update(Anam obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Anam> all(){
        try {
            PreparedStatement statement = connection.prepareStatement(allQuery);
            ResultSet result = statement.executeQuery();
            List<Anam> anamneses = new ArrayList<>();
            Anam anam = null;
            while(result.next()){
                anam = new Anam();
                anam.setId(result.getInt("id"));
                anam.setIdpsicologa(result.getInt("idpsicologa"));
                anam.setIdPaciente(result.getInt("idpaciente"));
                anam.setEncaminhamento(result.getString("encaminhamento"));
                anam.setDiagnostico(result.getString("diagnostico"));
                anam.setMedico(result.getString("medico"));
                anam.setHistoria(result.getString("historia"));
                anam.setQueixa(result.getString("queixa"));
                anam.setHistoricoClin(result.getString("historico"));
                anam.setMotivosImpExp(result.getString("motivos"));
                anam.setFormulacaoHipotese(result.getString("hipoteses"));
                anam.setPlanoTerap(result.getString("planoterapeutico"));
                anam.setData(result.getDate("dia"));
                anam.setInfEAdos(result.getString("infaciaadolescencia"));
                anam.setAspecPsico(result.getString("aspectospsicologicos"));
                anam.setEstrutFamiliar(result.getString("estruturafamiliar"));
                anam.setDadosPsicoSociais(result.getString("psicosociais"));
                anam.setReligiao(result.getString("religiao"));
                
                anamneses.add(anam);
                
            }
            return anamneses;
        } catch (SQLException ex) {
            Logger.getLogger(AnamDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    public List<Anam> readByidPaciente(int idPaciente){
        try {
            PreparedStatement statement = connection.prepareStatement(readByidPaciente);
            statement.setInt(1, idPaciente);
            ResultSet result = statement.executeQuery();
            List<Anam> anamneses = new ArrayList<>();
            Anam anam = null;
            while(result.next()){
                anam = new Anam();
                anam.setId(result.getInt("id"));
                anam.setIdpsicologa(result.getInt("idpsicologa"));
                anam.setIdPaciente(result.getInt("idpaciente"));
                anam.setEncaminhamento(result.getString("encaminhamento"));
                anam.setDiagnostico(result.getString("diagnostico"));
                anam.setMedico(result.getString("medico"));
                anam.setHistoria(result.getString("historia"));
                anam.setQueixa(result.getString("queixa"));
                anam.setHistoricoClin(result.getString("historico"));
                anam.setMotivosImpExp(result.getString("motivos"));
                anam.setFormulacaoHipotese(result.getString("hipoteses"));
                anam.setPlanoTerap(result.getString("planoterapeutico"));
                anam.setData(result.getDate("dia"));
                anam.setInfEAdos(result.getString("infaciaadolescencia"));
                anam.setAspecPsico(result.getString("aspectospsicologicos"));
                anam.setEstrutFamiliar(result.getString("estruturafamiliar"));
                anam.setDadosPsicoSociais(result.getString("psicosociais"));
                anam.setReligiao(result.getString("religiao"));
                
                anamneses.add(anam);
                
            }
            return anamneses;
        } catch (SQLException ex) {
            Logger.getLogger(AnamDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
}
