/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Distribuicao;
import model.Entregue;
import model.Item;
import model.Paciente;

/**
 *
 * @author Auxiliar
 */
public class DistribuicaoDAO extends DAO<Distribuicao> {

    private final String createQuery = "INSERT INTO ong2.DISTRIBUICAO(idpaciente,dataentrega, local, "
            + "cesta, leite) VALUES(?, ?, ?, ?, ?)RETURNING id;";
    private final String deleteQuery = "UPDATE ong2.DISTRIBUICAO SET status = false WHERE id = ?; ";
    private final String updateQuery = "UPDATE ong2.DISTRIBUICAO SET dataentrega = ?, local= ?, cesta = ?, leite = ? WHERE id = ?; ";
    private final String countQuery = "SELECT COUNT(*) FROM ong2.DISTRIBUICAO;";
    private final String readQueryWithoutDeleted = "SELECT * FROM ong2.DISTRIBUICAO WHERE idpaciente = ? and status = true;";
    private final String readQueryWithDeleted = "SELECT * FROM ong2.DISTRIBUICAO WHERE idpaciente = ?;";
    private final String readAll = "SELECT * FROM ong2.DISTRIBUICAO WHERE status = true;";
    private final String readAllArq = "SELECT * FROM ong2.DISTRIBUICAO;";
    private final String readQuery  = "SELECT * FROM ONG2.Distribuicao where id = ?;";
    PreparedStatement statement;

    //private final String 
    public DistribuicaoDAO(Connection connection) {
        super(connection);
    }

    public int contar() {
        try {
            statement = connection.prepareStatement(countQuery);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                return result.getInt("count");
            }
        } catch (SQLException ex) {
            Logger.getLogger(PacienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public int createQuery(Distribuicao obj) {
        DAOFactory daoFactory = null;
        try {
            daoFactory = new DAOFactory();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DistribuicaoDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DistribuicaoDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(DistribuicaoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        ItemDAO itemDao = daoFactory.getItemDAO();
        try {
            statement = connection.prepareStatement(createQuery);
            statement.setInt(1, obj.getPaciente());
            statement.setDate(2, obj.getData());
            statement.setString(3, obj.getLocal());
            statement.setBoolean(4, obj.isCesta());
            statement.setBoolean(5, obj.isLeite());
            ResultSet result = null;
            /*if(itemDao.check(obj.getItem()))
            {
                result = statement.executeQuery();    
                itemDao.desconta(obj.getItem());
            }
            else{
                System.out.println("Não existe a quantidade do alimento");
            }*/
            result = statement.executeQuery();
            return result.getInt("id");
        } catch (SQLException ex) {
            Logger.getLogger(DistribuicaoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    @Override
    public Distribuicao read(int id) {
        Distribuicao dist = new Distribuicao();
        ResultSet res;
        try {
            statement = connection.prepareStatement(readQuery);
            statement.setInt(1, id);
            ResultSet result = statement.executeQuery();
            List<Item> items = new ArrayList<Item>();
            Item item;
            PacienteDAO pacienteDao = null;
            try {
                DAOFactory daoFactory = new DAOFactory();
                pacienteDao = daoFactory.getPacienteDAO();

            } catch (ClassNotFoundException | IOException | SQLException ex) {
                Logger.getLogger(HospedagemDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (result.next()) {
                dist.setData(result.getDate("dataentrega"));
                dist.setId(result.getInt("id"));
                dist.setCesta(result.getBoolean("cesta"));
                dist.setLeite(result.getBoolean("leite"));
                dist.setLocal(result.getString("local"));
                dist.setPaciente(result.getInt("idpaciente"));
                Paciente pac = pacienteDao.read(dist.getPaciente());
                dist.setNome(pac.getPNome() + " " + pac.getSnome());
                dist.setStatus(result.getBoolean("status"));
                return dist;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DistribuicaoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public void update(Distribuicao obj) {
        try {
            statement = connection.prepareStatement(updateQuery);

            statement.setDate(1, obj.getData());
            statement.setString(2, obj.getLocal());
            statement.setBoolean(3, obj.isCesta());
            statement.setBoolean(4, obj.isLeite());
            statement.setInt(5, obj.getId());
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DistribuicaoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void delete(int id) {

        try {
            statement = connection.prepareStatement(deleteQuery);
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DistribuicaoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public List<Distribuicao> all() {
        List<Distribuicao> distribuicoes = new ArrayList<>();
        Distribuicao distribuicao;
        ResultSet result;
        try {
            statement = connection.prepareStatement(readAll);
            result = statement.executeQuery();
            PacienteDAO pacienteDao = null;
            try {
                DAOFactory daoFactory = new DAOFactory();
                pacienteDao = daoFactory.getPacienteDAO();

            } catch (ClassNotFoundException | IOException | SQLException ex) {
                Logger.getLogger(HospedagemDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
            while (result.next()) {
                distribuicao = new Distribuicao();
                distribuicao.setCesta(result.getBoolean("cesta"));
                distribuicao.setData(result.getDate("dataentrega"));
                distribuicao.setId(result.getInt("id"));
                distribuicao.setLeite(result.getBoolean("leite"));
                distribuicao.setLocal(result.getString("local"));
                distribuicao.setPaciente(result.getInt("idpaciente"));
                Paciente pac = pacienteDao.read(distribuicao.getPaciente());
                distribuicao.setNome(pac.getPNome() + " " + pac.getSnome());
                distribuicao.setStatus(result.getBoolean("status"));
                distribuicoes.add(distribuicao);
            }
            return distribuicoes;
        } catch (SQLException ex) {
            Logger.getLogger(DistribuicaoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public List<Distribuicao> allArq() {
        List<Distribuicao> distribuicoes = new ArrayList<>();
        Distribuicao distribuicao;
        ResultSet result;
        PacienteDAO pacienteDao = null;
        try {
            DAOFactory daoFactory = new DAOFactory();
            pacienteDao = daoFactory.getPacienteDAO();

        } catch (ClassNotFoundException | IOException | SQLException ex) {
            Logger.getLogger(HospedagemDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            statement = connection.prepareStatement(readAllArq);
            result = statement.executeQuery();
            while (result.next()) {
                distribuicao = new Distribuicao();
                distribuicao.setCesta(result.getBoolean("cesta"));
                distribuicao.setData(result.getDate("dataentrega"));
                distribuicao.setId(result.getInt("id"));
                distribuicao.setLeite(result.getBoolean("leite"));
                distribuicao.setLocal(result.getString("local"));
                distribuicao.setPaciente(result.getInt("idpaciente"));
                Paciente pac = pacienteDao.read(distribuicao.getPaciente());
                distribuicao.setNome(pac.getPNome() + " " + pac.getSnome());
                distribuicao.setStatus(result.getBoolean("status"));
                distribuicoes.add(distribuicao);
            }
            return distribuicoes;
        } catch (SQLException ex) {
            Logger.getLogger(DistribuicaoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public void create(Distribuicao obj) {
        try {
            statement = connection.prepareStatement(createQuery);
            statement.setInt(1, obj.getPaciente());
            statement.setDate(2, obj.getData());
            statement.setString(3, obj.getLocal());
            statement.setBoolean(4, obj.isCesta());
            statement.setBoolean(5, obj.isLeite());
            ResultSet result = null;
            result = statement.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(DistribuicaoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<Distribuicao> readByNameWithDeleted(String searchParam) {
        List<Distribuicao> distribuicoes = new ArrayList<>();
        List<Paciente> pacientes = new ArrayList<>();
        PreparedStatement statement;
        Distribuicao distribuicao;
        PacienteDAO pacienteDao;
        try {
            DAOFactory daoFactory = new DAOFactory();
            pacienteDao = daoFactory.getPacienteDAO();
            pacientes = pacienteDao.readByName(searchParam);

        } catch (ClassNotFoundException | IOException | SQLException ex) {
            Logger.getLogger(HospedagemDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            statement = connection.prepareStatement(readQueryWithDeleted);
            for (Paciente paciente : pacientes) {
                statement.setInt(1, paciente.getId());
                System.out.println(statement.toString());
                ResultSet result = statement.executeQuery();
                while (result.next()) {
                    distribuicao = new Distribuicao();
                    distribuicao.setCesta(result.getBoolean("cesta"));
                    distribuicao.setData(result.getDate("dataentrega"));
                    distribuicao.setId(result.getInt("id"));
                    distribuicao.setLeite(result.getBoolean("leite"));
                    distribuicao.setLocal(result.getString("local"));
                    distribuicao.setId(result.getInt("idpaciente"));
                    distribuicao.setNome(paciente.getPNome()+" "+paciente.getSnome());
                    distribuicao.setStatus(result.getBoolean("status"));
                    distribuicoes.add(distribuicao);
                }
            }
            return distribuicoes;
        } catch (SQLException ex) {
            Logger.getLogger(DoacaoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    public List<Distribuicao> readByNameWithoutDeleted(String searchParam) {
        List<Distribuicao> distribuicoes = new ArrayList<>();
        List<Paciente> pacientes = new ArrayList<>();
        PreparedStatement statement;
        Distribuicao distribuicao;
        PacienteDAO pacienteDao;
        try {
            DAOFactory daoFactory = new DAOFactory();
            pacienteDao = daoFactory.getPacienteDAO();
            pacientes = pacienteDao.readByName(searchParam);

        } catch (ClassNotFoundException | IOException | SQLException ex) {
            Logger.getLogger(HospedagemDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            statement = connection.prepareStatement(readQueryWithoutDeleted);
            for (Paciente paciente : pacientes) {
                statement.setInt(1, paciente.getId());
                System.out.println(statement.toString());
                ResultSet result = statement.executeQuery();
                while (result.next()) {
                    distribuicao = new Distribuicao();
                    distribuicao.setCesta(result.getBoolean("cesta"));
                    distribuicao.setData(result.getDate("dataentrega"));
                    distribuicao.setId(result.getInt("id"));
                    distribuicao.setLeite(result.getBoolean("leite"));
                    distribuicao.setLocal(result.getString("local"));
                    distribuicao.setId(result.getInt("idpaciente"));
                    distribuicao.setNome(paciente.getPNome()+" "+paciente.getSnome());
                    distribuicao.setStatus(result.getBoolean("status"));
                    distribuicoes.add(distribuicao);
                }
            }
            
            return distribuicoes;
        } catch (SQLException ex) {
            Logger.getLogger(DoacaoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

}
