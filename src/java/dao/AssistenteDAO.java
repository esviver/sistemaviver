/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Assistente;

/**
 *
 * @author matheus
 */
public class AssistenteDAO extends DAO<Assistente>{
    private final String createQuery = "INSERT INTO ong2.assistente_social(cress, idusuario)" 
    + "VALUES (?, ?) RETURNING id;";
    
    private final String readQueryByIdUsuario = "SELECT * FROM ong2.assistente_social WHERE idusuario = ?;";
    
    private final String updateQueryByIdUsuario = "UPDATE ong2.assistente_social SET cress = ?" +
                                                    " WHERE idusuario = ?;";
    
    private final String allQuery = " SELECT * FROM ong2.assistente_social;";
    
    public AssistenteDAO(Connection connection) {
        super(connection);
    }

    @Override
    public void create(Assistente ass)throws SQLException {
        try {
            PreparedStatement statement = connection.prepareStatement(createQuery);
            
            statement.setString(1, ass.getCress());
            statement.setInt(2, ass.getIdUsuario());
                        
            ResultSet result = statement.executeQuery();
            
            if (result.next()) {
                ass.setIdAss(result.getInt("id"));
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
            System.out.println(ex.getMessage());
            if(ex.getMessage().contains("pk_ass")){
               throw new SQLException("Erro ao cadastrar usuário: assistente social já existente");
            }else{
               throw new SQLException("Erro ao cadastrar usuário"); 
            }
        }
    }

    @Override
    public Assistente read(int id) {
        try {
            PreparedStatement statement = connection.prepareStatement(readQueryByIdUsuario);
            statement.setInt(1, id);
            
             ResultSet result = statement.executeQuery();
            
            Assistente ass = new Assistente();
            
            if (result.next()) {
                ass.setCress(result.getString("cress"));
                ass.setIdUsuario(result.getInt("idusuario"));
                ass.setIdAss(result.getInt("id"));
            }
            
            return ass;
        } catch (SQLException ex) {
            Logger.getLogger(AssistenteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public void update(Assistente ass) {
        try {
            PreparedStatement statement = connection.prepareStatement(updateQueryByIdUsuario);
            
            statement.setString(1, ass.getCress());
            statement.setInt(2, ass.getIdUsuario());
            
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AssistenteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void delete(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Assistente> all() {
        try {
            PreparedStatement statement = connection.prepareStatement(allQuery);
            List<Assistente> assistentes =  new ArrayList();
            Assistente assistente;
            
            ResultSet result =  statement.executeQuery();
            
            while(result.next()){
                assistente = new Assistente();
                
                assistente.setIdAss(result.getInt("id"));
                assistente.setIdUsuario(result.getInt("idusuario"));
                assistente.setCress(result.getString("cress"));
                
                assistentes.add(assistente);
            }
            return assistentes;
        } catch (SQLException ex) {
            Logger.getLogger(AssistenteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }

   
}