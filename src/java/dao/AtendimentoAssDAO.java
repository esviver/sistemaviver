/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.AtendAss;

/**
 *
 * @author matheus
 */
public class AtendimentoAssDAO extends DAO<AtendAss> {
    private final String createQuery = "INSERT INTO ong2.atendimento_ass(idassistente, data_at, relatorio, idpaciente, hora)"
                                    + " VALUES(?, ?, ?, ?, ?) RETURNING id;";
    private final String readQuery = "SELECT * FROM ong2.atendimento_ass"
                                  + " WHERE id = ?;";
    private final String allQuery = "SELECT * FROM ong2.atendimento_ass;";
    private final String readByidPaciente = "SELECT * FROM ong2.atendimento_ass WHERE idpaciente = ?;";
    private final String countQuery = "SELECT COUNT(*) FROM ong2.atendimento_ass;";
    public AtendimentoAssDAO(Connection connection) {
        super(connection);
    }
  public int contar()
    {
        try {
            PreparedStatement statement = connection.prepareStatement(countQuery);
            ResultSet result = statement.executeQuery();
            if(result.next())
                return result.getInt("count");
        } catch (SQLException ex) {
            Logger.getLogger(PacienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
    @Override
    public void create(AtendAss atendAss) {
        try {
            PreparedStatement statement = connection.prepareStatement(createQuery);
            statement.setInt(1,atendAss.getIdAss());
            statement.setDate(2, atendAss.getDataAtendimento());
            statement.setString(3,atendAss.getRelatorio());
            statement.setInt(4, atendAss.getIdPaciente());
            statement.setTime(5, atendAss.getHora());
            
            ResultSet result = statement.executeQuery();
            
            if(result.next()){
                atendAss.setId(result.getInt("id"));
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(AtendimentoAssDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public AtendAss read(int id) {
        try {
            PreparedStatement statement = connection.prepareStatement(readQuery);
            statement.setInt(1, id);
            ResultSet result = statement.executeQuery();
            AtendAss atendAss =  new AtendAss();
            if(result.next()){
                atendAss.setId(result.getInt("id"));
                atendAss.setDataAtendimento(result.getDate("data_at"));
                atendAss.setHora(result.getTime("hora"));
                atendAss.setIdAss(result.getInt("idassistente"));
                atendAss.setIdPaciente(result.getInt("idpaciente"));
                atendAss.setRelatorio(result.getString("relatorio"));
                
                return atendAss;
            }
        } catch (SQLException ex) {
            Logger.getLogger(AtendimentoAssDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public void update(AtendAss obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<AtendAss> all() {
        try {
            PreparedStatement statement = connection.prepareStatement(allQuery);
            List<AtendAss> atendimentos = new ArrayList();
            AtendAss atendAss;
            ResultSet result = statement.executeQuery();
            while(result.next()){
                atendAss = new AtendAss();
                
                atendAss.setId(result.getInt("id"));
                atendAss.setDataAtendimento(result.getDate("data_at"));
                atendAss.setHora(result.getTime("hora"));
                atendAss.setIdAss(result.getInt("idassistente"));
                atendAss.setIdPaciente(result.getInt("idpaciente"));
                atendAss.setRelatorio(result.getString("relatorio"));
                
                atendimentos.add(atendAss);
                
            }
            
            return atendimentos;
        } catch (SQLException ex) {
            Logger.getLogger(AtendimentoAssDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public List<AtendAss> readByidPaciente(int idPaciente){
        try {
            PreparedStatement statement = connection.prepareStatement(readByidPaciente);
            statement.setInt(1, idPaciente);
            
            List<AtendAss> atendimentos = new ArrayList();
            AtendAss atendAss;
            ResultSet result = statement.executeQuery();
            while(result.next()){
                atendAss = new AtendAss();
                
                atendAss.setId(result.getInt("id"));
                atendAss.setDataAtendimento(result.getDate("data_at"));
                atendAss.setHora(result.getTime("hora"));
                atendAss.setIdAss(result.getInt("idassistente"));
                atendAss.setIdPaciente(result.getInt("idpaciente"));
                atendAss.setRelatorio(result.getString("relatorio"));
                
                atendimentos.add(atendAss);
                
            }
            
            return atendimentos;
            
            
        } catch (SQLException ex) {
            Logger.getLogger(AtendimentoAssDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
}
