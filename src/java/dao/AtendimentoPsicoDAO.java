/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.AtendPsico;

/**
 *
 * @author matheus
 */
public class AtendimentoPsicoDAO extends DAO<AtendPsico>{
    private final String createQuery = "INSERT INTO ong2.atendimento_ps(idpsicologa, data_at, queixa,relatorio, idpaciente, hora, patologia)"
                                    + " VALUES(?, ?, ?, ?, ?, ?, ?) RETURNING id;";
    private final String readQuery = "SELECT * FROM ong2.atendimento_ps WHERE id = ?;";
    private final String allQuery = "SELECT * FROM ong2.atendimento_ps;";
    private final String countQuery = "SELECT COUNT(*) FROM ong2.atendimento_ps;";
    private final String readByidPaciente =  "SELECT * FROM ong2.atendimento_ps WHERE idpaciente = ?;";
    public AtendimentoPsicoDAO(Connection connection) {
        super(connection);
    }
  public int contar()
    {
        try {
            PreparedStatement statement = connection.prepareStatement(countQuery);
            ResultSet result = statement.executeQuery();
            if(result.next())
                return result.getInt("count");
        } catch (SQLException ex) {
            Logger.getLogger(PacienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
    @Override
    public void create(AtendPsico atendPsico) {
        try {
            PreparedStatement statement = connection.prepareStatement(createQuery);
            statement.setInt(1,atendPsico.getIdPsicologa());
            statement.setDate(2, atendPsico.getDataAtendimento());
            statement.setString(3,atendPsico.getQueixa());
            statement.setString(4, atendPsico.getAtendimento());
            statement.setInt(5, atendPsico.getIdPaciente());
            statement.setTime(6, atendPsico.getHora());
            statement.setString(7, atendPsico.getPatologia());
            
            ResultSet result = statement.executeQuery();
            
            if(result.next()){
                atendPsico.setId(result.getInt("id"));
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(AtendimentoPsicoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public AtendPsico read(int id) {
         try {
            PreparedStatement statement = connection.prepareStatement(readQuery);
            statement.setInt(1, id);
            ResultSet result = statement.executeQuery();
            AtendPsico atendPsico =  new AtendPsico();
            if(result.next()){
                atendPsico.setId(result.getInt("id"));
                atendPsico.setDataAtendimento(result.getDate("data_at"));
                atendPsico.setHora(result.getTime("hora"));
                atendPsico.setIdPsicologa(result.getInt("idpsicologa"));
                atendPsico.setIdPaciente(result.getInt("idpaciente"));
                atendPsico.setPatologia(result.getString("patologia"));
                atendPsico.setQueixa(result.getString("queixa"));
                atendPsico.setAtendimento(result.getString("relatorio"));
                return atendPsico;
            }
        } catch (SQLException ex) {
            Logger.getLogger(AtendimentoPsicoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public void update(AtendPsico obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<AtendPsico> all() {
       try {
            PreparedStatement statement = connection.prepareStatement(allQuery);
            List<AtendPsico> atendimentos = new ArrayList();
            AtendPsico atendPsico;
            ResultSet result = statement.executeQuery();
            while(result.next()){
                atendPsico = new AtendPsico();
                
                atendPsico.setId(result.getInt("id"));
                atendPsico.setDataAtendimento(result.getDate("data_at"));
                atendPsico.setHora(result.getTime("hora"));
                atendPsico.setIdPsicologa(result.getInt("idpsicologa"));
                atendPsico.setIdPaciente(result.getInt("idpaciente"));
                atendPsico.setPatologia(result.getString("patologia"));
                atendPsico.setQueixa(result.getString("queixa"));
                atendPsico.setAtendimento(result.getString("relatorio"));
                
                atendimentos.add(atendPsico);
                
            }
            
            return atendimentos;
        } catch (SQLException ex) {
            Logger.getLogger(AtendimentoPsicoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public List<AtendPsico> readByidPaciente(int idPaciente){
        try {
            PreparedStatement statement = connection.prepareStatement(readByidPaciente);
            statement.setInt(1, idPaciente);
            
            List<AtendPsico> atendimentos = new ArrayList();
            AtendPsico atendPsico;
            ResultSet result = statement.executeQuery();
            while(result.next()){
                atendPsico = new AtendPsico();
                
                atendPsico.setId(result.getInt("id"));
                atendPsico.setDataAtendimento(result.getDate("data_at"));
                atendPsico.setHora(result.getTime("hora"));
                atendPsico.setIdPsicologa(result.getInt("idpsicologa"));
                atendPsico.setIdPaciente(result.getInt("idpaciente"));
                atendPsico.setPatologia(result.getString("patologia"));
                atendPsico.setQueixa(result.getString("queixa"));
                atendPsico.setAtendimento(result.getString("relatorio"));
                
                atendimentos.add(atendPsico);
                
            }
            
            return atendimentos;
            
        } catch (SQLException ex) {
            Logger.getLogger(AtendimentoPsicoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
}
