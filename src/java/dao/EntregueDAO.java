/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Entregue;
import model.Item;



/**
 *
 * @author Auxiliar
 */
public class EntregueDAO extends DAO<Entregue>{
private final String addItemEntrega = "INSERT INTO ong2.ENTREGUE(iditem, qtde, identrega) VALUES(?, ?, ?);";
private final String getItens = "SELECT * FROM ong2.ENTREGUE WHERE identrega = ?;";
private final String deleteQueryItens = "DELETE FROM ong2.ENTREGUE WHERE identrega = ?;";
private final String readItems = "SELECT FROM ong2.ENTREGUE WHERE identrega = ?;";
    public EntregueDAO(Connection connection) {
        super(connection);
    }

    public void createQuery(List<Entregue> obj) {
        PreparedStatement statement = null;
        for (Entregue iten : obj) {
          try {
            statement = connection.prepareStatement(addItemEntrega);
            statement.setInt(1,iten.getIditem());
            statement.setInt(2,iten.getQtde());
            statement.setInt(3, iten.getIdentrega());
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(EntregueDAO.class.getName()).log(Level.SEVERE, null, ex);
        }  
        }
    }
    public List<Entregue> readQuery(int id)
    {
    
    List<Entregue> entregue = new ArrayList<Entregue>();
    try {
        PreparedStatement statement = connection.prepareStatement(getItens);
        statement.setInt(1, id);
        ResultSet result = statement.executeQuery();
        Entregue ent = null;
        while(result.next())
        {
            ent = new Entregue();
            ent.setIdentrega(result.getInt("identrega"));
            ent.setIditem(result.getInt("iditem"));
            ent.setQtde(result.getInt("qtde"));
            entregue.add(ent);
        }
        return entregue;
    } catch (SQLException ex) {
        Logger.getLogger(EntregueDAO.class.getName()).log(Level.SEVERE, null, ex);
    }
        
        return null;
    }

    @Override
    public Entregue read(int id) {
    List<Item>  item = new ArrayList<Item>();

       return null;
    }

    @Override
    public void update(Entregue obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Entregue> all() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void create(Entregue obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
