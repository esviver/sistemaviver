/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Doacao;

/**
 *
 * @author Auxiliar
 */
public class DoacaoDAO extends DAO<Doacao>{
    private final String createQuery = "INSERT INTO ong2.DOACAO(doador, telefonedoador, celulardoador, anonimo, dia, dinheiro, "
            + "isdinheiro, isoutro, outro, descricao) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?) RETURNING id;";
    private final String deleteQuery = "UPDATE ong2.DOACAO SET status = false WHERE id = ?;";
    private final String updateQuery = "UPDATE ong2.DOACAO SET doador = ?, telefonedoador = ?, celulardoador = ?, anonimo = ?,"
            + " dia = ?, dinheiro = ?, isdinheiro = ?, isoutro = ?, outro = ?, descricao = ? WHERE id = ?;";
    private final String readQuery = "SELECT * FROM ong2.DOACAO WHERE id = ?;";
    private final String readQueryByNameWithLike = "SELECT * FROM ong2.DOACAO WHERE doador LIKE ?;";
    private final String readQueryByNameWithoutDeletedWithLike = "SELECT * FROM ong2.DOACAO WHERE doador LIKE ? AND status = true;";
    private final String allQuery = "SELECT * FROM ong2.DOACAO;";
    private final String allQuery2 = "SELECT * FROM ong2.DOACAO WHERE status=true;";
    private final String recoverQuery = "UPDATE ong2.DOACAO SET status = true WHERE id = ?;";
    private final String countQuery = "SELECT COUNT(*) FROM ong2.DOACAO;";
    
    public DoacaoDAO(Connection connection) {
        super(connection);
    }
  public int contar()
    {
        try {
            PreparedStatement statement = connection.prepareStatement(countQuery);
            ResultSet result = statement.executeQuery();
            if(result.next())
                return result.getInt("count");
        } catch (SQLException ex) {
            Logger.getLogger(PacienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
    @Override
    public void create(Doacao obj) {
         try {
            PreparedStatement statement = connection.prepareStatement(createQuery);
            //doador, telefonedoador, celulardoador, anonimo, dia, dinheiro, isdinheiro, isoutro, outro, descricao
            statement.setString(1, obj.getDoador());
            statement.setString(2, obj.getTelDoador());
            statement.setString(3, obj.getCelDoador());
            statement.setBoolean(4, obj.isDoadorAnon());
            statement.setDate(5, (Date) obj.getData());
            statement.setFloat(6, obj.getDinheiro());
            statement.setBoolean(7, obj.isIsDinheiro());
            statement.setBoolean(8,obj.isIsOutro());
            statement.setString(9, obj.getOutro());
            statement.setString(10, obj.getDescricao());
            ResultSet result = statement.executeQuery();    
            
            if (result.next()) {
                obj.setId(result.getInt("id"));
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(DistribuicaoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public Doacao read(int id) {
        Doacao doacao = new Doacao();
        try {
            PreparedStatement statement = connection.prepareStatement(readQuery);
            statement.setInt(1, id);
            ResultSet result = statement.executeQuery();
            if(result.next()){
                doacao.setCelDoador(result.getString("celulardoador"));
                doacao.setTelDoador(result.getString("telefonedoador"));
                doacao.setDoador(result.getString("doador"));
                doacao.setData(result.getDate("dia"));
                doacao.setDescricao(result.getString("descricao"));
                doacao.setDinheiro(result.getFloat("dinheiro"));
                doacao.setDoadorAnon(result.getBoolean("anonimo"));
                doacao.setIsDinheiro(result.getBoolean("isdinheiro"));
                doacao.setIsOutro(result.getBoolean("isoutro"));
                doacao.setOutro(result.getString("outro"));
                doacao.setId(result.getInt("id"));
                doacao.setStatus(result.getBoolean("status"));
            }
            return doacao;
        } catch (SQLException ex) {
            Logger.getLogger(DoacaoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public List<Doacao> readByName(String name) {
        List<Doacao> doacoes = new ArrayList<>();
         PreparedStatement statement;
         Doacao doacao;
        try {
            statement = connection.prepareStatement(readQueryByNameWithLike);
            statement.setString(1, "%" + name + "%");
            ResultSet result = statement.executeQuery();
            while(result.next())
            {
                doacao = new Doacao();
                doacao.setCelDoador(result.getString("celulardoador"));
                doacao.setTelDoador(result.getString("telefonedoador"));
                doacao.setDoador(result.getString("doador"));
                doacao.setData(result.getDate("dia"));
                doacao.setDescricao(result.getString("descricao"));
                doacao.setDinheiro(result.getFloat("dinheiro"));
                doacao.setDoadorAnon(result.getBoolean("anonimo"));
                doacao.setIsDinheiro(result.getBoolean("isdinheiro"));
                doacao.setIsOutro(result.getBoolean("isoutro"));
                doacao.setId(result.getInt("id"));
                doacao.setOutro(result.getString("outro"));
                doacao.setStatus(result.getBoolean("status"));
                doacoes.add(doacao);
            }
            return doacoes;
        } catch (SQLException ex) {
            Logger.getLogger(DoacaoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public List<Doacao> readByNameWithoutDeleted(String name) {
        List<Doacao> doacoes = new ArrayList<>();
         PreparedStatement statement;
         Doacao doacao;
        try {
            statement = connection.prepareStatement(readQueryByNameWithoutDeletedWithLike);
            statement.setString(1, "%" + name + "%");
            ResultSet result = statement.executeQuery();
            while(result.next())
            {
                doacao = new Doacao();
                doacao.setCelDoador(result.getString("celulardoador"));
                doacao.setTelDoador(result.getString("telefonedoador"));
                doacao.setDoador(result.getString("doador"));
                doacao.setData(result.getDate("dia"));
                doacao.setDescricao(result.getString("descricao"));
                doacao.setDinheiro(result.getFloat("dinheiro"));
                doacao.setDoadorAnon(result.getBoolean("anonimo"));
                doacao.setIsDinheiro(result.getBoolean("isdinheiro"));
                doacao.setIsOutro(result.getBoolean("isoutro"));
                doacao.setId(result.getInt("id"));
                doacao.setOutro(result.getString("outro"));
                doacao.setStatus(result.getBoolean("status"));
                doacoes.add(doacao);
            }
            return doacoes;
        } catch (SQLException ex) {
            Logger.getLogger(DoacaoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public void update(Doacao obj) {
        try {
            PreparedStatement statement = connection.prepareStatement(updateQuery);
            
            statement.setString(1, obj.getDoador());
            statement.setString(2, obj.getTelDoador());
            statement.setString(3, obj.getCelDoador());
            statement.setBoolean(4, obj.isDoadorAnon());
            statement.setDate(5, (Date) obj.getData());
            statement.setFloat(6, obj.getDinheiro());
            statement.setBoolean(7, obj.isIsDinheiro());
            statement.setBoolean(8,obj.isIsOutro());
            statement.setString(9, obj.getOutro());
            statement.setString(10, obj.getDescricao());
            statement.setInt(11, obj.getId());
            
            statement.executeUpdate();    
            
        } catch (SQLException ex) {
            Logger.getLogger(DistribuicaoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void delete(int id) {
        PreparedStatement statement;
        try {
            statement = connection.prepareStatement(deleteQuery);
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DoacaoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void recover(int id) {
        PreparedStatement statement;
        try {
            statement = connection.prepareStatement(recoverQuery);
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DoacaoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public List<Doacao> all() {
        List<Doacao> doacoes = new ArrayList<>();
         PreparedStatement statement;
         Doacao doacao;
        try {
            statement = connection.prepareStatement(allQuery);
            ResultSet result = statement.executeQuery();
            while(result.next())
            {
            doacao = new Doacao();
            doacao.setCelDoador(result.getString("celulardoador"));
            doacao.setTelDoador(result.getString("telefonedoador"));
            doacao.setDoador(result.getString("doador"));
            doacao.setData(result.getDate("dia"));
            doacao.setDescricao(result.getString("descricao"));
            doacao.setDinheiro(result.getFloat("dinheiro"));
            doacao.setDoadorAnon(result.getBoolean("anonimo"));
            doacao.setIsDinheiro(result.getBoolean("isdinheiro"));
            doacao.setIsOutro(result.getBoolean("isoutro"));
            doacao.setId(result.getInt("id"));
            doacao.setOutro(result.getString("outro"));
            doacao.setStatus(result.getBoolean("status"));
            doacoes.add(doacao);
            }
            return doacoes;
        } catch (SQLException ex) {
            Logger.getLogger(DoacaoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public List<Doacao> allWithoutDeleted() {
        List<Doacao> doacoes = new ArrayList<>();
         PreparedStatement statement;
         Doacao doacao;
        try {
            statement = connection.prepareStatement(allQuery2);
            ResultSet result = statement.executeQuery();
            while(result.next())
            {
            doacao = new Doacao();
            doacao.setCelDoador(result.getString("celulardoador"));
            doacao.setTelDoador(result.getString("telefonedoador"));
            doacao.setDoador(result.getString("doador"));
            doacao.setData(result.getDate("dia"));
            doacao.setDescricao(result.getString("descricao"));
            doacao.setDinheiro(result.getFloat("dinheiro"));
            doacao.setDoadorAnon(result.getBoolean("anonimo"));
            doacao.setIsDinheiro(result.getBoolean("isdinheiro"));
            doacao.setIsOutro(result.getBoolean("isoutro"));
            doacao.setId(result.getInt("id"));
            doacao.setOutro(result.getString("outro"));
            doacao.setStatus(result.getBoolean("status"));
            doacoes.add(doacao);
            }
            return doacoes;
        } catch (SQLException ex) {
            Logger.getLogger(DoacaoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
