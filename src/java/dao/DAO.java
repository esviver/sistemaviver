package dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public abstract class DAO<T> {
    
    protected Connection connection;
    
    public DAO(Connection connection) {
        this.connection = connection;
    }
    
    public abstract void create(T obj) throws SQLException, NotImplementedException;
    
    public abstract T read(int id)throws SQLException;
    
    public abstract void update(T obj)throws SQLException;
    
    public abstract void delete(int id)throws SQLException;
    
    public abstract List<T> all()throws SQLException;
        
}
