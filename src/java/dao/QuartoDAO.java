package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Quarto;

public class QuartoDAO extends DAO<Quarto> {

    private final String createQuery = "INSERT INTO ong2.quarto(id, capacidade, status, ocupado) "
            + "VALUES (?, ?, true, false);";
    
    private final String readQuery = "SELECT * FROM ong2.quarto WHERE id = ?;";
    
    private final String updateQuery = "UPDATE ong2.quarto "
                                     + "SET capacidade = ?, ocupado = ? "
                                     + "WHERE id = ?;";
    
    private final String deleteQuery  = "UPDATE ong2.quarto SET status = false WHERE id = ?;";
    
    private final String recoverQuery  = "UPDATE ong2.quarto SET status = true WHERE id = ?;";
    
    private final String allQuery = "SELECT * FROM ong2.quarto;";
    
    private final String readQueryIfNotDeleted = "SELECT * FROM ong2.quarto WHERE id = ? AND status = true;";
    
    private final String allQueryWithoutDeleted = "SELECT * FROM ong2.quarto WHERE status=true;";
        
    public QuartoDAO(Connection connection) {
        super(connection);
    }

    @Override
    public void create(Quarto qua)throws SQLException {
        try {
            PreparedStatement statement = connection.prepareStatement(createQuery);
            
            statement.setInt(1, qua.getId());
            statement.setInt(2, qua.getCapacidade());
                        
            ResultSet result = statement.executeQuery();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
            System.out.println(ex.getMessage());
            if(ex.getMessage().contains("pk_quarto")){
                throw new SQLException("Erro ao cadastrar quarto: quarto já existente.");
            }
        }
    }

    @Override
    public Quarto read(int id) {
        try {
            PreparedStatement statement = connection.prepareStatement(readQuery);
            
            statement.setInt(1, id);
            
            ResultSet result = statement.executeQuery();
            
            Quarto qua = new Quarto();
            
            if (result.next()) {
                qua.setId(result.getInt("id"));
                qua.setCapacidade(result.getInt("capacidade"));
                qua.setStatus(result.getBoolean("status"));
                qua.setOcupado(result.getBoolean("ocupado"));
            }
            
            return qua;
        } catch (SQLException ex) {
            Logger.getLogger(QuartoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    public Quarto readIfNotDeleted(int id) {
        try {
            PreparedStatement statement = connection.prepareStatement(readQueryIfNotDeleted);
            
            statement.setInt(1, id);
            
            ResultSet result = statement.executeQuery();
            
            Quarto qua = new Quarto();
            
            if (result.next()) {
                qua.setId(result.getInt("id"));
                qua.setCapacidade(result.getInt("capacidade"));
                qua.setStatus(result.getBoolean("status"));
                qua.setOcupado(result.getBoolean("ocupado"));
            }
            
            return qua;
        } catch (SQLException ex) {
            Logger.getLogger(QuartoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }

    @Override
    public void update(Quarto qua) {
        try {
            PreparedStatement statement = connection.prepareStatement(updateQuery);
            
            statement.setInt(1, qua.getCapacidade());
            statement.setBoolean(2, qua.isOcupado());
            
            statement.setInt(3, qua.getId());
                        
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(QuartoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void delete(int id) {
        try {
            PreparedStatement statement = connection.prepareStatement(deleteQuery);
            
            statement.setInt(1, id);
                        
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(QuartoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void recover(int id) {
        try {
            PreparedStatement statement = connection.prepareStatement(recoverQuery);
            
            statement.setInt(1, id);
                        
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(QuartoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<Quarto> all() {
        try {
            PreparedStatement statement = connection.prepareStatement(allQuery);
            
            ResultSet result = statement.executeQuery();
            
            List<Quarto> quartos = new ArrayList<>();
            
            Quarto qua;
            
            while (result.next()) {
                qua = new Quarto();
                
                qua.setId(result.getInt("id"));
                qua.setCapacidade(result.getInt("capacidade"));
                qua.setStatus(result.getBoolean("status"));
                qua.setOcupado(result.getBoolean("ocupado"));
                
                quartos.add(qua);
            }
            
            return quartos;
        } catch (SQLException ex) {
            Logger.getLogger(QuartoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    public List<Quarto> allWithoutDeleted() {
        try {
            PreparedStatement statement = connection.prepareStatement(allQueryWithoutDeleted);
            
            ResultSet result = statement.executeQuery();
            
            List<Quarto> quartos = new ArrayList<>();
            
            Quarto qua;
            
            while (result.next()) {
                qua = new Quarto();
                
                qua.setId(result.getInt("id"));
                qua.setCapacidade(result.getInt("capacidade"));
                qua.setStatus(result.getBoolean("status"));
                qua.setOcupado(result.getBoolean("ocupado"));
                
                quartos.add(qua);
            }
            
            return quartos;
        } catch (SQLException ex) {
            Logger.getLogger(QuartoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
}
