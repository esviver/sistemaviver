/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Responsavel;

/**
 *
 * @author lshigaki
 */
public class ResponsavelDAO extends DAO<Responsavel> {

    private final String createQuery = "INSERT INTO ong2.responsavel (datanasc, cpf, escolaridade, estadocivil, "
            + "pnome, snome, profissao, rgnum, rgorg, telefone, id_endereco, id_pac) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
            + " RETURNING id;";

    private final String updateQuery = "UPDATE ong2.responsavel SET escolaridade = ?, estadocivil = ?, "
            + "profissao = ?, telefone = ?, id_endereco = ? WHERE id = ?;";

    private final String readQuery = "SELECT * FROM ong2.responsavel WHERE pnome = ?;";
    
    private final String readQueryByIdPac = "SELECT * FROM ong2.responsavel WHERE id_pac = ?;";
    
    private final String deleteQuery = "UPDATE ong2.responsavel SET status = FALSE WHERE id = ?;";

    private final String allQuery = "SELECT * FROM ong2.responsavel;";

    public ResponsavelDAO(Connection connection) {
        super(connection);
    }

    @Override
    public void create(Responsavel r) {

        try {
            PreparedStatement statement = connection.prepareStatement(createQuery);

            statement.setDate(1, r.getDatanasc());
            statement.setString(2, r.getCpf());
            statement.setString(3, r.getEscolaridade());
            statement.setString(4, r.getEstadoCivil());
            statement.setString(5, r.getPnomeResp());
            statement.setString(6, r.getSnomeResp());
            statement.setString(7, r.getProfissao());
            statement.setString(8, r.getRgNum());
            statement.setString(9, r.getRgOrg());
            statement.setString(10, r.getTelefone());
            statement.setInt(11, r.getIdEndereco());
            statement.setInt(12, r.getId_pac());

            ResultSet result = statement.executeQuery();

            if (result.next()) {
                r.setId(result.getInt("id"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ResponsavelDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public Responsavel read(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public Responsavel readByIdPac(int id) {
        try {
            PreparedStatement statement = connection.prepareStatement(readQueryByIdPac);

            statement.setInt(1, id);
            
            ResultSet result = statement.executeQuery();

            Responsavel r = null;
            
            if (result.next()) {
                r = new Responsavel();

                r.setId(result.getInt("id"));
                r.setDatanasc(result.getDate("datanasc"));
                r.setCpf(result.getString("cpf"));
                r.setEscolaridade(result.getString("escolaridade"));
                r.setEstadoCivil(result.getString("estadocivil"));
                r.setPnomeResp(result.getString("pnome"));
                r.setSnomeResp(result.getString("snome"));
                r.setProfissao(result.getString("profissao"));
                r.setRgNum(result.getString("rgnum"));
                r.setRgOrg(result.getString("rgorg"));
                r.setTelefone(result.getString("telefone"));
                r.setIdEndereco(result.getInt("id_endereco"));
                r.setStatus(result.getBoolean("status"));
                r.setId_pac(result.getInt("id_pac"));
            }
            
            return r;
        } catch (SQLException ex) {
            Logger.getLogger(VoluntarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public void update(Responsavel r) {

        try {
            PreparedStatement statement = connection.prepareStatement(updateQuery);

            statement.setString(1, r.getEscolaridade());
            statement.setString(2, r.getEstadoCivil());
            statement.setString(3, r.getProfissao());
            statement.setString(4, r.getTelefone());
            statement.setInt(5, r.getIdEndereco());
            statement.setInt(6, r.getId());

            statement.executeQuery();

        } catch (SQLException ex) {
            Logger.getLogger(ResponsavelDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public void delete(int id) {

        try {
            PreparedStatement statement = connection.prepareStatement(deleteQuery);

            statement.setInt(1, id);

            statement.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(VoluntarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public List<Responsavel> all() {
        try {
            PreparedStatement statement = connection.prepareStatement(allQuery);

            ResultSet result = statement.executeQuery();

            ArrayList<Responsavel> rList = new ArrayList<>();
            
            Responsavel r;
            
            while (result.next()) {
                r = new Responsavel();

                r.setId(result.getInt("id"));
                r.setDatanasc(result.getDate("datanasc"));
                r.setCpf(result.getString("cpf"));
                r.setEscolaridade(result.getString("escolaridade"));
                r.setEstadoCivil(result.getString("estadocivil"));
                r.setPnomeResp(result.getString("pnome"));
                r.setSnomeResp(result.getString("snome"));
                r.setProfissao(result.getString("profissao"));
                r.setRgNum(result.getString("rgnum"));
                r.setRgOrg(result.getString("rgorg"));
                r.setTelefone(result.getString("telefone"));
                r.setIdEndereco(result.getInt("id_endereco"));
                r.setStatus(result.getBoolean("status"));
                r.setId_pac(result.getInt("id_pac"));

                rList.add(r);
            }
            
            return rList;
        } catch (SQLException ex) {
            Logger.getLogger(VoluntarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

    public Responsavel readByName(String name) {

        try {
            PreparedStatement statement = connection.prepareStatement(readQuery);

            statement.setString(1, name);

            ResultSet result = statement.executeQuery();

            if (result.next()) {
                Responsavel r = new Responsavel();

                r.setId(result.getInt("id"));
                r.setDatanasc(result.getDate("datanasc"));
                r.setCpf(result.getString("cpf"));
                r.setEscolaridade(result.getString("escolaridade"));
                r.setEstadoCivil(result.getString("estadocivil"));
                r.setPnomeResp(result.getString("pnome"));
                r.setSnomeResp(result.getString("snome"));
                r.setProfissao(result.getString("profissao"));
                r.setRgNum(result.getString("rgnum"));
                r.setRgOrg(result.getString("rgorg"));
                r.setTelefone(result.getString("telefone"));
                r.setIdEndereco(result.getInt("id_endereco"));
                r.setStatus(result.getBoolean("status"));
                r.setId_pac(result.getInt("id_pac"));
                
                return r;
            }
        } catch (SQLException ex) {
            Logger.getLogger(VoluntarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }
}
