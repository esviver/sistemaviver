/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Acompanhante;

/**
 *
 * @author Auxiliar
 */
public class AcompanhanteDAO extends DAO<Acompanhante> {
    private final String createQuery = "INSERT INTO ong2.ACOMPANHANTES(idhospedagem, idresponsavel);";
    private final String deleteQuery = "DELETE FROM ong2.ACOMPANHANTES WHERE idhospedagem = ? AND idresponsavel = ?;";
    private final String readQuery = "SELECT* FROM ong2.ACOMPANHANTES WHERE idhospedagem = ?;";
    public AcompanhanteDAO(Connection connection) {
        super(connection);
    }


    

    @Override
    public void create(Acompanhante obj) {
        PreparedStatement statement;
        try {
            statement = connection.prepareStatement(createQuery);
            statement.setInt(1, obj.getIdhospedagem());
            statement.setInt(2, obj.getIdresponsavel());
            statement.executeUpdate(); 
        } catch (SQLException ex) {
            Logger.getLogger(AcompanhanteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public List<Acompanhante> allAc(int hospedagemid)
    {
        List<Acompanhante> acompanhantes = new ArrayList<Acompanhante>();
        Acompanhante acompanhante;
        PreparedStatement statement;
        try {
            statement = connection.prepareStatement(readQuery);
            statement.setInt(1, hospedagemid);
            ResultSet result = statement.executeQuery();
            while(result.next())
            {
                acompanhante = new Acompanhante();
                acompanhante.setIdhospedagem(result.getInt("idhospedagem"));
                acompanhante.setIdresponsavel(result.getInt("idresponsavel"));
                acompanhantes.add(acompanhante);
            }
            return acompanhantes;
        } catch (SQLException ex) {
            Logger.getLogger(AcompanhanteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    @Override
    public Acompanhante read(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public void update(Acompanhante obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Acompanhante> all() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
