package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Psicologa;

public class PsicologaDAO extends DAO<Psicologa> {

    private final String createQuery = "INSERT INTO ong2.psicologa(\n" +
    "crp,\n" +
    "idusuario) \n" +
    "VALUES (?, ?) RETURNING idP;";
    
    private final String readQueryByIdUsuario = "SELECT * FROM ong2.psicologa WHERE idusuario = ?;";
    
    private final String updateQueryByIdUsuario = "UPDATE ong2.psicologa\n" +
    "SET \n" +
    "crp = ? \n" +
    "WHERE idusuario = ?;";
        
    private final String allQuery = "SELECT * FROM ong2.psicologa;";
    
    public PsicologaDAO(Connection connection) {
        super(connection);
    }

    @Override
    public void create(Psicologa Psicologa) throws SQLException  {
        try {
            PreparedStatement statement = connection.prepareStatement(createQuery);
            
            statement.setString(1, Psicologa.getCrp());
            statement.setInt(2, Psicologa.getIdUsuario());
                        
            ResultSet result = statement.executeQuery();
            
            if (result.next()) {
                Psicologa.setIdP(result.getInt("idP"));
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
            System.out.println(ex.getMessage());
            if(ex.getMessage().contains("pk_psicologa")){
                 throw new SQLException("Erro ao cadastrar usuário: psicologa já existente");
            }else{
               throw new SQLException("Erro ao cadastrar usuário: psicologa já existente"); 
            }
        }
    }

    @Override
    public Psicologa read(int id) {
        try {
            
            PreparedStatement statement = connection.prepareStatement(readQueryByIdUsuario);
            
            statement.setInt(1, id);
            
            ResultSet result = statement.executeQuery();
            
            Psicologa Psicologa = new Psicologa();
            
            if (result.next()) {
                Psicologa.setCrp(result.getString("crp"));
                Psicologa.setIdUsuario(result.getInt("idusuario"));
                Psicologa.setIdP(result.getInt("idP"));
            }
            
            return Psicologa;
        } catch (SQLException ex) {
            Logger.getLogger(PsicologaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }

    @Override
    public void update(Psicologa Psicologa) {
        try {
            PreparedStatement statement = connection.prepareStatement(updateQueryByIdUsuario);
            
            statement.setString(1, Psicologa.getCrp());
            
            statement.setInt(2, Psicologa.getIdUsuario());
                        
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(PsicologaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void delete(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Psicologa> all() {
        try {
            PreparedStatement statement = connection.prepareStatement(allQuery);
            
            ResultSet result = statement.executeQuery();
            
            List<Psicologa> Psicologas = new ArrayList<>();
            
            Psicologa Psicologa;
            
            while (result.next()) {
                Psicologa = new Psicologa();
                
                Psicologa.setCrp(result.getString("crp"));
                Psicologa.setIdUsuario(result.getInt("idusuario"));
                Psicologa.setIdP(result.getInt("idP"));
                
                Psicologas.add(Psicologa);
            }
            
            return Psicologas;
        } catch (SQLException ex) {
            Logger.getLogger(PsicologaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }

}
