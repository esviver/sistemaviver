package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Usuario;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class UsuarioDAO2 extends DAO<Usuario> {

    private final String createQuery = "INSERT INTO ong2.usuario(\n" +
    "cpf,\n" +
    "rg_num,\n" +
    "rg_org,\n" +
    "login,\n" +
    "senha,\n" +
    "pnome,\n" +
    "snome,\n" +
    "endereco,\n" +
    "telefone,\n" +
    "celular,\n" +
    "tipo_de_user,\n" +
    "nascimento,\n" +
    "sexo,\n" +
    "anotacoes,\n" +
    "status)\n" +
    "VALUES (?, ?, ?, ?, md5(?), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) RETURNING id;";
    
    private final String updateQuery = "UPDATE ong2.usuario\n" +
    "SET \n" +
    "cpf = ?,\n" +
    "rg_num = ?,\n" +
    "rg_org = ?,\n" +
    "login = ?,\n" +
    "senha = md5(?),\n" +
    "pnome = ?,\n" +
    "snome = ?,\n" +
    "endereco = ?,\n" +
    "telefone = ?,\n" +
    "celular = ?,\n" +
    "tipo_de_user = ?,\n" +
    "nascimento = ?,\n" +
    "sexo = ?,\n" +
    "anotacoes = ?,\n" +
    "status = ? \n" +
    "WHERE id = ?;";
    
    private final String deleteQuery  = "UPDATE ong2.usuario SET status = false WHERE id = ?;";
    
    private final String recoverQuery  = "UPDATE ong2.usuario SET status = true WHERE id = ?;";
    
    private final String readQuery = "SELECT * FROM ong2.usuario WHERE id = ?;";
    
    private final String readQueryByName = "SELECT * FROM ong2.usuario WHERE pnome LIKE ?;";
    
    private final String readQueryByNameWithoutDeleted = "SELECT * FROM ong2.usuario WHERE pnome LIKE ? AND status = true;";
    
    private final String allQuery = "SELECT * FROM ong2.usuario;";
    
    private final String allQueryWithoutDeleted = "SELECT * FROM ong2.usuario WHERE status = true;";
    
    private final String authenticateQuery = "SELECT * FROM ong2.usuario where senha = md5(?) and login = ?;";
    
    public UsuarioDAO2(Connection connection) {
        super(connection);
    }

    
    
    public Usuario authenticate(String login, String senha) throws SecurityException, SQLException 
    {   
        try {
            PreparedStatement statement = connection.prepareStatement(authenticateQuery);
            statement.setString(1, senha);
            statement.setString(2, login);
            ResultSet result = statement.executeQuery();
             
            Usuario usuario = new Usuario();
            
            if (result.next()) {
                
                usuario.setLogin(result.getString("login"));
                usuario.setSenha(result.getString("senha"));
                usuario.setTelefone(result.getString("telefone"));
                usuario.setCelular(result.getString("celular"));
                usuario.setId_end(result.getInt("endereco"));
                usuario.setCpf(result.getString("cpf"));
                usuario.setDataNasc(result.getDate("nascimento"));
                usuario.setSexo(result.getString("sexo"));
                usuario.setAnotacoes(result.getString("anotacoes"));
                usuario.setTipoDeUser(result.getString("tipo_de_user"));
                usuario.setPnome(result.getString("pnome"));
                usuario.setSnome(result.getString("snome"));
                usuario.setRg_num(result.getString("rg_num"));
                usuario.setRg_org(result.getString("rg_org"));
                usuario.setStatus(result.getBoolean("status"));
                usuario.setId(result.getInt("id"));
                return usuario;
            }else{
                throw new SecurityException("Login ou senha incorretos.");
            }
            
            
        }catch (SQLException ex) {
            System.err.println(ex.getMessage());
            throw new SQLException("Erro ao autenticar usuário.");
        }
        
        
    }
    @Override
    public Usuario read(int id) {
        try {
            PreparedStatement statement = connection.prepareStatement(readQuery);
            
            statement.setInt(1, id);
            
            ResultSet result = statement.executeQuery();
            
            Usuario usuario = new Usuario();
            
            if (result.next()) {
                usuario.setLogin(result.getString("login"));
                usuario.setSenha(result.getString("senha"));
                usuario.setTelefone(result.getString("telefone"));
                usuario.setCelular(result.getString("celular"));
                usuario.setId_end(result.getInt("endereco"));
                usuario.setCpf(result.getString("cpf"));
                usuario.setDataNasc(result.getDate("nascimento"));
                usuario.setSexo(result.getString("sexo"));
                usuario.setAnotacoes(result.getString("anotacoes"));
                usuario.setTipoDeUser(result.getString("tipo_de_user"));
                usuario.setPnome(result.getString("pnome"));
                usuario.setSnome(result.getString("snome"));
                usuario.setRg_num(result.getString("rg_num"));
                usuario.setRg_org(result.getString("rg_org"));
                usuario.setStatus(result.getBoolean("status"));
                usuario.setId(result.getInt("id"));
                
            }

            return usuario;
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO2.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }

    public List<Usuario> readByName(String name) {
        try {
            PreparedStatement statement = connection.prepareStatement(readQueryByName);
            
            statement.setString(1, "%" + name + "%");
            
            ResultSet result = statement.executeQuery();
            
            List<Usuario> usuarios = new ArrayList<>();
            
            Usuario usuario;
            
            while (result.next()) {
                usuario = new Usuario();
                
                usuario.setId(result.getInt("id"));
                usuario.setLogin(result.getString("login"));
                usuario.setSenha(result.getString("senha"));
                usuario.setTelefone(result.getString("telefone"));
                usuario.setCelular(result.getString("celular"));
                usuario.setId_end(result.getInt("endereco"));
                usuario.setCpf(result.getString("cpf"));
                usuario.setDataNasc(result.getDate("nascimento"));
                usuario.setSexo(result.getString("sexo"));
                usuario.setAnotacoes(result.getString("anotacoes"));
                usuario.setTipoDeUser(result.getString("tipo_de_user"));
                usuario.setPnome(result.getString("pnome"));
                usuario.setSnome(result.getString("snome"));
                usuario.setRg_num(result.getString("rg_num"));
                usuario.setRg_org(result.getString("rg_org"));
                usuario.setStatus(result.getBoolean("status"));
                
                usuarios.add(usuario);
            }
            
            return usuarios;
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO2.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    public List<Usuario> readByNameWithoutDeleted(String name) {
        try {
            PreparedStatement statement = connection.prepareStatement(readQueryByNameWithoutDeleted);
            
            statement.setString(1, "%" + name + "%");
            
            ResultSet result = statement.executeQuery();
            
            List<Usuario> usuarios = new ArrayList<>();
            
            Usuario usuario;
            
            while (result.next()) {
                usuario = new Usuario();
                
                usuario.setId(result.getInt("id"));
                usuario.setLogin(result.getString("login"));
                usuario.setSenha(result.getString("senha"));
                usuario.setTelefone(result.getString("telefone"));
                usuario.setCelular(result.getString("celular"));
                usuario.setId_end(result.getInt("endereco"));
                usuario.setCpf(result.getString("cpf"));
                usuario.setDataNasc(result.getDate("nascimento"));
                usuario.setSexo(result.getString("sexo"));
                usuario.setAnotacoes(result.getString("anotacoes"));
                usuario.setTipoDeUser(result.getString("tipo_de_user"));
                usuario.setPnome(result.getString("pnome"));
                usuario.setSnome(result.getString("snome"));
                usuario.setRg_num(result.getString("rg_num"));
                usuario.setRg_org(result.getString("rg_org"));
                usuario.setStatus(result.getBoolean("status"));
                
                usuarios.add(usuario);
            }
            
            return usuarios;
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO2.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    @Override
    public void update(Usuario usuario) {
        try {
            PreparedStatement statement = connection.prepareStatement(updateQuery);
            
            statement.setString(4, usuario.getLogin());
            statement.setString(5, usuario.getSenha());
            statement.setString(9, usuario.getTelefone());
            statement.setString(10, usuario.getCelular());
            statement.setInt(8, usuario.getId_end());
            statement.setString(1, usuario.getCpf());
            statement.setDate(12, usuario.getDataNasc());
            statement.setString(13, usuario.getSexo());
            statement.setString(14, usuario.getAnotacoes());
            statement.setString(11, usuario.getTipoDeUser());
            statement.setString(6, usuario.getPnome());
            statement.setString(7, usuario.getSnome());
            statement.setString(2, usuario.getRg_num());
            statement.setString(3, usuario.getRg_org());
            statement.setBoolean(15, usuario.isStatus());
            
            statement.setInt(16, usuario.getId());
                        
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO2.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void delete(int id) {
        try {
            PreparedStatement statement = connection.prepareStatement(deleteQuery);
            
            statement.setInt(1, id);
                        
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO2.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void recover(int id) {
        try {
            PreparedStatement statement = connection.prepareStatement(recoverQuery);
            
            statement.setInt(1, id);
                        
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO2.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public List<Usuario> all() {
        try {
            PreparedStatement statement = connection.prepareStatement(allQuery);
            
            ResultSet result = statement.executeQuery();
            
            List<Usuario> usuarios = new ArrayList<>();
            
            Usuario usuario;
            
            while (result.next()) {
                usuario = new Usuario();
                
                usuario.setId(result.getInt("id"));
                usuario.setLogin(result.getString("login"));
                usuario.setSenha(result.getString("senha"));
                usuario.setTelefone(result.getString("telefone"));
                usuario.setCelular(result.getString("celular"));
                usuario.setId_end(result.getInt("endereco"));
                usuario.setCpf(result.getString("cpf"));
                usuario.setDataNasc(result.getDate("nascimento"));
                usuario.setSexo(result.getString("sexo"));
                usuario.setAnotacoes(result.getString("anotacoes"));
                usuario.setTipoDeUser(result.getString("tipo_de_user"));
                usuario.setPnome(result.getString("pnome"));
                usuario.setSnome(result.getString("snome"));
                usuario.setRg_num(result.getString("rg_num"));
                usuario.setRg_org(result.getString("rg_org"));
                usuario.setStatus(result.getBoolean("status"));
                
                usuarios.add(usuario);
            }
            
            return usuarios;
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO2.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    public List<Usuario> allWithoutDeleted() {
        try {
            PreparedStatement statement = connection.prepareStatement(allQueryWithoutDeleted);
            
            ResultSet result = statement.executeQuery();
            
            List<Usuario> usuarios = new ArrayList<>();
            
            Usuario usuario;
            
            while (result.next()) {
                usuario = new Usuario();
                
                usuario.setId(result.getInt("id"));
                usuario.setLogin(result.getString("login"));
                usuario.setSenha(result.getString("senha"));
                usuario.setTelefone(result.getString("telefone"));
                usuario.setCelular(result.getString("celular"));
                usuario.setId_end(result.getInt("endereco"));
                usuario.setCpf(result.getString("cpf"));
                usuario.setDataNasc(result.getDate("nascimento"));
                usuario.setSexo(result.getString("sexo"));
                usuario.setAnotacoes(result.getString("anotacoes"));
                usuario.setTipoDeUser(result.getString("tipo_de_user"));
                usuario.setPnome(result.getString("pnome"));
                usuario.setSnome(result.getString("snome"));
                usuario.setRg_num(result.getString("rg_num"));
                usuario.setRg_org(result.getString("rg_org"));
                usuario.setStatus(result.getBoolean("status"));
                
                usuarios.add(usuario);
            }
            
            return usuarios;
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO2.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }

    @Override
    public void create(Usuario usuario) throws SQLException, NotImplementedException {
       
        
            try(PreparedStatement statement = connection.prepareStatement(createQuery)){
            
            statement.setString(1, usuario.getCpf());
            statement.setString(2, usuario.getRg_num());
            statement.setString(3, usuario.getRg_org());
            statement.setString(4, usuario.getLogin());
            statement.setString(5, usuario.getSenha());
            statement.setString(6, usuario.getPnome());
            statement.setString(7, usuario.getSnome());
            statement.setInt(8, usuario.getId_end());
            statement.setString(9, usuario.getTelefone());
            statement.setString(10, usuario.getCelular());
            statement.setString(11, usuario.getTipoDeUser());
            statement.setDate(12, usuario.getDataNasc());
            statement.setString(13, usuario.getSexo());
            statement.setString(14, usuario.getAnotacoes());
            statement.setBoolean(15, usuario.isStatus());
                        
            ResultSet result = statement.executeQuery();
            
            if (result.next()) {
                usuario.setId(result.getInt("id"));
            }
        }  catch (SQLException ex) {
            System.err.println(ex.getMessage());
            System.out.println(ex.getMessage());
            if(ex.getMessage().contains("usuario_login_key")){
                throw new SQLException("Erro ao cadastrar usuário: login já existente.");
            }else if(ex.getMessage().contains("pk_funcionario")){
                throw new SQLException("Erro ao cadastrar usuário: usuario já cadastrado");
            }else{
                throw new SQLException("Erro ao inserir usuário.Tente novamente");
            }
        }
    }
    

}
