/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.RelatorioVolunt;

/**
 *
 * @author lshigaki
 */
public class RelatorioVoluntDAO extends DAO<RelatorioVolunt> {

    private final String createQuery = "INSERT INTO ong2.relatorio (id_voluntario, setor, dataa, hora, observacao, "
            + "ativ_desenvolvida, coordenador) VALUES (?, ?, ?, ?, ?, ?, ?) RETURNING id;";

    private final String readQuery = "SELECT * FROM ong2.relatorio WHERE id = ?;";

    private final String deleteQuery = "UPDATE ong2.relatorio SET status = false WHERE id = ?;";
    
    private final String recoverQuery = "UPDATE ong2.relatorio SET status = true WHERE id = ?;";
    
    private final String readQueryByNameVolunt = "SELECT r.*, u.pnome as nome, u.snome as sobrenome FROM ong2.relatorio r, ong2.voluntario v, ong2.usuario u WHERE r.id_voluntario = v.id AND v.idusuario = u.id AND u.pnome LIKE ?;";
    
    private final String readQueryByNameVoluntWithoutDeleted = "SELECT r.*, u.pnome as nome, u.snome as sobrenome FROM ong2.relatorio r, ong2.voluntario v, ong2.usuario u WHERE r.id_voluntario = v.id AND v.idusuario = u.id AND u.pnome LIKE ? AND r.status = true;";
    
    private final String allReportsWithVolunteerName = "SELECT r.*, u.pnome as nome, u.snome as sobrenome FROM ong2.usuario u, ong2.voluntario v, ong2.relatorio r WHERE u.id = v.idusuario AND r.id_voluntario = v.id;";
    
    private final String allReportsWithVolunteerNameWithoutDeleted = "SELECT r.*, u.pnome as nome, u.snome as sobrenome FROM ong2.usuario u, ong2.voluntario v, ong2.relatorio r WHERE u.id = v.idusuario AND r.id_voluntario = v.id AND r.status = true;";
    
    private final String aprovadoReportsWithVolunteerName = "SELECT r.*, u.pnome as nome, u.snome as sobrenome FROM ong2.usuario u, ong2.voluntario v, ong2.relatorio r WHERE u.id = v.idusuario AND r.id_voluntario = v.id AND r.aprovado = true;";
    
    private final String aprovadoReportsWithVolunteerNameWithoutDeleted = "SELECT r.*, u.pnome as nome, u.snome as sobrenome FROM ong2.usuario u, ong2.voluntario v, ong2.relatorio r WHERE u.id = v.idusuario AND r.id_voluntario = v.id AND r.aprovado = true AND r.status = true;";
    
    private final String reprovadoReportsWithVolunteerName = "SELECT r.*, u.pnome as nome, u.snome as sobrenome FROM ong2.usuario u, ong2.voluntario v, ong2.relatorio r WHERE u.id = v.idusuario AND r.id_voluntario = v.id AND r.aprovado = false;";
    
    private final String reprovadoReportsWithVolunteerNameWithoutDeleted = "SELECT r.*, u.pnome as nome, u.snome as sobrenome FROM ong2.usuario u, ong2.voluntario v, ong2.relatorio r WHERE u.id = v.idusuario AND r.id_voluntario = v.id AND r.aprovado = false AND r.status = true;";
    
    private final String notAnalysedReportsWithVolunteerName = "SELECT r.*, u.pnome as nome, u.snome as sobrenome FROM ong2.usuario u, ong2.voluntario v, ong2.relatorio r WHERE u.id = v.idusuario AND r.id_voluntario = v.id AND r.aprovado IS null;";
    
    private final String notAnalysedReportsWithVolunteerNameWithoutDeleted = "SELECT r.*, u.pnome as nome, u.snome as sobrenome FROM ong2.usuario u, ong2.voluntario v, ong2.relatorio r WHERE u.id = v.idusuario AND r.id_voluntario = v.id AND r.aprovado IS null AND r.status = true;";
    
    public RelatorioVoluntDAO(Connection connection) {
        super(connection);
    }

    @Override
    public void create(RelatorioVolunt rv) {

        try {
            PreparedStatement statement = connection.prepareStatement(createQuery);

            statement.setInt(1, rv.getIdVoluntario());
            statement.setString(2, rv.getSetor());
            statement.setDate(3, rv.getDia());
            statement.setTime(4, rv.getHora());
            statement.setString(5, rv.getObservacao());
            statement.setString(6, rv.getAtivDesenvolvida());
            statement.setString(7, rv.getCoordenador());

            ResultSet result = statement.executeQuery();

            if (result.next()) {
                rv.setId(result.getInt("id"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(RelatorioVoluntDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public RelatorioVolunt read(int id) {
        try {
            PreparedStatement statement = connection.prepareStatement(readQuery);
            
            statement.setInt(1, id);

            ResultSet result = statement.executeQuery();

            RelatorioVolunt rv = new RelatorioVolunt();

            if (result.next()) {
                rv.setId(result.getInt("id"));
                rv.setIdVoluntario(result.getInt("id_voluntario"));
                rv.setSetor(result.getString("setor"));
                rv.setDia(result.getDate("dataa"));
                rv.setHora(result.getTime("hora"));
                rv.setObservacao(result.getString("observacao"));
                rv.setAtivDesenvolvida(result.getString("ativ_desenvolvida"));
                rv.setAprovado(result.getBoolean("aprovado"));
                rv.setStatus(result.getBoolean("status"));
                rv.setCoordenador(result.getString("coordenador"));
            }

            return rv;
        } catch (SQLException ex) {
            Logger.getLogger(RelatorioVoluntDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public void update(RelatorioVolunt obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(int id) {
        
        try {
            PreparedStatement statement = connection.prepareStatement(deleteQuery);
            
            statement.setInt(1, id);
            
            statement.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(RelatorioVoluntDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void recover(int id) {
        
        try {
            PreparedStatement statement = connection.prepareStatement(recoverQuery);
            
            statement.setInt(1, id);
            
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(RelatorioVoluntDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    @Override
    public List<RelatorioVolunt> all() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public List<RelatorioVolunt> allReportsWithVolunteerName(boolean trash) {

        try {
            PreparedStatement statement;
            
            if(trash)
                statement = connection.prepareStatement(allReportsWithVolunteerName);
            else
                statement = connection.prepareStatement(allReportsWithVolunteerNameWithoutDeleted);

            ResultSet result = statement.executeQuery();

            List<RelatorioVolunt> rvList = new ArrayList<>();

            RelatorioVolunt rv;

            while (result.next()) {
                rv = new RelatorioVolunt();

                rv.setId(result.getInt("id"));
                rv.setIdVoluntario(result.getInt("id_voluntario"));
                rv.setSetor(result.getString("setor"));
                rv.setDia(result.getDate("dataa"));
                rv.setHora(result.getTime("hora"));
                rv.setObservacao(result.getString("observacao"));
                rv.setAtivDesenvolvida(result.getString("ativ_desenvolvida"));
                rv.setAprovado(result.getBoolean("aprovado"));
                rv.setStatus(result.getBoolean("status"));
                rv.setCoordenador(result.getString("coordenador"));
                rv.setAtrbExtraNome(result.getString("nome"));
                rv.setAtrbExtraSobrenome(result.getString("sobrenome"));

                rvList.add(rv);
            }

            return rvList;
        } catch (SQLException ex) {
            Logger.getLogger(RelatorioVoluntDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

    public List<RelatorioVolunt> aprovadoReportsWithVolunteerName(boolean trash) {

        try {
            PreparedStatement statement;
            
            if(trash)
                statement = connection.prepareStatement(aprovadoReportsWithVolunteerName);
            else
                statement = connection.prepareStatement(aprovadoReportsWithVolunteerNameWithoutDeleted);

            ResultSet result = statement.executeQuery();

            List<RelatorioVolunt> rvList = new ArrayList<>();

            RelatorioVolunt rv;

            while (result.next()) {
                rv = new RelatorioVolunt();

                rv.setId(result.getInt("id"));
                rv.setIdVoluntario(result.getInt("id_voluntario"));
                rv.setSetor(result.getString("setor"));
                rv.setDia(result.getDate("dataa"));
                rv.setHora(result.getTime("hora"));
                rv.setObservacao(result.getString("observacao"));
                rv.setAtivDesenvolvida(result.getString("ativ_desenvolvida"));
                rv.setAprovado(result.getBoolean("aprovado"));
                rv.setStatus(result.getBoolean("status"));
                rv.setCoordenador(result.getString("coordenador"));
                rv.setAtrbExtraNome(result.getString("nome"));
                rv.setAtrbExtraSobrenome(result.getString("sobrenome"));

                rvList.add(rv);
            }

            return rvList;
        } catch (SQLException ex) {
            Logger.getLogger(RelatorioVoluntDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }
    
    public List<RelatorioVolunt> reprovadoReportsWithVolunteerName(boolean trash) {

        try {
            PreparedStatement statement;
            
            if(trash)
                statement = connection.prepareStatement(reprovadoReportsWithVolunteerName);
            else
                statement = connection.prepareStatement(reprovadoReportsWithVolunteerNameWithoutDeleted);

            ResultSet result = statement.executeQuery();

            List<RelatorioVolunt> rvList = new ArrayList<>();

            RelatorioVolunt rv;

            while (result.next()) {
                rv = new RelatorioVolunt();

                rv.setId(result.getInt("id"));
                rv.setIdVoluntario(result.getInt("id_voluntario"));
                rv.setSetor(result.getString("setor"));
                rv.setDia(result.getDate("dataa"));
                rv.setHora(result.getTime("hora"));
                rv.setObservacao(result.getString("observacao"));
                rv.setAtivDesenvolvida(result.getString("ativ_desenvolvida"));
                rv.setAprovado(result.getBoolean("aprovado"));
                rv.setStatus(result.getBoolean("status"));
                rv.setCoordenador(result.getString("coordenador"));
                rv.setAtrbExtraNome(result.getString("nome"));
                rv.setAtrbExtraSobrenome(result.getString("sobrenome"));

                rvList.add(rv);
            }

            return rvList;
        } catch (SQLException ex) {
            Logger.getLogger(RelatorioVoluntDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }
    
    public List<RelatorioVolunt> notAnalysedReportsWithVolunteerName(boolean trash) {

        try {
            PreparedStatement statement;
            
            if(trash)
                statement = connection.prepareStatement(notAnalysedReportsWithVolunteerName);
            else
                statement = connection.prepareStatement(notAnalysedReportsWithVolunteerNameWithoutDeleted);

            ResultSet result = statement.executeQuery();

            List<RelatorioVolunt> rvList = new ArrayList<>();

            RelatorioVolunt rv;

            while (result.next()) {
                rv = new RelatorioVolunt();

                rv.setId(result.getInt("id"));
                rv.setIdVoluntario(result.getInt("id_voluntario"));
                rv.setSetor(result.getString("setor"));
                rv.setDia(result.getDate("dataa"));
                rv.setHora(result.getTime("hora"));
                rv.setObservacao(result.getString("observacao"));
                rv.setAtivDesenvolvida(result.getString("ativ_desenvolvida"));
                rv.setAprovado(result.getBoolean("aprovado"));
                rv.setStatus(result.getBoolean("status"));
                rv.setCoordenador(result.getString("coordenador"));
                rv.setAtrbExtraNome(result.getString("nome"));
                rv.setAtrbExtraSobrenome(result.getString("sobrenome"));

                rvList.add(rv);
            }

            return rvList;
        } catch (SQLException ex) {
            Logger.getLogger(RelatorioVoluntDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }
    
    public List<RelatorioVolunt> readReatorios(int id) {

        try {
            PreparedStatement statement = connection.prepareStatement(readQuery);

            statement.setInt(1, id);

            ResultSet result = statement.executeQuery();

            List<RelatorioVolunt> rvList = new ArrayList<>();

            RelatorioVolunt rv;

            while (result.next()) {
                rv = new RelatorioVolunt();

                rv.setId(result.getInt("id"));
                rv.setIdVoluntario(result.getInt("id_voluntario"));
                rv.setSetor(result.getString("setor"));
                rv.setDia(result.getDate("dataa"));
                rv.setHora(result.getTime("hora"));
                rv.setObservacao(result.getString("observacao"));
                rv.setAtivDesenvolvida(result.getString("ativ_desenvolvida"));
                rv.setAprovado(result.getBoolean("aprovado"));
                rv.setStatus(result.getBoolean("status"));
                rv.setCoordenador(result.getString("coordenador"));

                rvList.add(rv);
            }

            return rvList;
        } catch (SQLException ex) {
            Logger.getLogger(RelatorioVoluntDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }
    
    public List<RelatorioVolunt> readByNameVolunt(String name) {
        try {
            PreparedStatement statement = connection.prepareStatement(readQueryByNameVolunt);

            statement.setString(1, "%" + name + "%");

            ResultSet result = statement.executeQuery();

            List<RelatorioVolunt> rvList = new ArrayList<>();

            RelatorioVolunt rv;

            while (result.next()) {
                rv = new RelatorioVolunt();

                rv.setId(result.getInt("id"));
                rv.setIdVoluntario(result.getInt("id_voluntario"));
                rv.setSetor(result.getString("setor"));
                rv.setDia(result.getDate("dataa"));
                rv.setHora(result.getTime("hora"));
                rv.setObservacao(result.getString("observacao"));
                rv.setAtivDesenvolvida(result.getString("ativ_desenvolvida"));
                rv.setAprovado(result.getBoolean("aprovado"));
                rv.setStatus(result.getBoolean("status"));
                rv.setCoordenador(result.getString("coordenador"));

                rvList.add(rv);
            }

            return rvList;
        } catch (SQLException ex) {
            Logger.getLogger(RelatorioVoluntDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }
    
    public List<RelatorioVolunt> readByNameVoluntWithoutDeleted(String name) {
        try {
            PreparedStatement statement = connection.prepareStatement(readQueryByNameVoluntWithoutDeleted);

            statement.setString(1, "%" + name + "%");

            ResultSet result = statement.executeQuery();

            List<RelatorioVolunt> rvList = new ArrayList<>();

            RelatorioVolunt rv;

            while (result.next()) {
                rv = new RelatorioVolunt();

                rv.setId(result.getInt("id"));
                rv.setIdVoluntario(result.getInt("id_voluntario"));
                rv.setSetor(result.getString("setor"));
                rv.setDia(result.getDate("dataa"));
                rv.setHora(result.getTime("hora"));
                rv.setObservacao(result.getString("observacao"));
                rv.setAtivDesenvolvida(result.getString("ativ_desenvolvida"));
                rv.setAprovado(result.getBoolean("aprovado"));
                rv.setStatus(result.getBoolean("status"));
                rv.setCoordenador(result.getString("coordenador"));

                rvList.add(rv);
            }

            return rvList;
        } catch (SQLException ex) {
            Logger.getLogger(RelatorioVoluntDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

}
