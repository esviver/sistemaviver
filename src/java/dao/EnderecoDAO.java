/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Endereco;

/**
 *
 * @author Auxiliar
 */
public class EnderecoDAO extends DAO<Endereco> {
    private final String createQuery = "INSERT INTO ong2.ENDERECO(logradouro, bairro, numero, cidade, estado, cep, pontoreferencia)"
            + "VALUES(?, ?, ?, ?, ?, ?, ?) RETURNING id;";
    private final  String updateQuery = "UPDATE ong2.ENDERECO SET logradouro = ?, bairro = ?, numero = ?, cidade = ?, estado"
            + "= ?, cep = ?, pontoreferencia = ? WHERE id = ?;";
    private final String deleteQuery = "DELETE FROM ong2.ENDERECO WHERE id = ?;";
    private final String readQuery = "SELECT * FROM ong2.ENDERECO WHERE id = ?";
    private final String allQuery = "SELECT * FROM ong2.ENDERECO;";
    public EnderecoDAO(Connection connection) {
        super(connection);
    }

    @Override
    public void create(Endereco obj) {
        PreparedStatement statement;
        try {
            statement = connection.prepareStatement(createQuery);
            statement.setString(1, obj.getLogradouro());
            statement.setString(2, obj.getBairro());
            statement.setInt(3, obj.getNumero());
            statement.setString(4, obj.getCidade());
            statement.setString(5, obj.getEstado());
            statement.setString(6, obj.getCep());
            statement.setString(7, obj.getPontoreferencia());
            ResultSet result = statement.executeQuery();
            
            if (result.next()) {
                obj.setId(result.getInt("id"));
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(EnderecoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
           
        
    }

    @Override
    public Endereco read(int id) {
        Endereco endereco;
        try {
            PreparedStatement statement = connection.prepareStatement(readQuery);
            statement.setInt(1, id);
            ResultSet result = statement.executeQuery();
            if(result.next())
            {
                endereco = new Endereco();
                endereco.setBairro(result.getString("bairro"));
                endereco.setCep(result.getString("cep"));
                endereco.setCidade(result.getString("cidade"));
                endereco.setEstado(result.getString("estado"));
                endereco.setId(result.getInt("id"));
                endereco.setNumero(result.getInt("numero"));
                endereco.setLogradouro(result.getString("logradouro"));
                endereco.setPontoreferencia(result.getString("pontoreferencia"));
                return endereco;
            }
        } catch (SQLException ex) {
            Logger.getLogger(EnderecoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public void update(Endereco obj) {
                PreparedStatement statement;
        try {
            statement = connection.prepareStatement(updateQuery);
            statement.setString(1, obj.getLogradouro());
            statement.setString(2, obj.getBairro());
            statement.setInt(3, obj.getNumero());
            statement.setString(4, obj.getCidade());
            statement.setString(5, obj.getEstado());
            statement.setString(6, obj.getCep());
            statement.setString(7, obj.getPontoreferencia());
            statement.setInt(8, obj.getId());
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(EnderecoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
           
        
    }

    @Override
    public void delete(int id) {
        PreparedStatement statement;
        try {
            statement = connection.prepareStatement(deleteQuery);
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(EnderecoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    @Override
    public List<Endereco> all() {
        Endereco endereco;
        List<Endereco> enderecos = new ArrayList<Endereco>();
        try {
            PreparedStatement statement = connection.prepareStatement(readQuery);
            ResultSet result = statement.executeQuery();
            while(result.next())
            {
                endereco = new Endereco();
                endereco.setBairro(result.getString("bairro"));
                endereco.setCep(result.getString("cep"));
                endereco.setCidade(result.getString("cidade"));
                endereco.setEstado(result.getString("estado"));
                endereco.setId(result.getInt("id"));
                endereco.setNumero(result.getInt("numero"));
                endereco.setLogradouro(result.getString("logradouro"));
                endereco.setPontoreferencia(result.getString("pontoreferencia"));
                enderecos.add(endereco);
            }
            return enderecos;
        } catch (SQLException ex) {
            Logger.getLogger(EnderecoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
}
