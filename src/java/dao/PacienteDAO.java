package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Paciente;

public class PacienteDAO extends DAO<Paciente> {

    private final String createQuery = "INSERT INTO ong2.paciente(agua,\n" +
"                alimentacao,\n" +
"                aluguel,\n" +
"                apoioFam,\n" +
"                alta,\n" +
"                banheiro,\n" +
"                id_end,\n" +
"                religiaoCont,\n" +
"                comodos,\n" +
"                dataNasc,\n" +
"                energia,\n" +
"                gas,\n" +
"                histPatologia,\n" +
"                historicoFamiliar,\n" +
"                localNasc,\n" +
"                material,\n" +
"                medicamentos,\n" +
"                moradia,\n" +
"                pnome,\n" +
"                nomeEscola,\n" +
"                entrada,\n" +
"                outroAux,\n" +
"                patologia,\n" +
"                situacao,\n" +
"                religiao,\n" +
"                rendaMensal,\n" +
"                serie,\n" +
"                transporte,\n" +
"                nsus,\n" +
"                asfalto,\n" +
"                auxilioGas,\n" +
"                bolsaFam,\n" +
"                centroCom,\n" +
"                creche,\n" +
"                esgoto,\n" +
"                luzFraterna,\n" + 
"                snome,\n" + 
"                obito,\n" + 
"               id_end_esc)VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) RETURNING id;";
    
    private final String updateQueryById = "UPDATE ong2.paciente \n" +
    "SET \n" +
    "agua = ? = ?,\n" +
    "alimentacao = ? = ?,\n" +
    "aluguel = ? = ?,\n" +
    "apoioFam = ?,\n" +
    "alta = ?,\n" +
    "banheiro = ?,\n" +
    "id_end = ?,\n" +
    "religiaoCont = ?,\n" +
    "comodos = ?,\n" +
    "dataNasc = ?,\n" +
    "energia = ?,\n" +
    "gas = ?,\n" +
    "histPatologia = ?,\n" +
    "historicoFamiliar = ?,\n" +
    "localNasc = ?,\n" +
    "material = ?,\n" +
    "medicamentos = ?,\n" +
    "moradia = ?,\n" +
    "pnome = ?,\n" +
    "nomeEscola = ?,\n" +
    "entrada = ?,\n" +
    "outroAux = ?,\n" +
    "patologia = ?,\n" +
    "obito = ?,\n" +
    "religiao = ?,\n" +
    "rendaMensal = ?,\n" +
    "serie = ?,\n" +
    "transporte = ?,\n" +
    "situacao = ?,\n" +
    "asfalto = ?,\n" +
    "auxilioGas = ?,\n" +
    "bolsaFam = ?,\n" +
    "centroCom = ?,\n" +
    "creche = ?,\n" +
    "esgoto = ?,\n" +
    "luzFraterna = ?,\n" +
    "snome = ?,\n" +
    "obito = ?,\n" +
    "nsus = ?,\n" +
    "id_end_esc = ? \n" +
    "WHERE id = ?;";
    
    private final String readQuery = "SELECT * FROM ong2.paciente WHERE id = ?;";
    
    private final String readByNameWithLike = "SELECT * FROM ong2.paciente WHERE pnome LIKE ?;";
    
    private final String readByNameWithoutDeletedWithLike = "SELECT * FROM ong2.paciente WHERE pnome LIKE ? AND situacao = true;";
    
    private final String recoverQueryById = "UPDATE ong2.paciente SET situacao = true WHERE id = ?;";
    
    private final String deleteQuery = "UPDATE ong2.paciente SET situacao = false WHERE id = ?;";
    
    private final String allQuery = "SELECT * FROM ong2.paciente;";
    
    private final String allQueryWithoutDeleted = "SELECT * FROM ong2.paciente WHERE situacao = true;";
    
    private final String countQuery = "SELECT COUNT(*) FROM ong2.paciente;";
    
    //alterar, ver se vai ser identificado na busca pelos nsus mesmo
    public PacienteDAO(Connection connection) {
        super(connection);
    }


    public int contar()
    {
        try {
            PreparedStatement statement = connection.prepareStatement(countQuery);
            ResultSet result = statement.executeQuery();
            if(result.next())
                return result.getInt("count");
        } catch (SQLException ex) {
            Logger.getLogger(PacienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
    @Override
    public void create(Paciente pac) throws SQLException {
        try {
            PreparedStatement statement = connection.prepareStatement(createQuery);
            
            statement.setFloat(1, pac.getAgua());
            statement.setFloat(2, pac.getAlimentacao());
            statement.setFloat(3, pac.getAluguel());
            statement.setString(4, pac.getApoioFam());
            statement.setBoolean(5, pac.isAlta());
            statement.setString(6, pac.getBanheiro());
            statement.setInt(7, pac.getId_end());
            statement.setBoolean(8, pac.isReligiaoCont());
            statement.setInt(9, pac.getComodos());
            statement.setDate(10, pac.getDataNasc());
            statement.setFloat(11, pac.getEnergia());
            statement.setFloat(12, pac.getGas());
            statement.setString(13, pac.getHistPatologia());
            statement.setString(14, pac.getHistoricoFamiliar());
            statement.setString(15, pac.getLocalNasc());
            statement.setString(16, pac.getMaterial());
            statement.setFloat(17, pac.getMedicamentos());
            statement.setString(18, pac.getMoradia());
            statement.setString(19, pac.getPNome());
            statement.setString(20, pac.getNomeEscola());
            statement.setDate(21, pac.getEntrada());
            statement.setString(22, pac.getOutroAux());
            statement.setString(23, pac.getPatologia());
            statement.setBoolean(24, true);
            statement.setString(25, pac.getReligiao());
            statement.setFloat(26, pac.getRendaMensal());
            statement.setInt(27, pac.getSerie());
            statement.setFloat(28, pac.getTransporte());
            statement.setString(29, pac.getNsus());
            statement.setBoolean(30, pac.isAsfalto());
            statement.setBoolean(31, pac.isAuxilioGas());
            statement.setBoolean(32, pac.isBolsaFam());
            statement.setBoolean(33, pac.isCentroCom());
            statement.setBoolean(34, pac.isCreche());
            statement.setBoolean(35, pac.isEsgoto());
            statement.setBoolean(36, pac.isLuzFraterna());
            statement.setString(37, pac.getSnome());
            statement.setBoolean(38, pac.isObito());
            statement.setInt(39, pac.getId_end_escola());
                        
            ResultSet result = statement.executeQuery();
            
            if (result.next()) {
                pac.setId(result.getInt("id"));
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
            System.out.println(ex.getMessage());
            if(ex.getMessage().contains("uq_nsus")){
                throw new SQLException("Erro ao cadastrar usuário: paciente já existente.");
            }else{
                throw new SQLException("Erro ao cadastrar usuário.Tente novamente");
            }
        }
    }

    @Override
    public Paciente read(int id) {
        try {
            PreparedStatement statement = connection.prepareStatement(readQuery);
            
            statement.setInt(1, id);
            
            ResultSet result = statement.executeQuery();
            
            Paciente pac = new Paciente();
                        
            if (result.next()) {
                pac.setId(result.getInt("id"));
                pac.setAgua(result.getFloat("agua"));
                pac.setAlimentacao(result.getFloat("alimentacao"));
                pac.setAluguel(result.getFloat("aluguel"));
                pac.setApoioFam(result.getString("apoioFam"));
                pac.setBanheiro(result.getString("banheiro"));
                pac.setId_end(result.getInt("id_end"));
                pac.setReligiaoCont(result.getBoolean("religiaoCont"));
                pac.setComodos(result.getInt("comodos"));
                pac.setDataNasc(result.getDate("dataNasc"));
                pac.setEnergia(result.getFloat("energia"));
                pac.setGas(result.getFloat("gas"));
                pac.setHistPatologia(result.getString("histPatologia"));
                pac.setHistoricoFamiliar(result.getString("historicoFamiliar"));
                pac.setLocalNasc(result.getString("localNasc"));
                pac.setMaterial(result.getString("material"));
                pac.setMedicamentos(result.getFloat("medicamentos"));
                pac.setMoradia(result.getString("moradia"));
                pac.setPNome(result.getString("pnome"));
                pac.setNomeEscola(result.getString("nomeEscola"));
                pac.setOutroAux(result.getString("outroAux"));
                pac.setPatologia(result.getString("patologia"));
                pac.setReligiao(result.getString("religiao"));
                pac.setRendaMensal(result.getFloat("rendaMensal"));
                pac.setSerie(result.getInt("serie"));
                pac.setTransporte(result.getFloat("transporte"));
                pac.setNsus(result.getString("nsus"));
                pac.setAsfalto(result.getBoolean("asfalto"));
                pac.setAuxilioGas(result.getBoolean("auxilioGas"));
                pac.setBolsaFam(result.getBoolean("bolsaFam"));
                pac.setCentroCom(result.getBoolean("centroCom"));
                pac.setCreche(result.getBoolean("creche"));
                pac.setEsgoto(result.getBoolean("esgoto"));
                pac.setLuzFraterna(result.getBoolean("luzFraterna"));
                pac.setSnome(result.getString("snome"));
                pac.setObito(result.getBoolean("obito"));
                pac.setSituacao(result.getBoolean("situacao"));
                pac.setAlta(result.getBoolean("alta"));
                pac.setEntrada(result.getDate("entrada"));
                pac.setSaida(result.getDate("saida"));
                pac.setId_end_escola(result.getInt("id_end_esc"));
            }
            return pac;
        } catch (SQLException ex) {
            Logger.getLogger(PacienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    @Override
    public void update(Paciente pac) {
        try {
            PreparedStatement statement = connection.prepareStatement(updateQueryById);
            
            statement.setFloat(1, pac.getAgua());
            statement.setFloat(2, pac.getAlimentacao());
            statement.setFloat(3, pac.getAluguel());
            statement.setString(4, pac.getApoioFam());
            statement.setBoolean(5, pac.isAlta());
            statement.setString(6, pac.getBanheiro());
            statement.setInt(7, pac.getId_end());
            statement.setBoolean(8, pac.isReligiaoCont());
            statement.setInt(9, pac.getComodos());
            statement.setDate(10, pac.getDataNasc());
            statement.setFloat(11, pac.getEnergia());
            statement.setFloat(12, pac.getGas());
            statement.setString(13, pac.getHistPatologia());
            statement.setString(14, pac.getHistoricoFamiliar());
            statement.setString(15, pac.getLocalNasc());
            statement.setString(16, pac.getMaterial());
            statement.setFloat(17, pac.getMedicamentos());
            statement.setString(18, pac.getMoradia());
            statement.setString(19, pac.getPNome());
            statement.setString(20, pac.getNomeEscola());
            statement.setDate(21, pac.getEntrada());
            statement.setString(22, pac.getOutroAux());
            statement.setString(23, pac.getPatologia());
            statement.setBoolean(24, pac.isObito());
            statement.setString(25, pac.getReligiao());
            statement.setFloat(26, pac.getRendaMensal());
            statement.setInt(27, pac.getSerie());
            statement.setFloat(28, pac.getTransporte());
            statement.setBoolean(29, pac.isSituacao());
            statement.setBoolean(30, pac.isAsfalto());
            statement.setBoolean(31, pac.isAuxilioGas());
            statement.setBoolean(32, pac.isBolsaFam());
            statement.setBoolean(33, pac.isCentroCom());
            statement.setBoolean(34, pac.isCreche());
            statement.setBoolean(35, pac.isEsgoto());
            statement.setBoolean(36, pac.isLuzFraterna());
            statement.setString(37, pac.getSnome());
            statement.setString(38, pac.getNsus());
            statement.setInt(39, pac.getId_end_escola());
            
            statement.setInt(40, pac.getId());
                        
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(PacienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void delete(int id) {
        try{
            PreparedStatement statement = connection.prepareStatement(deleteQuery);
            
            statement.setInt(1, id);
                        
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(PacienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void recover(int id) {
        try{
            PreparedStatement statement = connection.prepareStatement(recoverQueryById);
            
            statement.setInt(1, id);
                        
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(PacienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<Paciente> all() {
        try {
            PreparedStatement statement = connection.prepareStatement(allQuery);
            
            ResultSet result = statement.executeQuery();
            
            List<Paciente> pacientes = new ArrayList<>();
            
            Paciente pac;
            
            while (result.next()) {
                pac = new Paciente();
                pac.setId(result.getInt("id"));
                pac.setAgua(result.getFloat("agua"));
                pac.setAlimentacao(result.getFloat("alimentacao"));
                pac.setAluguel(result.getFloat("aluguel"));
                pac.setApoioFam(result.getString("apoioFam"));
                pac.setBanheiro(result.getString("banheiro"));
                pac.setId_end(result.getInt("id_end"));
                pac.setReligiaoCont(result.getBoolean("religiaoCont"));
                pac.setComodos(result.getInt("comodos"));
                pac.setDataNasc(result.getDate("dataNasc"));
                pac.setEnergia(result.getFloat("energia"));
                pac.setGas(result.getFloat("gas"));
                pac.setHistPatologia(result.getString("histPatologia"));
                pac.setHistoricoFamiliar(result.getString("historicoFamiliar"));
                pac.setLocalNasc(result.getString("localNasc"));
                pac.setMaterial(result.getString("material"));
                pac.setMedicamentos(result.getFloat("medicamentos"));
                pac.setMoradia(result.getString("moradia"));
                pac.setPNome(result.getString("pnome"));
                pac.setNomeEscola(result.getString("nomeEscola"));
                pac.setOutroAux(result.getString("outroAux"));
                pac.setPatologia(result.getString("patologia"));
                pac.setReligiao(result.getString("religiao"));
                pac.setRendaMensal(result.getFloat("rendaMensal"));
                pac.setSerie(result.getInt("serie"));
                pac.setTransporte(result.getFloat("transporte"));
                pac.setNsus(result.getString("nsus"));
                pac.setAsfalto(result.getBoolean("asfalto"));
                pac.setAuxilioGas(result.getBoolean("auxilioGas"));
                pac.setBolsaFam(result.getBoolean("bolsaFam"));
                pac.setCentroCom(result.getBoolean("centroCom"));
                pac.setCreche(result.getBoolean("creche"));
                pac.setEsgoto(result.getBoolean("esgoto"));
                pac.setLuzFraterna(result.getBoolean("luzFraterna"));
                pac.setSnome(result.getString("snome"));
                pac.setObito(result.getBoolean("obito"));
                pac.setSituacao(result.getBoolean("situacao"));
                pac.setAlta(result.getBoolean("alta"));
                pac.setEntrada(result.getDate("entrada"));
                pac.setSaida(result.getDate("saida"));
                pac.setId_end_escola(result.getInt("id_end_esc"));
                
                pacientes.add(pac);
            }
            
            return pacientes;
        } catch (SQLException ex) {
            Logger.getLogger(PacienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }

    public List<Paciente> readByName(String name) {
        try {
            PreparedStatement statement = connection.prepareStatement(readByNameWithLike);
            
            statement.setString(1, "%" + name + "%");
            
            ResultSet result = statement.executeQuery();
            
            List<Paciente> pacientes = new ArrayList<>();
            
            Paciente pac;
            
            while (result.next()) {
                pac = new Paciente();
                pac.setId(result.getInt("id"));
                pac.setAgua(result.getFloat("agua"));
                pac.setAlimentacao(result.getFloat("alimentacao"));
                pac.setAluguel(result.getFloat("aluguel"));
                pac.setApoioFam(result.getString("apoioFam"));
                pac.setBanheiro(result.getString("banheiro"));
                pac.setId_end(result.getInt("id_end"));
                pac.setReligiaoCont(result.getBoolean("religiaoCont"));
                pac.setComodos(result.getInt("comodos"));
                pac.setDataNasc(result.getDate("dataNasc"));
                pac.setEnergia(result.getFloat("energia"));
                pac.setGas(result.getFloat("gas"));
                pac.setHistPatologia(result.getString("histPatologia"));
                pac.setHistoricoFamiliar(result.getString("historicoFamiliar"));
                pac.setLocalNasc(result.getString("localNasc"));
                pac.setMaterial(result.getString("material"));
                pac.setMedicamentos(result.getFloat("medicamentos"));
                pac.setMoradia(result.getString("moradia"));
                pac.setPNome(result.getString("pnome"));
                pac.setNomeEscola(result.getString("nomeEscola"));
                pac.setOutroAux(result.getString("outroAux"));
                pac.setPatologia(result.getString("patologia"));
                pac.setReligiao(result.getString("religiao"));
                pac.setRendaMensal(result.getFloat("rendaMensal"));
                pac.setSerie(result.getInt("serie"));
                pac.setTransporte(result.getFloat("transporte"));
                pac.setNsus(result.getString("nsus"));
                pac.setAsfalto(result.getBoolean("asfalto"));
                pac.setAuxilioGas(result.getBoolean("auxilioGas"));
                pac.setBolsaFam(result.getBoolean("bolsaFam"));
                pac.setCentroCom(result.getBoolean("centroCom"));
                pac.setCreche(result.getBoolean("creche"));
                pac.setEsgoto(result.getBoolean("esgoto"));
                pac.setLuzFraterna(result.getBoolean("luzFraterna"));
                pac.setSnome(result.getString("snome"));
                pac.setObito(result.getBoolean("obito"));
                pac.setSituacao(result.getBoolean("situacao"));
                pac.setAlta(result.getBoolean("alta"));
                pac.setEntrada(result.getDate("entrada"));
                pac.setSaida(result.getDate("saida"));
                pac.setId_end_escola(result.getInt("id_end_esc"));
                
                pacientes.add(pac);
            }
            
            return pacientes;
        } catch (SQLException ex) {
            Logger.getLogger(PacienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    public List<Paciente> readByNameWithoutDeleted(String name) {
        try {
            PreparedStatement statement = connection.prepareStatement(readByNameWithoutDeletedWithLike);
            
            statement.setString(1, "%" + name + "%");
            
            ResultSet result = statement.executeQuery();
            
            List<Paciente> pacientes = new ArrayList<>();
            
            Paciente pac;
            
            while (result.next()) {
                pac = new Paciente();
                pac.setId(result.getInt("id"));
                pac.setAgua(result.getFloat("agua"));
                pac.setAlimentacao(result.getFloat("alimentacao"));
                pac.setAluguel(result.getFloat("aluguel"));
                pac.setApoioFam(result.getString("apoioFam"));
                pac.setBanheiro(result.getString("banheiro"));
                pac.setId_end(result.getInt("id_end"));
                pac.setReligiaoCont(result.getBoolean("religiaoCont"));
                pac.setComodos(result.getInt("comodos"));
                pac.setDataNasc(result.getDate("dataNasc"));
                pac.setEnergia(result.getFloat("energia"));
                pac.setGas(result.getFloat("gas"));
                pac.setHistPatologia(result.getString("histPatologia"));
                pac.setHistoricoFamiliar(result.getString("historicoFamiliar"));
                pac.setLocalNasc(result.getString("localNasc"));
                pac.setMaterial(result.getString("material"));
                pac.setMedicamentos(result.getFloat("medicamentos"));
                pac.setMoradia(result.getString("moradia"));
                pac.setPNome(result.getString("pnome"));
                pac.setNomeEscola(result.getString("nomeEscola"));
                pac.setOutroAux(result.getString("outroAux"));
                pac.setPatologia(result.getString("patologia"));
                pac.setReligiao(result.getString("religiao"));
                pac.setRendaMensal(result.getFloat("rendaMensal"));
                pac.setSerie(result.getInt("serie"));
                pac.setTransporte(result.getFloat("transporte"));
                pac.setNsus(result.getString("nsus"));
                pac.setAsfalto(result.getBoolean("asfalto"));
                pac.setAuxilioGas(result.getBoolean("auxilioGas"));
                pac.setBolsaFam(result.getBoolean("bolsaFam"));
                pac.setCentroCom(result.getBoolean("centroCom"));
                pac.setCreche(result.getBoolean("creche"));
                pac.setEsgoto(result.getBoolean("esgoto"));
                pac.setLuzFraterna(result.getBoolean("luzFraterna"));
                pac.setSnome(result.getString("snome"));
                pac.setObito(result.getBoolean("obito"));
                pac.setSituacao(result.getBoolean("situacao"));
                pac.setAlta(result.getBoolean("alta"));
                pac.setEntrada(result.getDate("entrada"));
                pac.setSaida(result.getDate("saida"));
                pac.setId_end_escola(result.getInt("id_end_esc"));
                
                pacientes.add(pac);
            }
            
            return pacientes;
        } catch (SQLException ex) {
            Logger.getLogger(PacienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    public List<Paciente> allWithoutDeleted() {
        try {
            PreparedStatement statement = connection.prepareStatement(allQueryWithoutDeleted);
            
            ResultSet result = statement.executeQuery();
            
            List<Paciente> pacientes = new ArrayList<>();
            
            Paciente pac;
            
            while (result.next()) {
                pac = new Paciente();
                pac.setId(result.getInt("id"));
                pac.setAgua(result.getFloat("agua"));
                pac.setAlimentacao(result.getFloat("alimentacao"));
                pac.setAluguel(result.getFloat("aluguel"));
                pac.setApoioFam(result.getString("apoioFam"));
                pac.setBanheiro(result.getString("banheiro"));
                pac.setId_end(result.getInt("id_end"));
                pac.setReligiaoCont(result.getBoolean("religiaoCont"));
                pac.setComodos(result.getInt("comodos"));
                pac.setDataNasc(result.getDate("dataNasc"));
                pac.setEnergia(result.getFloat("energia"));
                pac.setGas(result.getFloat("gas"));
                pac.setHistPatologia(result.getString("histPatologia"));
                pac.setHistoricoFamiliar(result.getString("historicoFamiliar"));
                pac.setLocalNasc(result.getString("localNasc"));
                pac.setMaterial(result.getString("material"));
                pac.setMedicamentos(result.getFloat("medicamentos"));
                pac.setMoradia(result.getString("moradia"));
                pac.setPNome(result.getString("pnome"));
                pac.setNomeEscola(result.getString("nomeEscola"));
                pac.setOutroAux(result.getString("outroAux"));
                pac.setPatologia(result.getString("patologia"));
                pac.setReligiao(result.getString("religiao"));
                pac.setRendaMensal(result.getFloat("rendaMensal"));
                pac.setSerie(result.getInt("serie"));
                pac.setTransporte(result.getFloat("transporte"));
                pac.setNsus(result.getString("nsus"));
                pac.setAsfalto(result.getBoolean("asfalto"));
                pac.setAuxilioGas(result.getBoolean("auxilioGas"));
                pac.setBolsaFam(result.getBoolean("bolsaFam"));
                pac.setCentroCom(result.getBoolean("centroCom"));
                pac.setCreche(result.getBoolean("creche"));
                pac.setEsgoto(result.getBoolean("esgoto"));
                pac.setLuzFraterna(result.getBoolean("luzFraterna"));
                pac.setSnome(result.getString("snome"));
                pac.setObito(result.getBoolean("obito"));
                pac.setSituacao(result.getBoolean("situacao"));
                pac.setAlta(result.getBoolean("alta"));
                pac.setEntrada(result.getDate("entrada"));
                pac.setSaida(result.getDate("saida"));
                pac.setId_end_escola(result.getInt("id_end_esc"));
                
                pacientes.add(pac);
            }
            
            return pacientes;
        } catch (SQLException ex) {
            Logger.getLogger(PacienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
}
