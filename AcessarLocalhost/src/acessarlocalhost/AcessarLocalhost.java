package acessarlocalhost;

import java.net.URL;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.net.InetAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AcessarLocalhost {

    public static void main(String[] args) 
     {
         INDEX janela = new INDEX();
         janela.jProgressBar1.setMaximum(254);
         janela.jProgressBar1.setMinimum(2);
         String ipp = null;
        /*try {
            //String IPBase = "10.65.1.";//seria o ip pego por aquela funcao
            //ipp = InetAddress.getLocalHost().getHostAddress();
            //ipp = ipp.substring(0,ipp.lastIndexOf(".") + 1);
            System.out.println(ipp);
        } catch (UnknownHostException ex) {
            Logger.getLogger(AcessarLocalhost.class.getName()).log(Level.SEVERE, null, ex);
        }*/
        
        String IPBase = "192.168.25.";
        int numero = 1;
        String numeroS;
        URI uri;
        janela.setVisible(true);
        while(numero < 255){
            numero++;
            janela.jProgressBar1.setValue(numero);
            System.out.println(numero);
            try {
                numeroS = String.valueOf(numero);
                uri = null;
                try {
                    String teste = "http://" + IPBase + numeroS + ":8084/SistemaViver/conexaoController";
                    uri = new URI(teste);
                    
                } catch (URISyntaxException ex) {
                    Logger.getLogger(AcessarLocalhost.class.getName()).log(Level.SEVERE, null, ex);
                }
                URL url = uri.toURL();
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setConnectTimeout(100);
                try (BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()))) {
                    String line;
                    
                    while ((line = in.readLine()) != null) {
                        if(line.equals("conectado")){
                            Runtime.getRuntime().exec("cmd /c start http://" + IPBase + numeroS + ":8084/SistemaViver/view/menu/login");
                            janela.setVisible(false);
                            System.exit(0);
                        }
                    }
                }
                urlConnection.disconnect();
            } catch (MalformedURLException e) {
                System.out.println("Erro ao criar URL. Formato inválido.");
            } catch (IOException e2) {
                System.out.println("Erro ao acessar URL.");
            }
        }
    }

}
