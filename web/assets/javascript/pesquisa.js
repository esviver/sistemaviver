exibirItensExcluidos = false;
// Seta para a pesquisa exibir itens excluidos 
function setExibirItensExcluidos(valor) {
    this.exibirItensExcluidos = valor;
}
// Faz uma busca por pacientes com o termo de pesquisa INPUT
function pesquisarPaciente(event, input) {
    if (event.keyCode == 13 || event.which == 1) {
        if (input.length == 0) {
            document.getElementById("result_content").innerHTML = '<div class="search_msg_box search_msg_box_patient" width="100"></div>';
        } else {
            document.getElementById("result_content").innerHTML = "<div class='loading_box'>Carregando...</div>";
            // AJAX Assincrono
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (xhttp.readyState == 4 && xhttp.status == 200) {
                    document.getElementById("result_content").innerHTML = xhttp.responseText;
                }
            };
            xhttp.open("GET", "/SistemaViver/view/menu/patient/readBySearch?input=" + input + "&trash=" + exibirItensExcluidos, true);
            xhttp.send();
        }
    }
}
function mostrarTudoPaciente(event) {
    document.getElementById("result_content").innerHTML = "<div class='loading_box'>Carregando...</div>";
    // AJAX Assincrono
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            document.getElementById("result_content").innerHTML = xhttp.responseText;
        }
    };
    xhttp.open("GET", "/SistemaViver/view/menu/patient/readAll?trash=" + exibirItensExcluidos, true);
    xhttp.send();
}
function pesquisarUsuario(event, input) {
    if (event.keyCode == 13 || event.which == 1) {
        if (input.length == 0) {
            document.getElementById("result_content").innerHTML = '<div class="search_msg_box search_msg_box_account" width="100"></div>';
        } else {
            document.getElementById("result_content").innerHTML = "<div class='loading_box'>Carregando...</div>";
            // AJAX Assincrono
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (xhttp.readyState == 4 && xhttp.status == 200) {
                    document.getElementById("result_content").innerHTML = xhttp.responseText;
                }
            };
            xhttp.open("GET", "/SistemaViver/view/menu/account/readBySearch?input=" + input + "&trash=" + exibirItensExcluidos, true);
            xhttp.send();
        }
    }
}
function mostrarTudoUsuario(event) {
    document.getElementById("result_content").innerHTML = "<div class='loading_box'>Carregando...</div>";
    // AJAX Assincrono
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            document.getElementById("result_content").innerHTML = xhttp.responseText;
        }
    };
    xhttp.open("GET", "/SistemaViver/view/menu/account/readAll?trash=" + exibirItensExcluidos, true);
    xhttp.send();
}
// Faz uma busca por quartos com o termo de pesquisa INPUT
function pesquisarQuarto(event, input) {
    if (event.keyCode == 13 || event.which == 1) {
        if (input.length == 0) {
            document.getElementById("result_content").innerHTML = '<div class="search_msg_box search_msg_box_room" width="100"></div>';
        } else {
            document.getElementById("result_content").innerHTML = "<div class='loading_box'>Carregando...</div>";
            // AJAX Assincrono
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (xhttp.readyState == 4 && xhttp.status == 200) {
                    document.getElementById("result_content").innerHTML = xhttp.responseText;
                }
            };
            xhttp.open("GET", "/SistemaViver/view/menu/sleep/room/readBySearch?input=" + input + "&trash=" + exibirItensExcluidos, true);
            xhttp.send();
        }
    }
}
function mostrarTudoQuarto(event) {
    if (event.keyCode == 13 || event.which == 1) {
        document.getElementById("result_content").innerHTML = "<div class='loading_box'>Carregando...</div>";
        // AJAX Assincrono
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState == 4 && xhttp.status == 200) {
                document.getElementById("result_content").innerHTML = xhttp.responseText;
            }
        };
        xhttp.open("GET", "/SistemaViver/view/menu/sleep/room/readAll?trash=" + exibirItensExcluidos, true);
        xhttp.send();
    }
}
// Faz uma busca por doações com o termo de pesquisa INPUT
function pesquisarDoacao(event, input) {
    if (event.keyCode == 13 || event.which == 1) {
        if (input.length == 0) {
            document.getElementById("result_content").innerHTML = '<div class="search_msg_box search_msg_box_donate" width="100"></div>';
        } else {
            document.getElementById("result_content").innerHTML = "<div class='loading_box'>Carregando...</div>";
            // AJAX Assincrono
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (xhttp.readyState == 4 && xhttp.status == 200) {
                    document.getElementById("result_content").innerHTML = xhttp.responseText;
                }
            };
            xhttp.open("GET", "/SistemaViver/view/menu/donate/readBySearch?input=" + input + "&trash=" + exibirItensExcluidos, true);
            xhttp.send();
        }
    }
}
function mostrarTudoDoacao(event) {
    document.getElementById("result_content").innerHTML = "<div class='loading_box'>Carregando...</div>";
    // AJAX Assincrono
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            document.getElementById("result_content").innerHTML = xhttp.responseText;
        }
    };
    xhttp.open("GET", "/SistemaViver/view/menu/donate/readAll?trash=" + exibirItensExcluidos, true);
    xhttp.send();
}
// Faz uma busca por agendamentos de anamnese com o termo de pesquisa INPUT
function pesquisarAnamnese(event, input) {
    if (event.keyCode == 13 || event.which == 1) {
        if (input.length == 0) {
            document.getElementById("result_content").innerHTML = '<div class="search_msg_box search_msg_box_anamnese" width="100"></div>';
        } else {
            document.getElementById("result_content").innerHTML = "<div class='loading_box'>Carregando...</div>";
            // AJAX Assincrono
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (xhttp.readyState == 4 && xhttp.status == 200) {
                    document.getElementById("result_content").innerHTML = xhttp.responseText;
                }
            };
            xhttp.open("GET", "/SistemaViver/view/menu/agendamentos/anamnese/readBySearch?input=" + input, true);
            xhttp.send();
        }
    }
}
function mostrarTudoAnamnese(event) {
    document.getElementById("result_content").innerHTML = "<div class='loading_box'>Carregando...</div>";
    // AJAX Assincrono
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            document.getElementById("result_content").innerHTML = xhttp.responseText;
        }
    };
    xhttp.open("GET", "/SistemaViver/view/menu/agendamentos/anamnese/readAll", true);
    xhttp.send();
}
// Faz uma busca por agendamentos com psicologo com o termo de pesquisa INPUT
function pesquisarAtendimentoPsicologo(event, input) {
    if (event.keyCode == 13 || event.which == 1) {
        if (input.length == 0) {
            document.getElementById("result_content").innerHTML = '<div class="search_msg_box search_msg_box_psychology" width="100"></div>';
        } else {
            document.getElementById("result_content").innerHTML = "<div class='loading_box'>Carregando...</div>";
            // AJAX Assincrono
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (xhttp.readyState == 4 && xhttp.status == 200) {
                    document.getElementById("result_content").innerHTML = xhttp.responseText;
                }
            };
            xhttp.open("GET", "/SistemaViver/view/menu/agendamentos/psychology/readBySearch?input=" + input, true);
            xhttp.send();
        }
    }
}
function mostrarTudoAtendimentoPsicologo(event) {
    document.getElementById("result_content").innerHTML = "<div class='loading_box'>Carregando...</div>";
    // AJAX Assincrono
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            document.getElementById("result_content").innerHTML = xhttp.responseText;
        }
    };
    xhttp.open("GET", "/SistemaViver/view/menu/agendamentos/psychology/readAll", true);
    xhttp.send();
}
// Faz uma busca por atendimentos sociais com o termo de pesquisa INPUT
function pesquisarAtendimentoSocial(event, input) {
    if (event.keyCode == 13 || event.which == 1) {
        if (input.length == 0) {
            document.getElementById("result_content").innerHTML = '<div class="search_msg_box search_msg_box_psychology" width="100"></div>';
        } else {
            document.getElementById("result_content").innerHTML = "<div class='loading_box'>Carregando...</div>";
            // AJAX Assincrono
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (xhttp.readyState == 4 && xhttp.status == 200) {
                    document.getElementById("result_content").innerHTML = xhttp.responseText;
                }
            };
            xhttp.open("GET", "/SistemaViver/view/menu/agendamentos/social_care/readBySearch?input=" + input, true);
            xhttp.send();
        }
    }
}
function mostrarTudoAtendimentoSocial(event) {
    document.getElementById("result_content").innerHTML = "<div class='loading_box'>Carregando...</div>";
    // AJAX Assincrono
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            document.getElementById("result_content").innerHTML = xhttp.responseText;
        }
    };
    xhttp.open("GET", "/SistemaViver/view/menu/agendamentos/social_care/readAll", true);
    xhttp.send();
}
function pesquisarHospedagem(event, input) {
    
    if (event.keyCode == 13 || event.which == 1) {
        if (input.length == 0) {
            document.getElementById("result_content").innerHTML = '<div class="search_msg_box search_msg_box_account" width="100"></div>';
        } else {
            document.getElementById("result_content").innerHTML = "<div class='loading_box'>Carregando...</div>";
            // AJAX Assincrono
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (xhttp.readyState == 4 && xhttp.status == 200) {
                    document.getElementById("result_content").innerHTML = xhttp.responseText;
                }
            };
            xhttp.open("GET", "/SistemaViver/view/menu/sleep/sleeps/readBySearch?input=" + input + "&trash=" + exibirItensExcluidos, true);
            xhttp.send();
        }
    }
}
function mostrarTudoHospedagem(event) {
    document.getElementById("result_content").innerHTML = "<div class='loading_box'>Carregando...</div>";
    // AJAX Assincrono
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            document.getElementById("result_content").innerHTML = xhttp.responseText;
        }
    };
    xhttp.open("GET", "/SistemaViver/view/menu/sleep/sleeps/readAll?trash="+exibirItensExcluidos,exibirItensExcluidos, true);
    xhttp.send();
}

function pesquisarRelatorio(event, input) {
    if (event.keyCode == 13 || event.which == 1) {
        if (input.length == 0) {
            document.getElementById("result_content").innerHTML = '<div class="search_msg_box search_msg_box_volunteer" width="100"></div>';
        } else {
            document.getElementById("result_content").innerHTML = "<div class='loading_box'>Carregando...</div>";
            // AJAX Assincrono
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (xhttp.readyState == 4 && xhttp.status == 200) {
                    document.getElementById("result_content").innerHTML = xhttp.responseText;
                }
            };
            xhttp.open("GET", "/SistemaViver/view/menu/volunteer/readBySearch?input=" + input + "&trash=" + exibirItensExcluidos, true);
            xhttp.send();
        }
    }
}
function mostrarTudoRelatorio(event) {
    document.getElementById("result_content").innerHTML = "<div class='loading_box'>Carregando...</div>";
    var radios = document.getElementsByName('opcao');
    var opt;

    for (var i = 0, length = radios.length; i < length; i++) {
        if (radios[i].checked) {
            opt = radios[i].value;
            break;
        }
    }
    // AJAX Assincrono
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            document.getElementById("result_content").innerHTML = xhttp.responseText;
        }
    };
    xhttp.open("GET", "/SistemaViver/view/menu/volunteer/readAll?trash=" + exibirItensExcluidos + "&opcao=" + opt, true);
    xhttp.send();
}
function pesquisarDistribuicao(event, input) {
    
    if (event.keyCode == 13 || event.which == 1) {
        if (input.length == 0) {
            document.getElementById("result_content").innerHTML = '<div class="search_msg_box search_msg_box_account" width="100"></div>';
        } else {
            document.getElementById("result_content").innerHTML = "<div class='loading_box'>Carregando...</div>";
            // AJAX Assincrono
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (xhttp.readyState == 4 && xhttp.status == 200) {
                    document.getElementById("result_content").innerHTML = xhttp.responseText;
                }
            };
            xhttp.open("GET", "/SistemaViver/view/menu/basket/readBySearch?input=" + input + "&trash=" + exibirItensExcluidos, true);
            xhttp.send();
        }
    }
}
function mostrarTudoDistribuicao(event) {
    document.getElementById("result_content").innerHTML = "<div class='loading_box'>Carregando...</div>";
    // AJAX Assincrono
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            document.getElementById("result_content").innerHTML = xhttp.responseText;
        }
    };
    xhttp.open("GET", "/SistemaViver/view/menu/basket/readAll?trash="+ exibirItensExcluidos ,exibirItensExcluidos, true);
    xhttp.send();
}
