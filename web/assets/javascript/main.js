// Aplica transicao de chegada do menu
function introMenu(){
	document.getElementById("menu").className = "menu menu_loaded";
}
function introLogin(){
	document.getElementById("menu_back").className = "menu_back menu_back_loaded";
}
function introCard(){
	document.getElementById("card_support").className = "card_support_loaded";
}
// Disabilita a caixa de entrada do nome do doador
function setDonateNameBox(value){
    if(value == true){
        document.getElementById("doador_nome").disabled = true;
    }else{
        document.getElementById("doador_nome").disabled = false;
    }
    
}
// Mostra os dados extras de insercao de dados no cadastro
function setAccountExtras(value){
    if(value == "psicologo"){
        document.getElementById("dados_psicologico").style.display = "";
        document.getElementById("dados_funcionario_voluntario").style.display = "none";
        document.getElementById("dados_assistente").style.display = "none";
    }else if(value == "assistente"){
        document.getElementById("dados_psicologico").style.display = "none";
        document.getElementById("dados_funcionario_voluntario").style.display = "none";
        document.getElementById("dados_assistente").style.display = "";
    }else if(value == "voluntario"){
        document.getElementById("dados_psicologico").style.display = "none";
        document.getElementById("dados_funcionario_voluntario").style.display = "";
        document.getElementById("dados_assistente").style.display = "none";
    }else{
        document.getElementById("dados_psicologico").style.display = "none";
        document.getElementById("dados_funcionario_voluntario").style.display = "none";
        document.getElementById("dados_assistente").style.display = "none";
    }
}
function setMedicamento(value)
{
    if(value == "sim")
    {
        document.getElementById("nome_medicamento").style.display = "";
    }
    else if(value == "nao")
    {
        document.getElementById("nome_medicamento").style.display = "none";
    }
}
// Mostra as caixas apropriadas de insercao de acordo com o qual valor esta ativado
function setDoacaoMetodo(value){
    if(value == "dinheiro"){
        document.getElementById("metodoDinheiro").style.display = "";
        document.getElementById("metodoOutro").style.display = "none";
    }else if(value == "outro"){
        document.getElementById("metodoDinheiro").style.display = "none";
        document.getElementById("metodoOutro").style.display = "";
    }
    
}

// Ao mudar o item selecionado essa funcao atualiza as subinformacoes do paciente para confirmacao de paciente
function setPacienteSelectMenuHospedagem(id){
    e = document.getElementById(id);
    paciente_id = e.options[e.selectedIndex].value;
    if(paciente_id == "null"){
        document.getElementById("pacienteInfos").style.display = "none";
    }else{
        document.getElementById("pacienteInfos").style.display = "";
        rg = e.options[e.selectedIndex].getAttribute("rg");
        cpf = e.options[e.selectedIndex].getAttribute("cpf");
        sus = e.options[e.selectedIndex].getAttribute("sus");
        document.getElementById("rg").innerHTML = rg;
        document.getElementById("cpf").innerHTML = cpf;
        document.getElementById("sus").innerHTML = sus;
    }
}
// Ao mudar o item selecionado essa funcao atualiza as subinformacoes do paciente para confirmacao de paciente
function setPacienteSelectMenuPsicologo(id){
    e = document.getElementById(id);
    paciente_id = e.options[e.selectedIndex].value;
    if(paciente_id == "null"){
        document.getElementById("pacienteInfos").style.display = "none";
    }else{
        document.getElementById("pacienteInfos").style.display = "";
        dn = e.options[e.selectedIndex].getAttribute("dn");
        responsavel = e.options[e.selectedIndex].getAttribute("responsavel");
        patologia = e.options[e.selectedIndex].getAttribute("patologia");
        idPaciente = e.options[e.selectedIndex].getAttribute("idPaciente");
        document.getElementById("dn").innerHTML = dn;
        document.getElementById("responsavel").innerHTML = responsavel;
        document.getElementById("idPaciente").value = idPaciente;
        document.getElementById("patologia").value = patologia;
    }
}
function setTipoCesta(value)
{
    alert("leite");
    document.getElementById("cesta").value = value;
}
function setTipoLeite(value)
{
    alert("leite");
    document.getElementById("leite").value = value;
}
function setVoluntarioSelectMenu(id){
    e = document.getElementById(id);
    paciente_id = e.options[e.selectedIndex].value;
    idVoluntario = e.options[e.selectedIndex].getAttribute("idVoluntario");
    document.getElementById("idVoluntario").value = idVoluntario;
    
}
//
function setOutraFuncao(valor){
    if(valor == true){
        document.getElementById("outro_funcao").style.display = "";
    }else{
        document.getElementById("outro_funcao").style.display = "none";
    }   
}
