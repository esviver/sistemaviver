<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Ong Viver - Menu</title>
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/main.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/top_bar.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/bottom_bar.css">
<script src="${pageContext.servletContext.contextPath}/assets/javascript/main.js"></script>
</head>
<body>

<%@include file="view/includes/top_bar.jsp"%> 


<br>

<div align="center">
    <div class="navigator">
    	<a href="${pageContext.servletContext.contextPath}/view/menu/" class="navigator_link">Menu</a>
    </div>
</div>

<br>


<div align="center">
    <div class="error">${error}</div>
</div>

<br><br>

<%@include file="view/includes/bottom_bar.jsp"%>

</body>
</html>
