<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Ong Viver - Cadastrar nova doa��o</title>
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/main.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/top_bar.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/bottom_bar.css">
<script src="${pageContext.servletContext.contextPath}/assets/javascript/main.js"></script>
</head>
<body onLoad="introCard()">
<%@include file="../../../includes/top_bar.jsp"%>

<br>

<div align="center">
    <div class="navigator">
    	<a href="${pageContext.servletContext.contextPath}/view/menu/index.jsp" class="navigator_link">Menu</a>
        >
        <a href="${pageContext.servletContext.contextPath}/view/menu/donate/index.jsp" class="navigator_link">Doa��es</a>
        >
        <a href="${pageContext.servletContext.contextPath}/view/menu/donate/create/index.jsp" class="navigator_link">Cadastrar nova doa��o</a>
    </div>
</div>

<br>

<form action="${pageContext.servletContext.contextPath}/view/menu/donate/create" method="post">
<div align="center">
	<div align="right" class="card_support_loading" id="card_support">
        <div class="card" style="width:620px;" align="center">
            <table>
                <tr>
                    <td align="center" colspan="2" class="label_title_card">Cadastramento de doa��o</td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Doador: </td>
                    <td>
                        <input type="text" class="input_text it_large" placeholder="Digite o nome do doador" name="doadorNome" id="doador_nome">
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td class="label_list_item">
                        <input type="checkbox" onchange="setDonateNameBox(this.checked)" id="doador_anonimo" name="anonimo">
                        Doador an�nimo
                    </td>
                </tr>
                
                <tr>
                    <td align="right" class="label_list_item">Telefone: </td>
                    <td>
                    	<input type="text" class="input_text it_date_mini" maxlength="2" placeholder="DDD" name="telefonedoadorDDD">
                   		<input type="text" class="input_text it_date_bigger" maxlength="10" placeholder="Digite o telefone" name="telefonedoador">
                    </td>
                </tr>
                
                <tr>
                    <td align="right" class="label_list_item">Celular: </td>
                    <td>
                    	<input type="text" class="input_text it_date_mini" maxlength="2" placeholder="DDD" name="celulardoadorDDD">
                   		<input type="text" class="input_text it_date_bigger" maxlength="10" placeholder="Digite o celular" name="celulardoador">
                    </td>
                </tr>
                
                <tr>
                    <td align="right" class="label_list_item">Data: </td>
                    <td>
                        <input type="text" class="input_text it_date_mini" maxlength="2" placeholder="Dia" name="diaDia"> 
                        <input type="text" class="input_text it_date_mini" maxlength="2" placeholder="M�s" name="diaMes"> 
                        <input type="text" class="input_text it_date_large" maxlength="4" placeholder="Ano" name="diaAno">
                    </td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Tipo de doa��o: </td>
                    <td align="left">
                        <input type="radio" name="metodo" value="dinheiro" checked="checked" onchange="setDoacaoMetodo(this.value)"> <span class="label_list_item">Dinheiro</span>
                        <input type="radio" name="metodo" value="outro" onchange="setDoacaoMetodo(this.value)"> <span class="label_list_item">Outro</span>
                    </td>
                </tr>
                <tbody id="metodoDinheiro">
                    <tr>
                        <td align="right" class="label_list_item">Dinheiro: </td>
                        <td>
                            <input type="text" class="input_text it_date_bigger" maxlength="12" placeholder="Valor" name="dinheiro"> ,
                            <input type="text" class="input_text it_date_mini" maxlength="2" placeholder="00" name="dinheiroPosVirgula">
                        </td>
                    </tr>
                </tbody>
                <tbody id="metodoOutro" style="display:none">
                    <tr>
                        <td align="right" class="label_list_item">Outro: </td>
                        <td>
                            <textarea class="input_textarea it_large" placeholder="Digite por qual meio" name="outro"></textarea>
                        </td>
                    </tr>
                </tbody>
                <tr>
                    <td align="right" class="label_list_item">Descri��o: </td>
                    <td>
                        <textarea class="input_textarea it_large" placeholder="Digite uma descri��o" name="descricao"></textarea>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td align="center"><input type="submit" value="Salvar" class="input_button" style="width:326px; height:40px"></td>
                </tr>
                
            </table>
        </div>
    </div>
</div>
</form>





<br><br>

<%@include file="../../../includes/bottom_bar.jsp"%>
</body>
</html>
