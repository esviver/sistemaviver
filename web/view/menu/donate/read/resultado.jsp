<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<table border="0" align="left" class="table_hover" cellpadding="5" cellspacing="0">
    <tr style="background-color:#EEE !important;">
    	<td style="min-width:100px;"></td>
    	<td class="list_view_title" align="left">Doador</td>
        <td class="list_view_title" align="left">Telefone</td>
        <td class="list_view_title" align="left">Celular</td>
        <td class="list_view_title" align="left">Data</td>
        <td class="list_view_title" align="left">Valor</td>
        <td class="list_view_title" align="left">Outro</td>
        <td class="list_view_title" align="left">Descri��o</td>
    </tr>
    <c:set var="i" value="0"/>
    <c:forEach items="${listaPResultados}" var="resultado">
    <c:if test="${resultado.status == false}">
    <tr>
	<td>
            <a href="../update/index.jsp?id=${resultado.id}" class="list_view_link">Editar</a>
            �
            <a href="../recover?id=${resultado.id}" class="list_view_link">Recuperar</a>
        </td>
        <td class="label_list_item">
            <div style="width:100px" class="list_view_text_trash">${resultado.doador}</div>
        </td>
    	<td class="label_list_item">
            <div style="width:100px" class="list_view_text_trash">${resultado.telDoador}</div>
        </td>
        <td class="label_list_item">
            <div style="width:100px" class="list_view_text_trash">${resultado.celDoador}</div>
        </td>
    	<td class="label_list_item">
            <div style="width:100px" class="list_view_text_trash">${resultado.data}</div>
        </td>
        <td class="label_list_item">
            <div style="width:100px" class="list_view_text_trash">R$ ${resultado.dinheiro}</div>
        </td>
    	<td class="label_list_item">
            <div style="width:250px" class="list_view_text_trash">${resultado.outro}</div>
        </td>
        <td class="label_list_item">
            <div style="width:250px" class="list_view_text_trash">${resultado.descricao}</div>
        </td>
    </tr>
    </c:if>
    <c:if test="${resultado.status == true}">
    <tr>
	<td>
            <a href="../update?id=${resultado.id}" class="list_view_link">Editar</a>
            �
            <a href="../delete/index.jsp?id=${resultado.id}" class="list_view_link">Deletar</a>
        </td>
        <td class="label_list_item">
            <div style="width:100px" class="list_view_text">${resultado.doador}</div>
        </td>
    	<td class="label_list_item">
            <div style="width:100px" class="list_view_text">${resultado.telDoador}</div>
        </td>
        <td class="label_list_item">
            <div style="width:100px" class="list_view_text">${resultado.celDoador}</div>
        </td>
    	<td class="label_list_item">
            <div style="width:100px" class="list_view_text">${resultado.data}</div>
        </td>
        <td class="label_list_item">
            <div style="width:100px" class="list_view_text">R$ ${resultado.dinheiro}</div>
        </td>
    	<td class="label_list_item">
            <div style="width:250px" class="list_view_text">${resultado.outro}</div>
        </td>
        <td class="label_list_item">
            <div style="width:250px" class="list_view_text">${resultado.descricao}</div>
        </td>
    </tr>
    </c:if>
    <c:set var="i" value="${i+1}"/>
    </c:forEach>
</table>
<div style=" min-height:200px;">
</div>