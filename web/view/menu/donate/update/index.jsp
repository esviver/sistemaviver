<!doctype html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ong Viver - Atualizar uma doa��o</title>
        <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/main.css">
        <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/top_bar.css">
        <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/bottom_bar.css">
        <script src="${pageContext.servletContext.contextPath}/assets/javascript/main.js"></script>
    </head>
    <body onLoad="introCard()">
        <%@include file="../../../includes/top_bar.jsp"%>

        <br>

        <div align="center">
            <div class="navigator">
                <a href="${pageContext.servletContext.contextPath}/view/menu/index.jsp" class="navigator_link">Menu</a>
                >
                <a href="${pageContext.servletContext.contextPath}/view/menu/donate/index.jsp" class="navigator_link">Doa��es</a>
                >
                <a href="#" class="navigator_link">Atualizar uma doa��o</a>
            </div>
        </div>

        <br>

        <form action="${pageContext.servletContext.contextPath}/view/menu/donate/update" method="post">
            <div align="center">
                <div align="right" class="card_support_loading" id="card_support">
                    <div class="card" style="width:620px;" align="center">
                        <table>
                            <tr>
                                <td align="center" colspan="2" class="label_title_card">Atualiza��o de doa��o</td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">Doador: </td>
                                <td>
                                    <input type="text" class="input_text it_large" placeholder="Digite o nome do doador" name="doadorNome" id="doador_nome" value="${doacaoParaUpdate.doador}">
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td class="label_list_item">
                                    <c:choose>
                                        <c:when test="${doacaoParaUpdate.doadorAnon}">
                                            <input type="checkbox" onchange="setDonateNameBox(this.checked)" id="doador_anonimo" name="anonimo" CHECKED>
                                        </c:when>
                                        <c:otherwise>
                                            <input type="checkbox" onchange="setDonateNameBox(this.checked)" id="doador_anonimo" name="anonimo">
                                        </c:otherwise>
                                    </c:choose>
                                    Doador an�nimo
                                </td>
                            </tr>

                            <tr>
                                <td align="right" class="label_list_item">Telefone: </td>
                                <td>
                                    <input type="text" class="input_text it_date_mini" maxlength="2" placeholder="DDD" name="telefonedoadorDDD" value="${telDDD}">
                                    <input type="text" class="input_text it_date_bigger" maxlength="10" placeholder="Digite o telefone" name="telefonedoador" value="${tel}">
                                </td>
                            </tr>

                            <tr>
                                <td align="right" class="label_list_item">Celular: </td>
                                <td>
                                    <input type="text" class="input_text it_date_mini" maxlength="2" placeholder="DDD" name="celulardoadorDDD" value="${celDDD}">
                                    <input type="text" class="input_text it_date_bigger" maxlength="10" placeholder="Digite o celular" name="celulardoador" value="${cel}">
                                </td>
                            </tr>

                            <tr>
                                <td align="right" class="label_list_item">Data: </td>
                                <td>
                                    <input type="text" class="input_text it_date_mini" maxlength="2" placeholder="Dia" name="diaDia" value="${dia}"> 
                                    <input type="text" class="input_text it_date_mini" maxlength="2" placeholder="M�s" name="diaMes" value="${mes}"> 
                                    <input type="text" class="input_text it_date_large" maxlength="4" placeholder="Ano" name="diaAno" value="${ano}">
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">Tipo de doa��o: </td>
                                <td align="left">
                                    <input type="radio" name="metodo" value="dinheiro" <c:if test="${doacaoParaUpdate.isDinheiro == true}">checked="checked"</c:if> onchange="setDoacaoMetodo(this.value)"> <span class="label_list_item">Dinheiro</span>
                                    <input type="radio" name="metodo" value="outro" <c:if test="${doacaoParaUpdate.isOutro == true}">checked="checked"</c:if> onchange="setDoacaoMetodo(this.value)" onload='alert("a")'> <span class="label_list_item">Outro</span>
                                </td>
                            </tr>
                            <tbody id="metodoDinheiro" <c:if test="${doacaoParaUpdate.isDinheiro == false}"> style="display:none"</c:if>>
                                <tr>
                                    <td align="right" class="label_list_item">Dinheiro: </td>
                                    <td>
                                        <input type="text" class="input_text it_date_bigger" maxlength="12" placeholder="Valor" name="dinheiro" value="${intPart}"> ,
                                        <input type="text" class="input_text it_date_mini" maxlength="2" placeholder="00" name="dinheiroPosVirgula" value="${decPart}">
                                    </td>
                                </tr>
                            </tbody>
                            <tbody id="metodoOutro" <c:if test="${doacaoParaUpdate.isOutro == false}"> style="display:none"</c:if>>
                                <tr>
                                    <td align="right" class="label_list_item">Outro: </td>
                                    <td>
                                        <textarea class="input_textarea it_large" placeholder="Digite por qual meio" name="outro">${doacaoParaUpdate.outro}</textarea>
                                    </td>
                                </tr>
                            </tbody>
                            <tr>
                                <td align="right" class="label_list_item">Descri��o: </td>
                                <td>
                                    <textarea class="input_textarea it_large" placeholder="Digite uma descri��o" name="descricao">${doacaoParaUpdate.descricao}</textarea>
                                </td>
                            </tr>
                            <tr>
                                <td><input type="hidden" name="id" value="<%= request.getParameter("id")%>"></td>
                                <td align="center"><input type="submit" value="Salvar" class="input_button" style="width:326px; height:40px"></td>
                            </tr>

                        </table>
                    </div>
                </div>
            </div>
        </form>





        <br><br>

        <%@include file="../../../includes/bottom_bar.jsp"%>
    </body>
</html>
