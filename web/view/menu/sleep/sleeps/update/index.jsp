<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Ong Viver - Pesquisar hospedagens cadastradas</title>
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/main.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/top_bar.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/bottom_bar.css">
<script src="${pageContext.servletContext.contextPath}/assets/javascript/main.js"></script>
<script src="${pageContext.servletContext.contextPath}/assets/javascript/pesquisa.js"></script>
</head>
<body onLoad="introCard()">
<%@include file="../../../../includes/top_bar.jsp"%>

<br>

<div align="center">
    <div class="navigator">
    	<a href="${pageContext.servletContext.contextPath}/view/menu/" class="navigator_link">Menu</a>
        >
        <a href="${pageContext.servletContext.contextPath}/view/menu/sleep/index.jsp" class="navigator_link">Hospedagem</a>
        >
        <a href="${pageContext.servletContext.contextPath}/view/menu/sleep/sleeps/index.jsp" class="navigator_link">Hospedagens</a>
        >
        <a href="${pageContext.servletContext.contextPath}/view/menu/sleep/sleeps/read/index.jsp" class="navigator_link">Pesquisar hospedagens cadastradas</a>
    </div>
</div>


<br>


<div align="center">
<form action="${pageContext.servletContext.contextPath}/view/menu/sleep/sleeps/update" method="POST">
<div align="center">
	<div align="right" class="card_support_loading" id="card_support">
        <div class="card" style="width:620px;" align="center">
            <input type="hidden" id="idPaciente" name="idPaciente" value="${hospedagem.idpaciente}">
            <input type="hidden" id="idH" name="idH" value="${hospedagem.id}">
            <table>
                <tr>
                    <td align="center" colspan="2" class="label_title_card">Cadastrar uma Hospedagem</td>
                </tr>
                </tbody>
                <tr>
                    <td align="right" class="label_list_item">Data: </td>
                    <td>
                        <input type="text" class="input_text it_date_mini" maxlength="2" placeholder="Dia" name="dia" value="${dia}">
                        <input type="text" class="input_text it_date_mini" maxlength="2" placeholder="Mês" name="mes" value="${mes}">
                        <input type="text" class="input_text it_date_large" maxlength="4" placeholder="Ano" name="ano" value="${ano}">
                    </td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Número do Quarto: </td>
                    <td><input type="text" class="input_text " name="quarto" value = "${hospedagem.quarto}"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Anotações: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Informações adicionais" name="anotacoes" value="${hospedagem.anotacoes}"></td>
                </tr>                
                <tr>
                    <td></td>
                    <td align="center"><input type="submit" value="Salvar" class="input_button" style="width:326px; height:40px"></td>
                    
                </tr>
            </table>
        </div>
    </div>
</div>
</form>
</div>






<br><br>

<%@include file="../../../../includes/bottom_bar.jsp"%>
</body>
</html>