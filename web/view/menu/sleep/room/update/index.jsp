<!doctype html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
<meta charset="UTF-8">
<title>Ong Viver - Atualizar quarto</title>
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/main.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/top_bar.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/bottom_bar.css">
<script src="${pageContext.servletContext.contextPath}/assets/javascript/main.js"></script>
</head>
<body onLoad="introCard()">
<%@include file="../../../../includes/top_bar.jsp"%>

<br>

<div align="center">
    <div class="navigator">
    	<a href="${pageContext.servletContext.contextPath}/view/menu/" class="navigator_link">Menu</a>
        >
        <a href="${pageContext.servletContext.contextPath}/view/menu/sleep/index.jsp" class="navigator_link">Hospedagem</a>
        >
        <a href="${pageContext.servletContext.contextPath}/view/menu/sleep/room/index.jsp" class="navigator_link">Quartos</a>
        >
        <a href="#" class="navigator_link">Atualizar um quarto</a>
    </div>
</div>

<br>

<form action="${pageContext.servletContext.contextPath}/view/menu/sleep/room/update" method="post">
<div align="center">
	<div align="right" class="card_support_loading" id="card_support">
        <div class="card" style="width:620px;" align="center">
            <table>
                <tr>
                    <td align="center" colspan="2" class="label_title_card">Atualizar um quarto</td>
                </tr>
                <tr>
                    <td></td>
                    <td class="label_list_item">
                        <c:choose>
                        <c:when test="${quartoParaUpdate.ocupado}">
                            <input type="checkbox" id="ocupado" name="ocupado" CHECKED>
                        </c:when>
                        <c:otherwise>
                            <input type="checkbox" id="ocupado" name="ocupado">
                        </c:otherwise>
                        </c:choose>
                        Ocupado
                    </td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Capacidade: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite a quantidade m�xima de hospedes" name="capacidade" value="${quartoParaUpdate.capacidade}"></td>
                </tr>
                
                <tr>
                    <td><input type="hidden" name="id" value="<%= request.getParameter("id") %>"></td>
                    <td align="center"><input type="submit" value="Salvar" class="input_button" style="width:326px; height:40px"></td>
                </tr>
            </table>
        </div>
    </div>
</div>
</form>





<br><br>


<%@include file="../../../../includes/bottom_bar.jsp"%>
</body>
</html>
