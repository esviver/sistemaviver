<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Ong Viver - Quartos</title>
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/main.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/top_bar.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/bottom_bar.css">
<script src="${pageContext.servletContext.contextPath}/assets/javascript/main.js"></script>
</head>
<body onLoad="introMenu()">
<%@include file="../../../includes/top_bar.jsp"%>

<br>

<div align="center">
    <div class="navigator">
    	<a href="${pageContext.servletContext.contextPath}/view/menu/" class="navigator_link">Menu</a>
        >
        <a href="${pageContext.servletContext.contextPath}/view/menu/sleep/index.jsp" class="navigator_link">Hospedagem</a>
        >
        <a href="${pageContext.servletContext.contextPath}/view/menu/sleep/room/index.jsp" class="navigator_link">Quartos</a>
    </div>
</div>

<br>


<table class="menu menu_loading" align="center" cellpadding="0" cellspacing="0" id="menu" border="0">
	<tr>
    	<td align="left" valign="top">
            <a href="${pageContext.servletContext.contextPath}/view/menu/sleep/room/create/index.jsp" class="menu_box_link">
            	<div class="menu_box menu_sleep_room_add" align="center">Cadastrar quarto</div>
            </a>
            <div class="menu_hole"></div>
        </td>
        <td align="right" valign="top">
        	<a href="${pageContext.servletContext.contextPath}/view/menu/sleep/room/read/index.jsp" class="menu_box_link">
            	<div class="menu_box menu_sleep_room_search" align="center">Pesquisar quartos</div>
            </a>
			<div class="menu_hole"></div>
        </td>
    </tr>
    
</table>


<br><br>

<%@include file="../../../includes/bottom_bar.jsp"%>
</body>
</html>