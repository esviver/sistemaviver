<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Ong Viver - Cadastrar quarto</title>
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/main.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/top_bar.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/bottom_bar.css">
<script src="${pageContext.servletContext.contextPath}/assets/javascript/main.js"></script>
</head>
<body onLoad="introCard()">
<%@include file="../../../../includes/top_bar.jsp"%>

<br>

<div align="center">
    <div class="navigator">
    	<a href="${pageContext.servletContext.contextPath}/view/menu/" class="navigator_link">Menu</a>
        >
        <a href="${pageContext.servletContext.contextPath}/view/menu/sleep/index.jsp" class="navigator_link">Hospedagem</a>
        >
        <a href="${pageContext.servletContext.contextPath}/view/menu/sleep/room/index.jsp" class="navigator_link">Quartos</a>
        >
        <a href="${pageContext.servletContext.contextPath}/view/menu/sleep/room/create/index.jsp" class="navigator_link">Cadastrar quarto</a>
    </div>
</div>

<br>

<form action="${pageContext.servletContext.contextPath}/view/menu/sleep/room/create" method="POST">
<div align="center">
	<div align="right" class="card_support_loading" id="card_support">
        <div class="card" style="width:620px;" align="center">
            <table>
                <tr>
                    <td align="center" colspan="2" class="label_title_card">Cadastrar um novo quarto</td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">N�mero: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite o n�mero do quarto" name="id"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Capacidade: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite a quantidade m�xima de hospedes" name="capacidade"></td>
                </tr>
                
                <tr>
                    <td></td>
                    <td align="center"><input type="submit" value="Salvar" class="input_button" style="width:326px; height:40px"></td>
                </tr>
            </table>
        </div>
    </div>
</div>
</form>





<br><br>


<%@include file="../../../../includes/bottom_bar.jsp"%>
</body>
</html>
