<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<table border="0" align="left" class="table_hover" cellpadding="5" cellspacing="0">
    <tr style="background-color:#EEE !important;">
    	<td style="min-width:100px;"></td>
    	<td class="list_view_title" align="left">N�mero</td>
        <td class="list_view_title" align="left">Capacidade</td>
        <td class="list_view_title" align="left">Ocupado?</td>
    </tr>
    <c:set var="i" value="0"/>
    <c:forEach items="${listaPResultados}" var="resultado">
    <c:if test="${resultado.status == false}">
    <tr>
	<td>
            <a href="../update/index.jsp?id=${resultado.id}" class="list_view_link">Editar</a>
            �
            <a href="../recover?id=${resultado.id}" class="list_view_link">Recuperar</a>
        </td>
        <td class="label_list_item">
            <div style="width:100px" class="list_view_text_trash">${resultado.id}</div>
        </td>
    	<td class="label_list_item">
            <div style="width:100px" class="list_view_text_trash">${resultado.capacidade}</div>
        </td>
        <td class="label_list_item">
            <div style="width:100px" class="list_view_text_trash">${resultado.ocupado}</div>
        </td>
    </tr>
    </c:if>
    <c:if test="${resultado.status == true}">
    <tr>
	<td>
            <a href="../update?id=${resultado.id}" class="list_view_link">Editar</a>
            �
            <a href="../delete/index.jsp?id=${resultado.id}" class="list_view_link">Deletar</a>
        </td>
        <td class="label_list_item">
            <div style="width:100px" class="list_view_text">${resultado.id}</div>
        </td>
    	<td class="label_list_item">
            <div style="width:100px" class="list_view_text">${resultado.capacidade}</div>
        </td>
        <td class="label_list_item">
            <c:choose>
                <c:when test="${resultado.ocupado}">
                    <div style="width:100px" class="list_view_text">Sim</div>
                </c:when>
                <c:otherwise>
                    <div style="width:100px" class="list_view_text">N�o</div>
                </c:otherwise>
            </c:choose>
            
        </td>
    </tr>
    </c:if>
    <c:set var="i" value="${i+1}"/>
    </c:forEach>
</table>
<div style=" min-height:200px;">
</div>
