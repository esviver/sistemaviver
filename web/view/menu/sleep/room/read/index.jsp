<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Ong Viver - Pesquisar quartos cadastrados</title>
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/main.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/top_bar.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/bottom_bar.css">
<script src="${pageContext.servletContext.contextPath}/assets/javascript/main.js"></script>
<script src="${pageContext.servletContext.contextPath}/assets/javascript/pesquisa.js"></script>
</head>
<body onLoad="introCard()">
<%@include file="../../../../includes/top_bar.jsp"%>

<br>

<div align="center">
    <div class="navigator">
    	<a href="${pageContext.servletContext.contextPath}/view/menu/" class="navigator_link">Menu</a>
        >
        <a href="${pageContext.servletContext.contextPath}/view/menu/sleep/index.jsp" class="navigator_link">Hospedagem</a>
        >
        <a href="${pageContext.servletContext.contextPath}/view/menu/sleep/room/index.jsp" class="navigator_link">Quartos</a>
        >
        <a href="${pageContext.servletContext.contextPath}/view/menu/sleep/room/read/index.jsp" class="navigator_link">Pesquisar quartos cadastrados</a>
    </div>
</div>


<br>


<div align="center">
	<div align="right" class="card_support_loading" id="card_support">
        <div class="card" style="width:620px;" align="center">
            <span class="label_title_card">Pesquisar por quartos cadastrados</span><br><br>
            <table width="620">
            	<tr>
                    <td width="100%">
                    	<input id="search_box" autocomplete="off" type="text" placeholder="Fa�a sua pesquisa..." class="input_text" style="width:calc(100% - 24px);" onKeyUp="pesquisarQuarto(event,search_box.value)">
                    </td>
                    <td>
                    	<input style="height:44px;" type="button" value="Pesquisar" class="input_button" onClick="pesquisarQuarto(event,search_box.value)">
                    </td>
                </tr>
            </table>
            <br>
            <div class="result_content" id="result_content">
            	<div class="search_msg_box search_msg_box_room" width="100"></div>
            </div>
            <br>
            <input type="checkbox" onChange="setExibirItensExcluidos(this.checked)"> <span class="label_info">Mostrar itens exclu�dos</span><br>
            <a href="javascript:void(0)" onClick="mostrarTudoQuarto(event)" class="label_info">Mostrar tudo</a>
        </div>
    </div>
</div>






<br><br>

<%@include file="../../../../includes/bottom_bar.jsp"%>
</body>
</html>