<!doctype html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ong Viver - Atualizar cadastro</title>
        <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/main.css">
        <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/top_bar.css">
        <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/bottom_bar.css">
        <script src="${pageContext.servletContext.contextPath}/assets/javascript/main.js"></script>
    </head>
    <body onLoad="introCard()">    
        <%@include file="../../../includes/top_bar.jsp"%>

        <br>

        <div align="center">
            <div class="navigator">
                <a href="${pageContext.servletContext.contextPath}/view/menu/" class="navigator_link">Menu</a>
                >
                <a href="${pageContext.servletContext.contextPath}/view/menu/account/index.jsp" class="navigator_link">Meu cadastro</a>
                >
                <a href="${pageContext.servletContext.contextPath}/view/menu/account/update/index.jsp" class="navigator_link">Atualizar cadastro</a>
            </div>
        </div>


        <br>

        <form action="${pageContext.servletContext.contextPath}/view/menu/account/update" method="POST">
            <div align="center">
                <div align="right" class="card_support_loading" id="card_support">
                    <div class="card" style="width:620px;" align="center">
                        <table>
                            <tr>
                                <td align="center" colspan="2" class="label_title_card">Atualizar usu�rio<br>EM DESENVOLVIMENTO</td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item"></td>
                                <td class="label_list_item" style="color:#555;"><b>Dados de usu�rio</b></td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">Login: </td>
                                <td><input type="text" class="input_text it_large" placeholder="Digite o login" name="login" value="${usuarioParaUpdate.login}"></td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">Senha: </td>
                                <td><input type="password" class="input_text it_large" placeholder="Digite a senha" name="senha" value="${usuarioParaUpdate.senha}"></td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">Nome: </td>
                                <td><input type="text" class="input_text it_large" placeholder="Digite o nome" name="pNome" value="${usuarioParaUpdate.pnome}"></td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">Sobrenome: </td>
                                <td><input type="text" class="input_text it_large" placeholder="Digite o nome" name="sNome" value="${usuarioParaUpdate.snome}"></td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">Telefone: </td>
                                <td>
                                    <input type="text" class="input_text it_date_mini" maxlength="2" placeholder="DDD" name="dddTel" value="${telDDD}">
                                    <input type="text" class="input_text it_date_bigger" maxlength="10" placeholder="Digite o telefone" name="telefone" value="${tel}">
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">Celular: </td>
                                <td>
                                    <input type="text" class="input_text it_date_mini" maxlength="2" placeholder="DDD" name="dddCel" value="${celDDD}">
                                    <input type="text" class="input_text it_date_bigger" maxlength="10" placeholder="Digite o celular" name="celular" value="${cel}">
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">RG: </td>
                                <td><input type="text" class="input_text it_large" placeholder="Digite o RG" name="rg_num" value="${usuarioParaUpdate.rg_num}"></td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">CPF: </td>
                                <td><input type="text" class="input_text it_large" placeholder="Digite o CPF" name="cpf" value="${usuarioParaUpdate.cpf}"></td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">Data nascimento: </td>
                                <td>
                                    <input type="text" class="input_text it_date_mini" maxlength="2" placeholder="Dia" name="dataNascUsuarioDia" value="${ano}">
                                    <input type="text" class="input_text it_date_mini" maxlength="2" placeholder="M�s" name="dataNascUsuarioMes" value="${mes}">
                                    <input type="text" class="input_text it_date_large" maxlength="4" placeholder="Ano" name="dataNascUsuarioAno" value="${dia}">
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">Sexo: </td>
                                <td align="left">
                                    <c:choose>
                                        <c:when test="${usuarioParaUpdate.sexo == 'masculino'}">
                                            <input type="radio" name="sexo" value="masculino" checked> <span class="label_list_item">Masculino</span>
                                            <input type="radio" name="sexo" value="feminino"> <span class="label_list_item">Feminino</span>
                                        </c:when>
                                        <c:when test="${usuarioParaUpdate.sexo == 'feminino'}">
                                            <input type="radio" name="sexo" value="masculino"> <span class="label_list_item">Masculino</span>
                                            <input type="radio" name="sexo" value="feminino" checked> <span class="label_list_item">Feminino</span>
                                        </c:when>
                                        <c:otherwise>
                                            <input type="radio" name="sexo" value="masculino"> <span class="label_list_item">Masculino</span>
                                            <input type="radio" name="sexo" value="feminino"> <span class="label_list_item">Feminino</span>
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">Anota��es: </td>
                                <td>
                                    <textarea class="input_textarea it_large" placeholder="Digite alguma anota��es" name="anotacoes">${usuarioParaUpdate.anotacoes}</textarea>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item"></td>
                                <td class="label_list_item" style="color:#555;"><b>Endere�o</b></td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">Logradouro: </td>
                                <td><input type="text" class="input_text it_large" placeholder="Rua, Avenida, Travessa, Pra�a" name="logradouro" value="${userEnderecoParaUpdate.logradouro}"></td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">Numero: </td>
                                <td><input type="text" class="input_text it_large" placeholder="Digite o numero" name="numero" value="${userEnderecoParaUpdate.numero}"></td>
                            </tr>                   
                            <tr>
                                <td align="right" class="label_list_item">Bairro: </td>
                                <td><input type="text" class="input_text it_large" placeholder="Digite o bairro" name="bairro" value="${userEnderecoParaUpdate.bairro}"></td>
                            </tr> 
                            <tr>
                                <td align="right" class="label_list_item">CEP: </td>
                                <td><input type="text" class="input_text it_large" placeholder="Digite o seu CEP" name="cep" value="${userEnderecoParaUpdate.cep}"></td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">Ponto de referencia: </td>
                                <td><input type="text" class="input_text it_large" placeholder="Digite o ponto de referencia" name="pontoreferencia" value="${userEnderecoParaUpdate.pontoreferencia}"></td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">Cidade: </td>
                                <td><input type="text" class="input_text it_large" placeholder="Digite o nome da cidade" name="cidade" value="${userEnderecoParaUpdate.cidade}"></td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">Estado: </td>
                                <td><input type="text" class="input_text it_large" placeholder="Digite a sigla do estado" name="estado" value="${userEnderecoParaUpdate.estado}"></td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item"></td>
                                <td class="label_list_item" style="color:#555;"><b>Tipo de Cadastro</b></td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">Fun��o: </td>
                                <td align="left" class="label_list_item">
                                    <c:choose>
                                        <c:when test="${usuarioParaUpdate.tipoDeUser == 'funcionario'}">
                                            <input type="radio" name="tipo" value="funcionario" onchange="setAccountExtras(this.value)" checked> Funcion�rio<br>
                                    <input type="radio" name="tipo" value="adm" onchange="setAccountExtras(this.value)"> Adm<br>
                                            <input type="radio" name="tipo" value="psicologo" onchange="setAccountExtras(this.value)"> Psic�logo<br>
                                            <input type="radio" name="tipo" value="assistente" onchange="setAccountExtras(this.value)"> Assistente social<br>
                                            <input type="radio" name="tipo" value="voluntario" onchange="setAccountExtras(this.value)"> Volunt�rio
                                        </c:when>
                                        <c:when test="${usuarioParaUpdate.tipoDeUser == 'psicologo'}">
                                            <input type="radio" name="tipo" value="funcionario" onchange="setAccountExtras(this.value)"> Funcion�rio<br>
                                    <input type="radio" name="tipo" value="adm" onchange="setAccountExtras(this.value)"> Adm<br>
                                            <input type="radio" name="tipo" value="psicologo" onchange="setAccountExtras(this.value)" checked> Psic�logo<br>
                                            <input type="radio" name="tipo" value="assistente" onchange="setAccountExtras(this.value)"> Assistente social<br>
                                            <input type="radio" name="tipo" value="voluntario" onchange="setAccountExtras(this.value)"> Volunt�rio
                                        </c:when>
                                        <c:when test="${usuarioParaUpdate.tipoDeUser == 'assistente'}">
                                            <input type="radio" name="tipo" value="funcionario" onchange="setAccountExtras(this.value)"> Funcion�rio<br>
                                    <input type="radio" name="tipo" value="adm" onchange="setAccountExtras(this.value)"> Adm<br>
                                            <input type="radio" name="tipo" value="psicologo" onchange="setAccountExtras(this.value)"> Psic�logo<br>
                                            <input type="radio" name="tipo" value="assistente" onchange="setAccountExtras(this.value)" checked> Assistente social<br>
                                            <input type="radio" name="tipo" value="voluntario" onchange="setAccountExtras(this.value)"> Volunt�rio
                                        </c:when>
                                        <c:when test="${usuarioParaUpdate.tipoDeUser == 'voluntario'}">
                                            <input type="radio" name="tipo" value="funcionario" onchange="setAccountExtras(this.value)"> Funcion�rio<br>
                                    <input type="radio" name="tipo" value="adm" onchange="setAccountExtras(this.value)"> Adm<br>
                                            <input type="radio" name="tipo" value="psicologo" onchange="setAccountExtras(this.value)"> Psic�logo<br>
                                            <input type="radio" name="tipo" value="assistente" onchange="setAccountExtras(this.value)"> Assistente social<br>
                                            <input type="radio" name="tipo" value="voluntario" onchange="setAccountExtras(this.value)" checked> Volunt�rio
                                        </c:when>
                                        <c:otherwise>
                                            <input type="radio" name="tipo" value="funcionario" onchange="setAccountExtras(this.value)"> Funcion�rio<br>
                                            <input type="radio" name="tipo" value="adm" onchange="setAccountExtras(this.value)" checked> Adm<br>
                                            <input type="radio" name="tipo" value="psicologo" onchange="setAccountExtras(this.value)"> Psic�logo<br>
                                            <input type="radio" name="tipo" value="assistente" onchange="setAccountExtras(this.value)"> Assistente social<br>
                                            <input type="radio" name="tipo" value="voluntario" onchange="setAccountExtras(this.value)"> Volunt�rio
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                            </tr>
                            <tbody style="display: none" id="dados_psicologico">
                                <tr>
                                    <td align="right" class="label_list_item"></td>
                                    <td class="label_list_item" style="color:#555;"><b>Dados adicionais de psic�logo:</b></td>
                                </tr>
                                <tr>
                                    <c:choose>
                                        <c:when test="${isPsico}">
                                            <td align="right" class="label_list_item">CRP: </td>
                                            <td><input type="text" class="input_text it_date_large" placeholder="C�digo" name="crp" value="${crpPsico}"></td>
                                        </c:when>
                                        <c:otherwise>
                                            <td align="right" class="label_list_item">CRP: </td>
                                            <td><input type="text" class="input_text it_date_large" placeholder="C�digo" name="crp"></td>
                                        </c:otherwise>
                                    </c:choose>
                                </tr>
                            </tbody>
                            <tbody style="display: none" id="dados_assistente">
                                <tr>
                                    <td align="right" class="label_list_item"></td>
                                    <td class="label_list_item" style="color:#555;"><b>Dados adicionais de Assistente Social:</b></td>
                                </tr>
                                <tr>
                                    <c:choose>
                                        <c:when test="${isAss}">
                                            <td align="right" class="label_list_item">CRESS: </td>
                                            <td><input type="text" class="input_text it_date_large" placeholder="C�digo" name="cress" value="${cressAss}"></td>
                                        </c:when>
                                        <c:otherwise>
                                            <td align="right" class="label_list_item">CRESS: </td>
                                            <td><input type="text" class="input_text it_date_large" placeholder="C�digo" name="cress"></td>
                                        </c:otherwise>
                                    </c:choose>
                                </tr>
                            </tbody>
                            <tbody style="display: none" id="dados_funcionario_voluntario">
                                <tr>
                                    <td align="right" class="label_list_item"></td>
                                    <td class="label_list_item" style="color:#555;"><b>Dados adicionais de funcion�rio ou volunt�rio:</b></td>
                                </tr>
                                <tr>
                                    <td align="right" class="label_list_item">Email: </td>
                                    <td><input type="text" class="input_text it_large" placeholder="Digite o email" name="email"></td>
                                </tr>
                                <tr>
                                    <td align="right" class="label_list_item">Profiss�o: </td>
                                    <td><input type="text" class="input_text it_large" placeholder="Digite a profiss�o" name= "profissao" ></td>
                                </tr>
                                <tr>
                                    <td align="right" class="label_list_item">Escolaridade: </td>
                                    <td><input type="text" class="input_text it_large" placeholder="Digite a escolaridade" name="escolaridade"></td>
                                </tr>
                                <tr>
                                    <td align="right" class="label_list_item">Local de trabalho: </td>
                                    <td><input type="text" class="input_text it_large" placeholder="Digite o local de trabalho" name="localTrabalho"></td>
                                </tr>

                                <tr>
                                    <td align="right" class="label_list_item">Estado civil: </td>
                                    <td><input type="text" class="input_text it_large" placeholder="Digite o estado civil" name="estadoCivil"></td>
                                </tr>
                                <tr>
                                    <td align="right" class="label_list_item">Faz uso de algum<br> medicamento: </td>
                                    <td align="left">
                                        <input type="radio" name="medicamento" value="sim" onchange="setMedicamento(this.value)"> <span class="label_list_item">Sim</span>
                                        <input type="radio" name="medicamento" value="nao" onchange="setMedicamento(this.value)"> <span class="label_list_item">N�o</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" class="label_list_item">Medicamento: </td>
                                    <td><input type="text" class="input_text " placeholder="Digite o nome do medicamento" name="nomeMedicamento"></td>
                                </tr>
                                <tr>
                                    <td align="right" class="label_list_item"></td>
                                    <td class="label_list_item" style="color:#555;"><b>Em caso de emergencia quem chamar:</b></td>
                                </tr>
                                <tr>
                                    <td align="right" class="label_list_item">Nome: </td>
                                    <td><input type="text" class="input_text it_large" placeholder="Digite quem chamar em caso de emerg�ncia" name="??????"></td>
                                </tr>
                                <tr>
                                    <td align="right" class="label_list_item">Telefone: </td>
                                    <td>
                                        <input type="text" class="input_text it_date_mini" maxlength="2" placeholder="DDD" name="??????">
                                        <input type="text" class="input_text it_date_bigger" maxlength="10" placeholder="Digite o telefone" name="??????">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" class="label_list_item">Como conheceu a<br>organiza��o viver: </td>
                                    <td>
                                        <textarea class="input_textarea it_large" placeholder="Digite como voc� conheceu" name="?????"></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" class="label_list_item">Conhece algu�m que<br>realiza ou j� realizou<br>trabalho volunt�rio <br>nesta entidade?</td>
                                    <td>
                                        <textarea class="input_textarea it_large" placeholder="Digite o nome desta pessoa se conhecer" name="?????"></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" class="label_list_item">??: </td>
                                    <td>
                                        <textarea class="input_textarea it_large" placeholder="Digite quem chamar em caso de emerg�ncia" name="?????"></textarea>
                                    </td>
                                </tr>
                            </tbody>

                            <tr>
                                <td></td>
                                <td align="center">
                                    <input type="submit" value="Salvar" class="input_button" style="width:326px; height:40px">
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </form>





        <br><br>


        <%@include file="../../../includes/bottom_bar.jsp"%>
    </body>
</html>