<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ong Viver - Criar cadastro</title>
        <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/main.css">
        <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/top_bar.css">
        <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/bottom_bar.css">
        <script src="${pageContext.servletContext.contextPath}/assets/javascript/main.js"></script>
    </head>
    <body onLoad="introCard()">    
        <%@include file="../../../includes/top_bar.jsp"%>

        <br>

        <div align="center">
            <div class="navigator">
                <a href="${pageContext.servletContext.contextPath}/view/menu/" class="navigator_link">Menu</a>
                >
                <a href="${pageContext.servletContext.contextPath}/view/menu/account/index.jsp" class="navigator_link">Meu cadastro</a>
                >
                <a href="${pageContext.servletContext.contextPath}/view/menu/account/create/index.jsp" class="navigator_link">Criar cadastro</a>
            </div>
        </div>


        <br>

        <form action="${pageContext.servletContext.contextPath}/view/menu/account/create" method="POST">
            <div align="center">
                <div align="right" class="card_support_loading" id="card_support">
                    <div class="card" style="width:620px;" align="center">
                        <table>
                            <tr>
                                <td align="center" colspan="2" class="label_title_card">Cadastrar novo usu�rio<br>EM DESENVOLVIMENTO</td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item"></td>
                                <td class="label_list_item" style="color:#555;"><b>Dados de usu�rio</b></td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">Login: </td>
                                <td><input type="text" class="input_text it_large" placeholder="Digite o login" name="login" ></td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">Senha: </td>
                                <td><input type="password" class="input_text it_large" placeholder="Digite a senha" name="senha" ></td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">Nome: </td>
                                <td><input type="text" class="input_text it_large" placeholder="Digite o nome" name="pNome" ></td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">Sobrenome: </td>
                                <td><input type="text" class="input_text it_large" placeholder="Digite o nome" name="sNome" ></td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">Telefone: </td>
                                <td>
                                    <input type="text" class="input_text it_date_mini" maxlength="2" placeholder="DDD" name="dddTel" >
                                    <input type="text" class="input_text it_date_bigger" maxlength="10" placeholder="Digite o telefone" name="telefone" >
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">Celular: </td>
                                <td>
                                    <input type="text" class="input_text it_date_mini" maxlength="2" placeholder="DDD" name="dddCel" >
                                    <input type="text" class="input_text it_date_bigger" maxlength="10" placeholder="Digite o celular" name="celular" >
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">RG: </td>
                                <td><input type="text" class="input_text it_large" placeholder="Digite o RG" name="rg_num" ></td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">CPF: </td>
                                <td><input type="text" class="input_text it_large" placeholder="Digite o CPF" name="cpf"></td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">Data nascimento: </td>
                                <td>
                                    <input type="text" class="input_text it_date_mini" maxlength="2" placeholder="Dia" name="dataNascUsuarioDia" >
                                    <input type="text" class="input_text it_date_mini" maxlength="2" placeholder="M�s" name="dataNascUsuarioMes" >
                                    <input type="text" class="input_text it_date_large" maxlength="4" placeholder="Ano" name="dataNascUsuarioAno" >
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">Sexo: </td>
                                <td align="left">
                                    <input type="radio" name="sexo" value="masculino" checked> <span class="label_list_item">Masculino</span>
                                    <input type="radio" name="sexo" value="feminino"> <span class="label_list_item">Feminino</span>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">Anota��es: </td>
                                <td>
                                    <textarea class="input_textarea it_large" placeholder="Digite alguma anota��es" name="anotacoes">  </textarea>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item"></td>
                                <td class="label_list_item" style="color:#555;"><b>Endere�o</b></td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">Logradouro: </td>
                                <td><input type="text" class="input_text it_large" placeholder="Rua, Avenida, Travessa, Pra�a" name="logradouro" ></td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">Numero: </td>
                                <td><input type="text" class="input_text it_large" placeholder="Digite o numero" name="numero" ></td>
                            </tr>                   
                            <tr>
                                <td align="right" class="label_list_item">Bairro: </td>
                                <td><input type="text" class="input_text it_large" placeholder="Digite o bairro" name="bairro" ></td>
                            </tr> 
                            <tr>
                                <td align="right" class="label_list_item">CEP: </td>
                                <td><input type="text" class="input_text it_large" placeholder="Digite o seu CEP" name="cep" ></td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">Ponto de referencia: </td>
                                <td><input type="text" class="input_text it_large" placeholder="Digite o ponto de referencia" name="pontoreferencia" ></td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">Cidade: </td>
                                <td><input type="text" class="input_text it_large" placeholder="Digite o nome da cidade" name="cidade" ></td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">Estado: </td>
                                <td><input type="text" class="input_text it_large" placeholder="Digite a sigla do estado" name="estado" ></td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item"></td>
                                <td class="label_list_item" style="color:#555;"><b>Tipo de Cadastro</b></td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">Fun��o: </td>
                                <td align="left" class="label_list_item">
                                    <input type="radio" name="tipo" value="funcionario" onchange="setAccountExtras(this.value)" checked> Funcion�rio<br>
                                    <input type="radio" name="tipo" value="adm" onchange="setAccountExtras(this.value)"> Adm<br>
                                    <input type="radio" name="tipo" value="psicologo" onchange="setAccountExtras(this.value)"> Psic�logo<br>
                                    <input type="radio" name="tipo" value="assistente" onchange="setAccountExtras(this.value)"> Assistente social<br>
                                    <input type="radio" name="tipo" value="voluntario" onchange="setAccountExtras(this.value)"> Volunt�rio
                                </td>
                            </tr>
                            <tbody style="display: none" id="dados_psicologico">
                                <tr>
                                    <td align="right" class="label_list_item"></td>
                                    <td class="label_list_item" style="color:#555;"><b>Dados adicionais de psic�logo:</b></td>
                                </tr>
                                <tr>
                                    <td align="right" class="label_list_item">CRP: </td>
                                    <td><input type="text" class="input_text it_date_large" placeholder="C�digo" name="crp" ></td>
                                </tr>
                            </tbody>
                            <tbody style="display: none" id="dados_assistente">
                                <tr>
                                    <td align="right" class="label_list_item"></td>
                                    <td class="label_list_item" style="color:#555;"><b>Dados adicionais de Assistente Social:</b></td>
                                </tr>
                                <tr>
                                    <td align="right" class="label_list_item">CRESS: </td>
                                    <td><input type="text" class="input_text it_date_large" placeholder="C�digo" name="cress" ></td>
                                </tr>
                            </tbody>
                            <tbody style="display: none" id="dados_funcionario_voluntario">
                                <tr>
                                    <td align="right" class="label_list_item"></td>
                                    <td class="label_list_item" style="color:#555;"><b>Dados adicionais de volunt�rio:</b></td>
                                </tr>
                                <tr>
                                    <td align="right" class="label_list_item">Email: </td>
                                    <td><input type="text" class="input_text it_large" placeholder="Digite o email" name="email" ></td>
                                </tr>
                                <tr>
                                    <td align="right" class="label_list_item">Profiss�o: </td>
                                    <td><input type="text" class="input_text it_large" placeholder="Digite a profiss�o" name= "profissao"  ></td>
                                </tr>
                                <tr>
                                    <td align="right" class="label_list_item">Escolaridade: </td>
                                    <td><input type="text" class="input_text it_large" placeholder="Digite a escolaridade" name="escolaridade" ></td>
                                </tr>
                                <tr>
                                    <td align="right" class="label_list_item">Local de trabalho: </td>
                                    <td><input type="text" class="input_text it_large" placeholder="Digite o local de trabalho" name="localTrabalho" ></td>
                                </tr>

                                <tr>
                                    <td align="right" class="label_list_item">Estado civil: </td>
                                    <td><input type="text" class="input_text it_large" placeholder="Digite o estado civil" name="estadoCivil" ></td>
                                </tr>
                                <tr>
                                    <td align="right" class="label_list_item">Faz uso de algum<br> medicamento: </td>
                                    <td align="left">
                                        <input type="radio" name="medicamento" value="sim" onchange="setMedicamento(this.value)" checked> <span class="label_list_item">Sim</span>
                                        <input type="radio" name="medicamento" value="nao" onchange="setMedicamento(this.value)"> <span class="label_list_item">N�o</span>
                                    </td>
                                </tr>
                                <tr id="nome_medicamento" style="display:none">
                                    <td align="right" class="label_list_item">Medicamento: </td>
                                    <td><input type="text" class="input_text it_large" placeholder="Digite o nome do medicamento" name="nomeMedicamento" ></td>
                                </tr>
                                <tr>
                                    <td align="right" class="label_list_item"></td>
                                    <td class="label_list_item" style="color:#555;"><b>Em caso de emergencia quem chamar:</b></td>
                                </tr>
                                <tr>
                                    <td align="right" class="label_list_item">Nome: </td>
                                    <td><input type="text" class="input_text it_large" placeholder="Digite quem chamar em caso de emerg�ncia" name="cdenome" ></td>
                                </tr>
                                <tr>
                                    <td align="right" class="label_list_item">Telefone: </td>
                                    <td>
                                        <input type="text" class="input_text it_date_mini" maxlength="2" placeholder="DDD" name="cdetelefoneDDD" >
                                        <input type="text" class="input_text it_date_bigger" maxlength="10" placeholder="Digite o telefone" name="cdetelefone" >
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" class="label_list_item"></td>
                                    <td class="label_list_item" style="color:#555;"><b>Question�rio:</b></td>
                                </tr>
                                <tr>
                                    <td align="right" class="label_list_item">
                                        <div style="width: 150px">
                                            Como conheceu a organiza��o viver:
                                        </div>
                                    </td>
                                    <td>
                                        <textarea class="input_textarea it_large" placeholder="Digite como voc� conheceu" name="perg1">  </textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" class="label_list_item">
                                        <div style="width: 150px">
                                            Conhece algu�m que realiza ou j� realizou trabalho volunt�rio nesta entidade?
                                        </div>
                                    </td>
                                    <td>
                                        <textarea class="input_textarea it_large" placeholder="Digite o nome desta pessoa se conhecer" name="perg2">  </textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" class="label_list_item">
                                        <div style="width: 150px">
                                            De que forma voc� acredita que pode colaborar com os pacientes e fam�lias atendidas pela ong viver?
                                        </div>
                                    </td>
                                    <td>
                                        <textarea class="input_textarea it_large" placeholder="Digite como pretende colaborar" name="perg3">  </textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" class="label_list_item">
                                        <div style="width: 150px">
                                            J� teve experi�ncia com pacientes oncol�gicos, especificamente crian�as?
                                        </div>
                                        
                                    </td>
                                    <td>
                                        <textarea class="input_textarea it_large" placeholder="Digite as experi�ncias que voc� j� teve como volunt�rio" name="perg4">  </textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" class="label_list_item">
                                        <div style="width: 150px">
                                            Assinale as aptid�es que julga dominar e ter interesse em desempenhar durante trabalho volunt�rio. Utilize os seguintes par�metros para avaliar seu conhecimento:
                                        </div>
                                    </td>
                                    <td class="label_list_item">
                                        <input type="checkbox" name="adm" value="Administrativo"> Administrativo<br>
                                        <input type="checkbox" name="art" value="Artesanato"> Artesanato<br>
                                        <input type="checkbox" name="bar" value="Bazar"> Bazar<br>
                                        <input type="checkbox" name="cap" value="Capta��o de doa��o"> Capta��o de doa��o<br>
                                        <input type="checkbox" name="eve" value="Eventos"> Eventos<br>
                                        <input type="checkbox" name="lim" value="Limpeza"> Limpeza<br>
                                        <input type="checkbox" name="mon" value="Montagem de cesta b�sica"> Montagem de cesta b�sica<br>
                                        <input type="checkbox" name="rece" value="Recep��o"> Recep��o<br>
                                        <input type="checkbox" name="recr" value="Recrea��o"> Recrea��o<br>
                                        <input type="checkbox" name="recrH" value="Recrea��o no hospital"> Recrea��o no hospital<br>
                                        <input type="checkbox" name="out" value="Outro" onchange="setOutraFuncao(this.checked)"> Outro
                                    </td>
                                </tr>
                                <tr style="display:none" id="outro_funcao">
                                    <td align="right" class="label_list_item">Outro: </td>
                                    <td><input type="text" class="input_text it_large" placeholder="Digite qual outra fun��o" name="outroNome" ></td>
                                </tr>
                                <tr>
                                    <td align="right" class="label_list_item">
                                        <div style="width: 150px">
                                            Possui disponibilidade para atividades externas:
                                        </div>
                                    </td>
                                    <td class="label_list_item">
                                        <input type="radio" name="disponibilidade_atividades_externas" value="sim"> Sim<br>
                                        <input type="radio" name="disponibilidade_atividades_externas" value="nao"> N�o<br>
                                        <input type="radio" name="disponibilidade_atividades_externas" value="somente_fins_de_semana"> Somente finais de semana<br>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" class="label_list_item">
                                        <div style="width: 150px">
                                            Disponibilidade de hor�rios:
                                        </div>
                                    </td>
                                    <td class="label_list_item">
                                        <table border="0">
                                            <tr>
                                                <td><b>Seg</b></td>
                                                <td><b>Ter</b></td>
                                                <td><b>Qua</b></td>
                                                <td><b>Qui</b></td>
                                                <td><b>Sex</b></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td align="center"><input type="checkbox" name="manSeg" value="Segunda de manh�"></td>
                                                <td align="center"><input type="checkbox" name="manTer" value="Ter�a de manh�"></td>
                                                <td align="center"><input type="checkbox" name="manQua" value="Quarta de manh�"></td>
                                                <td align="center"><input type="checkbox" name="manQui" value="Quinta de manh�"></td>
                                                <td align="center"><input type="checkbox" name="manSex" value="Sexta de manh�"></td>
                                                <td><i>Manh� - 08h �s 12h</i></td>
                                            </tr>
                                            <tr>
                                                <td align="center"><input type="checkbox" name="tarSeg" value="Segunda a tarde"></td>
                                                <td align="center"><input type="checkbox" name="tarTer" value="Ter�a a tarde"></td>
                                                <td align="center"><input type="checkbox" name="tarQua" value="Quarta a tarde"></td>
                                                <td align="center"><input type="checkbox" name="tarQui" value="Quinta a tarde"></td>
                                                <td align="center"><input type="checkbox" name="tarSex" value="Sexta a tarde"></td>
                                                <td><i>Tarde - 13h �s 17h</i></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" class="label_list_item">Observa��es: </td>
                                    <td>
                                        <textarea class="input_textarea it_large" placeholder="Digite uma observa��o" name="obs_sobre_hor"></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" class="label_list_item">Setor: </td>
                                    <td><input type="text" class="input_text it_large" placeholder="Digite o nome do setor" name="setor"></td>
                                </tr>
                                <tr>
                                    <td align="right" class="label_list_item">Dias: </td>
                                    <td>
                                        <table border="0" class="label_list_item">
                                            <tr>
                                                <td><b>Seg</b></td>
                                                <td><b>Ter</b></td>
                                                <td><b>Qua</b></td>
                                                <td><b>Qui</b></td>
                                                <td><b>Sex</b></td>
                                            </tr>
                                            <tr>
                                                <td align="center"><input type="checkbox" name="seg" value="Segunda"></td>
                                                <td align="center"><input type="checkbox" name="ter" value="Ter�a"></td>
                                                <td align="center"><input type="checkbox" name="qua" value="Quarta"></td>
                                                <td align="center"><input type="checkbox" name="qui" value="Quinta"></td>
                                                <td align="center"><input type="checkbox" name="sex" value="Sexta"></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" class="label_list_item">Hor�rio: </td>
                                    <td>
                                        <select name="horas" class="input_select" style="display:inline">
                                        <option value="null">Horas</option>
                                        <c:forEach begin="0" end="23" var="i">
                                            <option value="<c:out value="${i}"/>"><c:out value="${i}"/></option>
                                        </c:forEach>
                                    </select>
                                    <select name="minutos" class="input_select"style="display:inline">
                                        <option value="null">Minutos</option>
                                        <c:forEach begin="0" end="59" var="i">
                                            <option value="<c:out value="${i}"/>"><c:out value="${i}"/></option>
                                        </c:forEach>
                                    </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" class="label_list_item">Data in�cio: </td>
                                    <td>
                                        <input type="text" class="input_text it_date_mini" maxlength="2" placeholder="Dia" name="diaDia">
                                        <input type="text" class="input_text it_date_mini" maxlength="2" placeholder="M�s" name="diaMes">
                                        <input type="text" class="input_text it_date_large" maxlength="4" placeholder="Ano" name="diaAno">
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td align="right">
                                        <br><br>
                                        <div class="label_list_item" style="width: 326px;" align="left">
                                            <div style="text-indent: 2em;">
                                                Declaro estar ciente que o trabalho a ser desenvolvido junto a Organiza��o Viver - Voluntariado de Apoio �s Crian�as e Adolescentes portadores de c�ncer - ser� regido de acordo com a lei n� 9608, de 18/02/1998, que disciplina o servi�o volunt�rio, n�o gerando v�nculo empregat�cio nem funcional, ou quaisquer obriga��es trabalhistas, previdenci�rias e afins.<br>
                                            </div>
                                            <div style="text-indent: 2em;">
                                                Lei n� 9608 - LEI DO SERVI�O VOLUNT�RIO<br>
                                            </div>
                                            <div style="text-indent: 2em;">
                                                Art. 1� Considera-se servi�o volunt�rio, para fins desta Lei, a atividade n�o remunerada, prestada por pessoa f�sica a entidade p�blica de qualquer natureza, ou a institui��o privada de fins n�o lucrativos, que tenha objetivos c�vicos, culturais, educacionais, cient�ficos, recreativos ou de assist�ncia social, inclusive mutualidade.<br>
                                            </div>
                                            <div style="text-indent: 2em;">
                                                Par�grafo �nico: O servi�o n�o gera v�nculo empregat�cio, nem de obriga��o de natureza trabalhista, previdenci�rias e afins.<br>
                                            </div>
                                            <div style="text-indent: 2em;">
                                                Declaro, ainda, que aceito atual como volunt�rio nos termos do presente documento, assumindo o compromisso de cumprir o dia e hor�rio por mim estabelecido.<br>
                                            </div> 
                                            <div style="text-indent: 2em;">
                                                Declaro tamb�m que estou ciente de que tenho conhecimento de que assumo que a Ong Viver n�o possui nenhuma responsabilidade sobre fotos de pacientes tiradas e/ou postada em minhas redes sociais, bem como n�o assumo nenhum car�ter de responsabilidade diante transporte de paciente em meu veiculo particular, sendo estar atividades de minha inteira responsabilidade.
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" class="label_list_item"></td>
                                    <td class="label_list_item">
                                        <br><br>
                                        <input type="checkbox" name="aceitacaoDosTermos" value="true"> Declaro ter lido o texto acima<br>
                                    </td>
                                </tr>

                            </tbody>

                            <tr>
                                <td></td>
                                <td align="left">
                                    <input type="submit" value="Salvar" class="input_button" style="width:326px; height:40px">
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </form>





        <br><br>


        <%@include file="../../../includes/bottom_bar.jsp"%>
    </body>
</html>
