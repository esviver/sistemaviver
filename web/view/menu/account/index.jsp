<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Ong Viver - Meu Cadastro</title>
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/main.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/top_bar.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/bottom_bar.css">
<script src="${pageContext.servletContext.contextPath}/assets/javascript/main.js"></script>
</head>
<body onLoad="introLogin()">
<%@include file="../../includes/top_bar.jsp"%>

<br>

<div align="center">
    <div class="navigator">
    	<a href="${pageContext.servletContext.contextPath}/view/menu/" class="navigator_link">Menu</a>
        >
        <a href="${pageContext.servletContext.contextPath}/view/menu/login" class="navigator_link">Meu Cadastro</a>
    </div>
</div>

<br>

<form action="${pageContext.servletContext.contextPath}/view/menu/login" method="POST">
<div align="center">
    <div class="menu_back menu_back_loading" id="menu_back">
        <div class="menu_login">
            <table border="0">
            	<tr>
                    <td align="center" class="label_login_title" style="padding-bottom:20px">
                    	Entrar no cadastro
                    </td>
                </tr>
                <tr>
                    <td align="center" class="label_login">Login</td>
                </tr>
                <tr>
                    <td align="center">
                        <input type="text" placeholder="Digite seu login" class="input_text" name="login" style="width:200px">
                    </td>
                </tr>
                <tr>
                    <td align="center" class="label_login">Senha</td>
                </tr>
                <tr>
                    <td align="center">
                        <input type="password" placeholder="Digite sua senha" name="senha" class="input_text" style="width:200px">
                    </td>
                </tr>
                <tr>
                    <td align="center" style="padding-top:10px;">
                        <input type="submit" value="Entrar" class="input_button"  style="width:224px; height:40px">
                    </td>
                </tr>
            </table>
        </div>
        <br>
        <a href="${pageContext.servletContext.contextPath}/view/menu/account/create" class="label_login_create">Criar minha conta</a>
        <c:if test="${isAdm}">
            <br><a href="${pageContext.servletContext.contextPath}/view/menu/account/read/index.jsp" class="label_login_create">Ver contas</a>
        </c:if> 
        
        
    </div>
</div>
</form>



<br><br>


<%@include file="../../includes/bottom_bar.jsp"%>
</body>
</html>