<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ong Viver - Novo atendimento anamnese</title>
        <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/main.css">
        <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/top_bar.css">
        <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/bottom_bar.css">
        <script src="${pageContext.servletContext.contextPath}/assets/javascript/main.js"></script>
    </head>
    <body onLoad="introCard()">
        <%@include file="../../../../includes/top_bar.jsp"%>

        <br>

        <div align="center">
            <div class="navigator">
                <a href="${pageContext.servletContext.contextPath}/view/menu/index.jsp" class="navigator_link">Menu</a>
                >
                <a href="${pageContext.servletContext.contextPath}/view/menu/agendamentos/index.jsp" class="navigator_link">Atendimentos</a>
                >
                <a href="${pageContext.servletContext.contextPath}/view/menu/agendamentos/psychology/index.jsp" class="navigator_link">Atendimento psicológico</a>
                >
                <a href="${pageContext.servletContext.contextPath}/view/menu/agendamentos/psychology/read/index.jsp" class="navigator_link">Pesquisar atendimento psicológico</a>
            </div>
        </div>

        <br>


        <div align="center">
            <div align="right" class="card_support_loading" id="card_support">
                <div class="card" style="width:620px;" align="center">
                    <table>
                        <tr>
                            <td align="center" colspan="2" class="label_title_card">Atendimento psicológico</td>
                        </tr>
                        <tr>
                            <td align="right" class="label_list_item">Paciente: </td>
                             <td><span id="dn" class="label_list_item label_bold">${atendimento.paciente.pnome} ${atendimento.paciente.snome}</span></td>
                        </tr>
                        <tr>
                            <td align="right" class="label_list_item">Data Atendimento: </td>
                            <td><span id="dn" class="label_list_item label_bold">${atendimento.atendPsico.dataAtendimento}</span></td>
                        </tr>
                        <tr>
                            <td align="right" class="label_list_item">Hora Atendimento: </td>
                            <td><span id="responsavel" class="label_list_item label_bold">${atendimento.atendPsico.hora}</span></td>
                        </tr>
                        <tr>
                            <td align="right" class="label_list_item">Patologia: </td>
                            <td><span id="responsavel" class="label_list_item label_bold">${atendimento.atendPsico.patologia}</span></td>
                        </tr>
                        <tr>
                            <td align="right" class="label_list_item">Queixa: </td>
                            <td><span id="responsavel" class="label_list_item label_bold">${atendimento.atendPsico.queixa}</span></td>

                        </tr>
                        <tr>
                            <td align="right" class="label_list_item">Atendimento: </td>
                            <td><span id="responsavel" class="label_list_item label_bold">${atendimento.atendPsico.atendimento}</span></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>






        <br><br>


        <%@include file="../../../../includes/bottom_bar.jsp"%>
    </body>
</html>