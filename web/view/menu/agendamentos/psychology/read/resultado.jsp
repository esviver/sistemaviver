<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<table border="0" align="left" class="table_hover" cellpadding="5" cellspacing="0">
    <tr style="background-color:#EEE !important;">
    	<td style="min-width:100px;"></td>
    	<td class="list_view_title" align="left">Paciente</td>
        <td class="list_view_title" align="left">Data</td>
        
    </tr>
    <c:set var="i" value="0"/>
    <c:forEach items="${atendimentos}" var="resultado">
    
    <tr>
	<td>
            <a href="../read?idPaciente=${resultado.paciente.id}&idAtendimento=${resultado.atendPsico.id}" class="list_view_link">Visualizar</a>
        </td>
        <td class="label_list_item">
            <div style="width:100px" class="list_view_text">${resultado.paciente.pnome} ${resultado.paciente.snome}</div>
        </td>
    	<td class="label_list_item">
            <div style="width:100px" class="list_view_text">${resultado.atendPsico.dataAtendimento}</div>
        </td>
    </tr>
    <c:set var="i" value="${i+1}"/>
    </c:forEach>
</table>
<div style=" min-height:200px;">
</div>