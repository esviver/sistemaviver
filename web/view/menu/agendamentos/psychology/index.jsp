<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Ong Viver - Psicológico</title>
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/main.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/top_bar.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/bottom_bar.css">
<script src="${pageContext.servletContext.contextPath}/assets/javascript/main.js"></script>
</head>
<body onLoad="introMenu()">
<%@include file="../../../includes/top_bar.jsp"%>

<br>

<div align="center">
    <div class="navigator">
    	<a href="${pageContext.servletContext.contextPath}/view/menu/" class="navigator_link">Menu</a>
        >
        <a href="${pageContext.servletContext.contextPath}/view/menu/agendamentos/index.jsp" class="navigator_link">Atendimentos</a>
        >
        <a href="${pageContext.servletContext.contextPath}/view/menu/agendamentos/psychology" class="navigator_link">Psicológico</a>
    </div>
</div>

<br>


<table class="menu menu_loading" align="center" cellpadding="0" cellspacing="0" id="menu" border="0">
	<tr>
    	<td align="left" valign="top">
        	<a href="${pageContext.servletContext.contextPath}/view/menu/agendamentos/psychology/create" class="menu_box_link">
            	<div class="menu_box menu_date_psychology_add" align="center">Cadastrar atendimento psicológico</div>
            </a>
            <div class="menu_hole"></div>
        </td>
        <td align="right" valign="top">
        	<a href="${pageContext.servletContext.contextPath}/view/menu/agendamentos/psychology/read/index.jsp" class="menu_box_link">
            	<div class="menu_box menu_date_psychology_search" align="center">Pesquisar atendimento psicológico</div>
            </a>
			<div class="menu_hole"></div>
        </td>
    </tr>
    
</table>


<br><br>
<%@include file="../../../includes/bottom_bar.jsp"%>
</body>
</html>
