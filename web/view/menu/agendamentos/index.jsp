<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Ong Viver - Agendamentos</title>
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/main.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/top_bar.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/bottom_bar.css">
<script src="${pageContext.servletContext.contextPath}/assets/javascript/main.js"></script>
</head>
<body onLoad="introMenu()">
<%@include file="../../includes/top_bar.jsp"%>

<br>

<div align="center">
    <div class="navigator">
    	<a href="${pageContext.servletContext.contextPath}/view/menu/" class="navigator_link">Menu</a>
        >
        <a href="${pageContext.servletContext.contextPath}/view/menu/agendamentos/index.jsp" class="navigator_link">Atendimentos</a>
    </div>
</div>

<br>


<table class="menu menu_loading" align="center" cellpadding="0" cellspacing="0" id="menu" border="0">
	<tr>
    	<td align="left" valign="top">
        	<a href="${pageContext.servletContext.contextPath}/view/menu/agendamentos/psychology" class="menu_box_link">
            	<div class="menu_box menu_date_psychology" align="center">Atendimento psicológico</div>
            </a>
            <div class="menu_hole"></div>
        </td>
        <td align="right" valign="top">
        	<a href="${pageContext.servletContext.contextPath}/view/menu/agendamentos/anamnese" class="menu_box_link">
            	<div class="menu_box menu_date_anamnesis" align="center">Atendimento anamnese</div>
            </a>
			<div class="menu_hole"></div>
        </td>
    </tr>
    <tr>
    	<td align="left" valign="top">
        	<a href="${pageContext.servletContext.contextPath}/view/menu/agendamentos/social_care" class="menu_box_link">
            	<div class="menu_box menu_date_social_care" align="center">Atendimento social</div>
            </a>
            <div class="menu_hole"></div>
        </td>
        <td align="right" valign="top">
        	
        </td>
    </tr>
</table>

                

<br><br>

<%@include file="../../includes/bottom_bar.jsp"%>
</body>
</html>