<?php $local_path = "http://localhost/ong/";?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Ong Viver - Editar agendamento anamnese</title>
<link rel="stylesheet" type="text/css" href="<?php echo $local_path;?>includes/assets/css/main.css">
<link rel="stylesheet" type="text/css" href="<?php echo $local_path;?>includes/assets/css/top_bar.css">
<link rel="stylesheet" type="text/css" href="<?php echo $local_path;?>includes/assets/css/bottom_bar.css">
<script src="<?php echo $local_path;?>includes/assets/javascript/main.js"></script>
</head>
<body onLoad="introCard()">
<?php include"../../../../../view/top_bar.php";?>

<br>

<div align="center">
    <div class="navigator">
    	<a href="<?php echo $local_path;?>includes/view/menu/index.php" class="navigator_link">Menu</a>
        >
        <a href="<?php echo $local_path;?>includes/view/menu/agendamentos/index.php" class="navigator_link">Agendamentos</a>
        >
        <a href="<?php echo $local_path;?>includes/view/menu/agendamentos/anamnese/index.php" class="navigator_link">Anamnese</a>
        >
        <a href="#" class="navigator_link">Editar agendamento</a>
    </div>
</div>

<br>


<div align="center">
	<div align="right" class="card_support_loading" id="card_support">
        <div class="card" style="width:620px;" align="center">
            <table>
                <tr>
                    <td align="center" colspan="2" class="label_title_card">Editar agendamento anamnese</td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Nome: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite o nome" name="??????" value="BANCO_DE_DADOS"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Responsável: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite o responsável" name="??????" value="BANCO_DE_DADOS"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Encaminhamento: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite o encaminhamento" name="??????" value="BANCO_DE_DADOS"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Diagnóstico: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite o diagnóstico" name="??????" value="BANCO_DE_DADOS"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Médico: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite o médico" name="??????" value="BANCO_DE_DADOS"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Data nascimento: </td>
                    <td>
                        <input type="text" class="input_text it_date_mini" maxlength="2" placeholder="Dia" name="??????" value="BANCO_DE_DADOS">
                        <input type="text" class="input_text it_date_mini" maxlength="2" placeholder="Mês" name="??????" value="BANCO_DE_DADOS">
                        <input type="text" class="input_text it_date_large" maxlength="4" placeholder="Ano" name="??????" value="BANCO_DE_DADOS">
                    </td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Sexo: </td>
                    <td align="left">
                        <input type="radio" name="sexo" value="masculino" checked="checked"> Masculino
                        <input type="radio" name="sexo" value="feminino"> Feminino
                    </td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Religião: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite a religião" name="??????" value="BANCO_DE_DADOS"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Cidade: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite a cidade" name="??????" value="BANCO_DE_DADOS"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">História: </td>
                    <td>
                        <textarea class="input_textarea it_large" placeholder="Digite a história" name="??????">BANCO_DE_DADOS</textarea>
                    </td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Histórico clínico: </td>
                    <td>
                        <textarea class="input_textarea it_large" placeholder="Digite o clínico" name="??????">BANCO_DE_DADOS</textarea>
                    </td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Queixa: </td>
                    <td>
                        <textarea class="input_textarea it_large" placeholder="Digite a queixa" name="??????">BANCO_DE_DADOS</textarea>
                    </td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Motivos<br>Explícitos<br>Implícitos: </td>
                    <td>
                        <textarea class="input_textarea it_large" placeholder="Digite os motivos explícitos e implícitos" name="??????">BANCO_DE_DADOS</textarea>
                    </td>
                </tr>
                
                <tr>
                    <td align="right" class="label_list_item">Formulação de hipótese: </td>
                    <td>
                        <textarea class="input_textarea it_large" placeholder="Digite a formulação de hipótese" name="??????">BANCO_DE_DADOS</textarea>
                    </td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Plano terapêutico: </td>
                    <td>
                        <textarea class="input_textarea it_large" placeholder="Digite o plano terapêutico" name="??????">BANCO_DE_DADOS</textarea>
                    </td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Dados psicosociais: </td>
                    <td>
                        <textarea class="input_textarea it_large" placeholder="Digite os dados psicosociais" name="??????">BANCO_DE_DADOS</textarea>
                    </td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Estrutura familiar<br>Rede de apoio: </td>
                    <td>
                        <textarea class="input_textarea it_large" placeholder="Digite a estrutura familiar ou rede de apoio" name="??????">BANCO_DE_DADOS</textarea>
                    </td>
                </tr>
                
                <tr>
                    <td align="right" class="label_list_item">Infância e adolescência: </td>
                    <td>
                        <textarea class="input_textarea it_large" placeholder="Digite a infância e adolescência" name="??????">BANCO_DE_DADOS</textarea>
                    </td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Aspectos psicológicos<br>Enfrentamento da doença: </td>
                    <td>
                        <textarea class="input_textarea it_large" placeholder="Digite os aspectos psicológicos e o enfrentamento da doença" name="??????">BANCO_DE_DADOS</textarea>
                    </td>
                </tr>
                
                
                <tr>
                    <td></td>
                    <td align="center"><input type="submit" value="Salvar" class="input_button" style="width:326px; height:40px"></td>
                </tr>
                
            </table>
        </div>
    </div>
</div>






<br><br>

<?php include"../../../../../view/bottom_bar.php";?>

</body>
</html>