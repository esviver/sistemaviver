<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ong Viver - Novo atendimento anamnese</title>
        <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/main.css">
        <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/top_bar.css">
        <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/bottom_bar.css">
        <script src="${pageContext.servletContext.contextPath}/assets/javascript/main.js"></script>
    </head>
    <body onLoad="introCard()">
        <%@include file="../../../../includes/top_bar.jsp"%>

        <br>

        <div align="center">
            <div class="navigator">
                <a href="${pageContext.servletContext.contextPath}/view/menu/index.jsp" class="navigator_link">Menu</a>
                >
                <a href="${pageContext.servletContext.contextPath}/view/menu/agendamentos/index.jsp" class="navigator_link">Atendimentos</a>
                >
                <a href="${pageContext.servletContext.contextPath}/view/menu/agendamentos/anamnese/index.jsp" class="navigator_link">Anamnese</a>
                >
                <a href="${pageContext.servletContext.contextPath}/view/menu/agendamentos/anamnese/read/index.jsp" class="navigator_link">Pesquisar atendimento anamnese</a>
            </div>
        </div>

        <br>


        <div align="center">
            <div align="right" class="card_support_loading" id="card_support">
                <div class="card" style="width:620px;" align="center">
                    <table>
                        <tr>
                            <td align="center" colspan="2" class="label_title_card">Atendimento anamnese</td>
                        </tr>
                        <tr>
                            <td align="right" class="label_list_item">Paciente: </td>
                             <td><span id="dn" class="label_list_item label_bold">${atendimento.paciente.pnome} ${atendimento.paciente.snome}</span></td>
                        </tr>
                        <tr>
                            <td align="right" class="label_list_item">Data Atendimento: </td>
                            <td><span id="dn" class="label_list_item label_bold">${atendimento.anam.data}</span></td>
                        </tr>
                        <tr>
                            <td align="right" class="label_list_item">Patologia: </td>
                            <td><span id="responsavel" class="label_list_item label_bold">${atendimento.paciente.patologia}</span></td>
                        </tr>
                        <tr>
                            <td align="right" class="label_list_item">Encaminhamento: </td>
                            <td><span id="responsavel" class="label_list_item label_bold">${atendimento.anam.encaminhamento}</span></td>

                        </tr>
                        <tr>
                            <td align="right" class="label_list_item">Diagnóstico: </td>
                            <td><span id="responsavel" class="label_list_item label_bold">${atendimento.anam.diagnostico}</span></td>
                        </tr>
                        <tr>
                            <td align="right" class="label_list_item">Médico: </td>
                            <td><span id="responsavel" class="label_list_item label_bold">${atendimento.anam.medico}</span></td>
                        </tr>
                       
                        <tr>
                            <td align="right" class="label_list_item">Religião </td>
                            <td><span id="responsavel" class="label_list_item label_bold">${atendimento.paciente.religiao}</span></td>
                        </tr>
                        <tr>
                            <td align="right" class="label_list_item">História: </td>
                            <td><span id="responsavel" class="label_list_item label_bold">${atendimento.anam.historia}</span></td>
                        </tr>
                        
                        <tr>
                            <td align="right" class="label_list_item">Histórico clínico: </td>
                            <td><span id="responsavel" class="label_list_item label_bold">${atendimento.anam.historicoClin}</span></td>
                        </tr>
                        <tr>
                            <td align="right" class="label_list_item">Queixa: </td>
                           <td><span id="responsavel" class="label_list_item label_bold">${atendimento.anam.queixa}</span></td>
                        </tr>
                        <tr>
                            <td align="right" class="label_list_item">Motivos<br>Explícitos<br>Implícitos: </td>
                            <td><span id="responsavel" class="label_list_item label_bold">${atendimento.anam.motivosImpExp}</span></td>
                        </tr>

                        <tr>
                            <td align="right" class="label_list_item">Formulação de hipótese: </td>
                            <td><span id="responsavel" class="label_list_item label_bold">${atendimento.anam.formulacaoHipotese}</span></td>
                        </tr>
                        <tr>
                            <td align="right" class="label_list_item">Plano terapêutico: </td>
                            <td><span id="responsavel" class="label_list_item label_bold">${atendimento.anam.planoTerap}</span></td>
                        </tr>
                        <tr>
                            <td align="right" class="label_list_item">Dados psicosociais: </td>
                            <td><span id="responsavel" class="label_list_item label_bold">${atendimento.anam.dadosPsicoSociais}</span></td>
                        </tr>
                        <tr>
                            <td align="right" class="label_list_item">Estrutura familiar<br>Rede de apoio: </td>
                            <td><span id="responsavel" class="label_list_item label_bold">${atendimento.anam.estrutFamiliar}</span></td>
                        </tr>

                        <tr>
                            <td align="right" class="label_list_item">Infância e adolescência: </td>
                            <td><span id="responsavel" class="label_list_item label_bold">${atendimento.anam.infEAdos}</span></td>
                        </tr>
                        <tr>
                            <td align="right" class="label_list_item">Aspectos psicológicos<br>Enfrentamento da doença: </td>
                            <td><span id="responsavel" class="label_list_item label_bold">${atendimento.anam.aspecPsico}</span></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>






        <br><br>


        <%@include file="../../../../includes/bottom_bar.jsp"%>
    </body>
</html>