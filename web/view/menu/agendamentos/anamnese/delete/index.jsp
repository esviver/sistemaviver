<?php $local_path = "http://localhost/ong/";?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Ong Viver - Deletar agendamento anamnese</title>
<link rel="stylesheet" type="text/css" href="<?php echo $local_path;?>includes/assets/css/main.css">
<link rel="stylesheet" type="text/css" href="<?php echo $local_path;?>includes/assets/css/top_bar.css">
<link rel="stylesheet" type="text/css" href="<?php echo $local_path;?>includes/assets/css/bottom_bar.css">
<script src="<?php echo $local_path;?>includes/assets/javascript/main.js"></script>
</head>
<body onLoad="introCard()">
<?php include"../../../../../view/top_bar.php";?>

<br>

<div align="center">
    <div class="navigator">
    	<a href="<?php echo $local_path;?>includes/view/menu/index.php" class="navigator_link">Menu</a>
        >
        <a href="<?php echo $local_path;?>includes/view/menu/agendamentos/index.php" class="navigator_link">Agendamentos</a>
        >
        <a href="<?php echo $local_path;?>includes/view/menu/agendamentos/anamnese/index.php" class="navigator_link">Anamnese</a>
        >
        <a href="#" class="navigator_link">Deletar agendamento anamnese</a>
    </div>
</div>

<br>


<div align="center">
	<div align="right" class="card_support_loading" id="card_support">
        <div class="card" style="width:620px;" align="center">
            <table>
                <tr>
                    <td align="center" class="label_title_card">Tem certeza que deseja deletar?</td>
                </tr>
                
                
                <tr>
                    <td align="center"><input type="submit" value="Sim" class="input_button" style="width:326px; height:40px"></td>
                </tr>
                
            </table>
        </div>
    </div>
</div>






<br><br>

<?php include"../../../../../view/bottom_bar.php";?>

</body>
</html>