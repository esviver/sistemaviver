<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ong Viver - Novo atendimento anamnese</title>
        <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/main.css">
        <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/top_bar.css">
        <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/bottom_bar.css">
        <script src="${pageContext.servletContext.contextPath}/assets/javascript/main.js"></script>
    </head>
    <body onLoad="introCard()">
        <%@include file="../../../../includes/top_bar.jsp"%>

        <br>

        <div align="center">
            <div class="navigator">
                <a href="${pageContext.servletContext.contextPath}/view/menu/index.jsp" class="navigator_link">Menu</a>
                >
                <a href="${pageContext.servletContext.contextPath}/view/menu/agendamentos/index.jsp" class="navigator_link">Atendimentos</a>
                >
                <a href="${pageContext.servletContext.contextPath}/view/menu/agendamentos/anamnese/index.jsp" class="navigator_link">Anamnese</a>
                >
                <a href="${pageContext.servletContext.contextPath}/view/menu/agendamentos/anamnese/create/index.jsp" class="navigator_link">Novo atendimento</a>
            </div>
        </div>

        <br>

        <form action="${pageContext.servletContext.contextPath}/view/menu/agendamentos/anamnese/create" method="POST">
            <div align="center">
                <div align="right" class="card_support_loading" id="card_support">
                    <div class="card" style="width:620px;" align="center">
                        <table>
                            <tr>
                                <td align="center" colspan="2" class="label_title_card">Cadastrar atendimento anamnese</td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">Paciente: </td>
                                <td>                        
                                    <select id="selectPaciente" class="input_select is_large" name="????????" id="paciente" onchange="setPacienteSelectMenuPsicologo(this.id)">
                                        <option value="null">Selecione um paciente...</option>
                                        <c:forEach items="${pacientes}" var="p">
                                            <option value="${p.id}" dn="${p.dataNasc}" idPaciente="${p.id}" responsavel="responsavel do paciente ${p.id}" patologia="${p.patologia}">${p.pnome} ${p.snome}</option>
                                        </c:forEach>
                                    </select>
                                </td>
                            </tr>
                            <tbody id="pacienteInfos" style="display:none">
                                <tr>
                                    <td align="right" class="label_list_item">Data nascimento: </td>
                                    <td><span id="dn" class="label_list_item label_bold">okok</span></td>
                                </tr>
                                <tr>
                                    <td align="right" class="label_list_item">Responsável: </td>
                                    <td><span id="responsavel" class="label_list_item label_bold">okokok</span></td>
                                </tr>
                                
                                
                            </tbody>
                            <tr>
                                <td align="right" class="label_list_item">Patologia: </td>
                                <td><input type="text" class="input_text it_large" placeholder="Digite a patologia" id="patologia"></td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">Encaminhamento: </td>
                                <td><input type="text" class="input_text it_large" placeholder="Digite o encaminhamento" name="encaminhamento"></td>
                               
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">Diagnóstico: </td>
                                <td><input type="text" class="input_text it_large" placeholder="Digite o diagnóstico" name="diagnostico"></td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">Médico: </td>
                                <td><input type="text" class="input_text it_large" placeholder="Digite o médico" name="medico"></td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">Sexo: </td>
                                <td align="left">
                                    <input type="radio" name="sexo" value="masculino" checked="checked"> <span class="label_list_item">Masculino</span>
                                    <input type="radio" name="sexo" value="feminino"> <span class="label_list_item">Feminino</span>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">Religião </td>
                                <td><input type="text" class="input_text it_large" placeholder="Digite a religião" name="religiao"></td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">Cidade: </td>
                                <td><input type="text" class="input_text it_large" placeholder="Digite a cidade" name="??????"></td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">História: </td>
                                <td>
                                    <textarea class="input_textarea it_large" placeholder="Digite a história" name="historia"></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">Histórico clínico: </td>
                                <td>
                                    <textarea class="input_textarea it_large" placeholder="Digite o histórico clínico" name="historico"></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">Queixa: </td>
                                <td>
                                    <textarea class="input_textarea it_large" placeholder="Digite a queixa" name="queixa"></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">Motivos<br>Explícitos<br>Implícitos: </td>
                                <td>
                                    <textarea class="input_textarea it_large" placeholder="Digite os motivos explícitos e implícitos:" name="motivos"></textarea>
                                </td>
                            </tr>

                            <tr>
                                <td align="right" class="label_list_item">Formulação de hipótese: </td>
                                <td>
                                    <textarea class="input_textarea it_large" placeholder="Digite a formulação de hipótese:" name="hipoteses"></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">Plano terapêutico: </td>
                                <td>
                                    <textarea class="input_textarea it_large" placeholder="Digite o plano terapêutico" name="planoterap"></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">Dados psicosociais: </td>
                                <td>
                                    <textarea class="input_textarea it_large" placeholder="Digite os dados psicosociais" name="dadosPsicoSociais"></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">Estrutura familiar<br>Rede de apoio: </td>
                                <td>
                                    <textarea class="input_textarea it_large" placeholder="Digite a estrutura familiar ou rede de apoio" name="estrutFamiliar"></textarea>
                                </td>
                            </tr>

                            <tr>
                                <td align="right" class="label_list_item">Infância e adolescência: </td>
                                <td>
                                    <textarea class="input_textarea it_large" placeholder="Digite a infância e adolescência:" name="infaciaadolescencia"></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">Aspectos psicológicos<br>Enfrentamento da doença: </td>
                                <td>
                                    <textarea class="input_textarea it_large" placeholder="Digite os aspectos psicológicos e o enfrentamento da doença" name="aspectospsicologicos"></textarea>
                                </td>
                            </tr>


                            <tr>
                                <td></td>
                                <input type="hidden" id="idPaciente" name="idPaciente" value="">
                                <td align="center"><input type="submit" value="Salvar" class="input_button" style="width:326px; height:40px"></td>
                            </tr>

                        </table>
                    </div>
                </div>
            </div>
        </form>





        <br><br>


        <%@include file="../../../../includes/bottom_bar.jsp"%>
    </body>
</html>