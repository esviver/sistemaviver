<?php $local_path = "http://localhost/ong/";?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Ong Viver - Novo agendamento psicólogo</title>
<link rel="stylesheet" type="text/css" href="<?php echo $local_path;?>includes/assets/css/main.css">
<link rel="stylesheet" type="text/css" href="<?php echo $local_path;?>includes/assets/css/top_bar.css">
<link rel="stylesheet" type="text/css" href="<?php echo $local_path;?>includes/assets/css/bottom_bar.css">
<script src="<?php echo $local_path;?>includes/assets/javascript/main.js"></script>
</head>
<body onLoad="introCard()">
<?php include"../../../../../view/top_bar.php";?>

<br>

<div align="center">
    <div class="navigator">
    	<a href="<?php echo $local_path;?>includes/view/menu/index.php" class="navigator_link">Menu</a>
        >
        <a href="<?php echo $local_path;?>includes/view/menu/agendamentos/index.php" class="navigator_link">Agendamentos</a>
        >
        <a href="<?php echo $local_path;?>includes/view/menu/agendamentos/psychology/index.php" class="navigator_link">Psicológico</a>
        >
        <a href="#" class="navigator_link">Editar agendamento</a>
    </div>
</div>

<br>


<div align="center">
	<div align="right" class="card_support_loading" id="card_support">
        <div class="card" style="width:620px;" align="center">
            <table>
                <tr>
                    <td align="center" colspan="2" class="label_title_card">Editar Agendamento atendimento psicológico</td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Nome: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite o nome" name="??????" value="BANCO_DE_DADOS"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Data nascimento: </td>
                    <td>
                        <input type="text" class="input_text it_date_mini" maxlength="2" placeholder="Dia" name="??????" value="BANCO_DE_DADOS">
                        <input type="text" class="input_text it_date_mini" maxlength="2" placeholder="Mês" name="??????" value="BANCO_DE_DADOS">
                        <input type="text" class="input_text it_date_large" maxlength="4" placeholder="Ano" name="??????" value="BANCO_DE_DADOS">
                    </td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Responsável: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite o responsável" name="??????" value="BANCO_DE_DADOS"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Data nascimento: </td>
                    <td>
                        <input type="text" class="input_text it_date_mini" maxlength="2" placeholder="Dia" name="??????" value="BANCO_DE_DADOS">
                        <input type="text" class="input_text it_date_mini" maxlength="2" placeholder="Mês" name="??????" value="BANCO_DE_DADOS">
                        <input type="text" class="input_text it_date_large" maxlength="4" placeholder="Ano" name="??????" value="BANCO_DE_DADOS">
                    </td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Patologia: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite a patologia" name="??????" value="BANCO_DE_DADOS"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Data início acompanhamento: </td>
                    <td>
                        <input type="text" class="input_text it_date_mini" maxlength="2" placeholder="Dia" name="??????" value="BANCO_DE_DADOS">
                        <input type="text" class="input_text it_date_mini" maxlength="2" placeholder="Mês" name="??????" value="BANCO_DE_DADOS">
                        <input type="text" class="input_text it_date_large" maxlength="4" placeholder="Ano" name="??????" value="BANCO_DE_DADOS">
                    </td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Queixa: </td>
                    <td>
                        <textarea class="input_textarea it_large" placeholder="Digite a queixa" name="??????">BANCO_DE_DADOS</textarea>
                    </td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Atendimento: </td>
                    <td>
                        <textarea class="input_textarea it_large" placeholder="Digite sobre o atendimento" name="??????">BANCO_DE_DADOS</textarea>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td align="center"><input type="submit" value="Salvar" class="input_button" style="width:326px; height:40px"></td>
                </tr>
                
            </table>
        </div>
    </div>
</div>






<br><br>

<?php include"../../../../../view/bottom_bar.php";?>

</body>
</html>