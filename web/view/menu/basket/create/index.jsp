<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ong Viver - Cadastrar nova doação</title>
        <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/main.css">
        <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/top_bar.css">
        <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/bottom_bar.css">
        <script src="${pageContext.servletContext.contextPath}/assets/javascript/main.js"></script>
    </head>
    <body onLoad="introCard()">
        <%@include file="../../../includes/top_bar.jsp"%>

        <br>

        <div align="center">
            <div class="navigator">
                <a href="${pageContext.servletContext.contextPath}/view/menu/index.jsp" class="navigator_link">Menu</a>
                >
                <a href="${pageContext.servletContext.contextPath}/view/menu/basket/index.jsp" class="navigator_link">Cesta básica</a>
                >
                <a href="${pageContext.servletContext.contextPath}/view/menu/basket/create/index.jsp" class="navigator_link">Registrar distribuição</a>
            </div>
        </div>

        <br>
        <form action="${pageContext.servletContext.contextPath}/view/menu/basket/create" method="post">
            <div align="center">
                <div align="right" class="card_support_loading" id="card_support">
                    <div class="card" style="width:620px;" align="center">
                        <table border="0">
                            <tr>
                                <td align="center" colspan="2" class="label_title_card">Registrar distribuição de cesta básica</td>
                            </tr>
                            <td align="right" class="label_list_item">Paciente: </td>
                            <td>                        
                                <select id="selectPaciente" class="input_select is_large" name="selecionaPac" id="paciente" onchange="setPacienteSelectMenuPsicologo(this.id)">
                                    <option value="null">Selecione um paciente...</option>
                                    <c:forEach items="${pacientes}" var="p">
                                        <option value="${p.id}" dn="${p.dataNasc}" idPaciente="${p.id}" responsavel="responsavel do paciente ${p.id}" patologia="${p.patologia}">${p.pnome} ${p.snome}</option>
                                    </c:forEach>
                                </select>
                            </td>
                            </tr>
                            <tbody id="pacienteInfos" style="display:none">
                                <tr>
                                    <td align="right" class="label_list_item">Data nascimento: </td>
                                    <td><span id="dn" class="label_list_item label_bold">okok</span></td>
                                </tr>
                                <tr>
                                    <td align="right" class="label_list_item">Responsável: </td>
                                    <td><span id="responsavel" class="label_list_item label_bold">okokok</span></td>
                                </tr>
                            </tbody>
                            <tr>
                                <td align="right" class="label_list_item">Retirada: </td>
                                <td align="left">
                                    <input type="checkbox" name="cesta" value="cesta"> <span class="label_list_item">Cesta</span>
                                    <input type="checkbox" name="leite" value="leite"> <span class="label_list_item">Leite</span>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">Local: </td>
                                <td align="left">
                                    <input type="radio" name="local" value="icl"> <span class="label_list_item" Checked>ICL</span>
                                    <otherwise>
                                    <input type="radio" name="local" value="hu"> <span class="label_list_item">HU</span>                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">Data: </td>
                                <td>
                                    <input type="text" class="input_text it_date_mini" maxlength="2" placeholder="Dia" name="dia"> 
                                    <input type="text" class="input_text it_date_mini" maxlength="2" placeholder="Mês" name="mes"> 
                                    <input type="text" class="input_text it_date_large" maxlength="4" placeholder="Ano" name="ano">
                                </td>
                            </tr>
                            <tr>
                                <td><input type="hidden" id="idPaciente" name="idPaciente" value=""></td>
                            
                            <td align="left"><input type="submit" value="Salvar" class="input_button" style="width:326px; height:40px"></td>
                            </tr>

                        </table>
                    </div>
                </div>
            </div>
        </form>





        <br><br>

        <%@include file="../../../includes/bottom_bar.jsp"%>
    </body>
</html>
