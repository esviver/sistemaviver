<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>
<table border="0" align="left" class="table_hover" cellpadding="5" cellspacing="0">
    <tr style="background-color:#EEE !important;">
        <td style="min-width:100px;"></td>
        <td class="list_view_title" align="left">Paciente</td>

        <td class="list_view_title" align="left">Data</td>

        <td class="list_view_title" align="left">Local</td>

        <td class="list_view_title" align="left">Cesta</td>

        <td class="list_view_title" align="left">Leite</td>
    </tr>
    <c:forEach items="${listaPResultados}" var="resultado">
        <tr>

            <td>
                <a href="../update?id=${resultado.id}" class="list_view_link">Editar</a>
                <c:if test="${resultado.status == false}">
                    <a href="../recover?id=${resultado.id}" class="list_view_link">Recuperar </a>
                </c:if>
                <c:if test="${resultado.status == true}">
                    <a href="../delete?id=${resultado.id}" class="list_view_link">Deletar</a>
                </c:if>
            </td>
            <td class="label_list_item">
                <div style="width:200px" class="list_view_text_trash">${resultado.nome}</div>
            </td>
            <td class="label_list_item">
                <div style="width:100px" class="list_view_text_trash">${resultado.data}</div>
            </td>
            <td class="label_list_item">
                <div style="width:100px" class="list_view_text_trash">${resultado.local}</div>
            </td>
            </td>
            <c:if test="${resultado.cesta==true}">
                <td class="label_list_item">
                    <div style="width:100px" class="list_view_text_trash">SIM</div>
                </td>
            </c:if>
            <c:if test="${resultado.cesta==false}">
                <td class="label_list_item">
                    <div style="width:100px" class="list_view_text_trash">NÃO</div>
                </td>
            </c:if>
            <c:if test="${resultado.leite==true}">
                  <td class="label_list_item">
                      <div style="width:100px" class="list_view_text_trash">SIM</div>
                  </td>
        </c:if>
        <c:if test="${resultado.leite==false}">
              <td class="label_list_item">
                  <div style="width:100px" class="list_view_text_trash">NÃO</div>
              </td>
    </c:if>
</tr>
</c:forEach>
</table>
<div style=" min-height:200px;">
</div>