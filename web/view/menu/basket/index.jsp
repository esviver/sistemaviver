<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Ong Viver - Cesta b�sica</title>
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/main.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/top_bar.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/bottom_bar.css">
<script src="${pageContext.servletContext.contextPath}/assets/javascript/main.js"></script>
</head>
<body onLoad="introMenu()">
<%@include file="../../includes/top_bar.jsp"%>

<br>

<div align="center">
    <div class="navigator">
    	<a href="${pageContext.servletContext.contextPath}/view/menu/" class="navigator_link">Menu</a>
        >
        <a href="${pageContext.servletContext.contextPath}/view/menu/basket/index.jsp" class="navigator_link">Cesta b�sica</a>
    </div>
</div>

<br>


<table class="menu menu_loading" align="center" cellpadding="0" cellspacing="0" id="menu" border="0">
	<tr>
    	<td align="left" valign="top">
        	<a href="${pageContext.servletContext.contextPath}/view/menu/basket/create" class="menu_box_link">
            	<div class="menu_box menu_basket_add" align="center">Cadastrar cesta b�sica</div>
            </a>
            <div class="menu_hole"></div>
        </td>
        <td align="right" valign="top">
            <a href="${pageContext.servletContext.contextPath}/view/menu/basket/read/index.jsp" class="menu_box_link">
            	<div class="menu_box menu_basket_search" align="center">Pesquisar cesta b�sica</div>
            </a>
            <div class="menu_hole"></div>
        </td>
    </tr>
    
</table>


<br><br>
<%@include file="../../includes/bottom_bar.jsp"%>
</body>
</html>