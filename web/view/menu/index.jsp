<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:if test="${isFuncionario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/view/menu/funcionarioMenu.jsp" ></c:redirect>
</c:if>
<c:if test="${isPsicologo}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/view/menu/psicologoAssistenteMenu.jsp" ></c:redirect>
</c:if>
<c:if test="${isAssistente}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/view/menu/psicologoAssistenteMenu.jsp" ></c:redirect>
</c:if>
<c:if test="${isVoluntario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/view/menu/voluntarioMenu.jsp" ></c:redirect>
</c:if>
<c:if test="${isAdm}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/view/menu/psicologoAssistenteMenu.jsp" ></c:redirect>
</c:if>   
 
    
<c:redirect context="${pageContext.servletContext.contextPath}" url="/view/menu/login" ></c:redirect>
    
  
  