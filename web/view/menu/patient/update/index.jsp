<!doctype html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
<meta charset="UTF-8">
<title>Ong Viver - Atualizar um paciente</title>
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/main.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/top_bar.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/bottom_bar.css">
<script src="${pageContext.servletContext.contextPath}/assets/javascript/main.js"></script>
</head>
<body onLoad="introCard()">
<%@include file="../../../includes/top_bar.jsp"%>

<br>

<div align="center">
    <div class="navigator">
    	<a href="${pageContext.servletContext.contextPath}/view/menu/index.jsp" class="navigator_link">Menu</a>
        >
        <a href="${pageContext.servletContext.contextPath}/view/menu/patient/index.jsp" class="navigator_link">Pacientes</a>
        >
        <a href="#" class="navigator_link">Atualizar um paciente</a>
    </div>
</div>

<br>

<form action="${pageContext.servletContext.contextPath}/view/menu/patient/update" method="post">
<div align="center">
	<div align="right" class="card_support_loading" id="card_support">
        <div class="card" style="width:620px;" align="center">
            <table>
                <tr>
                    <td align="center" colspan="2" class="label_title_card">Atualiza��o de paciente</td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item"></td>
                    <td class="label_list_item" style="color:#555;"><b>Paciente</b></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Nome: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite o nome" name="nomePaciente" value="${pacienteParaUpdate.pnome}"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Sobrenome: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite o sobrenome" name="sobrenomePaciente" value="${pacienteParaUpdate.snome}"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Data nascimento: </td>
                    <td>
                        <input type="text" class="input_text it_date_mini" maxlength="2" placeholder="Dia" name="dataNascPacDia" value="${pacDtNascDia}">
                        <input type="text" class="input_text it_date_mini" maxlength="2" placeholder="M�s" name="dataNascPacMes" value="${pacDtNascMes}">
                        <input type="text" class="input_text it_date_large" maxlength="4" placeholder="Ano" name="dataNascPacAno" value="${pacDtNascAno}">
                    </td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Patologia: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite a patologia" name="patologia" value="${pacienteParaUpdate.patologia}"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Cart�o SUS: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite o c�digo do cart�o SUS" name="nsus" value="${pacienteParaUpdate.nsus}"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Local nascimento: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite o local do nascimento" name="localNasc" value="${pacienteParaUpdate.localNasc}"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item"></td>
                    <td class="label_list_item" style="color:#555;"><b>Endereco</b></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Rua: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite a rua" name="logradouro" value="${pacEnderecoParaUpdate.logradouro}"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">N�mero: </td>
                    <td><input type="text" class="input_text it_mini" placeholder="N�mero" name="numero" value="${pacEnderecoParaUpdate.numero}"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Bairro: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite o nome do bairro" name="bairro" value="${pacEnderecoParaUpdate.bairro}"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Cidade: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite o nome da cidade" name="cidade" value="${pacEnderecoParaUpdate.cidade}"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Estado: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite o nome do estado" name="estado" value="${pacEnderecoParaUpdate.estado}"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">CEP: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite o n�mero do CEP" name="cep" value="${pacEnderecoParaUpdate.cep}"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Ponto de refer�ncia: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite algum ponto de refer�ncia" name="pontoreferencia" value="${pacEnderecoParaUpdate.pontoreferencia}"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item"></td>
                    <td class="label_list_item" style="color:#555;"><b>Escola</b></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Nome da escola: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite o nome da escola" name="nomeEscola" value="${pacienteParaUpdate.nomeEscola}"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">S�rie: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite o n�mero da s�rie" name="serie" value="${pacienteParaUpdate.serie}"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Endere�o da escola: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite o endere�o da escola" name="endEscola" value="${pacEnderecoEscolaParaUpdate.logradouro}"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">N�mero: </td>
                    <td><input type="text" class="input_text it_mini" placeholder="N�mero da escola" name="numEscola" value="${pacEnderecoEscolaParaUpdate.numero}"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Bairro: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite o bairro da escola" name="bairroEscola" value="${pacEnderecoEscolaParaUpdate.bairro}"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item"></td>
                    <td class="label_list_item" style="color:#555;"><b>Respons�vel</b></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Nome: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite o nome do respons�vel" name="pnomeResp" value="${pacResponsavelParaUpdate.pnomeResp}"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Sobrenome: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite o sobrenome do respons�vel" name="snomeResp" value="${pacResponsavelParaUpdate.snomeResp}"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Data nascimento: </td>
                    <td>
                        <input type="text" class="input_text it_date_mini" maxlength="2" placeholder="Dia" name="datanascDia" value="${respDia}">
                        <input type="text" class="input_text it_date_mini" maxlength="2" placeholder="M�s" name="datanascMes" value="${respMes}">
                        <input type="text" class="input_text it_date_large" maxlength="4" placeholder="Ano" name="datanascAno" value="${respAno}">
                    </td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">RG: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite o RG do respons�vel" name="rgnum" value="${pacResponsavelParaUpdate.rgNum}"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">CPF: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite o CPF do respons�vel" name="cpf" value="${pacResponsavelParaUpdate.cpf}"></td>
                </tr>
                    <td align="right" class="label_list_item">Estado civil: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite o estado civil do respons�vel" name="estadocivil" value="${pacResponsavelParaUpdate.estadoCivil}"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Profiss�o: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite a profiss�o do respons�vel" name="profissao" value="${pacResponsavelParaUpdate.profissao}"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Telefone: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite o telefone do respons�vel" name="telefone" value="${pacResponsavelParaUpdate.telefone}"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Escolaridade: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite a escolaridade do respons�vel" name="escolaridade" value="${pacResponsavelParaUpdate.escolaridade}"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item"></td>
                    <td class="label_list_item" style="color:#555;"><b>Moradia:</b></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Moradia: </td>
                    <td align="left">
                        <c:choose>
                            <c:when test="${pacienteParaUpdate.moradia == 'propria'}">
                                <input type="radio" name="moradia" value="propria" CHECKED> <span class="label_list_item">Pr�pria</span><br>
                                <input type="radio" name="moradia" value="alugada"> <span class="label_list_item">Alugada</span><br>
                                <input type="radio" name="moradia" value="financiada"> <span class="label_list_item">Financiada</span><br>
                                <input type="radio" name="moradia" value="cedida"> <span class="label_list_item">Cedida</span><br>
                            </c:when>
                            <c:when test="${pacienteParaUpdate.moradia == 'alugada'}">
                                <input type="radio" name="moradia" value="propria"> <span class="label_list_item">Pr�pria</span><br>
                                <input type="radio" name="moradia" value="alugada" CHECKED> <span class="label_list_item">Alugada</span><br>
                                <input type="radio" name="moradia" value="financiada"> <span class="label_list_item">Financiada</span><br>
                                <input type="radio" name="moradia" value="cedida"> <span class="label_list_item">Cedida</span><br>
                            </c:when>
                            <c:when test="${pacienteParaUpdate.moradia == 'financiada'}">
                                <input type="radio" name="moradia" value="propria"> <span class="label_list_item">Pr�pria</span><br>
                                <input type="radio" name="moradia" value="alugada"> <span class="label_list_item">Alugada</span><br>
                                <input type="radio" name="moradia" value="financiada" CHECKED> <span class="label_list_item">Financiada</span><br>
                                <input type="radio" name="moradia" value="cedida"> <span class="label_list_item">Cedida</span><br>
                            </c:when>
                            <c:when test="${pacienteParaUpdate.moradia == 'cedida'}">
                                <input type="radio" name="moradia" value="propria"> <span class="label_list_item">Pr�pria</span><br>
                                <input type="radio" name="moradia" value="alugada"> <span class="label_list_item">Alugada</span><br>
                                <input type="radio" name="moradia" value="financiada"> <span class="label_list_item">Financiada</span><br>
                                <input type="radio" name="moradia" value="cedida" CHECKED> <span class="label_list_item">Cedida</span><br>
                            </c:when>
                            <c:otherwise>
                                <input type="radio" name="moradia" value="propria"> <span class="label_list_item">Pr�pria</span><br>
                                <input type="radio" name="moradia" value="alugada"> <span class="label_list_item">Alugada</span><br>
                                <input type="radio" name="moradia" value="financiada"> <span class="label_list_item">Financiada</span><br>
                                <input type="radio" name="moradia" value="cedida"> <span class="label_list_item">Cedida</span><br>
                            </c:otherwise>
                        </c:choose>
                    </td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">C�modos: </td>
                    <td><input type="text" class="input_text it_mini" placeholder="Quantidade" name="comodos" value="${pacienteParaUpdate.comodos}"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Material: </td>
                    <td align="left">
                        <c:choose>
                            <c:when test="${pacienteParaUpdate.material == 'madeira'}">
                                <input type="radio" name="material" value="madeira" CHECKED> <span class="label_list_item">Madeira</span><br>
                                <input type="radio" name="material" value="alvenaria"> <span class="label_list_item">Alvenaria</span><br>
                                <input type="radio" name="material" value="mista"> <span class="label_list_item">Mista</span><br>
                            </c:when>
                            <c:when test="${pacienteParaUpdate.material == 'alvenaria'}">
                                <input type="radio" name="material" value="madeira"> <span class="label_list_item">Madeira</span><br>
                                <input type="radio" name="material" value="alvenaria" CHECKED> <span class="label_list_item">Alvenaria</span><br>
                                <input type="radio" name="material" value="mista"> <span class="label_list_item">Mista</span><br>
                            </c:when>
                            <c:when test="${pacienteParaUpdate.material == 'mista'}">
                                <input type="radio" name="material" value="madeira"> <span class="label_list_item">Madeira</span><br>
                                <input type="radio" name="material" value="alvenaria"> <span class="label_list_item">Alvenaria</span><br>
                                <input type="radio" name="material" value="mista" CHECKED> <span class="label_list_item">Mista</span><br>
                            </c:when>
                            <c:otherwise>
                                <input type="radio" name="material" value="madeira"> <span class="label_list_item">Madeira</span><br>
                                <input type="radio" name="material" value="alvenaria"> <span class="label_list_item">Alvenaria</span><br>
                                <input type="radio" name="material" value="mista"> <span class="label_list_item">Mista</span><br>
                            </c:otherwise>
                        </c:choose>
                    </td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Esgoto: </td>
                    <td align="left">
                        <c:choose>
                            <c:when test="${pacienteParaUpdate.esgoto}">
                                <input type="radio" name="esgoto" value="true" CHECKED> <span class="label_list_item">Sim</span><br>
                                <input type="radio" name="esgoto" value="false"> <span class="label_list_item">N�o</span><br>
                            </c:when>
                            <c:otherwise>
                                <input type="radio" name="esgoto" value="true"> <span class="label_list_item">Sim</span><br>
                                <input type="radio" name="esgoto" value="false" CHECKED> <span class="label_list_item">N�o</span><br>
                            </c:otherwise>
                        </c:choose>
                    </td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Asfalto: </td>
                    <td align="left">
                        <c:choose>
                            <c:when test="${pacienteParaUpdate.asfalto}">
                                <input type="radio" name="asfalto" value="true" CHECKED> <span class="label_list_item">Sim</span><br>
                                <input type="radio" name="asfalto" value="false"> <span class="label_list_item">N�o</span><br>
                            </c:when>
                            <c:otherwise>
                                <input type="radio" name="asfalto" value="true"> <span class="label_list_item">Sim</span><br>
                                <input type="radio" name="asfalto" value="false" CHECKED> <span class="label_list_item">N�o</span><br>
                            </c:otherwise>
                        </c:choose>
                    </td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Banheiro: </td>
                    <td align="left">
                        <c:choose>
                            <c:when test="${pacienteParaUpdate.banheiro == 'interno'}">
                                <input type="radio" name="banheiro" value="interno" CHECKED> <span class="label_list_item">Interno</span><br>
                                <input type="radio" name="banheiro" value="externo"> <span class="label_list_item">Externo</span><br>
                            </c:when>
                            <c:when test="${pacienteParaUpdate.banheiro == 'externo'}">
                                <input type="radio" name="banheiro" value="interno"> <span class="label_list_item">Interno</span><br>
                                <input type="radio" name="banheiro" value="externo" CHECKED> <span class="label_list_item">Externo</span><br>
                            </c:when>
                            <c:otherwise>
                                <input type="radio" name="banheiro" value="interno"> <span class="label_list_item">Interno</span><br>
                                <input type="radio" name="banheiro" value="externo"> <span class="label_list_item">Externo</span><br>
                            </c:otherwise>
                        </c:choose>
                    </td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item"></td>
                    <td class="label_list_item" style="color:#555;"><b>Despesas:</b></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">�gua: </td>
                    <td>
                   		<input type="text" class="input_text it_date_bigger" maxlength="12" placeholder="Valor" name="aguaInt" value="${aguaInt}"> ,
                        <input type="text" class="input_text it_date_mini" maxlength="2" placeholder="00" name="aguaDec" value="${aguaDec}">
                    </td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Energia: </td>
                    <td>
                   		<input type="text" class="input_text it_date_bigger" maxlength="12" placeholder="Valor" name="energiaInt" value="${energiaInt}"> ,
                        <input type="text" class="input_text it_date_mini" maxlength="2" placeholder="00" name="energiaDec" value="${energiaDec}">
                    </td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">G�s: </td>
                    <td>
                   		<input type="text" class="input_text it_date_bigger" maxlength="12" placeholder="Valor" name="gasInt" value="${gasInt}"> ,
                        <input type="text" class="input_text it_date_mini" maxlength="2" placeholder="00" name="gasDec" value="${gasDec}">
                    </td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Aluguel/Presta��o: </td>
                    <td>
                   		<input type="text" class="input_text it_date_bigger" maxlength="12" placeholder="Valor" name="aluguelInt" value="${aluguelInt}"> ,
                        <input type="text" class="input_text it_date_mini" maxlength="2" placeholder="00" name="aluguelDec" value="${aluguelDec}">
                    </td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Transporte: </td>
                    <td>
                   		<input type="text" class="input_text it_date_bigger" maxlength="12" placeholder="Valor" name="transporteInt" value="${transporteInt}"> ,
                        <input type="text" class="input_text it_date_mini" maxlength="2" placeholder="00" name="transporteDec" value="${transporteDec}">
                    </td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Medicamentos: </td>
                    <td>
                   		<input type="text" class="input_text it_date_bigger" maxlength="12" placeholder="Valor" name="medicamentosInt" value="${medicamentosInt}"> ,
                        <input type="text" class="input_text it_date_mini" maxlength="2" placeholder="00" name="medicamentosDec" value="${medicamentosDec}">
                    </td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Alimenta��o: </td>
                    <td>
                   		<input type="text" class="input_text it_date_bigger" maxlength="12" placeholder="Valor" name="alimentacaoInt" value="${alimentacaoInt}"> ,
                        <input type="text" class="input_text it_date_mini" maxlength="2" placeholder="00" name="alimentacaoDec" value="${alimentacaoDec}">
                    </td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item"></td>
                    <td class="label_list_item" style="color:#555;"><b>Recursos e/ou benef�cios usados:</b></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Creche: </td>
                    <td align="left">
                        <c:choose>
                            <c:when test="${pacienteParaUpdate.creche}">
                                <input type="radio" name="creche" value="true" CHECKED> <span class="label_list_item">Sim</span><br>
                                <input type="radio" name="creche" value="false"> <span class="label_list_item">N�o</span><br>
                            </c:when>
                            <c:otherwise>
                                <input type="radio" name="creche" value="true"> <span class="label_list_item">Sim</span><br>
                                <input type="radio" name="creche" value="false" CHECKED> <span class="label_list_item">N�o</span><br>
                            </c:otherwise>
                        </c:choose>
                    </td>
                </tr>
                
                
                <tr>
                    <td align="right" class="label_list_item">Centro comunit�rio: </td>
                    <td align="left">
                        <c:choose>
                            <c:when test="${pacienteParaUpdate.centroCom}">
                                <input type="radio" name="centro_comunitario" value="true" CHECKED> <span class="label_list_item">Sim</span><br>
                                <input type="radio" name="centro_comunitario" value="false"> <span class="label_list_item">N�o</span><br>
                            </c:when>
                            <c:otherwise>
                                <input type="radio" name="centro_comunitario" value="true"> <span class="label_list_item">Sim</span><br>
                                <input type="radio" name="centro_comunitario" value="false" CHECKED> <span class="label_list_item">N�o</span><br>
                            </c:otherwise>
                        </c:choose>
                    </td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Bolsa fam�lia: </td>
                    <td align="left">
                        <c:choose>
                            <c:when test="${pacienteParaUpdate.bolsaFam}">
                                <input type="radio" name="bolsa_familia" value="true" CHECKED> <span class="label_list_item">Sim</span><br>
                                <input type="radio" name="bolsa_familia" value="false"> <span class="label_list_item">N�o</span><br>
                            </c:when>
                            <c:otherwise>
                                <input type="radio" name="bolsa_familia" value="true"> <span class="label_list_item">Sim</span><br>
                                <input type="radio" name="bolsa_familia" value="false" CHECKED> <span class="label_list_item">N�o</span><br>
                            </c:otherwise>
                        </c:choose>
                    </td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Auxilio g�s: </td>
                    <td align="left">
                        <c:choose>
                            <c:when test="${pacienteParaUpdate.auxilioGas}">
                                <input type="radio" name="auxilio_gas" value="true" CHECKED> <span class="label_list_item">Sim</span><br>
                                <input type="radio" name="auxilio_gas" value="false"> <span class="label_list_item">N�o</span><br>
                            </c:when>
                            <c:otherwise>
                                <input type="radio" name="auxilio_gas" value="true"> <span class="label_list_item">Sim</span><br>
                                <input type="radio" name="auxilio_gas" value="false" CHECKED> <span class="label_list_item">N�o</span><br>
                            </c:otherwise>
                        </c:choose>
                    </td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Luz fraterna: </td>
                    <td align="left">
                        <c:choose>
                            <c:when test="${pacienteParaUpdate.luzFraterna}">
                                <input type="radio" name="luz_fraterna" value="true" CHECKED> <span class="label_list_item">Sim</span><br>
                                <input type="radio" name="luz_fraterna" value="false"> <span class="label_list_item">N�o</span><br>
                            </c:when>
                            <c:otherwise>
                                <input type="radio" name="luz_fraterna" value="true"> <span class="label_list_item">Sim</span><br>
                                <input type="radio" name="luz_fraterna" value="false" CHECKED> <span class="label_list_item">N�o</span><br>
                            </c:otherwise>
                        </c:choose>
                    </td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Religi�o: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite a religi�o" name="religiao" value="${pacienteParaUpdate.religiao}"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Contribui com a fam�lia: </td>
                    <td align="left">
                        <c:choose>
                            <c:when test="${pacienteParaUpdate.religiaoCont}">
                                <input type="radio" name="contribui_familia" value="true" CHECKED> <span class="label_list_item">Sim</span><br>
                                <input type="radio" name="contribui_familia" value="false"> <span class="label_list_item">N�o</span><br>
                            </c:when>
                            <c:otherwise>
                                <input type="radio" name="contribui_familia" value="true"> <span class="label_list_item">Sim</span><br>
                                <input type="radio" name="contribui_familia" value="false" CHECKED> <span class="label_list_item">N�o</span><br>
                            </c:otherwise>
                        </c:choose>
                    </td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Outro aux�lio: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite se tiver outro aux�lio" name="outroAux" value="${pacienteParaUpdate.outroAux}"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Apoio familiar: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite o apoio familiar" name="apoioFam" value="${pacienteParaUpdate.apoioFam}"></td>
                </tr>
                
                <tr>
                    <td align="right" class="label_list_item">Renda mensal: </td>
                    <td>
                   		<input type="text" class="input_text it_date_bigger" maxlength="12" placeholder="Valor" name="rendaMensalInt" value="${rendaMensalInt}"> ,
                        <input type="text" class="input_text it_date_mini" maxlength="2" placeholder="00" name="rendaMensalDec" value="${rendaMensalDec}">
                    </td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Hist�rico familiar: </td>
                    <td><input type="text" class="input_textarea it_large" placeholder="Digite o hist�rico familiar" name="historicoFamiliar" value="${pacienteParaUpdate.historicoFamiliar}"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Hist�rico da patologia: </td>
                    <td><input type="text" class="input_textarea it_large" placeholder="Hist�rico da patologia, tratamento e acompanhamento com evolu��o do caso" name="histPatologia" value="${pacienteParaUpdate.histPatologia}"></td>
                </tr>
                
                
                <tr>
                    <td><input type="hidden" name="id" value="<%= request.getParameter("id") %>"></td>
                    <td align="center"><input type="submit" value="Salvar" class="input_button" style="width:326px; height:40px"></td>
                </tr>
                
            </table>
        </div>
    </div>
</div>
</form>





<br><br>

<%@include file="../../../includes/bottom_bar.jsp"%>
</body>
</html>
