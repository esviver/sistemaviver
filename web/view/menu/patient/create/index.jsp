<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Ong Viver - Cadastrar paciente</title>
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/main.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/top_bar.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/bottom_bar.css">
<script src="${pageContext.servletContext.contextPath}/assets/javascript/main.js"></script>
</head>
<body onLoad="introCard()">
<%@include file="../../../includes/top_bar.jsp"%>

<br>

<div align="center">
    <div class="navigator">
    	<a href="${pageContext.servletContext.contextPath}/view/menu/" class="navigator_link">Menu</a>
        >
        <a href="${pageContext.servletContext.contextPath}/view/menu/patient/index.jsp" class="navigator_link">Paciente</a>
        >
        <a href="${pageContext.servletContext.contextPath}/view/menu/patient/create/index.jsp" class="navigator_link">Cadastrar paciente</a>
    </div>
</div>


<br>

<form action="${pageContext.servletContext.contextPath}/view/menu/patient/create" method="POST">
<div align="center">
	<div align="right" class="card_support_loading" id="card_support">
        <div class="card" style="width:620px;" align="center">
            <table>
                <tr>
                    <td align="center" colspan="2" class="label_title_card">
                        Cadastrar novo paciente<br>
                        P�GINA EM CONSTRUCAO:<br>
                        - Adicionar 1 campo d�vida
                    </td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item"></td>
                    <td class="label_list_item" style="color:#555;"><b>Paciente</b></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Nome: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite o nome" name="nomePaciente"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Sobrenome: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite o sobrenome" name="sobrenomePaciente"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Data nascimento: </td>
                    <td>
                        <input type="text" class="input_text it_date_mini" maxlength="2" placeholder="Dia" name="dataNascPacDia">
                        <input type="text" class="input_text it_date_mini" maxlength="2" placeholder="M�s" name="dataNascPacMes">
                        <input type="text" class="input_text it_date_large" maxlength="4" placeholder="Ano" name="dataNascPacAno">
                    </td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Patologia: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite a patologia" name="patologia"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Cart�o SUS: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite o c�digo do cart�o SUS" name="nsus"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Local nascimento: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite o local do nascimento" name="localNasc"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item"></td>
                    <td class="label_list_item" style="color:#555;"><b>Endereco</b></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Rua: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite a rua" name="logradouro"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">N�mero: </td>
                    <td><input type="text" class="input_text it_mini" placeholder="N�mero" name="numero"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Bairro: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite o nome do bairro" name="bairro"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Cidade: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite o nome da cidade" name="cidade"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Estado: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite o nome do estado" name="estado"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">CEP: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite o n�mero do CEP" name="cep"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Ponto de refer�ncia: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite algum ponto de refer�ncia" name="pontoreferencia"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item"></td>
                    <td class="label_list_item" style="color:#555;"><b>Escola</b></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Nome da escola: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite o nome da escola" name="nomeEscola"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">S�rie: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite o n�mero da s�rie" name="serie"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Endere�o da escola: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite o endere�o da escola" name="endEscola"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">N�mero: </td>
                    <td><input type="text" class="input_text it_mini" placeholder="N�mero da escola" name="numEscola"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Bairro: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite o bairro da escola" name="bairroEscola"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item"></td>
                    <td class="label_list_item" style="color:#555;"><b>Respons�vel</b></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Nome: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite o nome do respons�vel" name="pnomeResp"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Sobrenome: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite o sobrenome do respons�vel" name="snomeResp"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Data nascimento: </td>
                    <td>
                        <input type="text" class="input_text it_date_mini" maxlength="2" placeholder="Dia" name="datanascDia">
                        <input type="text" class="input_text it_date_mini" maxlength="2" placeholder="M�s" name="datanascMes">
                        <input type="text" class="input_text it_date_large" maxlength="4" placeholder="Ano" name="datanascAno">
                    </td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">RG: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite o RG do respons�vel" name="rg"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">CPF: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite o CPF do respons�vel" name="cpf"></td>
                </tr>
                    <td align="right" class="label_list_item">Estado civil: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite o estado civil do respons�vel" name="estadocivil"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Profiss�o: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite a profiss�o do respons�vel" name="profissao"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Telefone: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite o telefone do respons�vel" name="telefone"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Escolaridade: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite a escolaridade do respons�vel" name="escolaridade"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item"></td>
                    <td class="label_list_item" style="color:#555;"><b>Moradia:</b></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Moradia: </td>
                    <td align="left">
                        <input type="radio" name="moradia" value="propria"> <span class="label_list_item">Pr�pria</span><br>
                        <input type="radio" name="moradia" value="alugada"> <span class="label_list_item">Alugada</span><br>
                        <input type="radio" name="moradia" value="financiada"> <span class="label_list_item">Financiada</span><br>
                        <input type="radio" name="moradia" value="cedida"> <span class="label_list_item">Cedida</span><br>
                    </td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">C�modos: </td>
                    <td><input type="text" class="input_text it_mini" placeholder="Quantidade" name="comodos"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Material: </td>
                    <td align="left">
                        <input type="radio" name="material" value="madeira"> <span class="label_list_item">Madeira</span><br>
                        <input type="radio" name="material" value="alvenaria"> <span class="label_list_item">Alvenaria</span><br>
                        <input type="radio" name="material" value="mista"> <span class="label_list_item">Mista</span><br>
                    </td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Esgoto: </td>
                    <td align="left">
                        <input type="radio" name="esgoto" value="true"> <span class="label_list_item">Sim</span><br>
                        <input type="radio" name="esgoto" value="false"> <span class="label_list_item">N�o</span><br>
                    </td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Asfalto: </td>
                    <td align="left">
                        <input type="radio" name="asfalto" value="true"> <span class="label_list_item">Sim</span><br>
                        <input type="radio" name="asfalto" value="false"> <span class="label_list_item">N�o</span><br>
                    </td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Banheiro: </td>
                    <td align="left">
                        <input type="radio" name="banheiro" value="interno"> <span class="label_list_item">Interno</span><br>
                        <input type="radio" name="banheiro" value="externo"> <span class="label_list_item">Externo</span><br>
                    </td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item"></td>
                    <td class="label_list_item" style="color:#555;"><b>Despesas:</b></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">�gua: </td>
                    <td>
                   		<input type="text" class="input_text it_date_bigger" maxlength="12" placeholder="Valor" name="aguaInt"> ,
                        <input type="text" class="input_text it_date_mini" maxlength="2" placeholder="00" name="aguaDec">
                    </td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Energia: </td>
                    <td>
                   		<input type="text" class="input_text it_date_bigger" maxlength="12" placeholder="Valor" name="energiaInt"> ,
                        <input type="text" class="input_text it_date_mini" maxlength="2" placeholder="00" name="energiaDec">
                    </td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">G�s: </td>
                    <td>
                   		<input type="text" class="input_text it_date_bigger" maxlength="12" placeholder="Valor" name="gasInt"> ,
                        <input type="text" class="input_text it_date_mini" maxlength="2" placeholder="00" name="gasDec">
                    </td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Aluguel/Presta��o: </td>
                    <td>
                   		<input type="text" class="input_text it_date_bigger" maxlength="12" placeholder="Valor" name="aluguelInt"> ,
                        <input type="text" class="input_text it_date_mini" maxlength="2" placeholder="00" name="aluguelDec">
                    </td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Transporte: </td>
                    <td>
                   		<input type="text" class="input_text it_date_bigger" maxlength="12" placeholder="Valor" name="transporteInt"> ,
                        <input type="text" class="input_text it_date_mini" maxlength="2" placeholder="00" name="transporteDec">
                    </td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Medicamentos: </td>
                    <td>
                   		<input type="text" class="input_text it_date_bigger" maxlength="12" placeholder="Valor" name="medicamentosInt"> ,
                        <input type="text" class="input_text it_date_mini" maxlength="2" placeholder="00" name="medicamentosDec">
                    </td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Alimenta��o: </td>
                    <td>
                   		<input type="text" class="input_text it_date_bigger" maxlength="12" placeholder="Valor" name="alimentacaoInt"> ,
                        <input type="text" class="input_text it_date_mini" maxlength="2" placeholder="00" name="alimentacaoDec">
                    </td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item"></td>
                    <td class="label_list_item" style="color:#555;"><b>Recursos e/ou benef�cios usados:</b></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Creche: </td>
                    <td align="left">
                        <input type="radio" name="creche" value="true"> <span class="label_list_item">Sim</span><br>
                        <input type="radio" name="creche" value="false"> <span class="label_list_item">N�o</span><br>
                    </td>
                </tr>
                
                
                <tr>
                    <td align="right" class="label_list_item">Centro comunit�rio: </td>
                    <td align="left">
                        <input type="radio" name="centro_comunitario" value="true"> <span class="label_list_item">Sim</span><br>
                        <input type="radio" name="centro_comunitario" value="false"> <span class="label_list_item">N�o</span><br>
                    </td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Bolsa fam�lia: </td>
                    <td align="left">
                        <input type="radio" name="bolsa_familia" value="true"> <span class="label_list_item">Sim</span><br>
                        <input type="radio" name="bolsa_familia" value="false"> <span class="label_list_item">N�o</span><br>
                    </td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Auxilio g�s: </td>
                    <td align="left">
                        <input type="radio" name="auxilio_gas" value="true"> <span class="label_list_item">Sim</span><br>
                        <input type="radio" name="auxilio_gas" value="false"> <span class="label_list_item">N�o</span><br>
                    </td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Luz fraterna: </td>
                    <td align="left">
                        <input type="radio" name="luz_fraterna" value="true"> <span class="label_list_item">Sim</span><br>
                        <input type="radio" name="luz_fraterna" value="false"> <span class="label_list_item">N�o</span><br>
                    </td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Religi�o: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite a religi�o" name="religiao"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Contribui com a fam�lia: </td>
                    <td align="left">
                        <input type="radio" name="contribui_familia" value="true"> <span class="label_list_item">Sim</span><br>
                        <input type="radio" name="contribui_familia" value="false"> <span class="label_list_item">N�o</span><br>
                    </td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Outro aux�lio: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite se tiver outro aux�lio" name="outroAux"></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Apoio familiar: </td>
                    <td><input type="text" class="input_text it_large" placeholder="Digite o apoio familiar" name="apoioFam"></td>
                </tr>
                
                <tr>
                    <td align="right" class="label_list_item">Renda mensal: </td>
                    <td>
                   		<input type="text" class="input_text it_date_bigger" maxlength="12" placeholder="Valor" name="rendaMensalInt"> ,
                        <input type="text" class="input_text it_date_mini" maxlength="2" placeholder="00" name="rendaMensalDec">
                    </td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Hist�rico familiar: </td>
                    <td><textarea type="text" class="input_textarea it_large" placeholder="Digite o hist�rico familiar" name="historicoFamiliar"></textarea></td>
                </tr>
                <tr>
                    <td align="right" class="label_list_item">Hist�rico da patologia: </td>
                    <td><textarea type="text" class="input_textarea it_large" placeholder="Hist�rico da patologia, tratamento e acompanhamento com evolu��o do caso" name="histPatologia"></textarea></td>
                </tr>
                
                <tr>
                    <td></td>
                    <td align="center"><input type="submit" value="Salvar" class="input_button" style="width:326px; height:40px"></td>
                </tr>
            </table>
        </div>
    </div>
</div>
</form>





<br><br>

<%@include file="../../../includes/bottom_bar.jsp"%>
</body>
</html>