<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Ong Viver - Deletar Paciente</title>
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/main.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/top_bar.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/bottom_bar.css">
<script src="${pageContext.servletContext.contextPath}/assets/javascript/main.js"></script>
</head>
<body onLoad="introCard()">
<%@include file="../../../includes/top_bar.jsp"%>

<br>

<div align="center">
    <div class="navigator">
    	<a href="${pageContext.servletContext.contextPath}/view/menu/index.jsp" class="navigator_link">Menu</a>
        >
        <a href="${pageContext.servletContext.contextPath}/view/menu/patient/index.jsp" class="navigator_link">Pacientes</a>
        >
        <a href="#" class="navigator_link">Deletar paciente</a>
    </div>
</div>

<br>

<form action="${pageContext.servletContext.contextPath}/view/menu/patient/delete" method="post">
<div align="center">
	<div align="right" class="card_support_loading" id="card_support">
        <div class="card" style="width:620px;" align="center">
            <table>
                <tr>
                    <td align="center" class="label_title_card">Tem certeza que deseja deletar?</td>
                </tr>
                
                
                <tr>
                    <input type="hidden" name="id" value="<%= request.getParameter("id") %>">
                    <td align="center"><input type="submit" value="Sim" class="input_button" style="width:326px; height:40px"></td>
                </tr>
                
            </table>
        </div>
    </div>
</div>
</form>





<br><br>

<%@include file="../../../includes/bottom_bar.jsp"%>
</body>
</html>
