<!doctype html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ong Viver - Relatórios</title>
        <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/main.css">
        <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/top_bar.css">
        <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/bottom_bar.css">
        <script src="${pageContext.servletContext.contextPath}/assets/javascript/main.js"></script>
    </head>
    <body onLoad="introMenu()">
        <%@include file="../../includes/top_bar.jsp"%>

        <br>

        <div align="center">
            <div class="navigator">
                <a href="${pageContext.servletContext.contextPath}/view/menu/" class="navigator_link">Menu</a>
                >
                <a href="${pageContext.servletContext.contextPath}/view/menu/report/index.jsp" class="navigator_link">Relatórios</a>
            </div>
        </div>

        <br>



        <table class="menu menu_loading" align="center" cellpadding="0" cellspacing="0" id="menu" border="0">
            <tr>
                <td align="left" valign="top">
                    <a href="${pageContext.servletContext.contextPath}/view/menu/patient/read/index.jsp?all=true" class="menu_box_link">
                        <div class="menu_box menu_report_patient" align="center">
                            <div class="menu_box_quantity">${pacientes} itens</div>
                            Pacientes cadastrados
                        </div>
                    </a>
                    <div class="menu_hole"></div>
                </td>
                <td align="right" valign="top">
                    <a href="${pageContext.servletContext.contextPath}/view/menu/volunteer/read/index.jsp?all=true" class="menu_box_link">
                        <div class="menu_box menu_report_volunteer" align="center">
                            <div class="menu_box_quantity">${voluntarios} itens</div>
                            Voluntários cadastrados
                        </div>
                    </a>
                    <div class="menu_hole"></div>
                </td>
            </tr>
            <tr>
                <td align="left" valign="top">
                    <a href="${pageContext.servletContext.contextPath}/view/menu/sleep/sleeps/read/index.jsp?all=true" class="menu_box_link">
                        <div class="menu_box menu_report_sleep" align="center">
                            <div class="menu_box_quantity">${hospedagens} itens</div>
                            Hospedagens cadastradas
                        </div>
                    </a>
                    <div class="menu_hole"></div>
                </td>
                <td align="right" valign="top">
                    <a href="${pageContext.servletContext.contextPath}/view/menu/agendamentos/psychology/read/index.jsp?all=true" class="menu_box_link">
                        <div class="menu_box menu_report_psychology" align="center">
                            <div class="menu_box_quantity">${atendimentoPS} itens</div>
                            Psicológico
                        </div>
                    </a>
                    <div class="menu_hole"></div>
                </td>
            </tr>
            <tr>
                <td align="left" valign="top">
                    <a href="${pageContext.servletContext.contextPath}/view/menu/agendamentos/social_care/read/index.jsp?all=true" class="menu_box_link">
                        <div class="menu_box menu_report_social_care" align="center">
                            <div class="menu_box_quantity">${atendimentoASS} itens</div>
                            Atendimento social
                        </div>
                    </a>
                    <div class="menu_hole"></div>
                </td>
                <td align="right" valign="top">
                    <a href="${pageContext.servletContext.contextPath}/view/menu/agendamentos/anamnese/read/index.jsp?all=true" class="menu_box_link">
                        <div class="menu_box menu_report_anamnesis" align="center">
                            <div class="menu_box_quantity">${anamnese} itens</div>
                            Anamnese
                        </div>
                    </a>
                    <div class="menu_hole"></div>
                </td>
            </tr>
            <tr>
                <td align="left" valign="top">
                    <a href="${pageContext.servletContext.contextPath}/view/menu/donate/read/index.jsp?all=true" class="menu_box_link">
                        <div class="menu_box menu_report_donate" align="center">
                            <div class="menu_box_quantity">${doacoes} itens</div>
                            Doações
                        </div>
                    </a>
                    <div class="menu_hole"></div>
                </td>
                <td align="right" valign="top">
                    <a href="${pageContext.servletContext.contextPath}/view/menu/basket/read/index.jsp?all=true" class="menu_box_link">
                        <div class="menu_box menu_report_basket" align="center">
                            <div class="menu_box_quantity">${distribuicoes} itens</div>
                            Distribuição de cestas
                        </div>
                    </a>
                    <div class="menu_hole"></div>
                </td>
            </tr>

        </table>



        <br><br>
        <%@include file="../../includes/bottom_bar.jsp"%>
    </body>
</html>