<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ong Viver - Atualizar relat�rio de volunt�rio</title>
        <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/main.css">
        <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/top_bar.css">
        <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/bottom_bar.css">
        <script src="${pageContext.servletContext.contextPath}/assets/javascript/main.js"></script>
    </head>
    <body onLoad="introCard()">
        <%@include file="../../../includes/top_bar.jsp"%>

        <br>

        <div align="center">
            <div class="navigator">
                <a href="${pageContext.servletContext.contextPath}/view/menu/index.jsp" class="navigator_link">Menu</a>
                >
                <a href="${pageContext.servletContext.contextPath}/view/menu/volunteer/index.jsp" class="navigator_link">Relatorios</a>
                >
                <a href="#" class="navigator_link">Atualizar um relatorio</a>
            </div>
        </div>

        <br>

        <form action="${pageContext.servletContext.contextPath}/view/menu/volunteer/update" method="post">
            <div align="center">
                <div align="right" class="card_support_loading" id="card_support">
                    <div class="card" style="width:620px;" align="center">
                        <table>
                            <tr>
                                <td align="center" colspan="2" class="label_title_card">Atualizacao relat�rio de volunt�rio: </td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">Setor: </td>
                                <td>
                                    <input type="text" class="input_text it_large" placeholder="Digite o nome do setor" name="setor" value="${v.setor}">
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">Data: </td>
                                <td>
                                    <input type="text" class="input_text it_date_mini" maxlength="2" placeholder="Dia" name="diaDia" value="${dia}"> 
                                    <input type="text" class="input_text it_date_mini" maxlength="2" placeholder="M�s" name="diaMes" value="${mes}"> 
                                    <input type="text" class="input_text it_date_large" maxlength="4" placeholder="Ano" name="diaAno" value="${ano}">
                                </td>
                            </tr>
                            
                            <tr>
                                <td align="right" class="label_list_item">Hor�rio: </td>
                                <td>
                                    <select name="horas" class="input_select" style="display:inline">
                                        <option value="${horas}">Horas</option>
                                        <c:forEach begin="0" end="23" var="i">
                                            <option value="<c:out value="${i}"/>"><c:out value="${i}"/></option>
                                        </c:forEach>
                                    </select>
                                    <select name="minutos" class="input_select"style="display:inline">
                                        <option value="${minutos}">Minutos</option>
                                        <c:forEach begin="0" end="59" var="i">
                                            <option value="<c:out value="${i}"/>"><c:out value="${i}"/></option>
                                        </c:forEach>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">Observa��es: </td>
                                <td>
                                    <textarea class="input_textarea it_large" placeholder="Digite por qual meio" name="observacao">${v.observacao}</textarea>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">Atividades<br>desenvolvidas: </td>
                                <td>
                                    <textarea class="input_textarea it_large" placeholder="Digite por qual meio" name="ativ_desenvolvida">${v.ativDesenvolvida}</textarea>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">Coordenador: </td>
                                <td>
                                    <input type="text" class="input_text it_large" placeholder="Digite o nome do coordenador" name="coordenador" value="${v.coordenador}">
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="label_list_item">Aprovar? </td>
                                <td class=" label_list_item">
                                    <c:choose>
                                        <c:when test="${v.aprovado}">
                                            <input type="radio" name="aprovar" value="true" checked> Sim <br>
                                            <input type="radio" name="aprovar" value="false"> N�o <br>
                                        </c:when>
                                        <c:otherwise>
                                            <input type="radio" name="aprovar" value="true"> Sim <br>
                                            <input type="radio" name="aprovar" value="false" checked> N�o <br>
                                        </c:otherwise>
                                    </c:choose>
                                    
                                </td>
                            </tr>
                            <tr>
                                <td><input type="hidden" name="id" value="<%= request.getParameter("id")%>"></td>
                                <td align="center"><input type="submit" value="Salvar" class="input_button" style="width:326px; height:40px"></td>
                            </tr>
                            
                        </table>
                    </div>
                </div>
            </div>
        </form>





        <br><br>

        <%@include file="../../../includes/bottom_bar.jsp"%>
    </body>
</html>
