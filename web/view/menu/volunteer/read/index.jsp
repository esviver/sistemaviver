<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Ong Viver - Pesquisar relat�rio de volunt�rio</title>
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/main.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/top_bar.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/assets/css/bottom_bar.css">
<script src="${pageContext.servletContext.contextPath}/assets/javascript/main.js"></script>
<script src="${pageContext.servletContext.contextPath}/assets/javascript/pesquisa.js"></script>
</head>
<body onLoad="introCard()<c:if test='${param["all"] == true}'>,mostrarTudoRelatorio(event)</c:if>">
<%@include file="../../../includes/top_bar.jsp"%>

<br>


<div align="center">
    <div class="navigator">
    	<a href="${pageContext.servletContext.contextPath}/view/menu/" class="navigator_link">Menu</a>
        >
        <a href="${pageContext.servletContext.contextPath}/view/menu/volunteer/index.jsp" class="navigator_link">Relatorio</a>
        >
        <a href="${pageContext.servletContext.contextPath}/view/menu/volunteer/read/index.jsp" class="navigator_link">Pesquisar relatorio</a>
    </div>
</div>


<br>


<div align="center">
	<div align="right" class="card_support_loading" id="card_support">
        <div class="card" style="width:620px;" align="center">
            <span class="label_title_card">Pesquise por um relatorio</span><br><br>
            <table width="620">
            	<tr>
                    <td width="100%">
                    	<input id="search_box" autocomplete="off" type="text" placeholder="Fa�a sua pesquisa..." class="input_text" style="width:calc(100% - 24px);" onKeyUp="pesquisarRelatorio(event,search_box.value)">
                    </td>
                    <td>
                    	<input style="height:44px;" type="button" value="Pesquisar" class="input_button" onClick="pesquisarRelatorio(event,search_box.value)">
                    </td>
                </tr>
            </table>
            <br>
            <div class="result_content" id="result_content">
            	<div class="search_msg_box search_msg_box_volunteer" width="100"></div>
            </div>
            <br>
            <input type="checkbox" onChange="setExibirItensExcluidos(this.checked)"> <span class="label_info">Mostrar itens exclu�dos</span><br>
            
            
            <a href="javascript:void(0)" onClick="mostrarTudoRelatorio(event)" class="label_info">Mostrar tudo</a>
            <br><br>
            <table border="0" class="label_list_item" style="color:#888">
                <tr>
                    <td><input type="radio" name="opcao" value="aprovados"></td>
                    <td>Aprovados</td>
                </tr>
                <tr>
                    <td><input type="radio" name="opcao" value="reprovados"></td>
                    <td> Reprovados</td>
                </tr>
                <tr>
                    <td><input type="radio" name="opcao" value="nao_analisados"></td>
                    <td>N�o Analisados</td>
                </tr>
                <tr>
                    <td><input type="radio" name="opcao" value="todos" checked></td>
                    <td>Todos</td>
                </tr>
            </table>
            
            
            
        </div>
    </div>
</div>






<br><br>

<%@include file="../../../includes/bottom_bar.jsp"%>
</body>
</html>