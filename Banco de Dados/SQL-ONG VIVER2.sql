CREATE SCHEMA ONG2;


CREATE TABLE ONG2.ENDERECO(
	id SERIAL UNIQUE,
	logradouro VARCHAR(200),
	bairro VARCHAR(200),
	numero INT,
	cidade VARCHAR(200),
	estado VARCHAR(200),
	cep VARCHAR(10),
	pontoreferencia TEXT,
	CONSTRAINT pk_endereco PRIMARY KEY(id)
	);
CREATE TABLE ONG2.PACIENTE(
	id SERIAL UNIQUE,
	agua NUMERIC(8,2),
	alimentacao NUMERIC(8,2),
	aluguel NUMERIC(8,2),
	apoioFam VARCHAR(150),
	banheiro VARCHAR(150),
	id_end INT,
	religiaoCont BOOLEAN,
	comodos INT,
	dataNasc DATE,
	energia NUMERIC(8,2),
	gas NUMERIC(8,2),
	histPatologia VARCHAR(150),
	historicoFamiliar VARCHAR(150),
	localNasc VARCHAR(150),
	material VARCHAR(150),
	medicamentos NUMERIC(8,2),
	moradia VARCHAR(150),
	pnome VARCHAR(150),
	nomeEscola VARCHAR(150),
	outroAux VARCHAR(150),
	patologia VARCHAR(150),
	religiao VARCHAR(150),
	rendaMensal NUMERIC(8,2),
	serie INT,
	transporte NUMERIC(8,2),
	nsus CHAR(15),
	asfalto BOOLEAN,
	auxilioGas BOOLEAN,
	bolsaFam BOOLEAN,
	centroCom BOOLEAN,
	creche BOOLEAN,
	esgoto BOOLEAN,
	luzFraterna BOOLEAN,
	snome VARCHAR(150),
	obito BOOLEAN,
	situacao BOOLEAN,
	alta BOOLEAN,
	entrada DATE DEFAULT CURRENT_DATE,
	saida DATE,
	id_end_esc INT,
	CONSTRAINT pk_paciente PRIMARY KEY(id),
	CONSTRAINT fk_endereco FOREIGN KEY(id_end)
		REFERENCES ONG2.ENDERECO(id) ON UPDATE CASCADE
);
CREATE TABLE ONG2.quarto(
	id INT,
	status INT DEFAULT 0,
	capacidade INT,
	--status boolean default true,
	CONSTRAINT pk_quarto PRIMARY KEY(id)
);

CREATE TABLE ONG2.USUARIO(
	cpf  CHAR(11),
	rg_num VARCHAR(9),
	rg_org VARCHAR(20),
	login VARCHAR(30) UNIQUE,
	senha VARCHAR(8),
	pnome VARCHAR(50),
	snome VARCHAR(100),
	endereco INT,
	telefone VARCHAR (12),
	celular VARCHAR(12),
	tipo_de_user VARCHAR(12),
	nascimento date,
	sexo VARCHAR(10),
	anotacoes TEXT,
	id SERIAL UNIQUE,
	status boolean DEFAULT true,
	CONSTRAINT pk_funcionario PRIMARY KEY(cpf),
	CONSTRAINT fk_endereco FOREIGN KEY(endereco)
		REFERENCES ONG2.ENDERECO(id) ON UPDATE CASCADE
);
CREATE TABLE ONG2.PSICOLOGA(
	idusuario INT,
	crp VARCHAR(10),
	idP SERIAL UNIQUE,
	CONSTRAINT pk_psicologa PRIMARY KEY(crp),
	CONSTRAINT fk_usuario_psi FOREIGN KEY(idusuario)
		REFERENCES ONG2.USUARIO(id) ON UPDATE CASCADE
);
CREATE TABLE ONG2.ASSISTENTE_SOCIAL(
	idusuario INT,
	cress CHAR(5),
	id SERIAL UNIQUE,
	status boolean DEFAULT true,
	CONSTRAINT pk_ass PRIMARY KEY(cress),
	CONSTRAINT fk_func_ass FOREIGN KEY(idusuario)
		REFERENCES ONG2.USUARIO(id) ON UPDATE CASCADE
);
CREATE TABLE ONG2.RESPONSAVEL(
	cpf CHAR(11),
	pnome VARCHAR(50),
	snome VARCHAR(100),
	profissao VARCHAR(100),
	escolaridade VARCHAR(100),
	telefone VARCHAR(11),
	estadocivil VARCHAR(30),
	rgnum CHAR(9),
	rgorg VARCHAR(20),
	id_endereco INT,
	datanasc DATE,
	id SERIAL UNIQUE,
	id_pac INT,
	status boolean DEFAULT true,
	CONSTRAINT pk_responsavel PRIMARY KEY(cpf),
	CONSTRAINT fk_id_pac FOREIGN KEY (id_pac)
		REFERENCES ONG2.PACIENTE(id) ON UPDATE CASCADE,
	CONSTRAINT fk_endereco FOREIGN KEY (id_endereco)
		REFERENCES ONG2.ENDERECO(id) ON UPDATE CASCADE
	);

CREATE TABLE ONG2.ATENDIMENTO_PS(
	idpsicologa INT,
	data_at DATE,
	relatorio VARCHAR(1000),
	idpaciente INT,
	hora TIME,
	id SERIAL UNIQUE,
	CONSTRAINT fk_atend_psi FOREIGN KEY (idpsicologa)
		REFERENCES ONG2.PSICOLOGA(idP) ON DELETE RESTRICT,
	CONSTRAINT fk_atend_pac FOREIGN KEY(idpaciente)
		REFERENCES ONG2.PACIENTE(id) ON DELETE RESTRICT,
	CONSTRAINT pk_atend PRIMARY KEY(id)
);
CREATE TABLE ONG2.ANAMNESE(
	idpsicologa INT,
	idpaciente INT,
	encaminhamento TEXT,
	diagnostico TEXT,
	medico TEXT,
	genograma bytea,
	historia TEXT,
	historico TEXT,
	queixa TEXT,
	motivos TEXT,
	hipoteses TEXT,
	planoterapeutico TEXT,
	psicosociais TEXT,
	estruturafamiliar TEXT,
	infaciaadolescencia TEXT,
	aspectospsicologicos TEXT,
	dia DATE,
	id SERIAL UNIQUE,
	CONSTRAINT fk_psicologa FOREIGN KEY(idpsicologa)
		REFERENCES ONG2.PSICOLOGA(idP) ON UPDATE CASCADE,
	CONSTRAINT fk_paciente FOREIGN KEY(idpaciente)
		REFERENCES ONG2.PACIENTE(id) ON UPDATE CASCADE
		
	
);
CREATE TABLE ONG2.ATENDIMENTO_ASS(
	idassistente INT,
	data_at DATE,
	relatorio VARCHAR(1000),
	idpaciente INT,
	hora TIME,
	id SERIAL UNIQUE,
	CONSTRAINT fk_atend_ass FOREIGN KEY (idassistente)
		REFERENCES ONG2.ASSISTENTE_SOCIAL(id) ON DELETE RESTRICT,
	CONSTRAINT fk_atend_pac FOREIGN KEY(idpaciente)
		REFERENCES ONG2.PACIENTE(id) ON UPDATE CASCADE,
	CONSTRAINT pk_atend_ass PRIMARY KEY(id)
);


CREATE TABLE ONG2.VOLUNTARIO(
	idusuario INT,
	email VARCHAR(100),
	local_trab VARCHAR(100),
	escolaridade VARCHAR(100),
	cdetelefone VARCHAR(11),
	cdenome VARCHAR(50),
	profissao VARCHAR(100),
	estado_civil VARCHAR(50),
	usa_med boolean DEFAULT false,
	remedio_que_usa VARCHAR(100),
	perg1 VARCHAR(100),
	perg2 VARCHAR(100),
	perg3 VARCHAR(100),
	perg4 VARCHAR(100),
	aceitacaoDosTermos BOOLEAN,
	hora TIME,
	possui_disp VARCHAR(100),
	obs_sobre_hor VARCHAR(100),
	setor VARCHAR(100),
	data_inicio DATE,
	id SERIAL UNIQUE,	
	CONSTRAINT pk_voluntario PRIMARY KEY(id),
	CONSTRAINT fk_usuario FOREIGN KEY(idusuario)
		REFERENCES ONG2.USUARIO(id) ON UPDATE CASCADE
);

CREATE TABLE ONG2.ESTATISTICAS(
	id CHAR(3),
	descricao VARCHAR(50),
	qtde INT DEFAULT 0,
	CONSTRAINT pk_estatistica PRIMARY KEY(id)
	);
CREATE TABLE ONG2.RELATORIO(
	id SERIAL,
	status BOOLEAN DEFAULT true,
	aprovado BOOLEAN DEFAULT null,
	setor VARCHAR(20),
	dataa DATE DEFAULT CURRENT_DATE,
	hora TIME DEFAULT CURRENT_TIME,
	observacao VARCHAR(50),
	ativ_desenvolvida TEXT,
	coordenador VARCHAR(50),
	id_voluntario INT,
	CONSTRAINT pk_relatorio PRIMARY KEY(id),
	CONSTRAINT fk_relatorio_vol FOREIGN KEY(id_voluntario)
		REFERENCES ONG2.VOLUNTARIO(id) ON UPDATE CASCADE
);
CREATE TABLE ONG2.ITEM(
	nome VARCHAR(200),
	qtdade INT DEFAULT 0,
	id SERIAL,
	CONSTRAINT pk_item PRIMARY KEY(id)
	);
CREATE TABLE ONG2.DISTRIBUICAO(
	idpaciente INT,
	id SERIAL,
	dataentrega DATE DEFAULT CURRENT_DATE,
	CONSTRAINT pk_distribuicao PRIMARY KEY(id)	
);
CREATE TABLE ONG2.ENTREGUE(
	iditem INT,
	qtde INT,
	identrega INT,
	CONSTRAINT fk_item FOREIGN KEY(iditem)
		REFERENCES ONG2.ITEM(id) ON UPDATE CASCADE,
	CONSTRAINT fk_entrega FOREIGN KEY(identrega)
		REFERENCES ONG2.DISTRIBUICAO(id) ON UPDATE CASCADE
	);
CREATE TABLE ONG2.aptidoes (
  id INT,
  aptidao VARCHAR(25),
  CONSTRAINT pk_aptidoes PRIMARY KEY(id, aptidao)
);

CREATE TABLE ONG2.dataehora (
  id INT,
  dataehora VARCHAR(25),
  CONSTRAINT pk_dataehora PRIMARY KEY(id, dataehora)
);

CREATE TABLE ONG2.diassetor (
  id INT,
  dia VARCHAR(25),
  CONSTRAINT pk_diassetor PRIMARY KEY(id, dia)
);	

CREATE TABLE ONG2.DOACAO(
	doador VARCHAR(100),
	telefonedoador VARCHAR(12),
	celulardoador VARCHAR(12),
	anonimo BOOLEAN,
	dia DATE,
	dinheiro NUMERIC,
	isdinheiro BOOLEAN,
	isoutro BOOLEAN,
	outro TEXT,
	descricao TEXT,
	id SERIAL,
	status BOOLEAN DEFAULT true,
	CONSTRAINT pk_doacao PRIMARY KEY(id)		
);

CREATE TABLE ONG2.HOSPEDAGEM(
	idpaciente INT,
	dia DATE,
	idquarto INT,
	anotacoes VARCHAR(150),
	id SERIAL,
	CONSTRAINT pk_hospedagem PRIMARY KEY(id),
	CONSTRAINT fk_paciente FOREIGN KEY(idpaciente)
		REFERENCES ONG2.PACIENTE(id) ON UPDATE CASCADE
);
CREATE TABLE ONG2.ACOMPANHANTES(
	idhospedagem INT,
	idresponsavel INT,
	CONSTRAINT fk_hospedagem FOREIGN KEY(idhospedagem)
		REFERENCES ONG2.HOSPEDAGEM(id) ON UPDATE CASCADE,
	CONSTRAINT fk_responsavel FOREIGN KEY(idresponsavel)
		REFERENCES ONG2.RESPONSAVEL(id) ON UPDATE CASCADE
	
);


--se for uma mudança que nao vai ser necessaria de ser testada
--no momento, da pra alterar só aqui(sem precisa no server, ex:
--os ON UPDATE CASCADE NAS REFERENCES, TROQUEI SÓ AQUI)
