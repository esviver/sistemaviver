﻿
drop schema ong;
CREATE SCHEMA ONG;
drop table ong.usuario cascade;
drop table ong.psicologa cascade;
drop table ong.assistente_social cascade;
drop table ong.responsavel cascade;
drop table ong.paciente cascade;
drop table ong.atendimento_ps cascade;
drop table ong.quarto cascade;

CREATE TABLE ONG.quarto(
	id INT,
	status INT DEFAULT 0,
	capacidade INT,
	--status boolean default true,
	CONSTRAINT pk_quarto PRIMARY KEY(id)
);


CREATE TABLE ONG.ENDERECO(
	id SERIAL UNIQUE,
	logradouro VARCHAR(200),
	bairro VARCHAR(200),
	numero INT,
	cidade VARCHAR(200),
	estado VARCHAR(200),
	cep VARCHAR(10),
	pontoreferencia TEXT,
	CONSTRAINT pk_endereco PRIMARY KEY(id)
	);
Funcionario(CPF, RG_num,RG_org,Login, Senha, Salario, Nome, Endereco);
CREATE TABLE ONG.USUARIO(
	cpf  CHAR(11),
	rg_num VARCHAR(9),
	rg_org VARCHAR(20),
	login VARCHAR(30) UNIQUE,
	senha VARCHAR(8),
	pnome VARCHAR(50),
	snome VARCHAR(100),
	endereco INT,
	telefone VARCHAR (12),
	celular VARCHAR(12),
	tipo_de_user VARCHAR(12),
	nascimento date,
	sexo VARCHAR(10),
	anotacoes TEXT,
	id SERIAL UNIQUE,
	status boolean DEFAULT true,
	CONSTRAINT pk_funcionario PRIMARY KEY(cpf),
	CONSTRAINT fk_endereco FOREIGN KEY(endereco)
		REFERENCES ONG.ENDERECO(id)
);
Psicologa(Func.CPF,CRP);
CREATE TABLE ONG.PSICOLOGA(
	idusuario INT,
	crp VARCHAR(10),
	idP SERIAL UNIQUE,
	CONSTRAINT pk_psicologa PRIMARY KEY(crp),
	CONSTRAINT fk_usuario_psi FOREIGN KEY(idusuario)
		REFERENCES ONG.USUARIO(id) ON UPDATE CASCADE
);
Assitente_Social(Func.CPF, CRESS);
CREATE TABLE ONG.ASSISTENTE_SOCIAL(
	idusuario INT,
	cress CHAR(5),
	id SERIAL UNIQUE,
	status boolean DEFAULT true,
	CONSTRAINT pk_ass PRIMARY KEY(cress),
	CONSTRAINT fk_func_ass FOREIGN KEY(idusuario)
		REFERENCES ONG.USUARIO(id) ON UPDATE CASCADE
);
Resposavel(CPF, Nome, Profissão, Escolaridade, Telefone, Estado_civil, RG_num, RG_org, Endereco);
CREATE TABLE ONG.RESPONSAVEL(
	cpf CHAR(11),
	pnome VARCHAR(50),
	snome VARCHAR(100),
	profissao VARCHAR(100),
	escolaridade VARCHAR(100),
	telefone VARCHAR(11),
	estadocivil VARCHAR(30),
	rgnum CHAR(9),
	rgorg VARCHAR(20),
	id_endereco INT,
	datanasc DATE,
	id SERIAL UNIQUE,
	id_pac INT,
	status boolean DEFAULT true,
	CONSTRAINT pk_responsavel PRIMARY KEY(cpf),
	CONSTRAINT fk_id_pac FOREIGN KEY (id_pac)
		REFERENCES ONG.PACIENTE(id),
	CONSTRAINT fk_endereco FOREIGN KEY (id_endereco)
		REFERENCES ONG.ENDERECO(id)
	);
Paciente(N_SUS, Resp.CPF, Nome, Nasc, Escola, Serie, Patologia, Endereco, Obito, Alta);

CREATE TABLE ONG.PACIENTE(
	id SERIAL UNIQUE,
	agua NUMERIC(8,2),
	alimentacao NUMERIC(8,2),
	aluguel NUMERIC(8,2),
	apoioFam VARCHAR(150),
	banheiro VARCHAR(150),
	id_end INT,
	religiaoCont BOOLEAN,
	comodos INT,
	dataNasc DATE,
	energia NUMERIC(8,2),
	gas NUMERIC(8,2),
	histPatologia VARCHAR(150),
	historicoFamiliar VARCHAR(150),
	localNasc VARCHAR(150),
	material VARCHAR(150),
	medicamentos NUMERIC(8,2),
	moradia VARCHAR(150),
	pnome VARCHAR(150),
	nomeEscola VARCHAR(150),
	outroAux VARCHAR(150),
	patologia VARCHAR(150),
	religiao VARCHAR(150),
	rendaMensal NUMERIC(8,2),
	serie INT,
	transporte NUMERIC(8,2),
	nsus CHAR(15),
	asfalto BOOLEAN,
	auxilioGas BOOLEAN,
	bolsaFam BOOLEAN,
	centroCom BOOLEAN,
	creche BOOLEAN,
	esgoto BOOLEAN,
	luzFraterna BOOLEAN,
	snome VARCHAR(150),
	obito BOOLEAN,
	situacao BOOLEAN,
	alta BOOLEAN,
	entrada DATE DEFAULT CURRENT_DATE,
	saida DATE DEFAULT null,
	CONSTRAINT pk_paciente PRIMARY KEY(nsus, entrada, saida),
	CONSTRAINT fk_endereco FOREIGN KEY(id_end)
		REFERENCES ONG.ENDERECO(id)
);
Atendimento_PS(Func.Psi.CPF, Psi.CRP, Data, Relatorio, Pac.NSUS);
CREATE TABLE ONG.ATENDIMENTO_PS(
	idpsicologa INT,
	data_at DATE,
	relatorio VARCHAR(1000),
	idpaciente INT,
	hora TIME,
	id SERIAL UNIQUE,
	CONSTRAINT fk_atend_psi FOREIGN KEY (idpsicologa)
		REFERENCES ONG.PSICOLOGA(id) ON DELETE RESTRICT,
	CONSTRAINT fk_atend_pac FOREIGN KEY(idpaciente)
		REFERENCES ONG.PACIENTE(id) ON DELETE RESTRICT,
	CONSTRAINT pk_atend PRIMARY KEY(id)
);
Atendimento_ASS(Func.Ass.CPF, Psi.CRESS, Data, Relatorio, Pac.NSUS, pac_n_sus);
CREATE TABLE ONG.ANAMNESE(
	idpsicologa INT,
	idpaciente INT,
	encaminhamento TEXT,
	diagnostico TEXT,
	medico TEXT,
	genograma bytea,
	historia TEXT,
	historico TEXT,
	queixa TEXT,
	motivos TEXT,
	hipoteses TEXT,
	planoterapeutico TEXT,
	psicosociais TEXT,
	estruturafamiliar TEXT,
	infaciaadolescencia TEXT,
	aspectospsicologicos TEXT,
	dia DATE,
	id SERIAL UNIQUE,
	CONSTRAINT fk_psicologa FOREIGN KEY(idpsicologa)
		REFERENCES ONG.PSICOLOGA(id),
	CONSTRAINT fk_paciente FOREIGN KEY(idpaciente)
		REFERENCES ONG.PACIENTE(id)
		
	
);
CREATE TABLE ONG.ATENDIMENTO_ASS(
	idassistente INT,
	data_at DATE,
	relatorio VARCHAR(1000),
	idpaciente INT,
	hora TIME,
	id SERIAL UNIQUE,
	CONSTRAINT fk_atend_ass FOREIGN KEY (idassistente)
		REFERENCES ONG.ASSISTENTE_SOCIAL(id) ON DELETE RESTRICT,
	CONSTRAINT fk_atend_pac FOREIGN KEY(idpaciente)
		REFERENCES ONG.PACIENTE(id),
	CONSTRAINT pk_atend_ass PRIMARY KEY(id)
);
Voluntario(CPF, RG_num, RG_org, E-mail, Local_trab, Escolaridade, Telefone, Nome, Emergencia_avisar, Nasc,Profissao,
 Estado_civil, Usa_remedio, Tel_avisar,Endereco);

CREATE TABLE ONG.VOLUNTARIO(
	idusuario INT,
	email VARCHAR(100),
	local_trab VARCHAR(100),
	escolaridade VARCHAR(100),
	cdetelefone VARCHAR(11),
	cdenome VARCHAR(50),
	profissao VARCHAR(100),
	estado_civil VARCHAR(50),
	usa_med boolean DEFAULT false,
	remedio_que_usa VARCHAR(100),
	perg1 VARCHAR(100),
	perg2 VARCHAR(100),
	perg3 VARCHAR(100),
	perg4 VARCHAR(100),
	possui_disp VARCHAR(100),
	obs_sobre_hor VARCHAR(100),
	setor VARCHAR(100),
	data_inicio DATE,
	id SERIAL UNIQUE,	
	CONSTRAINT pk_voluntario PRIMARY KEY(id),
	CONSTRAINT fk_usuario FOREIGN KEY(idusuario)
		REFERENCES ONG.USUARIO(id)
);

Estatisticas(Atendimentos, Paciente_Inativos, Data, Altas, Obitos, Pacientes_Ativos)
CREATE TABLE ONG.ESTATISTICAS(
	id CHAR(3),
	descricao VARCHAR(50),
	qtde INT DEFAULT 0,
	CONSTRAINT pk_estatistica PRIMARY KEY(id)
	);
CREATE TABLE ONG.RELATORIO(
	id SERIAL,
	status BOOLEAN,
	setor VARCHAR(20),
	data DATE DEFAULT CURRENT_DATE,
	hora TIME DEFAULT CURRENT_TIME,
	observacao VARCHAR(50),
	ativ_desenvolvida TEXT,
	id_voluntario INT,
	CONSTRAINT pk_relatorio PRIMARY KEY(datasub, id_voluntario),
	CONSTRAINT fk_relatorio_vol FOREIGN KEY(idvoluntario)
		REFERENCES ONG.VOLUNTARIO(id)
);
CREATE TABLE ONG.ITEM(
	nome VARCHAR(200),
	qtdade INT DEFAULT 0,
	id SERIAL,
	CONSTRAINT pk_item PRIMARY KEY(id)
	);
CREATE TABLE ONG.DISTRIBUICAO(
	idpaciente INT,
	id SERIAL,
	dataentrega DATE DEFAULT CURRENT_DATE,
	CONSTRAINT pk_distribuicao PRIMARY KEY(id)	
);
CREATE TABLE ONG.ENTREGUE(
	iditem INT,
	qtde INT,
	identrega INT,
	CONSTRAINT fk_item FOREIGN KEY(iditem)
		REFERENCES ONG.ITEM(id),
	CONSTRAINT fk_entrega FOREIGN KEY(identrega)
		REFERENCES ONG.DISTRIBUICAO(id)
	);
CREATE TABLE ong.aptidoes (
  id INT,
  aptidao VARCHAR(20)
);

CREATE TABLE ong.dataehora (
  id INT,
  dataehora VARCHAR(20)
);

CREATE TABLE ong.diassetor (
  id INT,
  dias VARCHAR(20)
);	

CREATE TABLE ONG.DOACAO(
	doador VARCHAR(100),
	telefonedoador VARCHAR(12),
	celulardoador VARCHAR(12),
	anonimo BOOLEAN,
	dia DATE,
	dinheiro NUMERIC,
	isdinheiro BOOLEAN,
	isoutro BOOLEAN,
	outro TEXT,
	descricao TEXT,
	id SERIAL,
	status BOOLEAN DEFAULT true,
	CONSTRAINT pk_doacao PRIMARY KEY(id)		
);

CREATE TABLE ONG.HOSPEDAGEM(
	idpaciente INT,
	dia DATE,
	idquarto INT,
	anotacoes VARCHAR(150),
	id SERIAL,
	CONSTRAINT pk_hospedagem PRIMARY KEY(id),
	CONSTRAINT fk_paciente FOREIGN KEY(idpaciente)
		REFERENCES ONG.PACIENTE(id)
);
CREATE TABLE ONG.ACOMPANHANTES(
	idhospedagem INT,
	idresponsavel INT,
	CONSTRAINT fk_hospedagem FOREIGN KEY(idhospedagem)
		REFERENCES ONG.HOSPEDAGEM(id),
	CONSTRAINT fk_responsavel FOREIGN KEY(idresponsavel)
		REFERENCES ONG.RESPONSAVEL(id)
	
);
INSERT INTO ONG.VOLUNTARIO
	VALUES('00000','carinha','12345678','123456789', 'SSPPR', 'carinha@uel.br',
			'UEL', 'Mestre', '4324241122', 'carinha','da silva','190','puliça',
			'2009-04-30','matador','disquitado','omeprazol', 'rua da abobrinha bairro centro ibiporã pr');
			
INSERT INTO ONG.FUNCIONARIO
		VALUES('12345', '1010101','SSPSP', 'FREUD','TUDO',3000.00, 
		'Gabriela', 'Silva','longe da minha casa pra caramba,
		 poxa gabi :(');
		 
INSERT INTO ONG.FUNCIONARIO
		VALUES('1234', '1010101','SSPSP', 'LIBERA','LIBIDO',3000.00, 
		'CHACARA', 'AZUL','FESTINHA DAORA');

INSERT INTO ONG.ASSISTENTE_SOCIAL
	VALUES('1234','3333');
			 
INSERT INTO ONG.PSICOLOGA(crp, func_cpf)
	VALUES('444','12345');
	

	

a
INSERT INTO ONG.ATENDIMENTO_ASS(ass_cress,relatorio,pac_n_sus,data_at,hora)
	VALUES('3333','PACIENTE OK','111','2015-03-03','15:20:30');
BEGIN;
COMMIT;
ROLLBACK;			
SELECT * FROM ONG.VOLUNTARIO;
SELECT * FROM ONG.ASSISTENTE_SOCIAL;
SELECT * FROM ONG.ATENDIMENTO_ASS;
SELECT * FROM ONG.ATENDIMENTO_PS;
SELECT * FROM ONG.ESTATISTICAS;
SELECT * FROM ONG.FUNCIONARIO;
SELECT * FROM ONG.PACIENTE;
SELECT * FROM ONG.PSICOLOGA;
SELECT * FROM ONG.RESPONSAVEL;
ALTER TABLE ONG.VOLUNTARIO ADD COLUMN livre VARCHAR(20);
ALTER TABLE ONG.VOLUNTARIO ADD COLUMN compromisso DATE;
SELECT p_nome, s_nome FROM ONG.VOLUNTARIO WHERE(p_nome='carinha'AND e_mail='carinha@uel.br');
UPDATE ONG.VOLUNTARIO 
	SET livre = 'sex-tarde'
	WHERE cpf = '00000';
UPDATE ONG.VOLUNTARIO 
	SET compromisso = '2015-09-25'
	WHERE cpf = '00000';
SELECT * FROM ONG.ATENDIMENTO_ASS WHERE pac_n_sus ='1234';	
SELECT p_nome, s_nome, e_mail FROM ONG.VOLUNTARIO WHERE(livre = 'sex-tarde');
DELETE  FROM ONG.VOLUNTARIO WHERE (p_nome='carinha');	
SELECT e_mail FROM ONG.VOLUNTARIO WHERE( compromisso > CURRENT_DATE AND compromisso <= CURRENT_DATE + 7);
UPDATE ONG.VOLUNTARIO 
	SET compromisso = '2015-09-23';
CREATE  FUNCTION ONG.ATENDIMENTOS_ASS_to_pac(paciente_n_sus VARCHAR(15))
RETURNS record AS
$$
	SELECT * FROM ONG.ATENDIMENTO_ASS;
$$
LANGUAGE SQL;

SELECT ONG.ATENDIMENTOS_ASS_to_pac('1234');

SELECT * FROM ONG.PACIENTE e NATURAL JOIN ONG.ATENDIMENTO_ASS d
	WHERE e.n_sus='1234';
DROP FUNCTION statistica_pacientes();
DROP TRIGGER paciente_insert ON ONG.PACIENTE;

CREATE FUNCTION statistica_pacientes()
RETURNS TRIGGER AS $$ 
BEGIN
	IF(TG_OP = 'UPDATE') THEN
		IF(NEW.obito=1) THEN
			UPDATE ONG.ESTATISTICAS
				SET qtde = qtde+1
				WHERE descricao = 'obito';
		END IF;
		IF(NEW.alta=1) THEN
			UPDATE ONG.ESTATISTICAS
				SET qtde = qtde +1
				WHERE descricao = 'alta';
		END IF;
	END IF;
	IF(TG_OP = 'INSERT') THEN
		UPDATE ONG.ESTATISTICAS
			SET qtde = qtde+1
			WHERE descricao = 'paciente';
	END IF;
RETURN NULL;
END;
$$ LANGUAGE PLPGSQL;


CREATE TRIGGER paciente_insert AFTER 
INSERT OR UPDATE ON ONG.PACIENTE
FOR EACH ROW EXECUTE PROCEDURE statistica_pacientes ( );




CREATE FUNCTION statistica_atendimento()
RETURNS TRIGGER AS
$$ 
BEGIN
	UPDATE ONG.ESTATISTICAS
		SET qtde = qtde+1
		WHERE descricao = 'atendimentos';
RETURN NULL;
END;
$$ LANGUAGE PLPGSQL;

CREATE TRIGGER paciente_insert AFTER 
INSERT OR UPDATE ON ONG.ATENDIMENTO_PS
FOR EACH ROW EXECUTE PROCEDURE statistica_atendimento();

CREATE TRIGGER paciente_insert AFTER 
INSERT OR UPDATE ON ONG.ATENDIMENTO_ASS
FOR EACH ROW EXECUTE PROCEDURE statistica_atendimento();

DELETE FROM ONG.ATENDIMENTO_ASS;
DROP TRIGGER paciente_insert ON ONG.ATENDIMENTO_PS;
DROP FUNCTION statistica_atendimeto();



INSERT INTO ONG.ESTATISTICAS(id,descricao)
	VALUES(1,'obito');
INSERT INTO ONG.ESTATISTICAS(id,descricao)
	VALUES(2,'alta');
INSERT INTO ONG.ESTATISTICAS(id,descricao)
	VALUES(3,'paciente');
INSERT INTO ONG.ESTATISTICAS(id,descricao)
	VALUES(4,'atendimentos');


INSERT INTO ONG.PACIENTE(n_sus,p_nome,s_nome)
	VALUES('111','JOse','SILVA');
UPDATE ONG.PACIENTE
	SET obito = 1
	WHERE(n_sus = '13125');
	
SELECT * FROM ONG.ESTATISTICAS;
SELECT * FROM ONG.PACIENTE;

UPDATE ONG.ESTATISTICAS
	SET qtde =3
	WHERE descricao = 'alta';

UPDATE ONG.ESTATISTICAS
	SET qtde =5
	WHERE descricao = 'paciente';

UPDATE ONG.ESTATISTICAS
	SET qtde =1
	WHERE descricao = 'obito';


DELETE FROM ONG.ESTATISTICAS;